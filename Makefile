
all: init.el readme.org

init.el: init.org
	emacs --batch --eval '(progn (require (quote org)) (org-babel-tangle-file "init.org") (shell-command "espeak -v en -s 155 \"tangle done\""))'

readme.org: init.org
	ln -f init.org readme.org
