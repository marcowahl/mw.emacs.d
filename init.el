;;; init.el --- personal emacs config file marco wahl -*- lexical-binding: t ; eval: (view-mode 1) -*-

;; conform to el-tradition
;; :PROPERTIES:
;; :ID:       50b27b8e-ef4e-4b89-a810-90c7c56bb665
;; :END:

;; The following lines are traditionally in every elisp file.  These
;; lines also please checkdoc, the elisp documentation checker.


;; [[file:init.org::*conform to el-tradition][conform to el-tradition:1]]

;;; Commentary:

;; THIS FILE HAS BEEN GENERATED.


;;; Code:
;; conform to el-tradition:1 ends here

;; straight
;; :PROPERTIES:
;; :ID:       ce50a09a-6692-4b65-81da-d973c9008171
;; :END:

;; Straight is a package manager that looks nice to me.


;; [[file:init.org::*straight][straight:1]]
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
;; straight:1 ends here

;; package.el resources
;; :PROPERTIES:
;; :ID:       baff0889-3d65-48fa-a31e-b96c40012ea2
;; :END:

;; The gnu package repo is not included from the start.


;; [[file:init.org::*package.el resources][package.el resources:1]]
(require 'package)
;; (setf package-archives nil)
;; (add-to-list 'package-archives '("mw" . "/home/b/s/elisp/mw/packages/") t)
;; (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
;; package.el resources:1 ends here

;; recognize special coloring of compilation buffer
;; :PROPERTIES:
;; :ID:       d5e143ca-5c0f-4f3b-8802-7e2565bf05b0
;; :END:


;; [[file:init.org::*recognize special coloring of compilation buffer][recognize special coloring of compilation buffer:1]]
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region (point-min) (point-max))))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)
;; recognize special coloring of compilation buffer:1 ends here

;; don't load outdated byte code
;; :PROPERTIES:
;; :ID:       8fa15afd-71c1-44f3-bdca-2e547bd9f603
;; :END:


;; [[file:init.org::*don't load outdated byte code][don't load outdated byte code:1]]
(setq load-prefer-newer t)
;; don't load outdated byte code:1 ends here

;; org from source
;; :PROPERTIES:
;; :ID:       15ae3b79-0786-41d0-b7d4-1933d4b75c5b
;; :END:

;; Load early the local git version to avoid some other org version gets
;; loaded.


;; [[file:init.org::*org from source][org from source:1]]
(defvar mw-org-source-directory "~/s/org/org-mode")

(let ((orgmodelocation (expand-file-name mw-org-source-directory)))
  (push (concat orgmodelocation "/lisp") load-path)
  (push (concat orgmodelocation "/contrib/lisp") load-path))
;; org from source:1 ends here

;; use local org via straight
;; :PROPERTIES:
;; :ID:       752aed43-958f-4a51-ad80-5bc6137a4699
;; :END:

;; Let straight use the local checkout of Org.

;; In particular this is to avoid straight installing another Org for
;; packages depending on Org.


;; [[file:init.org::*use local org via straight][use local org via straight:1]]
(straight-use-package `(org :repo "marcow@git.savannah.gnu.org:/srv/git/emacs/org-mode.git" :local-repo ,mw-org-source-directory))
;; (straight-use-package `(org :repo "git@code.orgmode.org:bzg/org-mode.git" :local-repo ,mw-org-source-directory))
;; use local org via straight:1 ends here

;; activation
;; :PROPERTIES:
;; :ID:       8dcbeb51-31ce-42a6-a99b-183f3131f538
;; :END:


;; [[file:init.org::*activation][activation:1]]
(add-to-list 'load-path (expand-file-name "~/s/elisp/external/hyperbole/"))
(load "hyperbole-autoloads")
(load "hyperbole")
(require 'hyperbole)
;; activation:1 ends here

;; [[file:init.org::*activation][activation:2]]
(global-set-key (kbd "C-M-7") #'hkey-either)
(global-set-key (kbd "C-M-8") #'assist-key)
;; activation:2 ends here

;; bring back org-sparse-tree
;; :PROPERTIES:
;; :ID:       5cd26470-6876-466c-8334-e7d09c3c6cba
;; :END:

;; - [2020-03-12 Thu] hyperbole overwrites the standard org binding of
;;   org-sparse-tree.


;; [[file:init.org::*bring back org-sparse-tree][bring back org-sparse-tree:1]]
(org-defkey org-mode-map (kbd "C-c C-/") #'org-sparse-tree)
;; bring back org-sparse-tree:1 ends here

;; org files dir


;; [[file:init.org::*org files dir][org files dir:1]]
(defvar mw-personal-org-files-dir "~/o")
;; org files dir:1 ends here

;; basic
;; :PROPERTIES:
;; :ID:       ba37e8d8-5bab-47a6-94df-6d7a14ed2637
;; :END:


;; [[file:init.org::*basic][basic:1]]
(require 'org)
(require 'org-agenda)
(org-defkey org-agenda-mode-map (kbd "Y") #'org-agenda)
(org-defkey org-agenda-mode-map (kbd "5") #'org-agenda-schedule)

(with-eval-after-load 'info
  (progn (info-initialize)
         (add-to-list
          'Info-directory-list
          (concat (expand-file-name mw-org-source-directory)
                  "/doc"))))

(with-eval-after-load 'eww
  (progn (org-link-set-parameters "eww" :follow #'eww :store #'org-eww-store-link)))
;; basic:1 ends here

;; ol-bbdb
;; :PROPERTIES:
;; :ID:       4bb3cccd-da69-4244-b8a8-20fe60696e31
;; :END:


;; [[file:init.org::*ol-bbdb][ol-bbdb:1]]
(require 'ol-bbdb)
;; ol-bbdb:1 ends here

;; quickly jump to org source
;; :PROPERTIES:
;; :ID:       b6183cd0-1cd3-4039-8721-bb2eb5a19e7a
;; :END:


;; [[file:init.org::*quickly jump to org source][quickly jump to org source:1]]
(defun mw-find-org-source-directory ()
  (interactive)
  (push-mark)
  (find-file mw-org-source-directory))
;; quickly jump to org source:1 ends here

;; unbind some default keys of org
;; :PROPERTIES:
;; :ID:       ff2f32e5-fd96-47fc-a4a4-dba2b95ce8cc
;; :END:

;; Want C-, for more generic commands. Both bound in Org to
;; org-cycle-agenda-files.


;; [[file:init.org::*unbind some default keys of org][unbind some default keys of org:1]]
(progn
   (org-defkey org-mode-map (kbd "C-,") nil)
   (org-defkey org-mode-map (kbd "C-'") nil))
;; unbind some default keys of org:1 ends here

;; promote/demote highest level
;; :PROPERTIES:
;; :ID:       2d5a359e-9a14-4306-a416-0422abe9af44
;; :END:


;; [[file:init.org::*promote/demote highest level][promote/demote highest level:1]]
(defun org-demote-level-1 ()
  "Demote all level 1 subtrees."
  (interactive)
  (goto-char (point-min))
  (while (progn
           (org-next-visible-heading 1)
           (org-at-heading-p))
    (when (= 1 (plist-get (cadr (org-element-at-point)) :level))
      (org-demote-subtree))))
;; promote/demote highest level:1 ends here



;; I think the next one is implied with org-shift-meta-left (also on "L"
;; speed key afaict).


;; [[file:init.org::*promote/demote highest level][promote/demote highest level:2]]
(defun org-promote-level-2 ()
  "Promote all level 2 subtrees."
  (interactive)
  (goto-char (point-min))
  (while (progn
           (org-next-visible-heading 1)
           (org-at-heading-p))
    (when (= 2 (plist-get (cadr (org-element-at-point)) :level))
      (org-promote-subtree))))
;; promote/demote highest level:2 ends here

;; keys for org-attach-dired...
;; :PROPERTIES:
;; :ID:       e3b0ea97-fc6e-4d57-aa47-1f8425bfcc64
;; :END:

;; idea: use a submap to bind c-c c-x like:

;; (let ((map (make-sparse-keymap)))
;; (define-key map "a" #'...)
;; )
;; bind the map to c-c c-x.


;; [[file:init.org::*keys for org-attach-dired...][keys for org-attach-dired...:1]]
(add-hook
 'dired-mode-hook
 (lambda ()
   (define-key dired-mode-map (kbd "C-c C-x a") #'org-attach-dired-to-subtree)
   (define-key dired-mode-map (kbd "C-c C-x c")
     (lambda () (interactive)
       (let ((org-attach-method 'cp))
         (call-interactively #'org-attach-dired-to-subtree))))
   (define-key dired-mode-map (kbd "C-c C-x r")
     (lambda () (interactive)
       (let ((org-attach-method 'mv))
         (call-interactively #'org-attach-dired-to-subtree))))
   (define-key dired-mode-map (kbd "C-c C-x h")
     (lambda () (interactive)
       (let ((org-attach-method 'ln))
         (call-interactively #'org-attach-dired-to-subtree))))
   (define-key dired-mode-map (kbd "C-c C-x s")
     (lambda () (interactive)
       (let ((org-attach-method 'lns))
         (call-interactively #'org-attach-dired-to-subtree))))))
;; keys for org-attach-dired...:1 ends here

;; org todo dependencies
;; :PROPERTIES:
;; :ID:       ca70c911-9c5a-4d07-b499-9b854fed6e07
;; :END:

;; Activate dependency checks.


;; [[file:init.org::*org todo dependencies][org todo dependencies:1]]
(setq org-enforce-todo-dependencies t)
;; org todo dependencies:1 ends here

;; personal org indentation
;; :PROPERTIES:
;; :ID:       fef018c8-f725-47ef-8608-ef2045b88cfa
;; :END:


;; [[file:init.org::*personal org indentation][personal org indentation:1]]
(setq org-adapt-indentation nil       ;; no artificial space at the
      ;; left side.  I don't have
      ;; space for this. ;)
      )
;; personal org indentation:1 ends here

;; to org attachments
;; :PROPERTIES:
;; :ID:       26bf9d8b-f6aa-40db-bf28-3ed5e8efef88
;; :END:

;; No auto git commit for the attachment directory.


;; [[file:init.org::*to org attachments][to org attachments:1]]
(setq org-attach-commit nil)
;; to org attachments:1 ends here



;; No duplication of attachments as org properties.


;; [[file:init.org::*to org attachments][to org attachments:2]]
(setq org-attach-file-list-property nil)
;; to org attachments:2 ends here

;; org agenda include inactive timestamps
;; :PROPERTIES:
;; :ID:       df559437-3db6-4198-8878-c18efbbe0f33
;; :END:


;; [[file:init.org::*org agenda include inactive timestamps][org agenda include inactive timestamps:1]]
(setq org-agenda-include-inactive-timestamps t) ;;
;; (setq org-agenda-include-inactive-timestamps nil) ;; for not seeing them.
;; org agenda include inactive timestamps:1 ends here

;; get the outline path of an item
;; :PROPERTIES:
;; :ID:       53728e12-351c-4e90-8fb8-4c22ccedb2bb
;; :END:


;; [[file:init.org::*get the outline path of an item][get the outline path of an item:1]]
(defun mw-org-agenda-display-outline-path (&optional file current separator just-return-string)
  "Show the outline path of the item of the current line if possible.
See `org-agenda-display-outline-path' for explanation of the parameters."
  (interactive "P")
  (let ((m (org-get-at-bol 'org-marker)))
    (when (and (markerp m) (marker-buffer m))
      (org-with-point-at m
        (org-display-outline-path file current separator just-return-string)))))
;; get the outline path of an item:1 ends here

;; use T in agenda to switch to fast todo user interface
;; :PROPERTIES:
;; :ID:       804729bc-ac88-4176-808f-9d953569a5d4
;; :END:


;; [[file:init.org::*use T in agenda to switch to fast todo user interface][use T in agenda to switch to fast todo user interface:1]]
(org-defkey org-agenda-mode-map "T"
            (lambda (&optional arg)
              (interactive "P")
              (let ((org-use-fast-todo-selection 'auto))
                (org-agenda-todo arg))))
;; use T in agenda to switch to fast todo user interface:1 ends here

;; org column settings
;; :PROPERTIES:
;; :ID:       5bed4d4e-1d89-4cd8-8af9-f8ff04009a6c
;; :END:


;; [[file:init.org::*org column settings][org column settings:1]]
(setq
 org-columns-ellipses "…"
 org-columns-default-format "%ITEM %TODO %PRIORITY %TAGS")
;; org column settings:1 ends here

;; jump to org block bound
;; :PROPERTIES:
;; :ID:       304ea5c8-51b5-4c79-a945-a3c0cd200d21
;; :END:

;; The following bindings allow to find the next occurance of string '#+'
;; which typically indicate an org-block meta thing.


;; [[file:init.org::*jump to org block bound][jump to org block bound:1]]
(add-hook
 'org-mode-hook
 (lambda ()
   (local-set-key
    (kbd "C-c M-n")
    (lambda ()
      (interactive)
      (forward-char)
      (re-search-forward "#\\+")))))

(add-hook
 'org-mode-hook
 (lambda ()
   (local-set-key
    (kbd "C-c M-p")
    (lambda ()
      (interactive)
      (beginning-of-line)
      (re-search-backward "#\\+")))))
;; jump to org block bound:1 ends here

;; tab jump from code-block 'end' to 'begin'
;; :PROPERTIES:
;; :ID:       d0679e88-199a-4036-99f2-5aeb1995dd4a
;; :END:


;; [[file:init.org::*tab jump from code-block 'end' to 'begin'][tab jump from code-block 'end' to 'begin':1]]
;; Experimentation for more convenient block handling.
(defun mw-org-jump-to-beginning-of-block-maybe ()
  "When on a closing line of a block jump to the opening line of the block."
  (interactive)
  (let ((case-fold-search t)
        (org-block-end-line-regexp "^[ \t]*#\\+end_")
        (org-block-begin-line-regexp  "^[ \t]*#\\+begin_"))
    (when (save-excursion
            (beginning-of-line 1)
            (looking-at org-block-end-line-regexp))
      (progn
        (search-backward-regexp org-block-begin-line-regexp)
        t ;; signal that action has been taken
        ))))
;; tab jump from code-block 'end' to 'begin':1 ends here

;; [[file:init.org::*tab jump from code-block 'end' to 'begin'][tab jump from code-block 'end' to 'begin':2]]
;; Use tab-key for trigger the action.  This is done via hooking.
(with-eval-after-load 'org
  (add-to-list 'org-tab-first-hook 'mw-org-jump-to-beginning-of-block-maybe))
;; tab jump from code-block 'end' to 'begin':2 ends here

;; convenient go up to the beginning of a block
;; :PROPERTIES:
;; :ID:       999d0bbe-9e0b-43f6-9fb2-9bf367081cf0
;; :END:


;; [[file:init.org::*convenient go up to the beginning of a block][convenient go up to the beginning of a block:1]]
;; Experimentation for more convenient block handling.
(defun mw-org-search-backward-beginning-of-block ()
  "When on a closing line of a block jump to the opening line of the block."
  (interactive)
  (let ((case-fold-search t)
        (org-block-begin-line-regexp  "^[ \t]*#\\+begin_"))
    (search-backward-regexp org-block-begin-line-regexp)))
;; convenient go up to the beginning of a block:1 ends here

;; more key bindings for babeling
;; :PROPERTIES:
;; :ID:       7b38b04a-1ce9-48fc-a460-17562527b750
;; :END:


;; [[file:init.org::*more key bindings for babeling][more key bindings for babeling:1]]
(setq
 org-babel-key-bindings
 (append
  org-babel-key-bindings
  (list
   (cons "m" #'org-babel-mark-block)
   (cons "N" #'org-narrow-to-block)
   (cons "'" #'org-edit-special)
   (cons ">" ; jump to the end.
         (lambda () (let ((case-fold-search t)) ; don't care about case.
                      (search-forward-regexp "#\\+end_src")
                      (beginning-of-line)))))))
;; more key bindings for babeling:1 ends here

;; org velocity
;; :PROPERTIES:
;; :ID:       6a1165bb-f429-463a-ace1-05bb7c59c4d7
;; :END:

;; org velocity is a org-mode contrib extension.


;; [[file:init.org::*org velocity][org velocity:1]]
(with-eval-after-load 'org-velocity
  (setq org-velocity-bucket (expand-file-name "bucket.org" org-directory)))
;; org velocity:1 ends here

;; org-protocol for receiving from the outside
;; :PROPERTIES:
;; :ID:       833fb525-34bb-4403-aa83-0853ccce0088
;; :END:


;; [[file:init.org::*org-protocol for receiving from the outside][org-protocol for receiving from the outside:1]]
(require 'org-protocol)
;; org-protocol for receiving from the outside:1 ends here

;; highlight current line in agenda
;; :PROPERTIES:
;; :ID:       e9df3a2c-6adb-4687-a36f-8810c07ae578
;; :END:

;; From [[gnus:nntp+news.gmane.org:gmane.emacs.orgmode#87egnh7oos.fsf@mbork.pl][Email from Marcin Borkowski: Hl-line mode in agenda]]:


;; [[file:init.org::*highlight current line in agenda][highlight current line in agenda:1]]
(add-hook 'org-agenda-finalize-hook (lambda () (hl-line-mode 1)))
;; highlight current line in agenda:1 ends here

;; save the o-press when opening the agenda
;; :PROPERTIES:
;; :ID:       fd89d958-98f7-4668-9d5d-3d2f9d92115f
;; :END:


;; [[file:init.org::*save the o-press when opening the agenda][save the o-press when opening the agenda:1]]
;(add-hook 'org-agenda-finalize-hook (lambda () (delete-other-windows)))
;; (setq org-agenda-window-setup 'only-window)
;; save the o-press when opening the agenda:1 ends here

;; org-screenshot
;; :PROPERTIES:
;; :ID:       e5fc742e-7d15-4c37-8fc1-78ab99b2ba2d
;; :END:

;; using org-screenshot as a package now.


;; [[file:init.org::*org-screenshot][org-screenshot:1]]
;; (push "~/s/elisp/external/org-attach-screenshot" load-path)
;; (require 'org-attach-screenshot)
;; org-screenshot:1 ends here



;; screenshots for orgees.  in particular during capturing.


;; [[file:init.org::*org-screenshot][org-screenshot:2]]
(defun mw-org-attach-screenshot-as-standard-attachment ()
  "Trigger ‘org-attach-screenshot’ with target as Org standard attachment.
Create the attachment dir if not exists.

The enhancement compared with pure org-attach-screenshot is that
no decision about how to store the image has to be made."
  (interactive)
  (require 'org-attach)
  (org-attach-dir t)
  (let ((fname (format-time-string
                "screenshot-%Y%m%d-%H%M%S.png")))
    (org-attach-screenshot
     nil
     fname)
    (insert (concat "[[attachment:" fname "]]")))
  (org-attach-sync))
;; org-screenshot:2 ends here

;; jump from the agenda to the stars
;; :PROPERTIES:
;; :ID:       c71cdcc8-4135-4869-b9d7-4c1d73330a0b
;; :END:

;; In the agenda 'tab' per default jumps to the beginning of the headline
;; text.  For me it's a bit more convenient to jump to the beginning of
;; the stars.  Fortunately there is org-agenda-after-show-hook.


;; [[file:init.org::*jump from the agenda to the stars][jump from the agenda to the stars:1]]
(with-eval-after-load "org-agenda"
  (push #'beginning-of-line org-agenda-after-show-hook))
;; jump from the agenda to the stars:1 ends here

;; speed commands also at beginning of buffer
;; :PROPERTIES:
;; :ID:       9aeec743-8b52-40c4-9b54-5faf04c54e84
;; :END:


;; [[file:init.org::*speed commands also at beginning of buffer][speed commands also at beginning of buffer:1]]
;; (setq org-use-speed-commands
;;       (lambda ()
;;         (or (= (point-min) (point))
;;             (and (looking-at org-outline-regexp)))))
;; speed commands also at beginning of buffer:1 ends here

;; refile targets config
;; :PROPERTIES:
;; :ID:       edb7f61b-b12e-44d2-adf0-d7d0af042c0b
;; :END:


;; [[file:init.org::*refile targets config][refile targets config:1]]
(setq org-refile-targets
      (quote
       ((nil :maxlevel . 7)
        ;; (org-agenda-files :maxlevel . 1)
        (org-agenda-files :level . 0))))

(setq org-refile-use-outline-path (quote file))
;; refile targets config:1 ends here

;; org-image-actual-width
;; :PROPERTIES:
;; :ID:       2ca81619-ea09-451c-868d-2111f25e20b6
;; :END:


;; [[file:init.org::*org-image-actual-width][org-image-actual-width:1]]
(setq org-image-actual-width nil)
;; org-image-actual-width:1 ends here

;; indicate certain tags at beginning of line
;; :PROPERTIES:
;; :ID:       604b01f4-5279-4241-93ac-258079491564
;; :END:


;; [[file:init.org::*indicate certain tags at beginning of line][indicate certain tags at beginning of line:1]]
(add-hook
 'org-mode-hook
 (lambda ()
   (font-lock-add-keywords
    nil
    '(("^\\**\\(\\*\\) "
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          " ")
          nil))
      ("^\\**\\(\\*\\) .*:idea:"
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          "!")
          nil))
      ("^\\**\\(\\*\\) .*:idee:"
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          "!")
          nil))
      ("^\\**\\(\\*\\) .*:imp:"
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          "⁕")
          nil))
      ("^\\**\\(\\*\\) .*:urg:"
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          "⏰")
          nil))
      ("^\\**\\(\\*\\) .*:imp:.*urg:"
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          "●")
          nil))
      ("^\\**\\(\\*\\) .*:urg:.*imp:"
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          "●")
          nil))
      ("^\\**\\(\\*\\) .*:obs:"
       (1 (compose-region (- (match-end 1) 1)
                          (match-end 1)
                          "⚟")
          nil))))))
;; indicate certain tags at beginning of line:1 ends here

;; from agenda open narrowed view of subtree at point
;; :PROPERTIES:
;; :ID:       c23503b2-cf75-4308-9d52-8ace195dd83c
;; :END:


;; [[file:init.org::*from agenda open narrowed view of subtree at point][from agenda open narrowed view of subtree at point:1]]
(progn
  (org-defkey
   org-agenda-mode-map "s"
   (lambda ()
     "Foldout zoom into the subtree at point in same window.
With prefix argument window fills the frame."
     (interactive)
     (call-interactively #'org-agenda-switch-to)
     (foldout-zoom-subtree)
     (org-back-to-heading)))

  (org-defkey
   org-agenda-mode-map "S"
   (lambda (arg)
     "Narrow to the subtree for the item at point in other window.
With prefix argument ARG switch to that window."
     (interactive "P")
     (let ((win (selected-window)))
       (org-agenda-goto)
       (org-narrow-to-subtree)
       (org-back-to-heading)
       (unless arg
         (select-window win))))))
;; from agenda open narrowed view of subtree at point:1 ends here

;; hide up to first heading
;; :PROPERTIES:
;; :ID:       1f2cff5a-5e50-45a4-b87a-de2c617cf9af
;; :END:


;; [[file:init.org::*hide up to first heading][hide up to first heading:1]]
(defun mw-org-flag-above-first-heading (&optional arg)
  "Hide from bob up to the first heading.
Move point to the beginning of first heading or end of buffer."
  (interactive "P")
  (goto-char 1)
  (unless (org-at-heading-p)
    (outline-next-heading))
  (unless (bobp)
     (org-flag-region 1 (1- (point)) (not arg) 'outline)))
;; hide up to first heading:1 ends here

;; insert Org file as subtree
;; :PROPERTIES:
;; :ID:       c1a9cd8c-a93b-4379-84f1-c72c9573e9eb
;; :END:


;; [[file:init.org::*insert Org file as subtree][insert Org file as subtree:1]]
(defun org-demote-all ()
  "Demote all headings."
  (org-map-region 'org-demote (point-min) (point-max)))

(defun org-insert-file-as-subtree (filename)
  "Insert a file as sibling subtree at top.
TODO: find a reasonable behavior for this function to insert at point."
  (interactive "*fInsert file as subtree: ")
  (let ((old-buf (current-buffer))
        (buf (get-buffer-create " org subtree from file")))
    (unless (org-at-heading-p)
      (outline-next-heading))
    (let ((level
           (if (org-at-heading-p)
               (plist-get (cadr (org-element-at-point))
                          :level)
             1)))
      (set-buffer buf)
      (insert-file-contents filename)
      (org-demote-all)
      (goto-char (point-min))
      (org-insert-heading)
      (insert filename)
      (newline)
      (dotimes (_ (1- level))
        (org-demote-all)))
    (append-to-buffer old-buf (point-min) (point-max))
    (kill-buffer buf)))
;; insert Org file as subtree:1 ends here

;; disable <> as parenthesis
;; :PROPERTIES:
;; :ID:       124d9d5e-ad99-47ac-b043-1d0fd721ab2d
;; :END:


;; [[file:init.org::*disable <> as parenthesis][disable <> as parenthesis:1]]
(add-hook
 'org-mode-hook
 (lambda ()
    (modify-syntax-entry ?< ".")
    (modify-syntax-entry ?> ".")))
;; disable <> as parenthesis:1 ends here

;; function to reverse lines
;; :PROPERTIES:
;; :ID:       0f93673d-e5be-4aab-9ff2-f59097cfc9dc
;; :END:

;; the function is useful e.g. for the outcome of org-lint.  better
;; handle lines from bottom to top to not confuse the linking.


;; [[file:init.org::*function to reverse lines][function to reverse lines:1]]
(defun mw-reverse-buffer-force ()
  "Reverse line in buffer regardless of readonly state."
  (interactive)
  (let ((inhibit-read-only t))
    (reverse-region
     (point-min) (point-max))))
;; function to reverse lines:1 ends here

;; my personal filter on tags
;; :PROPERTIES:
;; :ID:       efd6cc35-b1af-449d-a306-48bcb59ab14d
;; :END:


;; [[file:init.org::*my personal filter on tags][my personal filter on tags:1]]
(setq org-agenda-auto-exclude-function
      (lambda (tag)
       (cond
        ((member tag '("evening" "abends"))
         (let ((hr (nth 2 (decode-time))))
           (if (or (< hr 20) (< 24 hr)) (concat "-" tag))))
        ;; ((string= tag "bbdb_")
        ;;  (concat "-" tag))
        (t nil))))
;; my personal filter on tags:1 ends here

;; dired-x
;; :PROPERTIES:
;; :ID:       625424b7-ae6d-4b9f-83f2-0a6ee10e743d
;; :END:

;; dired-x at least brings the feature to view a file as RMAIL file on
;; key =V=.


;; [[file:init.org::*dired-x][dired-x:1]]
(require 'dired-x)
;; dired-x:1 ends here

;; git-timemachine
;; :PROPERTIES:
;; :ID:       ee4ea8cb-e93a-4dd5-ae0d-3bf311c45023
;; :END:


;; [[file:init.org::*git-timemachine][git-timemachine:1]]
(straight-use-package 'git-timemachine)
;; git-timemachine:1 ends here

;; symex mode


;; [[file:init.org::*symex mode][symex mode:1]]
(straight-use-package 'symex)
;; symex mode:1 ends here

;; [[file:init.org::*symex mode][symex mode:2]]
(require 'symex)

(defun mw/symex-editing-quit ()
  (interactive)
  (symex-editing-mode -1))

(setq symex--user-evil-keyspec
      '(("Q" . mw/symex-editing-quit)
	("k" . symex-go-down)
	("j" . symex-go-up)
	("C-j" . symex-climb-branch)
	("C-k" . symex-descend-branch)
	("C-S-k" . symex-emit-backward)
	("C-S-j" . symex-emit-forward)
	("M-k" . symex-goto-lowest)
	("M-j" . symex-goto-highest)))
;; symex mode:2 ends here

;; [[file:init.org::*symex mode][symex mode:3]]
(symex-initialize)
;; symex mode:3 ends here

;; j-mode
;; :PROPERTIES:
;; :ID:       1a9f011e-0e9d-4e34-a157-7dd5f43f77e8
;; :END:

;; use a fork which does not redefine if-let.  I observed the issue when
;; tried the auto-correct package.


;; [[file:init.org::*j-mode][j-mode:1]]
(straight-use-package
 '(j-mode :type git :host github :repo "zellio/j-mode"))
;; j-mode:1 ends here

;; buttercup
;; :PROPERTIES:
;; :ID:       f04fcd18-134f-4e5f-b9d1-338e9091576c
;; :END:

;; a test framework for elisp.

;; exceptionally installed buttercup via classical package manager.
;; reason: could not easily get buttercup to run via the straight
;; install.

;; try via straight.


;; [[file:init.org::*buttercup][buttercup:1]]
(straight-use-package 'buttercup)
;; buttercup:1 ends here

;; assess
;; :PROPERTIES:
;; :ID:       67921df3-b7db-4e56-b15a-45924b761340
;; :END:


;; [[file:init.org::*assess][assess:1]]
(straight-use-package 'assess)
;; assess:1 ends here

;; m-buffer
;; :PROPERTIES:
;; :ID:       392b60be-cfef-44ac-8447-a965d6d0ec99
;; :END:


;; [[file:init.org::*m-buffer][m-buffer:1]]
(straight-use-package 'm-buffer)
;; m-buffer:1 ends here

;; elm-mode
;; :PROPERTIES:
;; :ID:       9c49fe85-e2e4-4cb7-a01f-3cb4a9ef3732
;; :END:


;; [[file:init.org::*elm-mode][elm-mode:1]]
(straight-use-package 'elm-mode)
;; elm-mode:1 ends here

;; elm-mode from local clone
;; :PROPERTIES:
;; :ID:       b501a0fc-f830-4203-8a82-85548500faf6
;; :END:


;; [[file:init.org::*elm-mode from local clone][elm-mode from local clone:1]]
;; (add-to-list 'load-path "~/s/elisp/external/elm-mode")
;; (require 'elm-mode)
;; elm-mode from local clone:1 ends here

;; fzf
;; :PROPERTIES:
;; :ID:       b9e98673-97e8-4ab1-9d1e-8f8523e6d2ee
;; :END:

;; fuzzy find files.  fzf is impressive on the command line.
;; - [2020-03-19 Thu] I don't really get it how to use in emacs.
;; - [2020-03-19 Thu] playing to better understand.


;; [[file:init.org::*fzf][fzf:1]]
(straight-use-package 'fzf)
;; fzf:1 ends here

;; feature-mode
;; :PROPERTIES:
;; :ID:       69cb51ce-f7c7-42b1-8189-d291a741a186
;; :END:

;; this is a mode for test files in the cucumber style.


;; [[file:init.org::*feature-mode][feature-mode:1]]
(straight-use-package 'feature-mode)
;; feature-mode:1 ends here

;; org-journal
;; :PROPERTIES:
;; :ID:       7e62283f-5c30-436d-b7c2-d78e5086b6d9
;; :END:

;; this is some journaling mode based on org.


;; [[file:init.org::*org-journal][org-journal:1]]
(straight-use-package 'org-journal)
;; org-journal:1 ends here

;; [[file:init.org::*org-journal][org-journal:2]]
(require 'org-journal)
(setq org-journal-dir (concat mw-personal-org-files-dir "journal/"))
;; org-journal:2 ends here

;; org-roam
;; :PROPERTIES:
;; :ID:       544e82c9-0e80-4ef8-a520-1bf65c9335e5
;; :END:

;; - try understand before enabling it.
;;   - I had some issues with the enabling.  but I did not understand.

;; - this is some orga thing for notes and stuff.


;; [[file:init.org::*org-roam][org-roam:1]]
;(straight-use-package '(org-roam :host github :repo "jethrokuan/org-roam" :branch "develop"))
;(require 'org-roam)
;; org-roam:1 ends here

;; [[file:init.org::*org-roam][org-roam:2]]
;(setq org-roam-directory mw-personal-org-files-dir)
;(add-hook 'org-mode-hook #'org-roam-mode)
;(add-hook 'after-init-hook #'org-roam--build-cache-async)
;; org-roam:2 ends here

;; smartparens
;; :PROPERTIES:
;; :ID:       11c6416f-02e7-43e5-8b77-a4760525e11f
;; :END:

;; - [2016-01-08 Fri 14:49] At first I thought smartparens-mode will replace paredit for me.  But somehow I always come back to paredit.
;; - [2020-02-12 Wed 11:21]


;; [[file:init.org::*smartparens][smartparens:1]]
(straight-use-package 'smartparens)
;; smartparens:1 ends here

;; dired-collapse
;; :PROPERTIES:
;; :ID:       5a2b27d6-b35a-475e-b42e-2483d5f9671a
;; :END:


;; [[file:init.org::*dired-collapse][dired-collapse:1]]
(straight-use-package 'dired-collapse)
;; dired-collapse:1 ends here

;; dired-filter
;; :PROPERTIES:
;; :ID:       692e8c46-0ffe-4178-a23d-0f77b65cc7b0
;; :END:


;; [[file:init.org::*dired-filter][dired-filter:1]]
(straight-use-package 'dired-filter)
;; dired-filter:1 ends here

;; avy-zap
;; :PROPERTIES:
;; :ID:       c0bfb12b-f9fc-4597-8736-492fc536ef06
;; :END:

;; An alternative to zap-to-char.


;; [[file:init.org::*avy-zap][avy-zap:1]]
(straight-use-package 'avy-zap)
;; avy-zap:1 ends here

;; [[file:init.org::*avy-zap][avy-zap:2]]
(global-set-key (kbd "C-M-z") #'avy-zap-up-to-char-dwim)
(global-set-key (kbd "C-M-Z") #'avy-zap-to-char-dwim)
;; avy-zap:2 ends here

;; nov
;; :PROPERTIES:
;; :ID:       c0fe03f9-0b27-4565-9f3a-d0ec5cff330b
;; :END:

;; epub reader.


;; [[file:init.org::*nov][nov:1]]
(straight-use-package 'nov)
;; nov:1 ends here

;; [[file:init.org::*nov][nov:2]]
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
;; nov:2 ends here

;; equake
;; :PROPERTIES:
;; :ID:       4ef2695d-6220-4af9-a376-b788c9e69458
;; :END:

;; a drop down console.


;; [[file:init.org::*equake][equake:1]]
(straight-use-package 'equake)
;; equake:1 ends here

;; [[file:init.org::*equake][equake:2]]
(require 'equake)
;; equake:2 ends here

;; noccur
;; :PROPERTIES:
;; :ID:       537120dc-424d-42bc-9cb3-d5a0ad430652
;; :END:


;; [[file:init.org::*noccur][noccur:1]]
(straight-use-package 'noccur)
;; noccur:1 ends here

;; nyan-mode
;; :PROPERTIES:
;; :ID:       13a7baa3-2b09-4239-bd29-fb01966bf211
;; :END:


;; [[file:init.org::*nyan-mode][nyan-mode:1]]
(straight-use-package 'nyan-mode)
;; nyan-mode:1 ends here

;; perspective
;; :PROPERTIES:
;; :ID:       5d5c0c26-05f6-4576-8165-48d72a88eb64
;; :END:

;; define workspaces.  a workspace is an image configuration and a
;; certain set of buffers.


;; [[file:init.org::*perspective][perspective:1]]
(straight-use-package 'perspective)
;; perspective:1 ends here

;; tuareg
;; :PROPERTIES:
;; :ID:       81844d76-f907-4725-98b0-57ab6bdf3987
;; :END:

;; a mode for OCaml.


;; [[file:init.org::*tuareg][tuareg:1]]
(straight-use-package 'tuareg)
;; tuareg:1 ends here

;; git-auto-commit-mode
;; :PROPERTIES:
;; :ID:       70ec63c9-3eaf-405b-b159-084a7c8fb207
;; :END:


;; [[file:init.org::*git-auto-commit-mode][git-auto-commit-mode:1]]
(straight-use-package 'git-auto-commit-mode)
;; git-auto-commit-mode:1 ends here

;; annot
;; :PROPERTIES:
;; :ID:       13fc044d-7a84-42b9-8c36-1f4946fe6582
;; :END:


;; [[file:init.org::*annot][annot:1]]
(straight-use-package 'annot)
;; annot:1 ends here

;; notmuch
;; :PROPERTIES:
;; :ID:       23656219-4a58-4c12-b2f1-3a224c80a860
;; :END:


;; [[file:init.org::*notmuch][notmuch:1]]
(straight-use-package 'notmuch)
;; notmuch:1 ends here

;; org-brain
;; :PROPERTIES:
;; :ID:       95588f13-6f63-474d-b7ac-15b2c222d270
;; :END:


;; [[file:init.org::*org-brain][org-brain:1]]
(straight-use-package 'org-brain)
;; org-brain:1 ends here

;; rainbow-delimiters
;; :PROPERTIES:
;; :ID:       c11bf770-eeef-4859-9b76-cee479c5ba97
;; :END:


;; [[file:init.org::*rainbow-delimiters][rainbow-delimiters:1]]
(straight-use-package 'rainbow-delimiters)
;; rainbow-delimiters:1 ends here

;; bbdb-vcard
;; :PROPERTIES:
;; :ID:       28b989be-abc6-40b9-b30b-09d752fbc67a
;; :END:

;; import a vcs to bbdb.


;; [[file:init.org::*bbdb-vcard][bbdb-vcard:1]]
(straight-use-package 'bbdb-vcard)
;; bbdb-vcard:1 ends here

;; side-note
;; :PROPERTIES:
;; :ID:       1b88c71f-79b7-4212-bb53-8fd4373ba0de
;; :END:


;; [[file:init.org::*side-note][side-note:1]]
(straight-use-package '(side-notes :type git :host github :repo "rnkn/side-notes"))
;; side-note:1 ends here

;; [[file:init.org::*side-note][side-note:2]]
(global-set-key (kbd "M-s n") #'side-notes-toggle-notes)
;; side-note:2 ends here

;; wgrep
;; :PROPERTIES:
;; :ID:       5b8559e3-8c71-4804-ba47-c663127907cc
;; :END:

;; #+begin_quote
;; Writable grep buffer and apply the changes to files.
;; #+end_quote


;; [[file:init.org::*wgrep][wgrep:1]]
(straight-use-package 'wgrep)
;; wgrep:1 ends here

;; multiple-cursors
;; :PROPERTIES:
;; :ID:       dc716307-f455-425c-a615-dba30297d8c1
;; :END:

;; multiple-cursors adds the feature to edit several locations at once.


;; [[file:init.org::*multiple-cursors][multiple-cursors:1]]
(straight-use-package 'multiple-cursors)
;; multiple-cursors:1 ends here

;; beginend
;; :PROPERTIES:
;; :ID:       d98907e9-16e7-4675-9698-3fc5ec5408ae
;; :END:

;; redefine M-< and M-> which originally was jump to the beginning and
;; jump to the end.


;; [[file:init.org::*beginend][beginend:1]]
(straight-use-package 'beginend)
;; beginend:1 ends here

;; beginning/end of buffer

;; still want bindings for beginning-of-buffer and end-of-buffer.


;; [[file:init.org::*beginning/end of buffer][beginning/end of buffer:1]]
(global-set-key (kbd "C-M-<")
                (lambda (arg) (interactive "P")
                  (beginning-of-buffer arg)))
(global-set-key (kbd "C-M->")
                (lambda (arg) (interactive "P")
                  (end-of-buffer arg)))
;; beginning/end of buffer:1 ends here

;; swiper
;; :PROPERTIES:
;; :ID:       5381268d-16c9-42a6-a0d6-8391bcaa7adf
;; :END:

;; swiper is a buffer search tool similar to isearch-forward.


;; [[file:init.org::*swiper][swiper:1]]
(straight-use-package 'swiper)
;; swiper:1 ends here

;; [[file:init.org::*swiper][swiper:2]]
(global-set-key (kbd "C-S-s") #'swiper)
;; swiper:2 ends here

;; counsel
;; :PROPERTIES:
;; :ID:       bfc4c5c1-71b4-4868-ba55-676979a1ba07
;; :END:

;; counsel offers several interfaces.  e.g.:
;; - counsel-M-x
;; - counsel-rg


;; [[file:init.org::*counsel][counsel:1]]
(straight-use-package 'counsel)
;; counsel:1 ends here

;; defrepeater
;; :PROPERTIES:
;; :ID:       a2cbfd10-2695-41f8-9333-88c56cfc4d3f
;; :END:

;; IIUC this is a mechanism to repeat commands with repeated press of the
;; last character of its key sequence.


;; [[file:init.org::*defrepeater][defrepeater:1]]
(straight-use-package 'defrepeater)
;; defrepeater:1 ends here

;; code
;; :PROPERTIES:
;; :ID:       ddd0cfe5-68be-44da-9e6e-8d559d1314be
;; :END:

;; - [2021-10-20 Wed] obs: this is realized in Emacs 29 in repeat-mode.
;;   I try to switch there.
;;   - this has been useful for me.


;; [[file:init.org::*code][code:1]]
(defun mw-other-window-repeat (count &optional all-frames)
  "Wrapper around `other-window' to continue to jump to other with key o."
  (interactive "p")
  (other-window count all-frames)
  (message "Use o to jump to next window.")
  (set-transient-map
   (let ((map (make-sparse-keymap)))
     (define-key map (kbd "o")
       (lambda () (interactive) (mw-other-window-repeat 1)))
     map)))

; (global-set-key (kbd "C-x o") #'mw-other-window-repeat)
; (global-set-key (kbd "C-x o") #'other-window)
;; code:1 ends here

;; [[file:init.org::*winner-undo][winner-undo:3]]
(with-eval-after-load 'winner
  (progn
     (defrepeater #'winner-undo)
     (define-key winner-mode-map [remap winner-undo] 'winner-undo-repeat)))
;; winner-undo:3 ends here

;; encourage-mode
;; :PROPERTIES:
;; :ID:       eaf01c71-e329-47c7-82f0-e44ed7882d90
;; :END:

;; Encouraging words occasionally.


;; [[file:init.org::*encourage-mode][encourage-mode:1]]
(straight-use-package 'encourage-mode)
(encourage-mode)
;; encourage-mode:1 ends here

;; frog-jump-buffer
;; :PROPERTIES:
;; :ID:       49a13c3f-59f6-4030-840a-38d00e9a307c
;; :END:

;; frog-jump-buffer is the fastest buffer-jumping Emacs lisp package around.


;; [[file:init.org::*frog-jump-buffer][frog-jump-buffer:1]]
(straight-use-package '(frog-jump-buffer :type git :host github :repo "waymondo/frog-jump-buffer"))
;; frog-jump-buffer:1 ends here

;; [[file:init.org::*frog-jump-buffer][frog-jump-buffer:2]]
(with-eval-after-load 'key-chord
  (key-chord-define-global "f6"
   (lambda (&optional arg)
     (interactive "P")
     (if arg
         (frog-jump-buffer-other-window)
       (frog-jump-buffer)))))
;; frog-jump-buffer:2 ends here

;; emacs-memento-mori
;; :PROPERTIES:
;; :ID:       ee2f8b5b-662c-40c6-984e-a6feaa88254a
;; :END:

;; Reminder of mortality in the mode-line.


;; [[file:init.org::*emacs-memento-mori][emacs-memento-mori:1]]
;; (straight-use-package '(emacs-memento-mori :local-repo "/home/b/s/elisp/mw/emacs-memento-mori"))
;; emacs-memento-mori:1 ends here

;; dired-du
;; :PROPERTIES:
;; :ID:       574af9db-8049-498f-852f-ddaed04aa012
;; :END:

;; display disk usage recursively in dired.


;; [[file:init.org::*dired-du][dired-du:1]]
(straight-use-package 'dired-du)
;; dired-du:1 ends here

;; sly
;; :PROPERTIES:
;; :ID:       af68594d-7c1d-43a7-98e8-383b4d7f8c32
;; :END:

;; sly is a lisp development environment forked from [[id:ec24ba1a-6989-472a-91c0-0002c0a9b1d2][slime]].


;; [[file:init.org::*sly][sly:1]]
(straight-use-package 'sly)
(with-eval-after-load "sly"
  (progn
     (setq inferior-lisp-program "sbcl")
     (setq common-lisp-hyperspec-root
           "file:///home/b/d/texts/it/languages/lisp/HyperSpec/")
     (define-key sly-doc-map (kbd "C-h") nil)
     (define-key sly-doc-map (kbd "h") 'sly-documentation-lookup)))
;; sly:1 ends here

;; chronos config
;; :PROPERTIES:
;; :ID:       8b37791c-066e-4af6-b185-78d7323efbfd
;; :END:


;; [[file:init.org::*chronos config][chronos config:1]]
(defvar *mw-timer* nil "For recall a timer for later kill.")
(straight-use-package 'chronos)
(require 'chronos)
(setf chronos-text-to-speech-program "espeak"
                ;; chronos-text-to-speech-program-parameters
                chronos-text-to-speech-program-parameters  '("-v" "de" "-s" "110")
                chronos-expiry-functions
                '(chronos-buffer-notify
                  chronos-text-to-speech-notify
                  (lambda (c)
                    (switch-to-buffer "*chronos*")
                    (delete-other-windows))))
;; chronos config:1 ends here

;; elpher
;; :PROPERTIES:
;; :creation_date: [2019-05-27 11:13]
;; :ID:       c02575d7-d3b9-4e77-a66f-e3adecc5fe32
;; :END:


;; [[file:init.org::*elpher][elpher:1]]
(straight-use-package 'elpher)
;; elpher:1 ends here

;; org-pretty-tags
;; :PROPERTIES:
;; :ID:       fe270774-b7dd-4302-a8cc-b6bf300fe7da
;; :END:

;; replace certain tags in Org with symbols or images.


;; [[file:init.org::*org-pretty-tags][org-pretty-tags:1]]
(push "~/s/elisp/mw/org-pretty-tags" load-path)
(require 'org-pretty-tags)
;; org-pretty-tags:1 ends here

;; selectric-mode :typing:
;; :PROPERTIES:
;; :ID:       5d3ebd11-fa23-4c0f-96f9-567ba65312aa
;; :catchwords: keyboard, click, sound
;; :END:

;; make some typewriter noise.


;; [[file:init.org::*selectric-mode][selectric-mode:1]]
(straight-use-package 'selectric-mode)
;; selectric-mode:1 ends here

;; markdown-mode
;; :PROPERTIES:
;; :ID:       e6509370-37b4-4f8d-bc5b-7344e1c7081d
;; :END:


;; [[file:init.org::*markdown-mode][markdown-mode:1]]
(straight-use-package 'markdown-mode)
;; markdown-mode:1 ends here

;; org-attach-screenshot
;; :PROPERTIES:
;; :ID:       b57f2c10-9e82-4fd1-8d8a-9bf9aec5014e
;; :END:

;; attach screenshots conveniently starting in an Org subtree.


;; [[file:init.org::*org-attach-screenshot][org-attach-screenshot:1]]
(straight-use-package 'org-attach-screenshot)
;; org-attach-screenshot:1 ends here

;; AUR access
;; :PROPERTIES:
;; :ID:       5c3c8a77-546a-4260-b415-d73b822e76c2
;; :END:

;; ~aurel~ helps with the management of the AUR-packages of the
;; Arch-Linux system.


;; [[file:init.org::*AUR access][AUR access:1]]
(straight-use-package 'aurel)
(autoload 'aurel-package-info "aurel" nil t)
(autoload 'aurel-package-search "aurel" nil t)
(autoload 'aurel-maintainer-search "aurel" nil t)
(autoload 'aurel-installed-packages "aurel" nil t)
(setq aurel-download-directory "~/AUR")
;; AUR access:1 ends here

;; enable impatient mode
;; :PROPERTIES:
;; :ID:       ca356620-84d9-4ddf-ae94-afcc07cd03ca
;; :END:


;; [[file:init.org::*enable impatient mode][enable impatient mode:1]]
(straight-use-package 'impatient-mode)
;; enable impatient mode:1 ends here

;; evil-numbers
;; :PROPERTIES:
;; :ID:       201ae3cb-9c59-43f2-b4fc-961839f4887e
;; :END:

;; Quickly add to integers in buffer with prefix-arguments for
;; adding/subtracting that value.  Default is 1.

;; these commands also act on regions.


;; [[file:init.org::*evil-numbers][evil-numbers:1]]
(straight-use-package 'evil-numbers)

(global-set-key (kbd "M-+") #'evil-numbers/inc-at-pt)
(global-set-key (kbd "M--") #'evil-numbers/dec-at-pt)
;; evil-numbers:1 ends here

;; haskell-mode
;; :PROPERTIES:
;; :ID:       4c3e32c2-0f66-4e03-917a-722ca74aa692
;; :END:

;; support for writing haskell programs.


;; [[file:init.org::*haskell-mode][haskell-mode:1]]
(straight-use-package 'haskell-mode)
;; haskell-mode:1 ends here

;; avy
;; :PROPERTIES:
;; :ID:       72c5f7a8-60b1-494e-ace6-69b608b3f674
;; :END:

;; Move cursor onto a visible character.

;; =avy= is similar to ace-jump-mode.  I read that avy is the variant
;; that gets maintained.


;; [[file:init.org::*avy][avy:1]]
(straight-use-package 'avy)

(defun mw-avy-goto-line (&optional arg)
  "Index lines below (above with ARG) for jump there quickly."
  (interactive "P")
  (funcall
   (cond
    ((equal '(16) arg) #'avy-goto-line)
    ((equal '(4) arg) #'avy-goto-line-above)
    (t #'avy-goto-line-below))))

(global-set-key (kbd "C-.") #'avy-goto-char)
(global-set-key (kbd "C->") #'avy-goto-char-in-line)
(global-set-key (kbd "C-,") #'mw-avy-goto-line)

(avy-setup-default)
  ;; [2016-05-03 Tue 15:56]: was
  ;;(with-eval-after-load "isearch" (define-key isearch-mode-map (kbd "C-'") 'avy-isearch))
;; avy:1 ends here

;; directory
;; :PROPERTIES:
;; :ID:       2f4e6b16-d230-4790-9931-c9fcb787d91a
;; :END:


;; [[file:init.org::*directory][directory:1]]
(straight-use-package 'dictionary)
;; directory:1 ends here

;; ledger-mode
;; :PROPERTIES:
;; :ID:       741d3b30-4b45-4854-87a5-5a645ca38410
;; :END:


;; [[file:init.org::*ledger-mode][ledger-mode:1]]
(straight-use-package 'ledger-mode)
;; ledger-mode:1 ends here



;; pretty money symbols in ledger.


;; [[file:init.org::*ledger-mode][ledger-mode:2]]
(add-hook
 'ledger-mode-hook
 (lambda ()
   (setq prettify-symbols-alist
         (append prettify-symbols-alist
                 '(("eur" . ?€)
                   ("eurd" . ?⅒)
                   ("eurc" . ?₰))))))
;; ledger-mode:2 ends here

;; expand-region
;; :PROPERTIES:
;; :ID:       196f772e-5a6f-4e19-8b6f-81a38cb0c9a6
;; :END:

;; =expand-region= helps to expand the region.


;; [[file:init.org::*expand-region][expand-region:1]]
(straight-use-package 'expand-region)
;; expand-region:1 ends here

;; [[file:init.org::*expand-region][expand-region:2]]
(global-set-key (kbd "C-=") #'er/expand-region)
;; expand-region:2 ends here

;; pinentry
;; :PROPERTIES:
;; :ID:       6bdb86a9-c6f1-4a92-b7bd-04b003eb9c2d
;; :END:

;; this program realizes input of passwords via emacs, i think.


;; [[file:init.org::*pinentry][pinentry:1]]
(straight-use-package 'pinentry)
;; pinentry:1 ends here

;; key chord
;; :PROPERTIES:
;; :ID:       a8cef132-dc5a-4d7d-afc7-bc3a591f9ba4
;; :END:


;; [[file:init.org::*key chord][key chord:1]]
(straight-use-package 'key-chord)
;; key chord:1 ends here

;; [[file:init.org::*key chord][key chord:2]]
(key-chord-mode 1)
;; key chord:2 ends here

;; hydra
;; :PROPERTIES:
;; :ID:       8b5cc921-e0bb-410d-86e6-d7598f8eb2a7
;; :END:

;; Hydra provides some convenient key maps organization.


;; [[file:init.org::*hydra][hydra:1]]
(straight-use-package 'hydra)
;; hydra:1 ends here

;; package provision
;; :PROPERTIES:
;; :ID:       3328012b-738c-4ff9-9eff-cf85940aef20
;; :END:

;; "emacs and scheme talk to each other"


;; [[file:init.org::*package provision][package provision:1]]
(straight-use-package 'geiser)
;(straight-use-package '(geiser :type git :host gitlab :repo "jaor/geiser"))
;; package provision:1 ends here

;; ido-hacks
;; :PROPERTIES:
;; :ID:       66bdba19-c2bf-4c68-b30d-493af73c92e7
;; :END:

;; ido-hacks sits on top of ido and makes ido even cooler.  When
;; ido-hacks-mode comes into the way then just switch it off.

;; - [2017-11-05 Sun 17:15] ido-hacks fails often.  it scrambles the
;;   characters often.
;; - [2017-11-05 Sun 17:18] disabling ido-hacks.
;; - [2017-12-19 Tue 14:19] reenabling.  is this adiction?
;; - [2018-07-08 Sun 14:31] LTM like the regexp variant is broken.


;; [[file:init.org::*ido-hacks][ido-hacks:1]]
(straight-use-package 'ido-hacks)
;; (require 'ido-hacks)
;; (ido-mode)
;; (when ido-enable-regexp
;;   (ido-toggle-regexp))
;; (ido-hacks-mode)
;; ido-hacks:1 ends here

;; filladapt
;; :PROPERTIES:
;; :ID:       fdacf250-50ca-45ac-a6f3-ce5f3479dd80
;; :END:

;; - claims to do the right fill in some cases iirc.
;; - [2020-02-24 Mon] kotl from hyperbole 7.1 needs that package AFAICT.


;; [[file:init.org::*filladapt][filladapt:1]]
(straight-use-package 'filladapt)
(require 'filladapt)
;; filladapt:1 ends here

;; dired-narrow
;; :PROPERTIES:
;; :ID:       5579337d-b273-4cdd-b211-9ba702055dbf
;; :END:

;; At very first invocation do ~M-x dired-narrow~ in a dired buffer.
;; After that the key binding is active.

;; Recall g for getting rid of all filtering.


;; [[file:init.org::*dired-narrow][dired-narrow:1]]
(straight-use-package 'dired-narrow)
;; dired-narrow:1 ends here

;; auto-correct
;; :PROPERTIES:
;; :ID:       02f5f74e-a90c-430a-b5ea-67aa8ab575e8
;; :END:


;; [[file:init.org::*auto-correct][auto-correct:1]]
(straight-use-package 'auto-correct)
;; auto-correct:1 ends here

;; quarter-plane
;; :PROPERTIES:
;; :ID:       64eaa95c-68dc-40f9-a5e6-66fc6a5949f8
;; :END:


;; [[file:init.org::*quarter-plane][quarter-plane:1]]
(straight-use-package 'quarter-plane)
;; quarter-plane:1 ends here

;; gited
;; :PROPERTIES:
;; :ID:       227f4a56-14d2-465c-a0ce-c6f55be7f16d
;; :END:

;; get into the mode with

;;     M-x gited-list-branches


;; [[file:init.org::*gited][gited:1]]
(straight-use-package 'gited)
(add-hook 'dired-mode-hook
          (lambda ()
            (define-key dired-mode-map "\C-x\C-g" 'gited-list-branches)))
;; gited:1 ends here

;; lua-mode
;; :PROPERTIES:
;; :ID:       38327bd2-5339-42b5-b199-a1b9e392df72
;; :END:


;; [[file:init.org::*lua-mode][lua-mode:1]]
(straight-use-package 'lua-mode)
;; lua-mode:1 ends here

;; mastodon
;; :PROPERTIES:
;; :ID:       7e599a1f-36cb-42d4-a7ce-77d9705d2a16
;; :END:


;; [[file:init.org::*mastodon][mastodon:1]]
(straight-use-package 'mastodon)
;; mastodon:1 ends here

;; racket-mode
;; :PROPERTIES:
;; :ID:       82828c19-1b09-4f33-afe1-1b02b455f718
;; :END:


;; [[file:init.org::*racket-mode][racket-mode:1]]
(straight-use-package 'racket-mode)
;; racket-mode:1 ends here

;; password-store
;; :PROPERTIES:
;; :ID:       5dff1977-9f71-4bcb-ac7f-d6e26fae33c4
;; :END:

;; access passwords via emacs.

;; alternative see [[id:2799929b-95cf-4667-8d42-bcde88027b27][pass]].


;; [[file:init.org::*password-store][password-store:1]]
(straight-use-package 'password-store)
;; password-store:1 ends here

;; pass
;; :PROPERTIES:
;; :ID:       2799929b-95cf-4667-8d42-bcde88027b27
;; :END:

;; access passwords via emacs via pass.

;; alternative see [[id:5dff1977-9f71-4bcb-ac7f-d6e26fae33c4][password-store]].


;; [[file:init.org::*pass][pass:1]]
(straight-use-package 'pass)
;; pass:1 ends here

;; herald-the-mode-line
;; :PROPERTIES:
;; :ID:       b858b1f7-cbf2-4b1d-be6c-3c2a8f3a7bd2
;; :END:

;; display mode-line in minibuffer.  this is useful when the mode-line
;; content does not fit into the mode-line.


;; [[file:init.org::*herald-the-mode-line][herald-the-mode-line:1]]
(straight-use-package '(herald-the-mode-line
           :type git
           :host gitlab
           :repo "marcowahl/herald-the-mode-line"))
;; herald-the-mode-line:1 ends here

;; rope read
;; :PROPERTIES:
;; :ID:       2c64db40-7503-4c19-a245-2686d6759164
;; :END:

;; Most important package!  Saves eye movements!


;; [[file:init.org::*rope read][rope read:1]]
(straight-use-package '(rope-read-mode :local-repo "/home/b/s/elisp/mw/rope-read-mode"))
;; rope read:1 ends here

;; async
;; :PROPERTIES:
;; :ID:       df37af31-8c32-4004-92c8-8f9d2f12f150
;; :END:


;; [[file:init.org::*async][async:1]]
(straight-use-package 'async)
(with-eval-after-load "dired-async"
  (dired-async-mode 1))
;; async:1 ends here

;; go-up
;; :PROPERTIES:
;; :ID:       49b54a86-7344-459e-a3d8-205da7cc6d97
;; :END:


;; [[file:init.org::*go-up][go-up:1]]
(straight-use-package
 '(go-up
   :local-repo "/home/b/s/elisp/mw/go-up"))
;; go-up:1 ends here

;; paredit
;; :PROPERTIES:
;; :ID:       f3105d1c-5b56-4f14-aad7-1ff73bbdb2f3
;; :END:

;; Very helpful mode for editing elisp.


;; [[file:init.org::*paredit][paredit:1]]
(straight-use-package 'paredit)

(global-set-key (kbd "C-M-<down>") #'paredit-splice-sexp)
(global-set-key (kbd "C-M-<up>") #'mw-paredit-elevate)
;; paredit:1 ends here

;; disable some paredit key settings
;; :PROPERTIES:
;; :ID:       b04eaab5-b46d-41c5-a1bd-292463f22480
;; :END:


;; [[file:init.org::*disable some paredit key settings][disable some paredit key settings:1]]
(with-eval-after-load 'paredit
  (progn
     (define-key paredit-mode-map (kbd "M-s") nil) ; want this as search key
     (define-key paredit-mode-map (kbd "M-r") nil) ; want this as move-to-window-line-top-bottom
))
;; disable some paredit key settings:1 ends here

;; elevate a sexp
;; :PROPERTIES:
;; :ID:       b68fea14-c89e-4f6c-a687-f50060972fa9
;; :END:

;; an addition for lisp editing.  elevate a sexp to a level above.


;; [[file:init.org::*elevate a sexp][elevate a sexp:1]]
(ert-deftest 9e04e044f2113716a37aeba7a26e7529a434a467 ()
  (string=
   (with-temp-buffer
     (insert "a")
     (goto-char 2)
     (call-interactively #'mw-paredit-elevate)
     (buffer-string))
   "a"))

(ert-deftest 6adde106c7c56a979ceeda1c77158035444edc30 ()
  (string=
   (with-temp-buffer
     (insert "(a)")
     (goto-char 2)
     (call-interactively #'mw-paredit-elevate)
     (buffer-string))
   "a
()"))

(ert-deftest 111c383248a7dfebb5c9d42bf685ec7e869de7fc ()
  (string=
   (with-temp-buffer
     (insert "((haha foo) (bar))")
     (goto-char 2)
     (call-interactively #'mw-paredit-elevate)
     (buffer-string))
   "(haha foo)
((bar))"))


(defun mw-paredit-elevate ()
  "Move sexp at point at backward up location."
  (interactive)
  (catch 'top-paren-level
    (paredit-handle-sexp-errors (paredit-enclosing-list-start)
      (throw 'top-paren-level nil))
    (kill-sexp)
    (paredit-backward-up)
    (yank)))
;; elevate a sexp:1 ends here

;; enable paredit in some cases
;; :PROPERTIES:
;; :ID:       0d34399f-8d10-4937-bc55-fe5063d1b0d1
;; :END:


;; [[file:init.org::*enable paredit in some cases][enable paredit in some cases:1]]
(mapc
 (lambda (x) (add-hook x #'paredit-mode))
 '(lisp-mode
   lisp-interaction-mode-hook
   emacs-lisp-mode-hook
   sly-mrepl-hook
   eval-expression-minibuffer-setup-hook))
;; enable paredit in some cases:1 ends here

;; underline-with-char
;; :PROPERTIES:
;; :ID:       e89c50bb-a312-4225-bd44-d72c2ac97613
;; :END:


;; [[file:init.org::*underline-with-char][underline-with-char:1]]
(straight-use-package
 '(underline-with-char
   :local-repo "/home/b/s/elisp/mw/underline-with-char"))
;; underline-with-char:1 ends here

;; selected.el
;; :PROPERTIES:
;; :ID:       d411d667-37ab-49ed-affe-940a063d226b
;; :END:


;; [[file:init.org::*selected.el][selected.el:1]]
(straight-use-package 'selected)
;(require 'selected)
;(selected-global-mode -1)
;; selected.el:1 ends here

;; [[file:init.org::*selected.el][selected.el:2]]
(with-eval-after-load "selected"
  (define-key selected-keymap (kbd "m") #'apply-macro-to-region-lines))
;; selected.el:2 ends here

;; [[file:init.org::*selected.el][selected.el:3]]
(defun mw-wrap-region (beg end string-beg string-end)
  (interactive "r")
  (goto-char end)
  (insert string-end)
  (goto-char beg)
  (insert string-beg))

(with-eval-after-load "selected"
  (define-key selected-keymap (kbd "\"") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "\"" "\"")))
  (define-key selected-keymap (kbd "`") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "`" "'")))
  (define-key selected-keymap (kbd "[") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "[" "]")))
  (define-key selected-keymap (kbd "(") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "(" ")")))
  (define-key selected-keymap (kbd "{") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "{" "}")))
  (define-key selected-keymap (kbd "~") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "~" "~")))
  (define-key selected-keymap (kbd "*") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "*" "*")))
  (define-key selected-keymap (kbd "_") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "_" "_")))
  (define-key selected-keymap (kbd "=") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "=" "=")))
  (define-key selected-keymap (kbd ":") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) ":" ":")))
  (define-key selected-keymap (kbd "q") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "#+begin_quote\n" "\n#+end_quote" )))
  (define-key selected-keymap (kbd "e") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "#+begin_example\n" "\n#+end_example" )))
  (define-key selected-keymap (kbd "v") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "#+begin_verse\n" "\n#+end_verse" )))
  (define-key selected-keymap (kbd "V") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "#+begin_verbatim\n" "\n#+end_verbatim" )))
  (define-key selected-keymap (kbd "s") (lambda () (interactive) (mw-wrap-region (region-beginning) (region-end) "#+begin_src \n" "\n#+end_src" ))))
;; selected.el:3 ends here

;; wrap-region
;; :PROPERTIES:
;; :ID:       27cd8405-697d-40af-b50c-b20a54b37114
;; :END:

;; press a key to wrap region in a certain way.

;; - [2019-08-20 Tue] switching to selected.el.
;; - [2019-08-20 Tue 11:06] wrap-region did not go well together with org-goto.
;; - [2019-10-24 Thu 16:23] trying to switch back.


;; [[file:init.org::*wrap-region][wrap-region:1]]
(straight-use-package 'wrap-region)
;; wrap-region:1 ends here



;; set some region wrappers.


;; [[file:init.org::*wrap-region][wrap-region:2]]
(wrap-region-global-mode t)
;; wrap-region:2 ends here

;; define some keys
;; :PROPERTIES:
;; :ID:       f8286acf-95d0-48af-8dd0-4fd8ca44e686
;; :END:


;; [[file:init.org::*define some keys][define some keys:1]]
(wrap-region-add-wrapper "`" "'")                  ; hit ` then region -> `region'
(wrap-region-add-wrapper "*" "*" nil 'org-mode)
(wrap-region-add-wrapper "/" "/" nil 'org-mode)
(wrap-region-add-wrapper "=" "=" nil 'org-mode)
;; define some keys:1 ends here

;; form-feed-mode
;; :PROPERTIES:
;; :ID:       5e169d24-bc68-42da-ae9d-2f3d190a9547
;; :END:

;; display page breaks nicely.


;; [[file:init.org::*form-feed-mode][form-feed-mode:1]]
(straight-use-package 'form-feed)
;; form-feed-mode:1 ends here

;; page-break-lines
;; :PROPERTIES:
;; :ID:       7fac0a11-7abe-403f-a1ec-d66ae1f41e87
;; :END:

;; display page breaks nicely.


;; [[file:init.org::*page-break-lines][page-break-lines:1]]
(straight-use-package 'page-break-lines)
;; page-break-lines:1 ends here

;; ztree
;; :PROPERTIES:
;; :ID:       0cff38d9-bfe1-4013-89c9-827e9445ea20
;; :END:


;; [[file:init.org::*ztree][ztree:1]]
(straight-use-package 'ztree)
;; ztree:1 ends here

;; company mode
;; :PROPERTIES:
;; :ID:       09620e2a-f12e-4b6a-bd06-68312c1b5264
;; :END:

;; company supports completion.


;; [[file:init.org::*company mode][company mode:1]]
(straight-use-package 'company)
;; company mode:1 ends here

;; [[file:init.org::*company mode][company mode:2]]
;(global-company-mode)
(require 'company)
;; company mode:2 ends here

;; tab to cycle through the candidates
;; :PROPERTIES:
;; :ID:       b0564f07-3d5d-4880-aa3e-181938b21dc3
;; :END:


;; [[file:init.org::*tab to cycle through the candidates][tab to cycle through the candidates:1]]
(define-key company-active-map [tab] #'company-complete-common-or-cycle)
;; tab to cycle through the candidates:1 ends here

;; tab to cycle backwards through the candidates
;; :PROPERTIES:
;; :ID:       1b17886a-62fd-4869-a06d-029a67db14c7
;; :END:

;; I define a whole new function to get full "symmetry" to tab.


;; [[file:init.org::*tab to cycle backwards through the candidates][tab to cycle backwards through the candidates:1]]
(defun mw-company-complete-common-or-cycle-reverse (&optional arg)
  "Insert the common part of all candidates, or select the previous one.

With ARG, move by that many elements."
  (interactive "p")
  (company-complete-common-or-cycle (when arg (- arg))))
;; tab to cycle backwards through the candidates:1 ends here

;; magit
;; :PROPERTIES:
;; :ID:       87c8cde9-367c-493f-8ec7-fc95c8d99ef3
;; :END:

;; magit is an interface to git.


;; [[file:init.org::*magit][magit:1]]
(straight-use-package 'magit)
(with-eval-after-load "magit-diff"
  (define-key magit-file-section-map "C"
     'magit-commit-add-log))
;; magit:1 ends here

;; better individual magit rebase menu documentation
;; :PROPERTIES:
;; :ID:       22e8eda9-11c2-4a1b-a1f2-1c413f2dd484
;; :END:

;; can never remember if the "previous" already there in the rebase-menu
;; means above or below.  it's above btw.  so displaying that info.

;; note that this information is now somewhat duplicated.


;; [[file:init.org::*better individual magit rebase menu documentation][better individual magit rebase menu documentation:1]]
(with-eval-after-load "git-rebase"
  (setq git-rebase-command-descriptions
	(append git-rebase-command-descriptions
                '((git-rebase-fixup . "fixup = like \"squash\", but discard this commit's log message"))
		'((git-rebase-squash . "squash = use commit, but meld into commit above (aka previous)")))))
;; better individual magit rebase menu documentation:1 ends here

;; deft
;; :PROPERTIES:
;; :ID:       381cf6f1-e88c-4161-b931-9421ea020224
;; :END:


;; [[file:init.org::*deft][deft:1]]
(straight-use-package 'deft)
;; deft:1 ends here

;; [[file:init.org::*deft][deft:2]]
(global-set-key (kbd "C-x d") #'deft)
;; deft:2 ends here

;; evil
;; :PROPERTIES:
;; :ID:       70d626b4-396e-4849-8261-2fd5901deaa3
;; :END:


;; [[file:init.org::*evil][evil:1]]
(straight-use-package 'evil)
;; evil:1 ends here

;; hack-time-mode
;; :PROPERTIES:
;; :ID:       f85375a8-ae48-4ac7-bff2-9bb7ccba4218
;; :END:


;; [[file:init.org::*hack-time-mode][hack-time-mode:1]]
(straight-use-package
 '(hack-time-mode
   :local-repo "/home/b/s/elisp/mw/hack-time-mode"))
;; hack-time-mode:1 ends here

;; shadow
;; :PROPERTIES:
;; :ID:       38b9ccba-6e33-453d-b10a-8df80ddfed82
;; :END:


;; [[file:init.org::*shadow][shadow:1]]
(straight-use-package 'shadow)
;; shadow:1 ends here

;; clojure and cider
;; :PROPERTIES:
;; :ID:       cf3bdd56-9141-4bb8-9743-a81821227746
;; :END:


;; [[file:init.org::*clojure and cider][clojure and cider:1]]
(straight-use-package 'cider)
;; clojure and cider:1 ends here

;; mpages
;; :PROPERTIES:
;; :ID:       cc89df5d-a0f8-4b36-aaff-9939161f5c5d
;; :END:

;; Using a local branch and not the package to test a version with
;; encryption.


;; [[file:init.org::*mpages][mpages:1]]
(push  "~/s/elisp/mw/mpages" load-path)
(autoload 'mpages "mpages" "For writing morning pages." t nil)

(defadvice mpages (after ctrlc-ctrlc-to-finish activate)
  "Set C-c C-c to close the mpage writing.
Set the key for encrytion, then save and kill the buffer.
This binding shall make the close more convenient."
  (local-set-key [?\C-c ?\C-c] (lambda ()
                                 (interactive)
                                 (setq epa-file-encrypt-to '("49010A040A3AE6F2"))
                                 (save-buffer)
                                 (kill-buffer))))
;; mpages:1 ends here

;; org-reveal
;; :PROPERTIES:
;; :ID:       723c931f-e0e7-4e6a-b0f2-c89d95b96e00
;; :END:

;; create slides from org.

;; - [2018-05-17 Thu 11:44] leaving the ~(require 'ox-reveal)~ in the
;;   config leads to some file related error which starts with "File mode
;;   specification error:".  (Seen in messages buffer.)  This is relevant
;;   because I e.g. can't org-capture anymore.
;;   - workaround: load the package when needed.
;;   - try understand or report.


;; [[file:init.org::*org-reveal][org-reveal:1]]
(add-to-list 'load-path "~/s/elisp/external/org-reveal")
; (require 'ox-reveal)
;; org-reveal:1 ends here

;; folding.el
;; :PROPERTIES:
;; :ID:       ed0e156b-7f1b-4ac2-ae6b-fe6917d752b9
;; :END:

;; this mode folds regions delimitted by {{{ and }}}.


;; [[file:init.org::*folding.el][folding.el:1]]
(add-to-list 'load-path "~/s/elisp/external/project-emacs--folding-mode")
(require 'folding)
;; folding.el:1 ends here

;; TODO need to study that mode some more...
;; :PROPERTIES:
;; :ID:       bbbce426-b210-4b55-b6d2-1fc89c159178
;; :END:

;; e.g. the following does not add folding to makefile buffers AFAICT.


;; [[file:init.org::*need to study that mode some more...][need to study that mode some more...:1]]
(folding-add-to-marks-list 'makefile-mode "# {{{ " "# }}}")
(folding-add-to-marks-list 'makefile-gmake-mode "# {{{ " "# }}}")
;; need to study that mode some more...:1 ends here

;; org-bullets :org:
;; :PROPERTIES:
;; :ID:       b43bd556-05c9-4d1a-be97-b1dca5bba221
;; :END:


;; [[file:init.org::*org-bullets][org-bullets:1]]
(push "~/s/elisp/external/org-bullets" load-path)
(require 'org-bullets)
;; org-bullets:1 ends here

;; bbdb
;; :PROPERTIES:
;; :ID:       c2a15a96-f49c-442b-b2b0-b722d1c0ff98
;; :END:

;; Big Brother DB.  Well integrated storage for data around institutions
;; and persons e.g. addresses.


;; [[file:init.org::*bbdb][bbdb:1]]
(push (expand-file-name "~/s/elisp/external/bbdb/lisp") load-path)
(require 'bbdb-loaddefs (expand-file-name "~/s/elisp/external/bbdb/lisp/bbdb-loaddefs.el"))
(bbdb-initialize 'gnus 'message 'anniv)
(bbdb-mua-auto-update-init 'gnus 'message)
(setq bbdb-mua-pop-up nil
      ;; bbdb-mua-pop-up-window-size 0.1
      bbdb-mua-update-interactive-p '(query . create)
      bbdb-mua-auto-update-p 'create ; st annoying.  disable with (setf bbdb-mua-auto-update-p nil)
      bbdb-update-records-p 'query
      ;; bbdb-ignore-message-alist
      ;;    '(("From" . "bugzilla-daemon"))
      )
(add-hook 'message-setup-hook 'bbdb-mail-aliases)
;; [2016-02-05 Fri 13:15] this is a try...
(add-hook 'bbdb-after-change-hook (lambda (_arg) (bbdb-save)))
;; Source [[gnus:nntp+news.gmane.org:gmane.emacs.bbdb.user#m28u2z8m57.fsf@charm-ecran.irisa.fr][Email from Alan Schmitt: Re: can I auto save the bbdb f]]
;; ...[2016-02-05 Fri 13:15]

(setq bbdb-snarf-rule-default 'eu)
;; bbdb:1 ends here

;; gopher.el
;; :PROPERTIES:
;; :ID:       97a3d175-43b5-457c-8282-55d36399de25
;; :END:

;; - browse the gopher net.
;; - possible start: gopher.club


;; [[file:init.org::*gopher.el][gopher.el:1]]
(push (expand-file-name "~/s/elisp/external/gopher.el/") load-path)
(require 'gopher)
;; gopher.el:1 ends here

;; forth


;; [[file:init.org::*forth][forth:1]]
(straight-use-package 'forth-mode)
;; forth:1 ends here

;; describe function shortcut for first ido candidate


;; [[file:init.org::*describe function shortcut for first ido candidate][describe function shortcut for first ido candidate:1]]
(defun mw-describe-first-ido-match ()
  (interactive)
  (if-let (match (car ido-matches))
      (describe-function (intern match))))
;; describe function shortcut for first ido candidate:1 ends here



;; bind to a globaly.


;; [[file:init.org::*describe function shortcut for first ido candidate][describe function shortcut for first ido candidate:2]]
(global-set-key (kbd "C-?") #'mw-describe-first-ido-match)
;; describe function shortcut for first ido candidate:2 ends here

;; make ! functional on non-file lines in dired :dired:
;; :PROPERTIES:
;; :ID:       77670ca6-b54b-4f0d-ac2b-12985c999e7b
;; :END:


;; [[file:init.org::*make ! functional on non-file lines in dired][make ! functional on non-file lines in dired:1]]
(add-hook
 'dired-mode-hook
 (lambda ()
   (define-key dired-mode-map "!"
     (lambda ()
       (interactive)
       (call-interactively
        (if (dired-get-filename nil t)
            (progn
              (require 'dired-aux)
              #'dired-do-shell-command)
          (require 'dired-x)
          #'dired-smart-shell-command))))))
;; make ! functional on non-file lines in dired:1 ends here

;; install org's C-a, C-e, C-k for visual-line-mode
;; :PROPERTIES:
;; :ID:       0d95826e-9d9e-49da-9968-99108f383dab
;; :END:


;; [[file:init.org::*install org's C-a, C-e, C-k for visual-line-mode][install org's C-a, C-e, C-k for visual-line-mode:1]]
(add-hook 'visual-line-mode-hook
          (lambda () (when (derived-mode-p 'org-mode)
                       (local-set-key (kbd "C-a") #'org-beginning-of-line)
                       (local-set-key (kbd "C-e") #'org-end-of-line)
                       (local-set-key (kbd "C-k") #'org-kill-line))))
;; install org's C-a, C-e, C-k for visual-line-mode:1 ends here

;; display html links in a different color :org:
;; :PROPERTIES:
;; :ID:       b5e60516-7a2e-4974-93d9-17011cc6ea74
;; :END:


;; [[file:init.org::*display html links in a different color][display html links in a different color:1]]
(org-link-set-parameters "http" :face '(:foreground "orange"))
(org-link-set-parameters "https" :face '(:foreground "orange"))
;; display html links in a different color:1 ends here

;; get the data of "sensors" into a buffer
;; :PROPERTIES:
;; :ID:       9b916825-24b5-42fa-bc88-91d035efd696
;; :END:

;; this is very specific to my machine.


;; [[file:init.org::*get the data of "sensors" into a buffer][get the data of "sensors" into a buffer:1]]
(defun mw-sensors ()
  (let ((buf (get-buffer-create "*sensors")))
    (set-buffer buf)
    (erase-buffer)
    (shell-command "sensors" buf)
    (goto-char (point-min))
    (forward-line 2)
    (delete-region (point-min) (point))
    (save-match-data
      (search-forward-regexp "^acpitz"))
    (beginning-of-line 0)
    (let ((point (point)))
      (forward-line 3)
      (delete-region point (point)))))
;; get the data of "sensors" into a buffer:1 ends here

;; refresh the buffer repeatedly
;; :PROPERTIES:
;; :ID:       ced7531d-ef3f-4ae0-8b36-23966b9932d9
;; :END:


;; [[file:init.org::*refresh the buffer repeatedly][refresh the buffer repeatedly:1]]
(defvar mw-sensors-timer nil
  "The sensors timer for mw-sensors-start and mw-sensors-start.")

(defun mw-sensors-stop ()
  (interactive)
  (cancel-timer mw-sensors-timer)
  (setq mw-sensors-timer nil))

(defun mw-sensors-start ()
  (interactive)
  (when mw-sensors-timer
    (cancel-timer mw-sensors-timer))
  (setq mw-sensors-timer (run-at-time 0 10 #'mw-sensors))
  (switch-to-buffer "*sensors"))
;; refresh the buffer repeatedly:1 ends here

;; reverse subrecords
;; :PROPERTIES:
;; :ID:       b52657bb-7fce-4eff-916f-0bca4fb06171
;; :END:
;; :doublelinks:
;; dependee [[id:44082f1d-2c64-4f47-9e74-c7d41c63cd95][reverse orgees]].
;; :end:


;; [[file:init.org::*reverse subrecords][reverse subrecords:1]]
(defun mw-decorate-numbering (nextrecfun)
  (let ((deco 0))
    (add-text-properties (point) (1+ (point)) `(record-number ,(incf deco)))
    (while (< (progn (forward-char) (funcall nextrecfun) (point)) (point-max))
      (add-text-properties (point) (1+ (point)) `(record-number ,(incf deco))))))

(defun mw-undecorate-numbering (nextrecfun)
  (remove-text-properties (point) (1+ (point)) '(record-number))
  (while (< (progn (forward-char) (funcall nextrecfun) (point)) (point-max))
    (remove-text-properties (point) (1+ (point)) '(record-number))))

(defun mw-reverse-subr (nextrecfun endrecfun)
  (let ((start-point (point)))
    (save-excursion
           (mw-decorate-numbering nextrecfun))
   (save-excursion
     (sort-subr t nextrecfun endrecfun
                (lambda () (get-text-property (point) 'record-number))))
   (goto-char start-point)
   (save-excursion
     (mw-undecorate-numbering nextrecfun))))
;; reverse subrecords:1 ends here

;; reverse orgees
;; :PROPERTIES:
;; :ID:       44082f1d-2c64-4f47-9e74-c7d41c63cd95
;; :END:
;; :doublelinks:
;; depends on [[id:b52657bb-7fce-4eff-916f-0bca4fb06171][reverse subrecords]].
;; :end:


;; [[file:init.org::*reverse orgees][reverse orgees:1]]
;;#deprecated
(defun mw-org-on-heading-or-next-heading nil
  (if (re-search-forward
       (concat "^"
               (regexp-quote (make-string (org-current-level) ?*))
               " +")
       nil t)
      (goto-char (match-beginning 0))
    (goto-char (point-max))))

;;#deprecated
(defun mw-org-next-heading nil
  (save-match-data
      (condition-case nil
          (outline-forward-same-level 1)
        (error
         (goto-char (point-max))))))

(defun mw-org-next-rec ()
    (if (re-search-forward
       (concat "^"
               (regexp-quote (make-string (org-current-level) ?*))
               " +")
       nil t)
      (goto-char (match-beginning 0))
      (goto-char (point-max))))

(defun mw-org-end-rec ()
    (save-match-data
      (condition-case nil
          (outline-forward-same-level 1)
        (error
         (goto-char (point-max))))))

(defun mw-org-reverse-entries ()
  (cl-assert (and (bolp) (org-at-heading-p)))
  (mw-reverse-subr
   #'mw-org-next-rec
   #'mw-org-end-rec))

(defun mw-org-reverse-siblings ()
  (interactive)
  (save-excursion
    (org-up-heading-or-point-min)
    (if-let ((pos-next-heading (save-excursion (outline-next-heading))))
      (let ((pos-bottom-heading (save-excursion (org-end-of-subtree t t))))
        (progn
          (save-restriction
            (narrow-to-region pos-next-heading pos-bottom-heading)
            (goto-char (point-min))
            (mw-org-reverse-entries)))))))
;; reverse orgees:1 ends here

;; implementation variant to reverse orgees
;; :PROPERTIES:
;; :ID:       e4ef0691-4ffa-4d1b-a381-63a6c3e2895e
;; :END:


;; [[file:init.org::*implementation variant to reverse orgees][implementation variant to reverse orgees:1]]
(defun mw-org-reverse-siblings-2 ()
  (interactive)
  (cl-block 'block
    (save-excursion
     (goto-char 1)
     (unless (outline-next-heading) ; no heading, no action.
       (return)))
    ;; at the top level assert a newline at the very end.
    (let ((level (or (plist-get (cadr (org-element-at-point)) :level) 1)))
      (when (= 1 level)
        (save-excursion
          (save-restriction
            (widen)
            (goto-char (point-max))
            (if (/= ?\n (preceding-char))
                (insert ?\n))))))
   (let ((scan-point-start (point)))
     (org-forward-heading-same-level nil t)
     (while (/= scan-point-start (point))
       (setq scan-point-start (point))
       (org-forward-heading-same-level nil t))
     ;; comment-assert: point at beginning of the last sibling.
     (let ((temp-buffer (generate-new-buffer "*tmp-for-reversing"))
           (end-of-last-sibling))
       (org-mark-subtree)
       (append-to-buffer temp-buffer (point) (mark))
       (setq end-of-last-sibling (mark)) ; for the later deletion of the siblings.
       (org-forward-heading-same-level -1 t)
       (while (< (point) scan-point-start)
         (setq scan-point-start (point))
         (org-mark-subtree)
         (append-to-buffer temp-buffer (point) (mark))
         (org-forward-heading-same-level -1 t))
       (let ((beg-pos (point)))
         (delete-region beg-pos end-of-last-sibling)
         (insert-buffer-substring temp-buffer)
         (goto-char beg-pos))
       (kill-buffer temp-buffer)))))
;; implementation variant to reverse orgees:1 ends here

;; has children :org:
;; :PROPERTIES:
;; :ID:       fedf32c1-924f-4da0-821e-8e09821f34a0
;; :END:


;; [[file:init.org::*has children][has children:1]]
(defun mw-org-has-children-p ()
  "Answer if the subtree has children."
  (save-excursion
    (unless (org-at-heading-p)
      (outline-previous-heading))
    (let ((parent-level (or (plist-get (cadr (org-element-at-point)) :level) 0)))
      (outline-next-heading)
      (and (not (eobp))
           (< parent-level (plist-get (cadr (org-element-at-point)) :level))))))
;; has children:1 ends here

;; reverse children
;; :PROPERTIES:
;; :ID:       11028765-baea-4c25-8735-523e301b0f31
;; :END:


;; [[file:init.org::*reverse children][reverse children:1]]
(defun mw-org-reverse-children ()
  "Reverse children.
Leave point onto first child if possible."
  (interactive)
  (unless (org-at-heading-p)
    (outline-previous-heading))
  (when (mw-org-has-children-p)
    (outline-next-heading)
    (let ((scan-point-start (point)))
      (org-forward-heading-same-level nil t)
      (while (/= scan-point-start (point))
        (setq scan-point-start (point))
        (org-forward-heading-same-level nil t))
      ;; comment-assert: point at beginning of the last sibling.
      (let ((temp-buffer (generate-new-buffer "*tmp-for-reversing-org-subtrees"))
            end-of-last-sibling)
        (org-mark-subtree)
        (append-to-buffer temp-buffer (point) (mark))
        (setq end-of-last-sibling (mark)) ; for the later deletion of the siblings.
        (org-forward-heading-same-level -1 t)
        (while (< (point) scan-point-start)
          (append-to-buffer temp-buffer (point) scan-point-start)
          (setq scan-point-start (point))
          (org-forward-heading-same-level -1 t))
        (let ((beg-pos (point)))
          (delete-region beg-pos end-of-last-sibling)
          (insert-buffer-substring temp-buffer)
          (kill-buffer temp-buffer)
          (goto-char beg-pos))))))
;; reverse children:1 ends here

;; mark magit section
;; :PROPERTIES:
;; :ID:       da5f429f-80cb-4236-bc62-483c9a661572
;; :END:


;; [[file:init.org::*mark magit section][mark magit section:1]]
(defun mw-magit-mark-section ()
  "Mark the current magit section."
  (interactive)
  (require 'magit)
  (let ((section (magit-current-section)))
    (if (eq section magit-root-section)
        (mark-whole-buffer)
      (when-let ((beg (oref section content)))
        (when-let ((end (oref section end)))
          (goto-char beg)
          (push-mark end))))))
;; mark magit section:1 ends here

;; narrow to magit section
;; :PROPERTIES:
;; :ID:       c2519135-d44a-4e83-954b-0dffc75b2f40
;; :END:


;; [[file:init.org::*narrow to magit section][narrow to magit section:1]]
(defun mw-magit-narrow-to-section ()
  "Narrow to current magit section."
  (interactive)
  (require 'magit)
  (let ((section (magit-current-section)))
    (when-let ((beg (oref section content)))
      (when-let ((end (oref section end)))
        (narrow-to-region beg end)))))
;; narrow to magit section:1 ends here

;; flymake for progmodes
;; :PROPERTIES:
;; :ID:       8d8d137c-4afe-4ddd-9ccb-1739b612a68e
;; :END:


;; [[file:init.org::*flymake for progmodes][flymake for progmodes:1]]
(add-hook 'prog-mode-hook #'flymake-mode)
;; flymake for progmodes:1 ends here

;; save agenda with timestamp :orga:
;; :PROPERTIES:
;; :ID:       df06436f-aef5-4bd1-8079-847f9940ab12
;; :END:

;; :doublelinks:
;; [[id:504b2d33-9758-4b9b-b1da-6aca8e2dbb8f][save agenda with timestamp]]
;; :end:


;; [[file:init.org::*save agenda with timestamp][save agenda with timestamp:1]]
(defun mw-org-agenda-write ()
  "Persist this agenda view to file with a timestamp."
  (interactive)
  (org-agenda-write
   (expand-file-name
    (concat mw-personal-org-files-dir
	    "/log/"
	    "orgagenda-"
	    (format-time-string "%Y%m%d%H%M%S.txt")))))
;; save agenda with timestamp:1 ends here

;; [[file:init.org::*save agenda with timestamp][save agenda with timestamp:2]]
(defun mw-org-agenda-create-and-write ()
  (interactive)
  (mw-kbd-macro-call-agenda-big)
  (mw-org-agenda-write))
;; save agenda with timestamp:2 ends here

;; prepend/append to kill
;; :PROPERTIES:
;; :ID:       509aca9c-dd83-4616-8b38-3d958e50bcde
;; :END:


;; [[file:init.org::*prepend/append to kill][prepend/append to kill:1]]
(defun mw-append-to-kill (text)
  (interactive "M")
  (with-temp-buffer
    (insert text)
    (append-next-kill)
    (kill-region (point-min) (point-max))))

(defun mw-prepend-to-kill (text)
  (interactive "M")
  (with-temp-buffer
    (insert text)
    (append-next-kill)
    (kill-region (point-max) (point-min))))
;; prepend/append to kill:1 ends here

;; QR code for region
;; :PROPERTIES:
;; :ID:       d25bf398-e1ef-4b61-b682-a4be15938863
;; :END:

;; - from [[https://www.emacswiki.org/emacs/QR_Code][EmacsWiki: QR Code]]

;; You can use QR codes to transfer strings such as passwords to your phone using the camera. The code below uses the qrencode command-line tool.


;; [[file:init.org::*QR code for region][QR code for region:1]]
(defun qr-code-region (start end &optional dotsize)
  "Show a QR code of the region.
DOTSIZE is an integer related to the size of the output."
  (interactive "r")
  (let ((dotsize (or dotsize 3))
        (buf (get-buffer-create "*QR*"))
        (inhibit-read-only t))
    (with-current-buffer buf
      (erase-buffer))
    (let ((coding-system-for-read 'raw-text))
      (shell-command-on-region
       start end
       (format "qrencode -s %d -o -" dotsize) buf))
    (switch-to-buffer buf)
    (image-mode)))
;; QR code for region:1 ends here

;; only-window
;; :PROPERTIES:
;; :ID:       7cd6e5f3-a9d7-45ae-9f96-3b7b79ae7c63
;; :END:


;; [[file:init.org::*only-window][only-window:1]]
(defalias 'only-window 'delete-other-windows)
;; only-window:1 ends here

;; find buffer at point
;; :PROPERTIES:
;; :ID:       c8314330-a768-4770-8f6a-a038b33c7c8d
;; :END:


;; [[file:init.org::*find buffer at point][find buffer at point:1]]
(defun mw-fbap ()
  (interactive)
  (if-let* ((name (thing-at-point 'filename))
            (buf (get-buffer (file-name-nondirectory name))))
      (switch-to-buffer buf)
    (ido-switch-buffer)))
;; find buffer at point:1 ends here

;; [[file:init.org::*find buffer at point][find buffer at point:2]]
(global-set-key (kbd "C-x b") #'mw-fbap)
;; find buffer at point:2 ends here

;; surf with surf
;; :PROPERTIES:
;; :ID:       fc0b8d87-c13f-46cb-85df-d1500d667f90
;; :END:


;; [[file:init.org::*surf with surf][surf with surf:1]]
(defun browse-url-surf (url &optional new-window)
  "Ask the surf WWW browser to load URL.
Defaults to the URL around or before point."
  (interactive (browse-url-interactive-arg "URL: "))
  (setq url (browse-url-encode-url url))
  (let* ((process-environment (browse-url-process-environment))
         (browse-url-surf-program "surf"))
    (start-process
     (concat "surf " url)
     nil
     browse-url-surf-program
     url)))
;; surf with surf:1 ends here

;; date in a list of dates
;; :PROPERTIES:
;; :ID:       01b3b208-8740-4b3c-9a50-1d248ff7ca6a
;; :END:


;; [[file:init.org::*date in a list of dates][date in a list of dates:1]]
(ert-deftest test/mw-date-in-date-list ()
  (should (mw-date-in-date-list '(2019 12 20) '((2019 12 20)))))

(defun mw-date-in-date-list (thedate date-list)
  "Return if THEDATE is in DATE-LIST.
THEDATE is a list like (y m d).
DATE-LIST is a list of (y m d) lists."
  (cl-find thedate date-list
           :test (lambda (o1 o2)
                   (reduce
                    (lambda (x y) (and x y))
                    (mapcar (lambda (n)
                              (= (nth n o1) (nth n o2)))
                            '(0 1 2))))))
;; date in a list of dates:1 ends here

;; [[file:init.org::*date in a list of dates][date in a list of dates:2]]
(defun mw-diary-date (date-list)
  "Return if \"date\" is in DATE-LIST.
DATE-LIST is a list of (y m d) lists.

This function can e.g. be used in diary sexps to exclude some
dates.  E.g. exclude 2019 Dec 25 from the emacs meetups like so
in the diary file

\%%(and (not (mw-diary-date '((2019 12 25)))) (diary-float t 3 -1 nil \"ε\")) 19:00 Emacs Berlin"
  (with-no-warnings (defvar date))
  (let ((y (calendar-extract-year date))
        (m (calendar-extract-month date))
        (d (calendar-extract-day date)))
    (mw-date-in-date-list (list y m d) date-list)))
;; date in a list of dates:2 ends here

;; date before some date
;; :PROPERTIES:
;; :ID:       2bdaa22b-772f-46b7-8345-fa774d892ec6
;; :END:


;; [[file:init.org::*date before some date][date before some date:1]]
(defun mw-diary-date-before (y m)
  "Return if \"date\" is before Y (year) M (month)."
  (with-no-warnings (defvar date))
  (let ((ydate (calendar-extract-year date))
        (mdate (calendar-extract-month date)))
    (or (< ydate y)
        (and (= ydate y)
             (< mdate m)))))
;; date before some date:1 ends here

;; date comparison function
;; :PROPERTIES:
;; :ID:       44dd6c12-4c60-4b1f-8198-15bbf95b48e0
;; :END:

;; this is an auxilliary here.


;; [[file:init.org::*date comparison function][date comparison function:1]]
(defun date< (t1 t2)
  "Is T1 < T2 regarded as date?
 e.g. (2000 12 30) < (2000 12 31)."
  (cond
   ((not (and t1 t2)) nil)
   ((< (nth 0 t1) (nth 0 t2)))
   ((= (nth 0 t1) (nth 0 t2))
    (date< (cdr t1) (cdr t2)))))
;; date comparison function:1 ends here

;; date in a span
;; :PROPERTIES:
;; :ID:       40a1efd8-7973-4277-9420-d09f84b07dbf
;; :END:


;; [[file:init.org::*date in a span][date in a span:1]]
(defun mw-diary-date-in-span-intern-v1 (date-beg date-end thedate)
  "Return if THEDATE is in between and including DATE-BEG and DATE-END.
The parameters are expected as lists like (y m d)."
  (cl-flet
      ((date-to-chrono (d)
                       "Build a unique number from y m d in chronological order."
                       (+ (nth 2 d)
                          (* 100 (nth 1 d))
                          (* 10000 (nth 0 d)))))
    (<= (date-to-chrono date-beg)
        (date-to-chrono thedate)
        (date-to-chrono date-end))))
;; date in a span:1 ends here

;; [[file:init.org::*date in a span][date in a span:2]]
(defun mw-diary-date-in-span-intern-v2 (date-beg date-end thedate)
  "Return if \"date\" is in intervall (inclusive) DATE-BEG DATE-END."
  (or (equal date-beg thedate)
      (and (date< date-beg thedate)
           (date< thedate date-end))
      (equal date-end thedate)))
;; date in a span:2 ends here

;; [[file:init.org::*date in a span][date in a span:3]]
(defun mw-diary-date-in-span (date-beg date-end)
  "Return if \"date\" is in between DATE-BEG and DATE-END.
The parameters are expected as lists like (y m d)."
  (with-no-warnings (defvar date))
  (let ((y (calendar-extract-year date))
        (m (calendar-extract-month date))
        (d (calendar-extract-day date)))
    (mw-diary-date-in-span-intern-v1 date-beg date-end (list y m d))))
;; date in a span:3 ends here

;; goto outermost list
;; :PROPERTIES:
;; :ID:       74959d37-eaae-4fef-8076-96ca712ce794
;; :END:


;; [[file:init.org::*goto outermost list][goto outermost list:1]]
(defun mw-backward-up-list-as-far-as-possible ()
  "Place point on the outermost list at its beginning.
Return point."
  (interactive)
  (let ((pos (point))
        moved)
    (condition-case nil
             (progn
               (while t
                 (setq pos (point))
                 ;; (backward-up-list 1) ; not too bad. fails starting from within strings.
                 (paredit-backward-up)
                 (forward-sexp)
                 (setq moved 'moved)))
      (error (progn
               (goto-char pos)
               (when moved
                 (backward-sexp))
               (point))))))

(defun mw-up-list-as-far-as-possible ()
  "Place point on the outermost list at its end."
  (interactive)
  (mw-backward-up-list-as-far-as-possible)
  (forward-sexp)
  (point))

(defun mw-eval-outermost-sexp (arg)
  "Evaluate the outermost sexp starting from point.
Point is moved to the end aof the outermost s-expresion."
  (interactive "P")
  (let ((beg (save-excursion (mw-backward-up-list-as-far-as-possible)))
        (end (save-excursion (mw-up-list-as-far-as-possible))))
    (and beg end
         ;; what about (call-interactively #'eval-last-sexp) here?
         (eval-region beg end (when arg t)))))
;; goto outermost list:1 ends here

;; [[file:init.org::*goto outermost list][goto outermost list:2]]
(defun mw-mark-outermost-sexp ()
  "Make the outermost sexp starting from point a region."
  (interactive)
  (let ((beg (save-excursion (mw-backward-up-list-as-far-as-possible)))
        (end (save-excursion (mw-up-list-as-far-as-possible))))
    (goto-char beg)
    (push-mark end t t)))
;; goto outermost list:2 ends here

;; fold item list :experimental:
;; :PROPERTIES:
;; :ID:       f7a90364-6e57-49bf-b092-98bc6c642f3c
;; :END:


;; [[file:init.org::*fold item list][fold item list:1]]
(add-hook
 'org-mode-hook
 (lambda ()
   (key-chord-define-local
    "f3"
    (lambda () (interactive)
      (org-list--fold-item-list)))))
;; fold item list:1 ends here

;; shortcut to open customized agenda
;; :PROPERTIES:
;; :ID:       6b5a50c0-65ed-4a2d-a3a4-60f9da96e2a4
;; :END:


;; [[file:init.org::*shortcut to open customized agenda][shortcut to open customized agenda:1]]
(fset 'mw-kbd-macro-call-agenda-big
   (kmacro-lambda-form [?\C-c ?a ?A] 0 "%d"))
;; shortcut to open customized agenda:1 ends here

;; [[file:init.org::*shortcut to open customized agenda][shortcut to open customized agenda:2]]
(fset 'mw-kbd-macro-call-agenda-n
   (kmacro-lambda-form [?\C-c ?a ?n] 0 "%d"))
;; shortcut to open customized agenda:2 ends here

;; [[file:init.org::*shortcut to open customized agenda][shortcut to open customized agenda:3]]
(fset 'mw-kbd-macro-call-agenda-w
   (kmacro-lambda-form [?\C-c ?a ?w] 0 "%d"))
;; shortcut to open customized agenda:3 ends here

;; dynamic agenda filtering :experimental:org:
;; :PROPERTIES:
;; :ID:       94657498-7005-4f72-832e-76ec95184095
;; :END:

;; Dynamic regexp agenda filtering.  i.e. type and watch the matches change.

;; This is in experimental stage.

;; Base for this has been org-velocity.


;; [[file:init.org::*dynamic agenda filtering][dynamic agenda filtering:1]]
(defun org-agenda-dynamic-filter-minibuffer-contents ()
  "Return the contents of the minibuffer when it is active."
  (when (active-minibuffer-window)
    (with-current-buffer (window-buffer (active-minibuffer-window))
      (minibuffer-contents))))

(defun org-agenda-dynamic-filter-update-regexp ()
  (with-current-buffer "*Org Agenda*"
    (org-agenda-remove-filter 'regexp))
  (setq org-agenda-regexp-filter
        (if (string= "" (org-agenda-dynamic-filter-minibuffer-contents))
            nil
          (list (concat "+" (org-agenda-dynamic-filter-minibuffer-contents)))))
  (with-current-buffer "*Org Agenda*"
    (cl-flet ((recenter (&optional arg redisplay) nil))
      (org-agenda-finalize))))

(defun org-agenda-dynamic-filter-regexp-read ()
  "Read string with PROMPT and display results dynamically.
See also `org-agenda-filter-by-regexp'."
  (interactive)
  (unwind-protect
      (catch 'click
        (add-hook 'post-command-hook #'org-agenda-dynamic-filter-update-regexp)
        (read-string "Regexp: "))
    (remove-hook 'post-command-hook #'org-agenda-dynamic-filter-update-regexp)))

(org-defkey org-agenda-mode-map "&" #'org-agenda-dynamic-filter-regexp-read)
;; dynamic agenda filtering:1 ends here

;; cycle delimiters
;; :PROPERTIES:
;; :ID:       4697a0fe-cb71-4026-8b5e-0bbf0850ca29
;; :END:


;; [[file:init.org::*cycle delimiters][cycle delimiters:1]]
(require 'key-chord)
(key-chord-define-global "()" #'mw-cycle-delimiters)
;; cycle delimiters:1 ends here

;; [[file:init.org::*cycle delimiters][cycle delimiters:2]]
(ert-deftest test--mw-cdr-starting-with ()
  (should (equal (mw-cdr-starting-with 1 '(1 2)) '(1 2)))
  (should (equal (mw-cdr-starting-with 1 nil) nil))
  (should (equal (mw-cdr-starting-with 1 '(2 12 a 1 2)) '(1 2)))
  (should (equal (mw-cdr-starting-with 1 '(2 12 a 1 2 3 11 1 1)) '(1 2 3 11 1 1)))
  (should (equal (mw-cdr-starting-with 41 '(2 12 a 1 2 3 11 1 1)) nil)))

(defun mw-cdr-starting-with (elem sequence)
  "Return the cdr of SEQUENCE starting with ELEM or nil."
  (when sequence
    (if (equal (car sequence) elem)
        sequence
      (mw-cdr-starting-with elem (cdr sequence)))))

(defun mw-cycle-delimiters ()
  (interactive)
  (let* ((delimiter-pairs '((?\( . ?\)) (?\[ . ?\]) (?\{ . ?\})))
         (delimiters (append (mapcar (lambda (x) (car x)) delimiter-pairs)
                              `(,(caar delimiter-pairs))))
         (following-char (following-char))
         (new-opener (cadr (mw-cdr-starting-with following-char delimiters)))
         (new-closer (cdr (assoc new-opener delimiter-pairs))))
    (unless (member following-char delimiters)
      (user-error "Can only act on known delimiters: %s" delimiters))
    (save-excursion
      (mark-sexp)
      (let ((beg (region-beginning))
            (end (region-end)))
        (goto-char end)
        (delete-char -1)
        (insert new-closer)
        (goto-char beg)
        (delete-char 1)
        (insert new-opener)))))
;; cycle delimiters:2 ends here

;; transpose words keeping relative cursor position
;; :PROPERTIES:
;; :ID:       f3975521-0a5d-4fbe-9c41-1f6f7c770e53
;; :END:

;; - [2019-11-28 Thu] started with
;;   https://emacs.stackexchange.com/questions/54046/how-to-transpose-words-keeping-the-cursor-at-the-same-relative-location-to-the/54047#54047


;; [[file:init.org::*transpose words keeping relative cursor position][transpose words keeping relative cursor position:1]]
(defun mw-transpose-words (arg)
  "Transpose words keeping the cursor position in the word."
  (interactive "*p")
  (let ((chars-to-word-end
         (let ((point (point)))
           (if (save-match-data
                 (or      ; cases to behave like plain transpose-words.
                  (looking-at "\\>")
                  (looking-at "\\(?:[ \t]*\n\\)*[ \t]*\\<") ))
               0
             (save-excursion
               (forward-word)
               (- (point) point))))))
    (transpose-words arg)
    (backward-char chars-to-word-end)))
;; transpose words keeping relative cursor position:1 ends here

;; follow global org link in other frame :org:
;; :PROPERTIES:
;; :ID:       4811fb50-b761-4122-af6a-5b2d985489da
;; :END:


;; [[file:init.org::*follow global org link in other frame][follow global org link in other frame:1]]
(defun org-open-at-point-global-other-frame ()
  (interactive)
  (select-frame (make-frame-command))
  (raise-frame)
  (condition-case nil
      (org-open-at-point-global)
    (user-error (delete-frame))))
;; follow global org link in other frame:1 ends here

;; [[file:init.org::*follow global org link in other frame][follow global org link in other frame:2]]
(global-set-key (kbd "C-x 5 C-c o") #'org-open-at-point-global-other-frame)
;; follow global org link in other frame:2 ends here

;; line numbers
;; :PROPERTIES:
;; :ID:       239285fc-7b8f-40cb-9bd3-4865793a2b93
;; :END:

;; I guess display line numbers is the fastest of the line number display in emacs.

;; I like the setting "visual" which shows the visual line numbers
;; opposed to the absolute line numbers, in particular when regions are
;; hidden, e.g. some folded trees.


;; [[file:init.org::*line numbers][line numbers:1]]
(setq display-line-numbers 'visual)
;; line numbers:1 ends here

;; set foreground to background for some time
;; :PROPERTIES:
;; :ID:       9e464b42-679d-4463-80aa-c7e0b955d687
;; :END:


;; [[file:init.org::*set foreground to background for some time][set foreground to background for some time:1]]
(defun mw-set-foreground-color-in-all-frames (color-name)
"Set foreground color to COLOR-NAME in all frames."
(let ((selected-frame-at-entry (selected-frame)))
  (set-foreground-color color-name)
  (while (not (equal selected-frame-at-entry (select-frame (next-frame (selected-frame) t))))
    (set-foreground-color color-name))))
;; set foreground to background for some time:1 ends here

;; [[file:init.org::*set foreground to background for some time][set foreground to background for some time:2]]
(defun mw-set-foreground-same-as-background-for-some-time ()
  "Set foreground same as background in all frames for some time."
  (interactive)
  (let
      ((fg (substring-no-properties (cdr (assoc 'foreground-color (frame-parameters)))))
       (bg (substring-no-properties (cdr (assoc 'background-color (frame-parameters))))))
    (mw-set-foreground-color-in-all-frames bg)
    (run-at-time "23 sec" nil (lambda () (mw-set-foreground-color-in-all-frames fg)))))
;; set foreground to background for some time:2 ends here

;; display tree as top level :org:
;; :PROPERTIES:
;; :ID:       95f1392f-3f00-473e-848b-cad75a222484
;; :END:


;; [[file:init.org::*display tree as top level][display tree as top level:1]]
(defvar *mw-keep-a-reference*
  (let (overlays it-s-on)

    (defun mw-org-decorate-as-top-level-establish ()
      (interactive)
      (let (first-level)
        (org-map-entries
         (lambda ()
           (let ((level (org-outline-level)))
             (unless first-level
               (setf first-level level))
             (push (make-overlay (point) (1+ (point)) ) overlays)
             (overlay-put (car overlays) 'display "^")
             (push (make-overlay (1+ (point)) (+ (point) first-level)) overlays)
             (overlay-put (car overlays) 'invisible t)))
         t 'tree)))

    (defun mw-org-decorate-as-top-level-unravel ()
      (interactive)
      (dolist (ol overlays)
        (delete-overlay ol))
      (setf overlays nil))

    (list #'mw-org-decorate-as-top-level-establish
            #'mw-org-decorate-as-top-level-unravel))

  "To prevent garbage collection for some items.")
;; display tree as top level:1 ends here

;; selective display more intuitive
;; :PROPERTIES:
;; :ID:       284fe490-136a-459b-8338-6cb97235f014
;; :END:


;; [[file:init.org::*selective display more intuitive][selective display more intuitive:1]]
(defun mw-set-selective-display (arg)
  "Set selective display according point.  Unset with prefix argument."
  (interactive "P")
  (set-selective-display
   (unless arg
     (1+ (current-column)))))

(global-set-key (kbd "C-x $") #'mw-set-selective-display)
;; selective display more intuitive:1 ends here

;; follow in other window
;; :PROPERTIES:
;; :ID:       bf53e56d-b10f-42cc-98e7-44ab3f74c016
;; :END:

;; use ==C-c 4== as entry to "follow in other window" functionality.


;; [[file:init.org::*follow in other window][follow in other window:1]]
(defvar mw-org-other-window-map
  (let ((map (make-sparse-keymap)))
     (define-key map (kbd "b")
       (lambda () (interactive)
         (org-switch-to-buffer-other-window (completing-read "Org buffer: "
		      (mapcar #'list (mapcar #'buffer-name (org-buffer-list)))
		      nil t))))
     (define-key map (kbd "RET") #'mw-org-open-at-point-other-window)
     (define-key map (kbd "o") #'mw-org-open-at-point-other-window)
     (define-key map (kbd "C-c C-o") #'mw-org-open-at-point-other-window)
     (define-key map (kbd "C-c o") #'mw-org-open-at-point-global-other-window)
     map)
  "Keymap to switch to org thing in other buffer.")

(global-set-key (kbd "C-c 4") mw-org-other-window-map)
;; follow in other window:1 ends here

;; follow org link global and show in new window
;; :PROPERTIES:
;; :ID:       162fc09e-bf57-446a-898f-2ef5e910e199
;; :END:


;; [[file:init.org::*follow org link global and show in new window][follow org link global and show in new window:1]]
(defun mw-org-open-at-point-global-other-window ()
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (select-window (next-window))
  (org-open-at-point-global))
;; follow org link global and show in new window:1 ends here

;; follow org link and show in new window
;; :PROPERTIES:
;; :ID:       67e3e310-f28c-455a-b702-8ffb4ed03ede
;; :END:

;; <2019-10-15 Tue>


;; [[file:init.org::*follow org link and show in new window][follow org link and show in new window:1]]
(defun mw-org-open-at-point-other-window ()
  (interactive)
  (delete-other-windows)
  (split-window-horizontally)
  (select-window (next-window))
  (org-open-at-point))
;; follow org link and show in new window:1 ends here

;; follow in other frame


;; [[file:init.org::*follow in other frame][follow in other frame:1]]
(defvar mw-org-other-frame-map
  (let ((map (make-sparse-keymap)))
     (define-key map (kbd "b")
       (lambda () (interactive)
         (switch-to-buffer-other-frame
                (completing-read
                 "Org buffer: "
		 (mapcar #'list (mapcar #'buffer-name (org-buffer-list)))
		 nil t))))
     map)
  "Map for org specific way to deal with frames.")
;; follow in other frame:1 ends here

;; [[file:init.org::*follow in other frame][follow in other frame:2]]
(global-set-key (kbd "C-c 5") mw-org-other-frame-map)
;; follow in other frame:2 ends here

;; region as string


;; [[file:init.org::*region as string][region as string:1]]
(defun mw-region-string ()
  (buffer-substring (region-beginning) (region-end)))
;; region as string:1 ends here

;; individual parameters
;; :PROPERTIES:
;; :ID:       c97535fb-2559-4520-a26e-b9e8ea6774ac
;; :END:


;; [[file:init.org::*individual parameters][individual parameters:1]]
(defcustom mw-text-eta-wpm 80
  "Estimation of read velocity in words per minute."
:type 'integer)

(defcustom mw-text-eta--very-big-threshold 200000
  "Number of chars to be considered a big rest of a buffer starting at point."
:type 'integer)
;; individual parameters:1 ends here

;; eta to the end of the buffer
;; :PROPERTIES:
;; :ID:       34dce1fe-40c4-408c-b356-7813c02ebc84
;; :END:


;; [[file:init.org::*eta to the end of the buffer][eta to the end of the buffer:1]]
(defun mw--text-eta ()
  "Text representing the eta to the bottom."
  (if (< (- (point-max) (point)) mw-text-eta--very-big-threshold)
      (format "eta: %.0f min" (/ (float (count-words-region (point) (point-max)))
                                  mw-text-eta-wpm))
    (format "The text looks big.  eta: beyond %.0f hours"
             (/ (float (count-words-region
                        (point)
                        (+ (point) mw-text-eta--very-big-threshold)))
                mw-text-eta-wpm 60))))

(defun mw-text-eta ()
  "Print message with estimated time of duration of read til point max."
  (interactive)
  (if (region-active-p)
      (mw-region-text-eta (region-beginning) (region-end))
    (message (mw--text-eta)))
  (sit-for 2))

(defun my-advice-to-display-text-eta (&optional _)
  (mw-text-eta))

;; (advice-add 'rope-read-next-paragraph :after #'my-advice-to-display-text-eta)
;; (advice-add 'scroll-up :after #'my-advice-to-display-text-eta)
;; p in rope-read
;; (advice-remove 'rope-read-next-paragraph #'my-advice-to-display-text-eta)
;; (advice-remove 'scroll-up #'my-advice-to-display-text-eta)
;; eta to the end of the buffer:1 ends here

;; eta for region
;; :PROPERTIES:
;; :ID:       cf266bd2-be07-4de7-a410-b544f885f420
;; :END:


;; [[file:init.org::*eta for region][eta for region:1]]
(defun mw-region-text-eta-in-minutes (beg end)
  "Return estimated time of arrival in minutes for reading the text in the region.
Return a cons cell with first argument 'big-text and the second
argument the calulated lower text eta bound for text bigger than
`mw-text-eta--very-big-threshold'."
  (interactive "r")
  (if (< (- end beg) mw-text-eta--very-big-threshold)
      (/ (float (count-words-region beg end))
         mw-text-eta-wpm)
    (cons 'big-text .
          (/ (float (count-words-region
                     beg (+ beg mw-text-eta--very-big-threshold)))
             mw-text-eta-wpm))))

(defun mw-region-text-eta (beg end)
  "Write a message about the estimated time for reading the text in the region."
  (interactive "r")
  (cl-assert (<= beg end))
  (let ((eta (mw-region-text-eta-in-minutes beg end)))
    (if (consp eta)
        (message "This text is big! Eta: at least %.1f min." (cdr eta))
      (message "Eta: %.1f min." eta))))
;; eta for region:1 ends here

;; switch off defun gnus
;; :PROPERTIES:
;; :ID:       04079c82-363e-4b56-8121-1808a6e40745
;; :END:

;; sometimes I don't want to be able to call gnus from a certain emacs.


;; [[file:init.org::*switch off defun gnus][switch off defun gnus:1]]
(defun mw-disable-gnus ()
  "Disable function gnus.
Reactivate by re-evaluate function gnus.  Why this fun?  To have
an Emacs without accidentially calling gnus and another for gnus."
  (interactive)
  (setf (symbol-function 'gnus)
        (lambda ()
          (interactive)
          (run-at-time .1 nil (lambda () (message "gnus has been disabled via fun mw-disable-gnus"))))))
;; switch off defun gnus:1 ends here

;; el-csv
;; :PROPERTIES:
;; :ID:       0e5d3853-5b4f-4f82-8423-f7d0ec36ca03
;; :END:

;; parse csv.


;; [[file:init.org::*el-csv][el-csv:1]]
(push "~/s/elisp/external/el-csv" load-path)
(require 'parse-csv)
;; el-csv:1 ends here

;; activation
;; :PROPERTIES:
;; :ID:       47e8c93d-d9f0-46ec-ab9a-5662a500bc04
;; :END:


;; [[file:init.org::*activation][activation:1]]
(global-set-key (kbd "C-x C-b") #'electric-buffer-list)
;; activation:1 ends here

;; electric buffer list tweaks
;; :PROPERTIES:
;; :ID:       b8cea47b-a591-4b3a-a602-20afc6c97c96
;; :END:

;; I want some functionality.


;; [[file:init.org::*electric buffer list tweaks][electric buffer list tweaks:1]]
(require 'ebuff-menu)
(define-key electric-buffer-menu-mode-map "o" #'delete-other-windows) ; same as in org agenda.
(define-key electric-buffer-menu-mode-map "D"
  (lambda () (interactive)
    (save-excursion
      (call-interactively #'Buffer-menu-delete))
    (previous-line)))
(define-key electric-buffer-menu-mode-map "b" #'Buffer-menu-bury)
(define-key electric-buffer-menu-mode-map "/" #'isearch-forward)
(define-key electric-buffer-menu-mode-map "x" #'Buffer-menu-execute)
(define-key electric-buffer-menu-mode-map (kbd "C-l") #'recenter-top-bottom)
(defvar mw-electric-buffer-C-x-map
  (let ((map (make-sparse-keymap)))
     (define-key map "(" #'kmacro-start-macro)
     (define-key map ")" #'kmacro-end-macro)
     (define-key map "e" #'kmacro-end-and-call-macro)
     map)
  "Keymap for electric buffer for C-x.")
(define-key electric-buffer-menu-mode-map (kbd "C-x") mw-electric-buffer-C-x-map)
;; electric buffer list tweaks:1 ends here

;; minute-timer

;; a simple timer with an interface like a kitchen timer.

;; possibly better use a hardware kitchen timer.  but if you don't have
;; one you can use this.

;; this is a simplification starting with podomoro.


;; [[file:init.org::*minute-timer][minute-timer:1]]
(push "~/s/elisp/mw/minute-timer/" load-path)
(require 'minute-timer)
;; minute-timer:1 ends here

;; convenient access
;; :PROPERTIES:
;; :ID:       fbb91a92-4a49-4db3-95ce-f2767d73dc25
;; :END:


;; [[file:init.org::*convenient access][convenient access:1]]
(defhydra hydra-minute-timer ()
  "Hydra access to minute-timer."
  ("-" minute-timer-show "display timer mode-line" :exit t)
  ("s" minute-timer-start "start the timer" :exit t)
  ("b" minute-timer-start "start the timer" :exit t)
  ("c" minute-timer-cancel "cancel the timer" :exit t))
;; convenient access:1 ends here

;; activate the program
;; :PROPERTIES:
;; :ID:       668aa7f2-37dc-45e1-92ef-b76847ce0096
;; :END:


;; [[file:init.org::*activate the program][activate the program:1]]
(push "~/s/elisp/mw/pomodoro.el/" load-path)
(require 'pomodoro)
;; activate the program:1 ends here

;; convenient access
;; :PROPERTIES:
;; :ID:       fbb91a92-4a49-4db3-95ce-f2767d73dc25
;; :END:


;; [[file:init.org::*convenient access][convenient access:1]]
(defhydra hydra-pomodoro (:body-pre (pomodoro-add-to-mode-line))
  "Hydra access to pomodoro."
  ("-" pomodoro-add-to-mode-line "see timer in mode-line" :exit t)
  ("b" pomodoro-start "begin pomodoro" :exit t)
  ("e" pomodoro-stop "end pomodoro" :exit t))
;; convenient access:1 ends here

;; [[file:init.org::*convenient access][convenient access:2]]
(setq pomodoro-hook '(mw-set-foreground-same-as-background-for-some-time))
;; convenient access:2 ends here

;; video recording
;; :PROPERTIES:
;; :ID:       3ed189ad-7d4f-48a3-ab59-81679a87a74c
;; :END:


;; [[file:init.org::*video recording][video recording:1]]
(defvar video-recordings-dir "/home/b/Videos/emacs/"
  "This is your video recording directory. Keep slash on the end")

(defvar video-recording-extension ".ogv"
  "This is your video file extension")

(defun record-screen ()
  "Records screencast. It is recommended to bind the function to
a key. Press key to start screen recording. Program
`recordmydesktop` is used but other screen recording command
could be used as well. You could modify the keybinding to stop
the recording. It is set to be <f5>. See below. Once you stop
recording the video is being prepared. Wait that process
finishes, then press `q` two times to remove the buffer and get
to the recorded file."
  (interactive)
  (let* ((filepath (concat video-recordings-dir (format-time-string "%Y/%m/%Y-%m-%d/")))
     (filename (concat filepath (format-time-string "%Y-%m-%d-%H:%M:%S") video-recording-extension))
         (command-1 (screen-record-command filename))
     (current-buffer (current-buffer))
     (keybinding-stop (kbd "<f5>"))
     (buffer "*Screen Recording*"))
    (make-directory filepath t)
    (switch-to-buffer buffer)
    (erase-buffer)
    (setq-local header-line-format "➜ Screen recording, use 'q' when process finishes to get the recorded file, use globally s-u to stop recording.")
    (let* ((process (start-process-shell-command buffer buffer command-1)))
      (message (prin1-to-string process))
      (local-set-key "q" `(lambda () (interactive) (signal-process ,process 'TERM)
                (local-set-key "q" (lambda () (interactive)
                         (kill-current-buffer)
                         (find-file ,filepath)
                         (revert-buffer)))))
      (switch-to-buffer current-buffer)
      (global-set-key keybinding-stop `(lambda () (interactive) (signal-process ,process 'TERM)
                     (switch-to-buffer ,buffer))))))

(defun screen-record-command (filename &optional device)
  "Record screen with the default device"
  (let* ((device (if device device "pulse"))
     (command
      (format (concat "recordmydesktop --pause-shortcut Alt-p"
                      " --stop-shortcut Alt-n --workdir '%s'"
                      " --no-frame --device %s"
                      ;; " -x 1 -y 1"
                      " --width 1680 --height 1050"
                     ;; " --width 840 --height 525"
                      " -o \"%s\"")
              temporary-file-directory device filename)))
    command))

(global-set-key (kbd "s-z") 'record-screen)
;; video recording:1 ends here

;; poor man's crop
;; :PROPERTIES:
;; :ID:       69221c48-401d-4d21-919a-b736d2a01673
;; :END:


;; [[file:init.org::*poor man's crop][poor man's crop:1]]
(defvar mw-crop-window-wc nil
  "To save the window config before the crop setup for later restore..")

(defun mw-crop-window-setup ()
  (interactive)
  (setq mw-crop-window-wc (current-window-configuration))
  (delete-other-windows)
  (set-window-buffer (split-window-below) (get-buffer-create " *right"))
  (set-window-buffer (split-window-right) (get-buffer-create " *bottom"))
  (use-local-map
   (let ((map (make-sparse-keymap)))
     (set-keymap-parent map (eval (intern (concat (symbol-name major-mode) "-map"))))
     (define-key map (kbd "M-<left>") #'shrink-window-horizontally)
     (define-key map (kbd "M-<right>") #'enlarge-window-horizontally)
     (define-key map (kbd "M-<up>") #'shrink-window)
     (define-key map (kbd "M-<down>") #'enlarge-window)
     (define-key map (kbd "q")
       (lambda () (interactive)
         (use-local-map nil)
         (set-window-configuration mw-crop-window-wc)))
     map)))
;; poor man's crop:1 ends here

;; dired-follow-mode
;; :PROPERTIES:
;; :ID:       cf076ca9-2d27-43e2-b11b-aeaa877e4412
;; :END:

;; this is a spontaneous hack based on the question
;; https://emacs.stackexchange.com/questions/51592/enable-follow-mode-in-dired


;; [[file:init.org::*dired-follow-mode][dired-follow-mode:1]]
(define-minor-mode dired-follow-mode
  "Diplay file at point in dired after a move."
  :lighter " dired-f"
  :global t
  (if dired-follow-mode
      (advice-add 'dired-next-line :after (lambda (arg) (dired-display-file)))
    (advice-remove 'dired-next-line (lambda (arg) (dired-display-file)))))
;; dired-follow-mode:1 ends here

;; ace-link
;; :PROPERTIES:
;; :ID:       6014b5b1-4a6c-46c9-9282-8f3fb0530ed1
;; :END:

;; Quickly follow links in certain modes e.g. info-mode.


;; [[file:init.org::*ace-link][ace-link:1]]
(straight-use-package 'ace-link)
;; ace-link:1 ends here

;; [[file:init.org::*ace-link][ace-link:2]]
(ace-link-setup-default)
;; ace-link:2 ends here

;; hippie expand
;; :PROPERTIES:
;; :ID:       858cfba2-7bb2-48a2-9ec1-d3cc037840ef
;; :END:

;; Hippie expand is using various sources as potential for expansion.


;; [[file:init.org::*hippie expand][hippie expand:1]]
(global-set-key (kbd "M-/") 'hippie-expand)
;; hippie expand:1 ends here

;; point to point in every window
;; :PROPERTIES:
;; :ID:       3440933b-3950-43e5-90bc-b5ae3195966a
;; :END:

;; this function can help to see where point is located in the other
;; window.


;; [[file:init.org::*point to point in every window][point to point in every window:1]]
(defun mw-point-to-point-in-windows ()
  "Somehow point to point in all the windows."
  (interactive)
  (mapc (lambda (x)
          (set-frame-selected-window (selected-frame) x)
          (mw-dance-around-the-cursor-b))
        (nreverse
         (cons
          (car (window-list))
          (nreverse (copy-seq (cdr (window-list))))))))
;; point to point in every window:1 ends here

;; tiro
;; :PROPERTIES:
;; :ID:       3e60b9c1-240a-4fb7-9aea-c0c8c8402c2b
;; :END:

;; tiro is an abbrev like program.  found btw.

;; tiro depends on cl-format.


;; [[file:init.org::*tiro][tiro:1]]
(straight-use-package '(cl-format :type git :host github :repo "alvinfrancis/cl-format"))
;; tiro:1 ends here

;; [[file:init.org::*tiro][tiro:2]]
(push "~/s/elisp/external/tiro" load-path)
(require 'tiro)
;; tiro:2 ends here

;; org-velocity
;; :PROPERTIES:
;; :ID:       9870e3f6-cd28-44a1-90e9-c3499b92be70
;; :END:

;; org-velocity from the repo.  (opposed to the org contrib module.)


;; [[file:init.org::*org-velocity][org-velocity:1]]
(push "~/s/elisp/external/org-velocity" load-path)
(require 'org-velocity)
;; org-velocity:1 ends here

;; point to locations


;; [[file:init.org::*point to locations][point to locations:1]]
(set-register ?~ (cons 'file "~/"))
(set-register ?e (cons 'file "~/s/elisp"))
(set-register ?g (cons 'file "~/.hyperb/HYPB")) ; global buttons
(set-register ?i (cons 'file "~/s/elisp/mw/.emacs.d/init.org"))
(set-register ?o (cons 'file "~/s/org/org-mode"))
;; point to locations:1 ends here

;; text


;; [[file:init.org::*text][text:1]]
(set-register ?L "log --format='%aD %s' --since=\"1am\"")
;; text:1 ends here

;; shortcuts functions to info
;; :PROPERTIES:
;; :ID:       3dde8c10-fd52-47af-a169-3346733f8e50
;; :END:

;; use M-x emacs-info and M-x elisp-info.  the idea comes from org-info.


;; [[file:init.org::*shortcuts functions to info][shortcuts functions to info:1]]
(defmacro shortcut-to-info-topic (info-topic)
  `(defun ,(intern (concat info-topic "-info")) (&optional node)
,(concat "Open info browser for topic '" info-topic "'.")
     (interactive)
     (info (format ,(concat "(" info-topic ")%s" ) (or node "")))))

(shortcut-to-info-topic "emacs")
(shortcut-to-info-topic "elisp")
;; shortcuts functions to info:1 ends here

;; toggle in current frame
;; :PROPERTIES:
;; :ID:       92268224-f85b-431c-a0bb-4daa84653fec
;; :END:


;; [[file:init.org::*toggle in current frame][toggle in current frame:1]]
(defun mw-toggle-foreground-background ()
  "Toggle foreground and background colors in current frame."
  (let
      ((fg (substring-no-properties (cdr (assoc 'foreground-color (frame-parameters)))))
       (bg (substring-no-properties (cdr (assoc 'background-color (frame-parameters))))))
    (set-foreground-color bg)
    (set-background-color fg)))
;; toggle in current frame:1 ends here

;; toggle in all frames and possibly freshly created frames
;; :PROPERTIES:
;; :ID:       97df7d4a-ff3f-49cc-9e91-a5cf60d27190
;; :END:


;; [[file:init.org::*toggle in all frames and possibly freshly created frames][toggle in all frames and possibly freshly created frames:1]]
(defun mw-toggle-foreground-background-all-frames ()
  "Toggle foreground and background colors in all frames.
Apply these settings also for newly created frames."
  (interactive)
  (modify-all-frames-parameters
   `((foreground-color . ,(substring-no-properties (cdr (assoc 'background-color (frame-parameters)))))
     (background-color . ,(substring-no-properties (cdr (assoc 'foreground-color (frame-parameters))))))))
;; toggle in all frames and possibly freshly created frames:1 ends here

;; add revert to save-some-buffers-action-alist
;; :PROPERTIES:
;; :ID:       14373e90-fd2d-4837-96fa-314d3b1e8cc8
;; :END:


;; [[file:init.org::*add revert to save-some-buffers-action-alist][add revert to save-some-buffers-action-alist:1]]
(push
 '(?r
   (lambda  (buf)
     (with-current-buffer buf
       (let ((this-command 'revert-buffer))
         (revert-buffer :ignore-auto :noconfirm)))
     ;; revert and continue asking save actions for other buffers
     :continue)
   "revert this buffer (discard modifications) and continue")
 save-some-buffers-action-alist)
;; add revert to save-some-buffers-action-alist:1 ends here

;; Pull out section of orgee 'Commentary'
;; :PROPERTIES:
;; :ID:       807a30b5-c8cf-42a0-b764-c67597339707
;; :END:


;; [[file:init.org::*Pull out section of orgee 'Commentary'][Pull out section of orgee 'Commentary':1]]
(defun mw-org-commentary-content-section (headline-title)
  "Return list of begin end of HEADLINE-TITLE section in current buffer."
  (let ((section
         (car
          (org-element-map
              (seq-filter
               (lambda (x) (string= headline-title (car (org-element-property :title x))))
               (org-element-map (org-element-parse-buffer) 'headline #'identity))
              'section #'identity))))
    (when section
      (list (org-element-property :contents-begin section)
            (org-element-property :contents-end section)))))

(defun mw-org-babel-insert-org-section-as-elisp-comments (headline-title)
  "Insert the section of orgee 'Commentary' as elisp comment.
This is thought to ease DRY for commentary for literate
programming."
  (let* ((s_e (mw-org-commentary-content-section headline-title))
         (start (car s_e))
         (end (cadr s_e))
         (buf (current-buffer)))
    (insert
     (with-temp-buffer
       (let ((tbuf (current-buffer)))
         (with-current-buffer buf
           (append-to-buffer tbuf start end)))
       (emacs-lisp-mode)
       (comment-region (point-min) (point-max))
       (buffer-string)))))

(defun mw-org-babel-insert-org-commentary-section-as-elisp-comments ()
    (interactive)
    (mw-org-babel-insert-org-section-as-elisp-comments "Commentary"))

(defun mw-org-babel-insert-org-license-section-as-elisp-comments ()
  (interactive)
  (mw-org-babel-insert-org-section-as-elisp-comments "License"))
;; Pull out section of orgee 'Commentary':1 ends here

;; [[file:init.org::*Pull out section of orgee 'Commentary'][Pull out section of orgee 'Commentary':2]]
(defun org-agenda-property-action ()
  "Property action for the current headline in the agenda."
  (interactive)
  (org-agenda-check-no-diary)
  (let* ((hdmarker (or (org-get-at-bol 'org-hd-marker)
		       (org-agenda-error)))
	 (buffer (marker-buffer hdmarker))
	 (pos (marker-position hdmarker))
	 (inhibit-read-only t)
	 newhead)
    (org-with-remote-undo buffer
      (with-current-buffer buffer
	(widen)
	(goto-char pos)
	(org-show-context 'agenda)
	(call-interactively 'org-property-action)))))
;; Pull out section of orgee 'Commentary':2 ends here

;; Remove Prefix from Org Link Descriptions
;; :PROPERTIES:
;; :ID:       52afa065-e90a-4c53-978b-d8043e34a604
;; :END:

;; Remove prefix 'file:' from Org link descriptions.

;; This is a convenience command.  Such prefixes often occur since this
;; is the default naming for file-links with Org AFAICT.  I want to get
;; easily rid of those prefixes since they block ffap, which I like to
;; use often.  Further I don't want to see this prefix.

;; [2016-11-05 Sat 14:38] Maybe the new generation of Org links (v 9) can
;; solve this issue.  I want to invesigate the possibilities of the new
;; links.


;; [[file:init.org::*Remove Prefix from Org Link Descriptions][Remove Prefix from Org Link Descriptions:1]]
(defun mw-org-link-strip-decoration ()
  "Remove the \"file:\" prefix from an Org link."
  (interactive)
  (when (org-in-regexp org-bracket-link-regexp 1) ;; We do have a link at point.
    (let (remove desc link)
      (setq remove (list (match-beginning 0) (match-end 0)))
      (setq desc (when (match-end 3)
                   (if (string= "file:" (substring (match-string 3) 0 5))
                       (substring (match-string 3) 5)
                     (match-string 3))))
      (setq link (match-string 1))
      (apply #'delete-region remove)
      (goto-char (car remove))
      (insert (concat "[[" link "][" desc "]]")))))
;; Remove Prefix from Org Link Descriptions:1 ends here

;; [[file:init.org::*Remove Prefix from Org Link Descriptions][Remove Prefix from Org Link Descriptions:2]]
(defalias 'mw-org-link-remove-file-decoration 'mw-org-link-strip-decoration)
(make-obsolete
 'mw-org-link-remove-file-decoration
 "use the `mw-org-link-strip-decoration' instead." "[2016-09-23 Fri 11:01]")
;; Remove Prefix from Org Link Descriptions:2 ends here

;; Clear Checkboxes in a Subtree
;; :PROPERTIES:
;; :ID:       87594db8-b09e-4528-aa0f-158b0aaf7f6e
;; :END:


;; [[file:init.org::*Clear Checkboxes in a Subtree][Clear Checkboxes in a Subtree:1]]
(defun mw-org-clear-checkboxes-in-subtree ()
  (interactive)
  (save-restriction
    (save-excursion
      (require 'org)
      (org-narrow-to-subtree)
      (goto-char (point-min))
      (while (re-search-forward "\\[X\\]" (point-max) t)
        (replace-match "[ ]")))))
;; Clear Checkboxes in a Subtree:1 ends here

;; Rise Org Subtree
;; :PROPERTIES:
;; :ID:       e2e30620-a918-4a72-9ae1-e8cf895b62fd
;; :END:

;; Pull out an orgee immediately before its parent.

;; Rise is slightly different from the classical promotion.

;; Rationale of this functionality: The risen subtree cannot
;; accidentially slurp siblings when risen.


;; [[file:init.org::*Rise Org Subtree][Rise Org Subtree:1]]
(defun org-rise ()
  "Rise current subtree to one level higher if possible."
  (interactive)
  (cl-assert (not (org-before-first-heading-p)) "Before first heading")
  (cl-assert (< 1 (org-outline-level)) "At top outline level")
  (org-back-to-heading)
  (let ((subtree-start (point)))
    (outline-up-heading 1)
    (let ((insert-here (point)))
      (goto-char subtree-start)
      (if (eq last-command 'kill-region)
          (setq last-command 'ignore)) ; break the kill accumulation
      (org-cut-subtree)
      (goto-char insert-here)
      (org-paste-subtree nil org-subtree-clip))))
;; Rise Org Subtree:1 ends here

;; Mark column in an org table
;; :PROPERTIES:
;; :ID:       dc510ff4-4498-43f6-b610-7bb0ccf75987
;; :END:

;; Mark the column containing point in an Org table.

;; There is also a function for marking a whole Org table.


;; [[file:init.org::*Mark column in an org table][Mark column in an org table:1]]
(require 'org)
(defun mw-org-table-mark-column ()
  "Mark the column containing point.

For tables with horizontal lines this function can fail."
  (interactive)
  (unless (org-at-table-p) (user-error "Not at a table"))
  (org-table-find-dataline)
  (org-table-check-inside-data-field)
  (let* ((col (org-table-current-column))
         (beg (org-table-begin))
	 (end (org-table-end)))
    (goto-char beg)
    (org-table-goto-column col)
    (re-search-backward "|" nil t)
    (push-mark)
    (goto-char (1- end))
    (org-table-goto-column (1+ col))
    (re-search-backward "|" nil t)
    (exchange-point-and-mark)
    (forward-char)))

(defun mw-org-table-mark-table ()
  "Mark the table at point."
  (interactive)
  (unless (org-at-table-p) (user-error "Not at a table"))
  (goto-char (1- (org-table-end)))
  (push-mark (point) t t)
  (goto-char (org-table-begin)))
;; Mark column in an org table:1 ends here

;; Narrow to level above
;; :PROPERTIES:
;; :ID:       40a52fd3-ca25-4d1f-951e-b7e1613b6947
;; :END:

;; This function narrows to the level above.  When narrowing to subtree
;; is in effect, this function may widen one level.

;; IIRC Bernt Hansen also wrote such function.


;; [[file:init.org::*Narrow to level above][Narrow to level above:1]]
(defun mw-org-narrow-to-one-level-above ()
  "Narrow to one level above relative to point."
  (interactive)
  (widen)
  (let ((point-recall (point)))
    (unless (org-before-first-heading-p)
      (unless (outline-on-heading-p)
        (outline-previous-heading))
      (beginning-of-line)
      (if (= 1 (org-reduced-level (org-outline-level)))
          (widen)
        (assert (outline-on-heading-p))
        (assert (< 1 (org-reduced-level (org-outline-level))))
        (outline-up-heading 1)
        (org-narrow-to-subtree)))
    (goto-char point-recall)))
;; Narrow to level above:1 ends here

;; View Subtree as top level
;; :PROPERTIES:
;; :ID:       8a4095fd-0470-43c5-afcf-84f0f652918b
;; :END:


;; [[file:init.org::*View Subtree as top level][View Subtree as top level:1]]
(let ((top-level-risen-indicator-char " ")
      overlays it-s-on)

  (defun mw-org-subtree-as-top-level-tree-establish ()
    "View Subtree as top level."
    (mw-org-subtree-as-top-level-tree-unravel)
    (unless (< (org-outline-level) 2)
      (let (first-level)
        (org-map-entries
         (lambda ()
           (let ((level (org-outline-level)))
             (unless first-level
               (setf first-level level))
             (push (make-overlay (1+ (point)) (+ (point) first-level)) overlays)
             (overlay-put (car overlays) 'invisible t)
             (unless (= first-level level)
               (push (make-overlay
                     (+ (point) first-level)
                     (+ 1 (point) first-level))
                    overlays)
               (overlay-put (car overlays) 'display top-level-risen-indicator-char))))
         t 'tree))
      (setq it-s-on t)))

  (defun mw-org-subtree-as-top-level-tree-unravel ()
    "Undo all decoration as top level."
    (dolist (ol overlays)
      (delete-overlay ol))
    (setf overlays nil)
    (setq it-s-on nil))

  (defun mw-org-subtree-as-top-level-tree-toggle ()
    (interactive)
    (if it-s-on
        (mw-org-subtree-as-top-level-tree-unravel)
      (mw-org-subtree-as-top-level-tree-establish)))

  (list
   #'mw-org-subtree-as-top-level-tree-toggle))
;; View Subtree as top level:1 ends here

;; Show/Hide meta-info lines
;; :PROPERTIES:
;; :ID:       bfb32b64-e275-4376-8375-f8f513419034
;; :END:

;; E.g. hide src-block-delimiters.


;; [[file:init.org::*Show/Hide meta-info lines][Show/Hide meta-info lines:1]]
(let (ols)

  (defun mw-org-hide-meta-heading-info ()
    "Hide meta data following headings."
    (interactive)
    (org-show-all) ; expand all props before make invisible to avoid ellipses.
    (save-excursion
      (goto-char (point-min))
      (unless (org-at-heading-p) (outline-next-heading))
      (while (not (eobp))
        (let ((beg (1+ (progn (end-of-line) (point))))
              (end (1- (progn (org-end-of-meta-data t) (point)))))
          (when (< beg end)
            (push (make-overlay beg end) ols)
            (overlay-put (car ols) 'invisible t)))
        (when (not (org-at-heading-p))
          (outline-next-heading)))))

  (defun mw-org-hide-meta-info-lines ()
    "Hide parts of meta info.
E.g. hide all property sections."
    (interactive)
    (org-show-all)
    (save-excursion
      (goto-char (point-min))
      (let ((case-fold-search t))
        (while (re-search-forward
                (concat
                 "^\\( *scheduled:.*\\)" "\\|"
                 "^\\( *deadline:.*\\)" "\\|"
                 "^\\( *closed:.*\\)" "\\|"
                 "^\\( *#\\+begin_src.*\\)" "\\|"
                 "^\\( *#\\+end_src.*\\)" "\\|"
                 "^\\( *#\\+begin_center.*\\)" "\\|"
                 "^\\( *#\\+end_center.*\\)" "\\|"
                 "^\\( *#\\+begin_example.*\\)" "\\|"
                 "^\\( *#\\+end_example.*\\)" "\\|"
                 "^\\( *#\\+begin_quote.*\\)" "\\|"
                 "^\\( *#\\+end_quote.*\\)" "\\|"
                 "^\\( *#\\+begin_verse.*\\)" "\\|"
                 "^\\( *#\\+end_verse.*\\)" "\\|"
                 "^\\( *#\\+results:\\)" "\\|"
                 "\\(^ *:properties:.*\\)\\(\n.*\\)*?\n\\( *:end:.*\\)" "\\|"
                 "\\(^ *:logbook:.*\\)\\(\n.*\\)*?\n\\( *:end:.*\\)"
                 )
                nil t)
          (push (make-overlay (match-beginning 0) (1+ (match-end 0)))
                ols)
          (overlay-put (car ols) 'invisible t)))))

  (defun mw-org-show-meta-info-lines ()
    "Show meta info."
    (interactive)
    (mapc #'delete-overlay ols)
    (setq ols nil)))
;; Show/Hide meta-info lines:1 ends here

;; Count direct sub orgees
;; :PROPERTIES:
;; :ID:       3b435db2-7425-4309-bdbb-0c071d89f384
;; :END:


;; [[file:init.org::*Count direct sub orgees][Count direct sub orgees:1]]
(defun mw-org-count-subtrees ()
  "Print the count of the direct subtrees below point."
  (interactive)
  (message "[%d direct subtrees]" (mw-org--count-subtrees)))

(defun mw-org--count-subtrees ()
  "Count the direct subtrees below the current outline level."
  (let ((level (org-outline-level)))
    (save-excursion
      (outline-next-heading)
      (if (<= (org-outline-level) level)
          0
        (let ((count 0)
              (level-to-count (org-outline-level))
              pos)
          (while (progn
                   (setq pos (point))
                   (when  (eq level-to-count (org-outline-level))
                     (incf count))
                   (and (outline-next-heading)
                        (<= level-to-count (org-outline-level))
                        (< pos (point)))))
          count)))))
;; Count direct sub orgees:1 ends here

;; Jump to end of Orgee
;; :PROPERTIES:
;; :ID:       521d9edd-c347-4668-8625-09f7e8edb25a
;; :END:

;; Actually this functionality is already implemented in Org but not as command.


;; [[file:init.org::*Jump to end of Orgee][Jump to end of Orgee:1]]
(defun org-jump-to-end-of-subtree ()
  "Move to the end of the current subtree."
  (interactive)
  (org-end-of-subtree))
;; Jump to end of Orgee:1 ends here

;; Switch external minibuffer :lab:
;; :PROPERTIES:
;; :ID:       c09c4615-5741-47a9-866f-d8ca98fd6176
;; :END:

;; This is for setting the minibuffer into an extra frame.

;; Get back a minibuffer in the frame using

;; { M-x mw-set-minibuffer-in-default-frame RET}
;; { C-x 5 2 }
;; { C-x 5 1 }


;; [[file:init.org::*Switch external minibuffer][Switch external minibuffer:1]]
(defun mw-unset-minibuffer-in-default-frame ()
  (interactive)
  (setf (cdr (assoc 'minibuffer default-frame-alist)) nil))

(defun mw-set-minibuffer-in-default-frame ()
  (interactive)
  (if (assoc 'minibuffer default-frame-alist)
      (setf (cdr (assoc 'minibuffer default-frame-alist)) t)
    (setf default-frame-alist
          (append (list (cons 'minibuffer t )) default-frame-alist))))
;; Switch external minibuffer:1 ends here

;; create a pair only/no-minibuffer
;; :PROPERTIES:
;; :ID:       4671e76a-9ea7-499a-9472-0be51175e6c7
;; :END:


;; [[file:init.org::*create a pair only/no-minibuffer][create a pair only/no-minibuffer:1]]
(defun mw-make-frames-only/no-minibuffer ()
  (interactive)
  (make-frame
   `((minibuffer
      . ,(car (window-list
               (make-frame '((minibuffer . only))) t))))))
;; create a pair only/no-minibuffer:1 ends here

;; Hours in the current day
;; :PROPERTIES:
;; :ID:       1dfe7c95-99b3-4842-baff-f14bb05346b7
;; :END:

;; Time of day as float.


;; [[file:init.org::*Hours in the current day][Hours in the current day:1]]
(defun mw-current-time-of-day-decimal ()
  "Return the decimal hours of the time of day.
E.g. 11.9 is almost noon, 12.0 is noon.
"
  (let ((time (current-time)))
    (+ (string-to-number (format-time-string "%H" time))
       (/ (string-to-number (format-time-string "%M" time)) 60.0))))
;; Hours in the current day:1 ends here

;; Exterminate top of kill ring
;; :PROPERTIES:
;; :ID:       b230a067-ea44-4ac5-8da5-23040c342b63
;; :END:


;; [[file:init.org::*Exterminate top of kill ring][Exterminate top of kill ring:1]]
(defun mw-kill-ring-exterminate-top ()
  "Exterminate the current element from the `kill-ring'."
  (interactive)
  (setq kill-ring (cdr kill-ring))
  (setq kill-ring-yank-pointer kill-ring))
;; Exterminate top of kill ring:1 ends here

;; Refile to known location :lab:
;; :PROPERTIES:
;; :ID:       a2413673-51dd-4a8f-a02f-19bba0dc634a
;; :END:

;; Use bookmark `org-refile-direct-target' as target for the refile.

;; This functionality is for saving the time to wait for the
;; completion of the possible candidates for the case when you already
;; know where the refile should go.

;; Of course you must have set the target first.  This means setting
;; bookmark `org-refile-direct-target'.  This can be done with
;; function `mw-org-refile-set-direct-target-bm' or by foot via
;; setting the apropriate bookmark.  E.g. at the target point set the
;; mark with C-x rm `org-refile-direct-target'.

;; =M-x `mw-org-refile-refile-to-direct-target'= refiles the current
;; outline to the `org-refile-direct-target' bookmark.

;; Reminder: Get the list of bookmarks with C-x rl.
;; `counsel-bookmark' is an alternative.


;; [[file:init.org::*Refile to known location][Refile to known location:1]]
(defun mw-org-refile-set-direct-target-bm ()
  "Set to bookmark `org-refile-direct-target' to current point."
  (interactive)
  (bookmark-set "org-refile-direct-target")
  (message "bookmark org-refile-direct-target set"))
;; Refile to known location:1 ends here

;; [[file:init.org::*Refile to known location][Refile to known location:2]]
(defun mw-org-refile-refile-to-direct-target ()
  "Refile to bookmark `org-refile-direct-target'."
  (interactive)
  (let ((file (bookmark-get-filename "org-refile-direct-target"))
        (pos (bookmark-get-position "org-refile-direct-target")))
    (if (not file)
        (message "Doing nothing.  Bookmark `org-refile-direct-target' not set")
      (org-refile nil (current-buffer) `(nil ,file nil ,pos)))))
;; Refile to known location:2 ends here

;; Refile DWIM :lab:
;; :PROPERTIES:
;; :ID:       d206f4ae-d024-4058-8877-2fa7d11ab541
;; :END:

;; Does not work for refiling to the same buffer.


;; [[file:init.org::*Refile DWIM][Refile DWIM:1]]
(defun org-other-org-win ()
  "Return next best visible window in org mode."
  (get-window-with-predicate
   (lambda (window)
     (with-current-buffer (window-buffer window)
       (derived-mode-p 'org-mode)))))

(defun org-refile-other-window ()
  "Refile to position in other Org window.
Inspired by dired operations with `dired-dwim-target' t."
  (interactive)
  (if-let ((win (org-other-org-win)))
      (unless (eq (selected-window) win)
        (save-window-excursion
          (select-window win)
          (mw-org-refile-set-direct-target-bm))
        (mw-org-refile-refile-to-direct-target))))
;; Refile DWIM:1 ends here

;; Wget
;; :PROPERTIES:
;; :ID:       8e484acf-1d5e-4b72-81f7-ea3b7132bf88
;; :END:

;; Get a file from the net.

;;  - Scenaria:
;;   - You have an address of a file in the web.
;;   - You want a copy of that file in the current directory.


;; [[file:init.org::*Wget][Wget:1]]
(defun wget (url)
  "Download URL."
  (interactive (list (read-string "URL: ")))
  (call-process "wget" nil nil nil url)
  ;; (url-retrieve url 'wget-render
  ;;       	(list url (point) (current-buffer)))
  )
;; Wget:1 ends here

;; [[file:init.org::*Wget][Wget:2]]
(defun raw-browse (url)
  "Fetch URL to a buffer."
  (interactive
   (list (read-string "URL: " nil nil nil)))
  (url-retrieve
   url
   (lambda (status url &optional point buffer encode)
     (message "url: %s, status: %s" url status)
     (let ((buffer-name (concat  "*raw " url "*")))
       (append-to-buffer
        buffer-name
        (point-min) (point-max))
       (switch-to-buffer buffer-name)))
   (list url nil (current-buffer))))
;; Wget:2 ends here

;; Repeat last command
;; :PROPERTIES:
;; :ID:       fc379f42-b346-48b3-8f18-d519859ce974
;; :END:

;; Repeat the last command of the extended command history.

;; The functionality is (of course) already long available in Emacs.  Do
;; e.g. M-x M-p RET and there you are.

;; So this section is about to shorten this already short dance.

;; This is very experimental.  E.g. breaks when choosing a command that
;; needs a parameter.


;; [[file:init.org::*Repeat last command][Repeat last command:1]]
(defun mw-repeat-last-command ()
  "Execute first command in `extended-command-history'."
  (interactive)
  (funcall-interactively
   (read
    (car
     (remove-if
      (lambda (x) (string= "mw-repeat-last-command" x))
      extended-command-history)))))
;; Repeat last command:1 ends here

;; [[file:init.org::*Repeat last command][Repeat last command:2]]
(defun mw-message-last-command ()
  "Display the command which would be activated by `mw-repeat-last-command'."
  (interactive)
  (message
   (car (remove-if
         (lambda (x) (string= "mw-repeat-last-command" x))
         extended-command-history))))
;; Repeat last command:2 ends here

;; Convert to double-space sentence
;; :PROPERTIES:
;; :ID:       aa39cb94-8625-42aa-94b8-a5ff2038a58c
;; :END:

;; Sometimes text is formatted to end with one space at the ends of
;; sentences but the user want it to be two space.


;; [[file:init.org::*Convert to double-space sentence][Convert to double-space sentence:1]]
(defun mw-convert-next-sentence-end-double-space ()
  "Go forward a sentence and add a SPC when there is only one yet."
  (interactive)
  (let ((sentence-end-double-space nil))
    (forward-sentence)
    (when (looking-at " [^ ]") (insert " "))))
;; Convert to double-space sentence:1 ends here

;; Extra Point
;; :PROPERTIES:
;; :ID:       129937cc-a03d-4b92-918c-f5badda3283c
;; :END:

;; The feature Extra Point is just one extra location you can set or go
;; to.

;; A typical scenario for Extra Point is given when you are at a location
;; and you feel you want to go somewhere else but you are quite sure you
;; want to come back to the current location.

;; Set the Extra Point with

;; #+begin_example
;; ;; M-x mw-set-extra-point
;; #+end_example

;; ;; Go to the Extra Point with

;; #+begin_example
;; ;; M-x mw-goto-extra-point
;; #+end_example


;; [[file:init.org::*Extra Point][Extra Point:1]]
(defvar *mw-extra-point* nil)

(defun mw-set-extra-point ()
  "Save current position as Extra Position."
  (interactive)
  (setf *mw-ariadne-point* (cons (current-buffer) (point)))
  (message "Extra point has been set."))

(defun mw-goto-extra-point ()
  "Set current cursor to the Extra Point if possible."
  (interactive)
  (when *mw-ariadne-point*
    (if (buffer-live-p (car *mw-ariadne-point*))
        (progn
          (switch-to-buffer (car *mw-ariadne-point*))
          (goto-char (cdr *mw-ariadne-point*))
          (message "At current Extra Point."))
      (message "Warning: Extra Point not found."))))
;; Extra Point:1 ends here

;; Random Name
;; :PROPERTIES:
;; :ID:       5ad6248b-7ba2-436c-b1f0-3443d4c2aa00
;; :END:

;; Save time to find a name if all you want is a name.


;; [[file:init.org::*Random Name][Random Name:1]]
(defun mw-random-name ()
  "Insert a random name at point."
  (interactive)
  (insert "name_" (format "%019d" (abs (random)))))

(defun name-0040310557124765049-test ()
  (let ((a 23))
    (while (< 0 a)
      (mw-random-name)
      (insert "\n")
      (setq a (- a 1)))))
;; Random Name:1 ends here

;; File copy with sha1 name
;; :PROPERTIES:
;; :ID:       17bb878c-bfb5-4c87-99d1-ae92c1783dd8
;; :END:


;; [[file:init.org::*File copy with sha1 name][File copy with sha1 name:1]]
(defun mw-copy-file-with-hash-name (filename)
  "BROKEN! Copy file FILENAME using essentially its hash as filename of
the copy."
  (interactive
   (find-file-read-args "Find file: " t)
   )
  ;; (when filename
  ;;   (find-file-literally filename)
  ;;   ;; (let ((target (concat
  ;;   ;;                (file-name-directory filename)
  ;;   ;;                (sha1 (current-buffer)) "-"
  ;;   ;;                (file-name-nondirectory buffer-file-name))))
  ;;   ;;   ;; (copy-file filename target)
  ;;   ;;   )
  ;;   )
  ;; (kill-buffer)
  )
;; File copy with sha1 name:1 ends here

;; Buffer copy with sha1 name
;; :PROPERTIES:
;; :ID:       2b774acd-bd54-48a2-b3d9-67aa21871fd4
;; :END:


;; [[file:init.org::*Buffer copy with sha1 name][Buffer copy with sha1 name:1]]
(defun mw--create-sha1-hash-named-copy-of-buffer ()
  (let ((hash (sha1 (current-buffer))))
    (unless (get-buffer hash)
      (append-to-buffer hash (point-min) (point-max)))
    (get-buffer hash)))

(defun mw--create-sha1-hash-named-copy-of-region (start end)
  (cl-assert (<= start end))
  (save-restriction
    (narrow-to-region start end)
    (let* ((hash (sha1 (current-buffer))))
      (unless (get-buffer hash)
        (append-to-buffer hash start end))
      (get-buffer hash))))

(defun mw-create-sha1-hash-named-copy-of-buffer ()
  "Copy buffer and name with its hash and switch to it."
  (interactive)
  (switch-to-buffer (mw--create-sha1-hash-named-copy-of-buffer))
  (view-mode))

(defun mw-create-sha1-hash-named-copy-of-region (start end)
  "Copy region to buffer named with hash of its content."
  (interactive "r")
  (switch-to-buffer (mw--create-sha1-hash-named-copy-of-region start end))
  (setq buffer-read-only t))
;; Buffer copy with sha1 name:1 ends here

;; sha1 as defun name
;; :PROPERTIES:
;; :ID:       d0a6532d-9b7d-4e12-92e7-98389b7a6d1c
;; :END:


;; [[file:init.org::*sha1 as defun name][sha1 as defun name:1]]
(defun mw--sha1-of-defun ()
  "Return sha1 of current defun.
The function text starts at the argument list and ends at the
last paren (exclusive)."
  (save-excursion
    (let* ((start (progn
                    (beginning-of-defun)
                    (search-forward-regexp "\(" nil nil 2)
                    (backward-char)
                    (point)))
           (end (progn
                  (end-of-defun)
                  (backward-char)
                  (point))))
      (sha1 (current-buffer) start end))))
;; sha1 as defun name:1 ends here

;; [[file:init.org::*sha1 as defun name][sha1 as defun name:2]]
(defun mw-sha1-of-defun ()
  "Put the sha1 of a function text into the kill ring.
The function text starts at the argument list and ends at the
last paren (exclusive)."
  (interactive)
  (kill-new (mw--sha1-of-defun)))
;; sha1 as defun name:2 ends here

;; [[file:init.org::*sha1 as defun name][sha1 as defun name:3]]
(defun mw-sha1-as-defun-name ()
  "Append or refresh sha1hash as postfix of a function name.
If the function name is less than 40 characters the sha1 gets appended.
Else the last 40 chars get replaced by the hash."
  (interactive)
  (save-match-data
    (save-excursion
     (let ((sha1 (mw--sha1-of-defun))
           (defunstart (progn
                     (beginning-of-defun)
                     (search-forward-regexp "\(" nil nil 2)
                     (backward-char)
                     (cl-assert (looking-at "("))
                     (skip-chars-backward " \t")
                     (point)))
           (sha1namelength 40))
       (search-backward-regexp "[[:space:]]")
       (cl-assert (looking-at "[[:space:]]"))
       (forward-char)
       (if (< (- defunstart (point)) sha1namelength)
           (goto-char defunstart)
         (delete-region (- defunstart sha1namelength) defunstart)
         (goto-char (- defunstart sha1namelength)))
       (insert sha1)))))

(defalias 'mw-hash-as-defun-name 'mw-sha1-as-defun-name ) ; better find that fun.
;; sha1 as defun name:3 ends here

;; sha1 of region
;; :PROPERTIES:
;; :ID:       19dc574d-c5e7-426c-8c68-8b608575fffb
;; :END:


;; [[file:init.org::*sha1 of region][sha1 of region:1]]
(defun mw-sha1-of-region-to-kill-ring (start end)
  "See sha1 docu for the parameters."
  (interactive "r")
  (let ((hash (sha1 (current-buffer) start end)))
    (deactivate-mark)
    (kill-new hash))
  (message "car kill-ring: %s" (car kill-ring)))
;; sha1 of region:1 ends here

;; Display all appointments
;; :PROPERTIES:
;; :ID:       afbb74d0-4d80-4934-af3d-a2c5054821e8
;; :END:

;; Display all appointments of the Emacs calendar.  This function
;; completes the appt functions AFAICT.


;; [[file:init.org::*Display all appointments][Display all appointments:1]]
(defun mw-appt-show ()
  "Shout out all appointments for today."
  (interactive)
  (if-let ((appt-list appt-time-msg-list))
    (with-temp-buffer
      (while appt-list
        (insert (format "%s" (substring-no-properties (cadar appt-list) 0)))
        (setq appt-list (cdr appt-list))
        (when appt-list (insert "\n"))
        (message (buffer-substring (point-min) (point-max)))))))
;; Display all appointments:1 ends here

;; Kill Buffer and Buffer Filename
;; :PROPERTIES:
;; :ID:       6dfe514c-4714-4504-93b4-18b5af0bbdd7
;; :END:


;; [[file:init.org::*Kill Buffer and Buffer Filename][Kill Buffer and Buffer Filename:1]]
(defun mw-kill-buffer-name ()
  "Append buffer-name to `kill-ring'."
  (interactive)
  (when (buffer-name)
    (kill-new (buffer-name))
    (message "%s entered kill-ring" (buffer-name))))

(defun mw-kill-buffer-filename ()
  "Append buffer-filename to `kill-ring'."
  (interactive)
  (let ((bfn (cond
              ((buffer-file-name))
              ((eq major-mode 'dired-mode)
               dired-directory))))
  (when bfn
    (kill-new bfn)
    (message "%s entered kill-ring" bfn))))
;; Kill Buffer and Buffer Filename:1 ends here

;; Umlautify
;; :PROPERTIES:
;; :ID:       fed97beb-09b6-4ed4-9ac3-f756f60ba2a5
;; :END:


;; [[file:init.org::*Umlautify][Umlautify:1]]
(defun mw-umlautify-before-point ()
  "Change character before point to its umlaut."
  (interactive)
  (let* ((conversion-table
         '((?a . "ä")
           (?o . "ö")
           (?u . "ü")
           (?s . "ß")
           (?A . "Ä")
           (?O . "Ö")
           (?U . "Ü")))
        (conversion (assoc (char-before) conversion-table)))
    (when conversion
          (delete-char -1)
          (insert (cdr conversion)))))
;; Umlautify:1 ends here

;; Buddy buffer
;; :PROPERTIES:
;; :ID:       330e690f-cbd2-41b1-bb62-3362c5981bb6
;; :END:


;; [[file:init.org::*Buddy buffer][Buddy buffer:1]]
(defvar *mw-buddy-buffer* (current-buffer))

(defun mw-set-current-as-buddy ()
  "Set current buffer as buddy buffer."
  (interactive)
  (setq *mw-buddy-buffer* (current-buffer))
  (message "'%s' is buddy buffer now." (buffer-name *mw-buddy-buffer*)))

(defun mw-exchange-to-buddy ()
  "Make buddy buffer current buffer and vice versa."
  (interactive)
  (let ((other (current-buffer)))
    (and (buffer-live-p *mw-buddy-buffer*)
         (not (eq other *mw-buddy-buffer*))
         (switch-to-buffer *mw-buddy-buffer*))
    (if (eq other *mw-buddy-buffer*)
        (message "Already on buddy buffer.  '%s' remains buddy buffer."
                 (buffer-name *mw-buddy-buffer*))
      (setq *mw-buddy-buffer* other)
      (message "'%s' is buddy buffer now." (buffer-name *mw-buddy-buffer*)))))
;; Buddy buffer:1 ends here

;; Append to scratch
;; :PROPERTIES:
;; :ID:       d2f367d8-b32f-4104-9145-fbf21b0c0ada
;; :END:


;; [[file:init.org::*Append to scratch][Append to scratch:1]]
(defun mw-append-to-scratch (beg end)
  "Append region to buffer scratch."
  (interactive "r")
  (append-to-buffer (get-buffer-create "*scratch*") beg end))
;; Append to scratch:1 ends here

;; Kill outline-path
;; :PROPERTIES:
;; :ID:       d3e6c193-efad-4c34-a951-629a8ac58334
;; :END:


;; [[file:init.org::*Kill outline-path][Kill outline-path:1]]
(defun mw-org-outline-path-starting-with-file ()
  "Return outline-path starting with file."
  (org-display-outline-path t (not (org-before-first-heading-p)) nil t))

(defun mw-org-outline-path-kill ()
  "Add the outline path to where point is to the `kill-ring'."
  (interactive)
  (let ((outline-path (mw-org-outline-path-starting-with-file)))
    (kill-new outline-path)
    (message "'%s' added to kill-ring" outline-path)))
;; Kill outline-path:1 ends here

;; Horizontally Split window below point
;; :PROPERTIES:
;; :ID:       2f44d2be-454a-4aab-9fd6-a00feeb950eb
;; :END:


;; [[file:init.org::*Horizontally Split window below point][Horizontally Split window below point:1]]
(defun mw-visual-column-number ()
  "Visual column number of point."
  (save-excursion
    (let ((end-point (point)))
      (beginning-of-line)
      (1+ (- end-point (point))))))

(defun mw-split-window-horizontally-at-point ()
  "Split window horizontally at point."
  (interactive)
  (let ((split-point (point)))
    (save-excursion
      (save-restriction
        (split-window-right (mw-visual-column-number))))))
;; Horizontally Split window below point:1 ends here

;; Split window below point
;; :PROPERTIES:
;; :ID:       01af2ac1-04c8-44a4-a029-10737c53fcf4
;; :END:


;; [[file:init.org::*Split window below point][Split window below point:1]]
(defun mw-visual-line-number ()
  "Visual line number of the line containing point."
  (save-excursion
    (let ((end-point (progn (beginning-of-visual-line) (point)))
          (count 1)
          (line-move-visual t))
      (move-to-window-line (1- count))
      (while (< (point) end-point)
        (incf count)
        (next-line))
      count)))

(defun mw-split-window-vertically-at-point ()
  "Split window vertically at point.
The top window stands still.

This has been useful for screenshots which just take the window."
  (interactive)
  (let ((split-point (point)))
    (save-excursion
      (save-restriction
        (split-window-below (1+ (mw-visual-line-number)))
        (goto-char split-point)
        (recenter -1)
        ))
    (other-window 1)
    (forward-line 1)
    (recenter 0)
    (other-window -1)))
;; Split window below point:1 ends here

;; Print numbers for visual lines :lab:
;; :PROPERTIES:
;; :ID:       15c808d2-69ea-49a4-a093-633b45013377
;; :END:

;; Variations of counting the lines as they appear in an Emacs window.

;; Note the difference from counting the lines of a file.


;; [[file:init.org::*Print numbers for visual lines][Print numbers for visual lines:1]]
(defun mw-visual-line-number-with-point ()
  ;; Note: Think about speed up by exponential seach.
  (let ((pt (save-excursion (forward-line 0) (point)))
        (n 0))
    (save-excursion
      (while (< (progn
                   (move-to-window-line n)
                   (point))
                 pt)
        (setq n (1+ n))))
    n))
;; Print numbers for visual lines:1 ends here

;; [[file:init.org::*Print numbers for visual lines][Print numbers for visual lines:2]]
(defun mw-ephemeral-count-window-lines ()
  "Count the lines of the window."
  (let ((pt (save-excursion (move-to-window-line -1) (point)))
        (n 0))
    (save-excursion
      (while (< (progn
                   (move-to-window-line n)
                   (point))
                 pt)
        (setq n (1+ n))))
    n))
;; Print numbers for visual lines:2 ends here

;; Decorate buffer with various information but just ephemerally.
;; :PROPERTIES:
;; :ID:       ef5250b2-9a9e-47b9-8ed7-63d5205a3bd0
;; :END:


;; [[file:init.org::*Decorate buffer with various information but just ephemerally.][Decorate buffer with various information but just ephemerally.:1]]
(let (overlays)

  (defun mw-ephemeral-delete-overlays ()
    "Delete all overlays."
    (mapc #'delete-overlay overlays)
    (setq overlays nil))

  (defun mw-ephemeral-rectangle-enumerate-lines-sit (beg end)
    "Enumerate the region lines in the column of BEG.
The enumeration disappears on any keypress or after a few
seconds.  Argument END is the other end of the region."
    (interactive "r")
    (save-excursion
      (let ((col (- (progn (goto-char beg) (point))
                    (progn (beginning-of-line) (point))))
            (ctr 1)
            (fill-spaces-number 0))
        (goto-char beg)
        (while
            (progn
              (push (make-overlay (point) (point))
                    overlays)
              (overlay-put
               (car overlays)
               'after-string (format "%s %2d " (make-string fill-spaces-number 32) ctr))
              (setq ctr (1+ ctr)
                    fill-spaces-number 0)
              (forward-line)
              (let ((linewidth (- (progn (end-of-line) (point))
                                  (progn (beginning-of-line) (point)))))
                (if (< col linewidth)
                    (forward-char col)
                  (end-of-line)
                  (setq fill-spaces-number (- col linewidth))))
              (< (point) end)))))
    (sit-for 9)
    (mw-ephemeral-delete-overlays))

  (defun mw-ephemeral-rectangle-enumerate-lines-timer (beg end)
    "Enumerate the region lines in the column of BEG.
The enumeration disappears on any keypress or after a few
seconds.  Argument END is the other end of the region."
    (interactive "r")
    (save-excursion
      (let ((col (- (progn (goto-char beg) (point))
                    (progn (beginning-of-line) (point))))
            (ctr 1)
            (fill-spaces-number 0))
        (goto-char beg)
        (while
            (progn
              (push (make-overlay (point) (point))
                    overlays)
              (overlay-put
               (car overlays)
               'after-string (format "%s %2d " (make-string fill-spaces-number 32) ctr))
              (setq ctr (1+ ctr)
                    fill-spaces-number 0)
              (forward-line)
              (let ((linewidth (- (progn (end-of-line) (point))
                                  (progn (beginning-of-line) (point)))))
                (if (< col linewidth)
                    (forward-char col)
                  (end-of-line)
                  (setq fill-spaces-number (- col linewidth))))
              (< (point) end)))))
    (run-with-timer 9 nil #'mw-ephemeral-delete-overlays))

  (defun mw-ephemeral-enumerate-visual-lines ()
    "Enumerate the visual lines in the window.
The numbering disappears when the user does soething in the
window or after some time.

Check the term visual line in case you are uncertain.

This enumeration can help to find a prefix argument to use
command `move-to-window-line'."
    (interactive)
    (let ((n 0)
          (lnmax-pos (save-excursion (move-to-window-line -2)))
          (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
      (save-excursion
        (while (and (<= (move-to-window-line n) lnmax-pos)
                    (< n line-number-enumerate-max))
          (push (make-overlay (point) (progn (end-of-visual-line) (point))) overlays)
          (overlay-put
           (car overlays) 'before-string
           (format "%3d " (1+ n)))
          (setq n (1+ n))))
      (sit-for 23)
      (mw-ephemeral-delete-overlays)))

  (defun mw-ephemeral-enumerate-visual-lines-in-region (beg end)
    "Enumerate the visual lines down starting with the line containing point."
    (interactive "r")
    (let* ((point-on-top-window-line
            (save-excursion
              (move-to-window-line 0)
              (point)))
           (point-on-bottom-window-line
            (save-excursion
              (move-to-window-line -1)
              (point)))
           (top-line
            (save-excursion
              (goto-char (max beg point-on-top-window-line))
              (mw-visual-line-number-with-point)))
           (bottom-line
            (save-excursion
              (goto-char (min end point-on-bottom-window-line))
              (mw-visual-line-number-with-point))))
      (save-excursion
        (move-to-window-line top-line)
        (push (make-overlay (point) (point)) overlays)
        (overlay-put
         (car overlays) 'after-string
         (format "%3d " 1))
        (let ((n 2))
          (while (<= n (1+ (- bottom-line top-line)))
            (move-to-window-line (1- (+ n top-line)))
            (push (make-overlay (point) (point)) overlays)
            (overlay-put
             (car overlays) 'after-string
             (format "%3d " n))
            (setq n (1+ n))))))
    (sit-for 23)
    (mw-ephemeral-delete-overlays))

  (defalias 'mw-region-ephemeral-enumerate-visual-lines
    'mw-ephemeral-enumerate-visual-lines-in-region)

  (defun mw-ephemeral-enumerate-visual-lines-relative-to-point ()
    "Enumerate the visual lines in the window relative to point."
    (interactive)
    (let ((n 0)
          (lnwp (mw-visual-line-number-with-point))
          (line-number-enumerate-max (save-excursion (move-to-window-line -1))))
      (save-excursion
        (while (not (= line-number-enumerate-max (move-to-window-line n)))
          (push (make-overlay (point) (point)) overlays)
          (overlay-put
           (car overlays) 'after-string
           (format "%3d " (- n lnwp)))
          (setq n (1+ n)))
        (when (not (= (point-max) (progn (move-to-window-line -1) (point))))
          (push (make-overlay (point) (point)) overlays)
          (overlay-put
           (car overlays) 'after-string
           (format "%3d " (- n lnwp)))))
      (sit-for 23)
      (mw-ephemeral-delete-overlays)))

  (defun mw-ephemeral-enumerate-visual-lines-dist-to-border ()
    "Enumerate the visible lines in the window with distance to
borders.

This enumeration can help to find a prefix argument to use
command `move-to-window-line'.
"
    (interactive)
    (let ((n 0)
          (window-lines (mw-count-window-lines))
          (half-window-lines (/ (mw-count-window-lines) 2)))
      (while (<= n half-window-lines)
        (save-excursion
          (move-to-window-line n)
          (push (make-overlay (point) (point)) overlays)
          (overlay-put
           (car overlays) 'after-string
           (format "%3d " n)))
        (let ((m (- (1+ n))))
          (if (or (< n half-window-lines)
                  (oddp window-lines))
              (save-excursion
                (move-to-window-line m)
                (push (make-overlay (point) (point)) overlays)
                (overlay-put
                 (car overlays) 'after-string
                 (format "%3d " m)))))
        (setq n (1+ n)))
      (sit-for 23)
      (mw-ephemeral-delete-overlays))))
;; Decorate buffer with various information but just ephemerally.:1 ends here

;; duplicate line
;; :PROPERTIES:
;; :ID:       5dda19da-444d-4fc8-a80f-f570de6ef9c0
;; :END:


;; [[file:init.org::*duplicate line][duplicate line:1]]
(defun mw-duplicate-line ()
  "Duplicate the line containing point."
  (interactive)
  (let ((line (buffer-substring
               (point-at-bol) (point-at-eol))))
    (forward-line)
    (insert line "\n")
    (forward-line -1)))
;; duplicate line:1 ends here

;; [[file:init.org::*duplicate line][duplicate line:2]]
(defun mw-duplicate-line-as-term ()
  "Append equal sign and newline and duplicate the line containing point."
  (interactive)
  (beginning-of-line)
  (skip-chars-forward " =\t")
  (let ((content (buffer-substring
                  (point) (point-at-eol))))
    (end-of-line)
    (newline)
    (insert "= ")
    (let ((rempt (point)))
      (insert content)
      (push-mark rempt t t))))
;; duplicate line:2 ends here

;; duplicate region as term


;; [[file:init.org::*duplicate region as term][duplicate region as term:1]]
(defun mw-region-duplicate-as-term (beg end)
  "Prepend copy of region BEG END followed by equal sign.
See the region via C-xC-x."
  (interactive "r")
  (when (use-region-p)
    (let ((sep " = ")
          (content (buffer-substring
                    beg end)))
      (goto-char end)
      (insert sep content)
      (push-mark (+ (length sep) end) nil t))))
;; duplicate region as term:1 ends here

;; get day of the week for date
;; :PROPERTIES:
;; :ID:       29bca59f-ab8b-4350-9167-eb356704cf4a
;; :END:


;; [[file:init.org::*get day of the week for date][get day of the week for date:1]]
(defun mw-day-of-week (date)
  "Return day of week as number 0 means Sunday.
DATE is a list like (2018 9 23) (Year Month Day).

Example: (mw-day-of-week '(2018 9 23))"
  (calendar-day-of-week (append (cdr date) (list (car date)))))
;; get day of the week for date:1 ends here

;; save text property images in files
;; :PROPERTIES:
;; :ID:       7619c8df-c78f-4bf7-8ed3-f321ed57574b
;; :END:

;; Save images given as data in text-properties to hash directory.


;; [[file:init.org::*save text property images in files][save text property images in files:1]]
(defvar image-to-file-sha1-hashed-directory "~/h"
  "Directory for files named with their hash for .")
;; save text property images in files:1 ends here

;; [[file:init.org::*save text property images in files][save text property images in files:2]]
(defun save-text-property-image-to-hashed-name (text-properties)
  "Write image given inline in TEXT-PROPERTIES to hash directory."
  (when-let* ((display-data (cdr (plist-get text-properties 'display)))
              (data (plist-get display-data :data)))
    (let ((filename
           (concat image-to-file-sha1-hashed-directory (sha1 data) ".someimage")))
      (if (file-exists-p filename)
          (message "Filename %s exists.  Writing nothing." filename)
        (with-temp-file filename
          (setq buffer-file-coding-system 'binary)
          (set-buffer-multibyte nil)
          (insert data))
        (message "Written %s" filename)))))

(defun save-text-property-images-in-region-to-hashed-files (beg end)
  "Copy text-property image data to files in region BEG END.
The files get the sha1 hash of their content as name."
  (interactive "r")
  ;; I guess this should be refined.  todo: check out the
  ;; text-properties world!
  (when (plist-get (text-properties-at beg) 'display)
    (save-text-property-image-to-hashed-name (text-properties-at beg)))
  (let ((pos (next-property-display-data beg end)))
    (while (< pos end)
      (save-text-property-image-to-hashed-name (text-properties-at pos))
      (setq pos (next-property-display-data pos end)))))
;; save text property images in files:2 ends here

;; Functions for operating on the last number in a string.
;; :PROPERTIES:
;; :ID:       33b8105a-a1cb-42c5-8832-4f310a384e69
;; :END:


;; [[file:init.org::*Functions for operating on the last number in a string.][Functions for operating on the last number in a string.:1]]
(defvar mw-fill-with-zeros t
  "nil means to neither introduce nor respect given leading zeros
  in numbers for the string operations.  Non-nil means to keep
  the width for a given number by using the respective number of
  leading zeros.")
;; Functions for operating on the last number in a string.:1 ends here

;; [[file:init.org::*Functions for operating on the last number in a string.][Functions for operating on the last number in a string.:2]]
(defun mw-increment-last-number (string)
  "Increase the last number in STRING by one."
  (mw-operate-on-last-number string '1+))

(defun mw-decrement-last-number (string)
  "Decrease the last number in STRING by one."
  (mw-operate-on-last-number string '1-))

(defun mw-operate-on-last-number (string fun)
  "Apply FUN on the last number in STRING."
  (when (string-match "\\([[:digit:]]+\\)[^[:digit:]]*$" string)
    (let* ((number-string (match-string 1 string))
	   (operated-number-string
	    (number-to-string (funcall fun (string-to-number number-string)))))
      (replace-match
       (concat
        (if mw-fill-with-zeros
            (let ((length-diff
                   (- (length number-string)
                      (length operated-number-string))))
              (if (< 0 length-diff)
                  (make-string length-diff ?0)
                ""))
          "")
        operated-number-string)
       nil nil string 1))))
;; Functions for operating on the last number in a string.:2 ends here

;; visible-mode control for ediff
;; :PROPERTIES:
;; :ID:       49cffd80-cf3d-4c02-89fd-c9e1cb97b3d2
;; :END:

;; Enable visible-mode for the buffers in a ediff session.  This can be
;; helpful e.g. for org-mode buffers which often have a lot of invisible
;; parts.


;; [[file:init.org::*visible-mode control for ediff][visible-mode control for ediff:1]]
(defun mw-ediff-set-visible-mode-in-ediff-buffers ()
"Set `visible-mode' in all buffers of a ediff session.
Only functional in an ediff control buffer."
  (interactive)
  (ediff-barf-if-not-control-buffer)
  (message "ediff buffers set to visible-mode to reveal hidden parts.
M-x visible-mode toggles visible-mode for a buffer.")
  (when ediff-buffer-A (save-excursion (set-buffer ediff-buffer-A)
                                       (visible-mode)))
  (when ediff-buffer-B (save-excursion (set-buffer ediff-buffer-B)
                                       (visible-mode)))
  (when ediff-buffer-C (save-excursion (set-buffer ediff-buffer-C)
                                       (visible-mode))))
;; visible-mode control for ediff:1 ends here

;; Switch Comma and Dot
;; :PROPERTIES:
;; :ID:       1b1715bf-de03-4445-9e2d-017cf9a31d5d
;; :END:

;; Sometimes it helps to have a tool to change 5.000,00 to 5,000.00
;; quickly.  In Germany they often use 5.000,00 while in the US they use
;; 5,000.00.

;; Concretely this has been useful for bringing some numbers into the
;; right format for the ledger program.


;; [[file:init.org::*Switch Comma and Dot][Switch Comma and Dot:1]]
(defun mw-switch-comma-and-dot-in-string (string-with-number)
  (with-temp-buffer
    (insert string-with-number)
    (goto-char (point-min))
    (while (search-forward-regexp "\[.,\]" nil t)
      (let ((insert-char (if (equal ?. (char-before)) ?, ?.)))
        (delete-char -1)
        (insert insert-char)))
    (buffer-string)))
;; Switch Comma and Dot:1 ends here

;; [[file:init.org::*Switch Comma and Dot][Switch Comma and Dot:2]]
(defun mw-switch-comma-and-dot-in-region (beg end)
  "Replace . with , and vice versa in region."
  (interactive "r")
  (let ((converted (mw-switch-comma-and-dot-in-string (buffer-substring beg end))))
    (delete-region beg end)
    (goto-char beg)
    (insert converted)))

(defalias 'mw-region-switch-comma-and-dot 'mw-switch-comma-and-dot-in-region)
;; Switch Comma and Dot:2 ends here

;; set a line as top line
;; :PROPERTIES:
;; :ID:       e5dd3315-8cc1-41ad-9cf1-6432a6a71b07
;; :END:


;; [[file:init.org::*set a line as top line][set a line as top line:1]]
(defun mw-recenter-jump-to-top ()
  (interactive)
  (advice-add 'avy-goto-line :after (lambda () (recenter 0)))
  (avy-goto-line)
  (advice-remove 'avy-goto-line (lambda () (recenter 0))))
;; set a line as top line:1 ends here

;; html copy for gnus articles
;; :PROPERTIES:
;; :ID:       ee306c3b-65f3-4c1a-9690-eba5c58dff9e
;; :END:

;; define key C-cx M-w as in eww buffers.


;; [[file:init.org::*html copy for gnus articles][html copy for gnus articles:1]]
(with-eval-after-load "gnus-art"
  (add-hook 'gnus-article-mode-hook
             (lambda ()
               (define-key gnus-article-mode-map "\C-c\C-x\M-w" 'org-eww-copy-for-org-mode))))
;; html copy for gnus articles:1 ends here

;; real kill
;; :PROPERTIES:
;; :ID:       2ff4a92f-b510-4fcd-a17e-6cd70f3c5da5
;; :END:

;; Real kill of region, not this 'play it save and put the delete into
;; kill-ring' kindergarden stuff.


;; [[file:init.org::*real kill][real kill:1]]
(global-set-key (kbd "\C-c w") #'delete-region)
;; real kill:1 ends here

;; hide dot files in dired
;; :PROPERTIES:
;; :ID:       4aa01cc3-605b-4b11-81ee-8bed80cd7b9f
;; :END:

;; a function to hide files starting with a dot in dired.


;; [[file:init.org::*hide dot files in dired][hide dot files in dired:1]]
(defun mw-dired-hide-dot-files ()
  "Hide the files starting with a dot in dired."
  (interactive)
  (unless (eq 'dired-mode major-mode)
    (user-error
     (concat
      "Buffer is not in dired-mode.  "
      "Command applies only in dired-mode")))
  (save-excursion
    (dired-mark-files-regexp "^\\.")
    (dired-do-kill-lines)))
;; hide dot files in dired:1 ends here

;; access to dict.leo.org :weak:
;; :PROPERTIES:
;; :ID:       29eb9c88-24c3-4159-b51f-ea597b9523ab
;; :END:

;; translate using leo.org.


;; [[file:init.org::*access to dict.leo.org][access to dict.leo.org:1]]
(defun mw-ask-leo-about-word-at-point ()
  "Call the leo translation page for the word at point."
  (interactive)
  (mw-ask-leo (word-at-point)))

(defun mw-search-leo-relevant-section ()
  (search-forward-regexp "# +suchwort")
  (remove-hook 'eww-after-render-hook 'mw-search-leo-relevant-section))

(defun mw-ask-leo (arg)
  "Call the leo translation page for a string.
Use region as default string."
  (interactive
   (let ((text-region
          (when (region-active-p)
            (buffer-substring (region-beginning) (region-end)))))
     (list (read-from-minibuffer
       "word: " text-region))))
  (add-hook 'eww-after-render-hook 'mw-search-leo-relevant-section)
  (eww (format "https://pda.leo.org/englisch-deutsch/%s" arg)))
;; access to dict.leo.org:1 ends here

;; org-mind-map
;; :PROPERTIES:
;; :ID:       232a7c1b-5f5b-44ef-bd2b-744969b505b7
;; :END:

;; tool to visualize orgs as graphs.


;; [[file:init.org::*org-mind-map][org-mind-map:1]]
(add-to-list 'load-path "~/s/elisp/external/org-mind-map")
;(require 'org-mind-map)
;; org-mind-map:1 ends here

;; display live keystrokes in emacs
;; :PROPERTIES:
;; :ID:       31fd797b-cf44-4486-8927-924a1283c62d
;; :END:


;; [[file:init.org::*display live keystrokes in emacs][display live keystrokes in emacs:1]]
(add-to-list 'load-path "~/s/elisp/mw/keystrokes/")
(require 'keystrokes)
;; display live keystrokes in emacs:1 ends here

;; add a key to feh for images
;; :PROPERTIES:
;; :ID:       240b5fcc-be09-464e-aeb7-8980f675f89b
;; :END:


;; [[file:init.org::*add a key to feh for images][add a key to feh for images:1]]
(with-eval-after-load
    'image
  (define-key image-map "e" (lambda () (interactive) (async-shell-command (concat "feh " (buffer-file-name))))))
(with-eval-after-load
    'dired
  (define-key dired-mode-map "f" (lambda () (interactive) (async-shell-command (concat "feh " (dired-get-file-for-visit))))))
;; add a key to feh for images:1 ends here

;; add keys to resize images to window-width and -height
;; :PROPERTIES:
;; :ID:       1245dee6-b64b-46f6-8686-fd349f8b4d59
;; :END:


;; [[file:init.org::*add keys to resize images to window-width and -height][add keys to resize images to window-width and -height:1]]
(defun mw-image-change-width-to-window-width ()
"Resize image width to match window-width."
  (interactive)
  (let* ((image (image--get-image))
         (new-image (image--image-without-parameters image)))
    (setcdr image (cdr new-image))
    (plist-put (cdr image) :width (nth 2 (window-inside-pixel-edges)))))

(defun mw-image-change-height-to-window-height ()
"Resize image height to match window-height."
  (interactive)
  (let* ((image (image--get-image))
         (new-image (image--image-without-parameters image)))
    (setcdr image (cdr new-image))
    (plist-put (cdr image) :height (nth 3 (window-inside-pixel-edges)))))
;; add keys to resize images to window-width and -height:1 ends here

;; [[file:init.org::*add keys to resize images to window-width and -height][add keys to resize images to window-width and -height:2]]
(define-key image-map "W" #'mw-image-change-width-to-window-width)
(define-key image-map "H" #'mw-image-change-height-to-window-height)
;; add keys to resize images to window-width and -height:2 ends here

;; slim lisp butts
;; :PROPERTIES:
;; :ID:       9220397c-7279-48db-aaf9-d09ff7adddca
;; :END:


;; [[file:init.org::*slim lisp butts][slim lisp butts:1]]
(push "~/s/elisp/mw/lisp-butt-mode" load-path)
(require 'lisp-butt-mode)
;; slim lisp butts:1 ends here

;; set visible mode for ediffed buffers
;; :PROPERTIES:
;; :ID:       95a7af9e-1050-461b-8324-96fc103d15c3
;; :END:


;; [[file:init.org::*set visible mode for ediffed buffers][set visible mode for ediffed buffers:1]]
(add-hook 'ediff-keymap-setup-hook
          (lambda ()
            (define-key
              ediff-mode-map "8"
              #'mw-ediff-set-visible-mode-in-ediff-buffers)))
;; set visible mode for ediffed buffers:1 ends here

;; from dired to ediff
;; :PROPERTIES:
;; :ID:       afbf5575-7fac-4e9f-a71c-b644c19c77ab
;; :END:


;; [[file:init.org::*from dired to ediff][from dired to ediff:1]]
(add-hook 'dired-mode-hook
          (lambda ()
            (define-key dired-mode-map "E" #'dired-ediff)))

(defun dired-ediff ()
  "Run ediff on the files at point and mark from dired."
  (interactive)
  (let
      ((file (dired-get-filename t))
       (other (if (and transient-mark-mode mark-active)
                  (save-excursion (goto-char (mark t))
                                  (dired-get-filename t t)))))
    (when (and file other (not (equal file other)))
      (ediff file other))))
;; from dired to ediff:1 ends here

;; from diff to ediff
;; :PROPERTIES:
;; :ID:       ec3fdb80-6d3f-486f-bdbe-c4bb0304aad3
;; :END:


;; [[file:init.org::*from diff to ediff][from diff to ediff:1]]
(add-hook 'diff-mode-hook (lambda () (define-key diff-mode-map "E" #'mw-diff-to-ediff)))

(defun mw-diff-to-ediff ()
  "Run ediff for the diff in the buffer."
  (interactive)
  (ediff
   (diff-find-file-name nil nil)
   (diff-find-file-name t nil)))
;; from diff to ediff:1 ends here

;; from ediff to diff
;; :PROPERTIES:
;; :ID:       c2d1588d-41b0-42ce-a1cc-8e994d642833
;; :END:

;; - see also buffer *diff-ediff* which seems to be created by ediff somehow.
;; - possibly just go there and activate diff-mode.


;; [[file:init.org::*from ediff to diff][from ediff to diff:1]]
(defun mw-ediff-to-diff ()
  "Run diff for the ediff."
  (interactive)
  (ediff-barf-if-not-control-buffer)
  (and ediff-buffer-A ediff-buffer-B
       (diff-buffers ediff-buffer-A ediff-buffer-B)))
;; from ediff to diff:1 ends here

;; org-section-numbers :org:
;; :PROPERTIES:
;; :ID:       a613b467-845b-48db-a416-0d62f4d7e627
;; :END:

;; note: there is also org-num-mode which is much more dynamic.

;; keeping org-section-numbers because it supports narrowing in the sense
;; that the numbering starts at the beginning of the narrow.


;; [[file:init.org::*org-section-numbers][org-section-numbers:1]]
(push "~/s/elisp/mw/org-section-numbers" load-path)
(require 'org-section-numbers)
;; org-section-numbers:1 ends here

;; filler text
;; :PROPERTIES:
;; :ID:       01006958-0cf9-4de3-ad86-4bc202850cfd
;; :catchwords: blindtext
;; :END:


;; [[file:init.org::*filler text][filler text:1]]
(defun mw-insert-filler-text (&optional arg)
  "Insert filler-text.  With number ARG make that much copies."
  (interactive "p")
  (let ((text "Lorem ipsum dolor sit amet,
consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation
ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate
velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt
in culpa qui officia deserunt mollit anim id est laborum."))
    (insert text)
    (when arg
      (cl-assert (< 0 arg))
      (dotimes (_ (1- arg))
        (newline)
        (newline)
        (insert text)))))
;; filler text:1 ends here

;; end of meta data
;; :PROPERTIES:
;; :ID:       21271394-b13c-46c4-85e0-6b280eafe354
;; :END:


;; [[file:init.org::*end of meta data][end of meta data:1]]
(defun mw-org-end-of-meta-data ()
  "Move point to the end of the meta data of current orgee."
  (interactive)
  (org-end-of-meta-data t)
  (skip-chars-backward "\n\t ")
  (forward-char))
;; end of meta data:1 ends here

;; emacsed :ed:
;; :PROPERTIES:
;; :ID:       d537f571-9b20-4594-bdc7-371927dd3a39
;; :END:

;; bring the line editor ed package into emacs.


;; [[file:init.org::*emacsed][emacsed:1]]
(push "~/s/elisp/external/emacsed" load-path)
(require 'ed "emacsed")
;; emacsed:1 ends here

;; screen
;; :PROPERTIES:
;; :ID:       4bf02c92-06c4-4ca2-aa65-dcc906528f63
;; :END:

;; Seamless exchange with screen.


;; [[file:init.org::*screen][screen:1]]
(defvar mw-screen-exchange-filename
  "/tmp/screen-exchange"
  "Name of the file used by screen copy and paste.")
;; screen:1 ends here

;; screen like commands for slurp and write
;; :PROPERTIES:
;; :ID:       6499898f-fec7-4123-8d85-83cc1bde1f04
;; :END:


;; [[file:init.org::*screen like commands for slurp and write][screen like commands for slurp and write:1]]
(defun mw-screen-exchange-slurp-insert ()
  (interactive)
  (insert-file-contents mw-screen-exchange-filename))

(defun mw-screen-exchange-write-region (start end)
  (interactive "r")
  (write-region start end mw-screen-exchange-filename))
;; screen like commands for slurp and write:1 ends here

;; editing the screen-exchange file
;; :PROPERTIES:
;; :ID:       f5354f6b-9262-44a4-841c-71902283a01a
;; :END:


;; [[file:init.org::*editing the screen-exchange file][editing the screen-exchange file:1]]
(defun mw-screen-exchange-open-buffer ()
  "Open the screen exchange file in auto revert mode."
  (interactive)
  (set-buffer (find-file mw-screen-exchange-filename))
  (auto-revert-mode))
;; editing the screen-exchange file:1 ends here

;; [[file:init.org::*get a typical version as list][get a typical version as list:2]]
(defun mw-parse-version-string (version-string)
  "Return three dot separated numbers as list."
  (unless  (string-match
            (rx bol (group (one-or-more digit))
                "." (group (one-or-more digit))
                "." (group (one-or-more digit))
                eol)
            version-string)
    (user-error "version-string non in format like \"1.2.3\"."))
  (mapcar (lambda (x)
            (string-to-number
             (match-string x version-string)))
          '(1 2 3)))
;; get a typical version as list:2 ends here

;; set background in all frames
;; :PROPERTIES:
;; :ID:       b85b9573-95e2-43b2-a142-a2b9636e38e4
;; :END:


;; [[file:init.org::*set background in all frames][set background in all frames:1]]
(defun set-background-color-all-frames (color-name)
  "Set the background color of all frames to COLOR-NAME."
  (interactive (list (read-color "Background color: ")))
  (let ((recent-frame (selected-frame)))
    (mapc
     (lambda (frame)
       (select-frame frame)
       (set-background-color color-name))
     (frame-list))
    (select-frame (select-frame recent-frame))))
;; set background in all frames:1 ends here

;; supercite
;; :PROPERTIES:
;; :ID:       43eb86e0-6897-4030-b518-3069c68843b9
;; :END:


;; [[file:init.org::*supercite][supercite:1]]
(require 'supercite)
;; supercite:1 ends here



;; #+name: minibuffer-frame

;; [[file:init.org::minibuffer-frame][minibuffer-frame]]
(defun mw-minibuffer-frame ()
  "Return the frame named ' *Minibuf-0*'."
  (let ((minibuf-frame-list
         (cl-remove-if-not
          (lambda (frame)
            (eq 'only (frame-parameter frame 'minibuffer)))
          (frame-list))))
    (cl-assert (= 1 (cl-list-length minibuf-frame-list)))
    (car minibuf-frame-list)))
;; minibuffer-frame ends here

;; [[file:init.org::*set font height in minibuffer frame][set font height in minibuffer frame:3]]
(defun mw-minibuffer-frame ()
  "Return the frame named ' *Minibuf-0*'."
  (let ((minibuf-frame-list
         (cl-remove-if-not
          (lambda (frame)
            (eq 'only (frame-parameter frame 'minibuffer)))
          (frame-list))))
    (cl-assert (= 1 (cl-list-length minibuf-frame-list)))
    (car minibuf-frame-list)))

(defun mw-set-height-in-minibuffer-frame (arg)
  "Set height of font in minibuffer frame to ARG."
  (interactive "p")
  (set-face-attribute
   'default
   (mw-minibuffer-frame)
   :height arg))
;; set font height in minibuffer frame:3 ends here

;; agenda in other frame
;; :PROPERTIES:
;; :ID:       36e5a0ee-928d-4ba9-ad8d-f1ba0adc6859
;; :END:


;; [[file:init.org::*agenda in other frame][agenda in other frame:1]]
(defun mw-org-agenda-list-in-other-frame ()
  "Open agenda list in other frame."
  (let ((org-agenda-window-setup 'other-frame))
    (org-agenda-list)))
;; agenda in other frame:1 ends here

;; [[file:init.org::*insert a zero width whitespace][insert a zero width whitespace:2]]
(defun mw-insert-zero-width-whitespace ()
  "Insert a space with zero width."
  (interactive)
  (insert ?\x200B))
;; insert a zero width whitespace:2 ends here

;; lyrics
;; :PROPERTIES:
;; :ID:       29d33f5c-1305-4ebb-a3b8-3410ef994a23
;; :END:


;; [[file:init.org::*lyrics][lyrics:1]]
(straight-use-package 'lyrics)
;; lyrics:1 ends here

;; debbugs
;; :PROPERTIES:
;; :ID:       db22be24-51ad-4d11-8b65-bc3d18779733
;; :END:


;; [[file:init.org::*debbugs][debbugs:1]]
(straight-use-package 'debbugs)
;; debbugs:1 ends here

;; mode for reddit
;; :PROPERTIES:
;; :ID:       75894476-2f66-49e5-b784-ae2f9f92c017
;; :END:

;; - note :: the name of the package is that crazy because of brand name
;;   usage laws IIUC.


;; [[file:init.org::*mode for reddit][mode for reddit:1]]
(straight-use-package 'md4rd)
;; mode for reddit:1 ends here

;; org-sink
;; :PROPERTIES:
;; :ID:       a5eaa753-fb10-460e-9e39-bd998e499e4f
;; :END:


;; [[file:init.org::*org-sink][org-sink:1]]
(defun org-sink ()
  (interactive)
  (org-move-subtree-down)
  (org-demote-subtree)
  (mw-org-subtree-make-first-sibling))
;; org-sink:1 ends here

;; org habit quick toggle show-all-today-toggle
;; :PROPERTIES:
;; :ID:       959d255a-2008-49ee-8029-87dae5d84d6d
;; :END:

;; this is just a shortcut to toggle org-habit-show-all-today-toggle.


;; [[file:init.org::*org habit quick toggle show-all-today-toggle][org habit quick toggle show-all-today-toggle:1]]
(defmacro togglef (place)
  "Toggle PLACE.  non-nil becomes nil, nil becomes t.
PLACE may be a symbol, or any generalized variable allowed by ‘setf’.
The return value is the new value of PLACE."
  `(setf ,place (not ,place)))

(defun mw-org-agenda-habit-show-all-today-toggle ()
  "Toggle between display all habits or only habits still undone today."
  (interactive)
  (org-agenda-check-type t 'agenda)
  (togglef org-habit-show-all-today)
  (org-agenda-redo))
;; org habit quick toggle show-all-today-toggle:1 ends here

;; [[file:init.org::*org habit quick toggle show-all-today-toggle][org habit quick toggle show-all-today-toggle:2]]
(defun org-habit-toggle-display-in-agenda (arg)
"Toggle display of habits in agenda.
With ARG toggle display of habits only for today."
    (interactive "P")
  (if arg
      (mw-org-agenda-habit-show-all-today-toggle)
    (org-habit-toggle-habits)))

(with-eval-after-load 'org-agenda
  (with-eval-after-load 'org-habit
     (org-defkey org-agenda-mode-map "K"
                  #'org-habit-toggle-display-in-agenda)))
;; org habit quick toggle show-all-today-toggle:2 ends here

;; flycheck-package
;; :PROPERTIES:
;; :ID:       a7ccda13-936e-4d57-8cd2-06a186fd7b7b
;; :END:


;; [[file:init.org::*flycheck-package][flycheck-package:1]]
(straight-use-package 'flycheck-package)
;; flycheck-package:1 ends here

;; rase
;; :PROPERTIES:
;; :ID:       840cc11e-5a99-49d6-a091-72fae9c03e0d
;; :END:

;; =rase= is for triggering actions at sunrise and sunset.


;; [[file:init.org::*rase][rase:1]]
(straight-use-package 'rase)
;; rase:1 ends here

;; via toggle fg/bg
;; :PROPERTIES:
;; :ID:       4fbeaf9d-c6f3-42ce-84ef-d28f4335fc0b
;; :END:


;; [[file:init.org::*via toggle fg/bg][via toggle fg/bg:1]]
(defun mw-rase-fg-bg-adaption (sun-event &optional first-run)
  (cond
   (first-run (let ((solar-rise-set  (solar-sunrise-sunset (calendar-current-date)))
                    (time-of-day (mw-current-time-of-day-decimal)))
                (set-face-background 'org-block "white")
                (when (or (< time-of-day (caar solar-rise-set))
                        (<= (caadr solar-rise-set) time-of-day))
                  (mw-toggle-foreground-background-all-frames)
                  (set-face-background 'org-block "black"))))
   ((member sun-event '(sunrise sunset))
    (mw-toggle-foreground-background-all-frames)
    (cond
     ((eq sun-event 'sunrise) (set-face-background 'org-block "white"))
     ((eq sun-event 'sunset) (set-face-background 'org-block "black"))))
   (t (message "a rase event gets ignored.  not completely because e.g. of this message"))))

(add-hook 'rase-functions #'mw-rase-fg-bg-adaption)
;; via toggle fg/bg:1 ends here

;; [[file:init.org::*via toggle fg/bg][via toggle fg/bg:2]]
(add-hook 'emacs-startup-hook
          (lambda ()
            (rase-start t)))
;; via toggle fg/bg:2 ends here

;; narrow-to-below, narrow-to-above
;; :PROPERTIES:
;; :ID:       3b102193-ad89-416b-bd91-1ed9d864d46f
;; :END:


;; [[file:init.org::*narrow-to-below, narrow-to-above][narrow-to-below, narrow-to-above:1]]
(defun mw-narrow-to-below ()
  "Narrow to the half below the cursor."
  (interactive)
  (narrow-to-region (point) (point-max)))

(defun mw-narrow-to-above ()
  "Narrow to half above the cursor."
  (interactive)
  (narrow-to-region (point-min) (point)))
;; narrow-to-below, narrow-to-above:1 ends here

;; [[file:init.org::*narrow-to-below, narrow-to-above][narrow-to-below, narrow-to-above:2]]
(define-key narrow-map ">" #'mw-narrow-to-below)
(define-key narrow-map "<" #'mw-narrow-to-above)
;; narrow-to-below, narrow-to-above:2 ends here

;; narrow-to-line
;; :PROPERTIES:
;; :ID:       88bdb1d7-7e34-4255-8ebd-428692413a4b
;; :END:


;; [[file:init.org::*narrow-to-line][narrow-to-line:1]]
(defun mw-narrow-to-line ()
"Narrow to line containing point."
(interactive)
(save-excursion
  (narrow-to-region
   (progn (beginning-of-line) (point))
   (progn (end-of-line) (point)))))
;; narrow-to-line:1 ends here

;; [[file:init.org::*narrow-to-line][narrow-to-line:2]]
(define-key narrow-map "l" #'mw-narrow-to-line)
;; narrow-to-line:2 ends here

;; [[file:init.org::*narrow-to-line][narrow-to-line:3]]
(defun mw-narrow-to-next-line ()
  "Narrow to next line.
Widens buffer before advancing."
  (interactive)
  (widen)
  (forward-visible-line 1)
  (narrow-to-region
   (point)
   (progn (end-of-line) (point)))
  (beginning-of-line))

(defun mw-narrow-to-previous-line ()
  "Narrow to previous line.
Widens buffer before advancing."
  (interactive)
  (widen)
  (forward-visible-line -1)
  (narrow-to-region
   (point)
   (progn (end-of-line) (point)))
  (beginning-of-line))
;; narrow-to-line:3 ends here

;; [[file:init.org::*narrow-to-line][narrow-to-line:4]]
(defun mw-narrow-to-next-line-repeat ()
  "Wrapper around `mw-narrow-to-next-line' for easy repeating."
  (interactive)
  (mw-narrow-to-next-line)
  (message "Use v to repeat.")
  (set-transient-map
   (let ((map (make-sparse-keymap)))
     (define-key map (kbd "v")
       (lambda () (interactive) (mw-narrow-to-next-line-repeat)))
     map)))
;; narrow-to-line:4 ends here

;; [[file:init.org::*narrow-to-line][narrow-to-line:5]]
(defun mw-narrow-to-previous-line-repeat ()
  "Wrapper around `mw-narrow-to-previous-line' for easy repeating."
  (interactive)
  (mw-narrow-to-previous-line)
  (message "Use ^ to repeat.")
  (set-transient-map
   (let ((map (make-sparse-keymap)))
     (define-key map (kbd "^")
       (lambda () (interactive) (mw-narrow-to-previous-line-repeat)))
     map)))
;; narrow-to-line:5 ends here

;; [[file:init.org::*narrow-to-line][narrow-to-line:6]]
(define-key narrow-map "v" #'mw-narrow-to-next-line-repeat)
(define-key narrow-map "^" #'mw-narrow-to-previous-line-repeat)
;; narrow-to-line:6 ends here

;; save buffer scratch at kill
;; :PROPERTIES:
;; :ID:       ac94820c-f8cb-4c17-8452-d99a18452168
;; :END:

;; save the content of the scratch buffer in a config variable.  hook
;; that in at the end of each Emacs session.


;; [[file:init.org::*save buffer scratch at kill][save buffer scratch at kill:1]]
(defun mw-save-scratch-buffer ()
  "Save the content of the scratch buffer in `initial-scratch-message'."
  (interactive)
  (when (get-buffer "*scratch*")
    (with-current-buffer "*scratch*"
      (widen)
      (customize-save-variable
       'initial-scratch-message
       (buffer-substring-no-properties (point-min) (point-max))))))

(add-hook 'kill-emacs-hook 'mw-save-scratch-buffer)
;; save buffer scratch at kill:1 ends here

;; start eshell top with one command
;; :PROPERTIES:
;; :ID:       9bca120b-e724-49f2-bdf3-7cbd4a431af1
;; :END:


;; [[file:init.org::*start eshell top with one command][start eshell top with one command:1]]
(defun mw-top ()
  "Start the top that comes up when called via eshell."
  (interactive)
  (require 'em-term)
  (eshell-exec-visual "top"))
;; start eshell top with one command:1 ends here

;; jump to first/last sibling :+org:
;; :PROPERTIES:
;; :ID:       f9c66acd-9754-4a23-9ccc-abd76c568698
;; :END:


;; [[file:init.org::*jump to first/last sibling :+org:][jump to first/last sibling :+org::1]]
(defun mw-org-jump-to-last-heading-same-level ()
  "Move to last sibling with same level."
(interactive)
  (let ((p (point)))
    (while (progn (org-forward-heading-same-level 1)
                  (< p (point)))
      (setq p (point)))))
;; jump to first/last sibling :+org::1 ends here

;; [[file:init.org::*jump to first/last sibling :+org:][jump to first/last sibling :+org::2]]
(defun mw-org-jump-to-first-heading-same-level ()
  "Move to last sibling with same level."
(interactive)
  (let ((p (point)))
    (while (progn (org-forward-heading-same-level -1)
                  (< (point) p))
      (setq p (point)))))
;; jump to first/last sibling :+org::2 ends here

;; control visibility of top level plain list :org:
;; :PROPERTIES:
;; :ID:       ee30dad7-7fe7-4c99-8781-db8f4d8bc353
;; :END:


;; [[file:init.org::*control visibility of top level plain list][control visibility of top level plain list:1]]
(defun mw-org-items-fold-top-level ()
"Set visibility of all plain list items to folded."
  (interactive)
  (let* ((struct (org-list-struct))
         (parents (org-list-parents-alist struct)))
    (mapc (lambda (e)
	    (org-list-set-item-visibility (car e) struct 'folded))
          parents)))

(defun mw-org-items-children-all ()
"Set visibility of all plain list items to children."
  (interactive)
  (let* ((struct (org-list-struct))
         (parents (org-list-parents-alist struct)))
    (mapc (lambda (e)
	    (org-list-set-item-visibility (car e) struct 'children))
          parents)))
;; control visibility of top level plain list:1 ends here

;; eavesdrop the keys
;; :PROPERTIES:
;; :ID:       594fd7e1-83d2-4460-a558-cc493ee4be5e
;; :END:


;; [[file:init.org::*eavesdrop the keys][eavesdrop the keys:1]]
(defun mw-message-key ()
  "Output for every key that key as message."
  (let* ((key (this-single-command-keys)))
    (message "key: %s" key)))
;; eavesdrop the keys:1 ends here

;; espeak with typing :fun:
;; :PROPERTIES:
;; :ID:       40e695b1-66d3-48c3-902f-70a6828cc5d1
;; :END:

;; espeak the words when typing.

;; this is highly experimental.


;; [[file:init.org::*espeak with typing][espeak with typing:1]]
(defun mw-espeak-previous-word ()
  (let ((async-shell-command-buffer 'new-buffer))
    (async-shell-command
     (format "espeak -v en -s 110 \"%s\""
             (buffer-substring
              (save-excursion
                (backward-word)
                (point))
              (point))))))

(defun mw-espeak-previous-word-at-certain-input ()
  "Speak word before point.
Consider setting async-shell-command-buffer to 'new-buffer."
  (let* ((key (this-single-command-keys)))
    (unless (equal [] key)
      (when (member (aref key 0) '(9 13 32))
        (mw-espeak-previous-word)))))
;; espeak with typing:1 ends here

;; provide week number for the diary
;; :PROPERTIES:
;; :ID:       f7252cf3-f1ce-4b64-ae88-b58fde4740eb
;; :END:

;; week number for diary.


;; [[file:init.org::*provide week number for the diary][provide week number for the diary:1]]
(defun mw-diary-week-of-year ()
  "Week number of year."
  (with-no-warnings (defvar date))
  (format "Week %d."
          (car (calendar-iso-from-absolute
                (calendar-absolute-from-gregorian date)))))
;; provide week number for the diary:1 ends here

;; provide day name for the diary
;; :PROPERTIES:
;; :ID:       ec434d4b-4e17-422d-be7a-101160717ec6
;; :END:

;; day name for diary via &%%(mw-diary-day-of-week).


;; [[file:init.org::*provide day name for the diary][provide day name for the diary:1]]
(defun mw-diary-day-of-week ()
  "Week number of year."
  (with-no-warnings (defvar date))
  (calendar-day-name date))
;; provide day name for the diary:1 ends here

;; replace sexp with its evaluation :el:
;; :PROPERTIES:
;; :ID:       748410ed-649f-4a89-af1d-8c757ab45ab9
;; :END:


;; [[file:init.org::*replace sexp with its evaluation][replace sexp with its evaluation:1]]
(defun mw-replace-eval-last-sexp ()
  "Replace last sexp with its evaluation."
  (interactive)
  (let ((o (point)))
    (eval-last-sexp '(4))
    (let ((len (- (point) o)))
      (backward-char len)
      (kill-sexp -1)
      (forward-char len))))
;; replace sexp with its evaluation:1 ends here

;; expand <n to "name" and such
;; :PROPERTIES:
;; :ID:       c5c68e34-c478-4e2d-a0c2-03f2d3c57da7
;; :END:

;; - see also skeletons for a similar thing.
;;   - get a inline source block quickly currently via skeleton on ~sbi~.


;; [[file:init.org::*expand <n to "name" and such][expand <n to "name" and such:1]]
;; (require 'org-tempo)
;; (with-eval-after-load "org-tempo"
;; (progn
;; (setq org-tempo-keywords-alist '((110 . "name") (76 . "latex") (72 . "html") (65 . "ascii") (105 . "index")))
;  (push '(?n . "name") org-tempo-keywords-alist)
;  (org-tempo-add-templates)))
;; expand <n to "name" and such:1 ends here

;; [[file:init.org::*expand <n to "name" and such][expand <n to "name" and such:2]]
(with-eval-after-load 'org-tempo
(progn
(tempo-define-template "org-src_name"
			 '("#+name: ")
			 "<n" "Insert Org src name" 'org-tempo-tags)
(tempo-define-template "org-src_emacs-lisp"
			 '("#+begin_src emacs-lisp"  n p
			  n "#+end_src" )
			 "<m" "Insert emacs-lisp block" 'org-tempo-tags)
(tempo-define-template "org-src_named-emacs-lisp"
			 '("#+name: " p  n
			   "#+begin_src emacs-lisp"   n
			  n "#+end_src" )
			 "<M" "Insert named emacs-lisp block" 'org-tempo-tags)
))
;; expand <n to "name" and such:2 ends here

;; carry-region
;; :PROPERTIES:
;; :ID:       1fe74b31-e22b-498b-9779-2e1649f50883
;; :END:


;; [[file:init.org::*carry-region][carry-region:1]]
(push "~/s/elisp/mw/carry-region" load-path)
(require 'carry-region)

(defalias 'mw-region-carry-toggle 'mw-carry-region-toggle "Alternative reliable name.")
;; carry-region:1 ends here

;; display org log narrowed in clone :org:
;; :PROPERTIES:
;; :ID:       edf8366b-8854-4fcb-8390-7bb0e4691041
;; :END:

;; See [[id:e5a42da8-fea8-4057-8b10-2b9ca1029c74][Display Org properties in other buffer]] for a way to do it.

;; Recall defun org-log-beginning.


;; [[file:init.org::*display org log narrowed in clone][display org log narrowed in clone:1]]
(defun mw-org-get-log-block ()
  "Return the (beg . end) range of the body of the logbook drawer or nil."
  (org-with-wide-buffer
   (unless (org-before-first-heading-p)
     (let ((log-beginning (org-log-beginning)))
       (if log-beginning
           (save-excursion
             (goto-char log-beginning)
             (let ((element (org-element-at-point)))
               (catch 'found
                 (while element
                   (when (eq 'drawer (car element))
                     (throw 'found (cons log-beginning (org-element-property :contents-end element))))
                   (setq element (plist-get (cadr element) :parent)))))))))))

(defun mw-org-show-log-in-clone ()
  "Display Org notes in a narrowed clone."
  (interactive)
  (assert (eq 'org-mode major-mode))
  (let ((range-block (mw-org-get-log-block)))
    (when range-block
      (clone-indirect-buffer-other-window nil t)
      (narrow-to-region
       (car range-block) (cdr range-block))
      (org-reveal))))
;; display org log narrowed in clone:1 ends here

;; [[file:init.org::*display org log narrowed in clone][display org log narrowed in clone:2]]
(defalias 'mw-org-log-show-in-clone 'mw-org-show-log-in-clone)
;; display org log narrowed in clone:2 ends here

;; pick current agenda filter
;; :PROPERTIES:
;; :ID:       252fe790-d35f-4559-b9dd-7da3f4edb374
;; :END:

;; [2016-05-25 Wed 19:26] I wanted this.  Practise shall show if this
;; helps some.


;; [[file:init.org::*pick current agenda filter][pick current agenda filter:1]]
(defun mw-org-agenda-store-current-filters-as-custom-agenda ()
  "Make current setting of agenda a custom agenda \"x\" ."
  (interactive)
  (org-add-agenda-custom-command
   `("x" "Custom agenda possibly stored by `mw-org-agenda-store-current-filters-as-custom-agenda'" agenda ""
     ((org-agenda-overriding-header
       "Custom agenda possibly stored by { M-x mw-org-agenda-store-current-filters-as-custom-agenda RET }")
      (org-agenda-tag-filter-preset
       ',org-agenda-tag-filter)
      (org-agenda-regexp-filter-preset
       ',org-agenda-regexp-filter)
      (org-agenda-category-filter-preset
       ',org-agenda-category-filter)))))
;; pick current agenda filter:1 ends here

;; insert response of url
;; :PROPERTIES:
;; :ID:       69a9bbac-3648-4b7c-90d6-1514c0ad92e0
;; :END:


;; [[file:init.org::*insert response of url][insert response of url:1]]
(defun mw-insert-url-response (url)
  "Insert response from url at point."
  (interactive
   (list (read-string "url: ")))
  (url-retrieve
   url
   (lambda (status url point buffer)
     (append-to-buffer buffer (point-min) (point-max)))
   (list url (point) (current-buffer))))
;; insert response of url:1 ends here

;; reverse-words
;; :PROPERTIES:
;; :ID:       dee2134c-0e50-404f-a68a-82124eb6616e
;; :END:


;; [[file:init.org::*reverse-words][reverse-words:1]]
(push "~/s/elisp/mw/reverse-words/dist" load-path)
(require 'reverse-words "reverse-words-0.0.0.el")
;; reverse-words:1 ends here

;; major-mode-stack
;; :PROPERTIES:
;; :ID:       8c39923f-b0af-4285-b770-a5ea7a464601
;; :END:

;; tool to stack major modes.


;; [[file:init.org::*major-mode-stack][major-mode-stack:1]]
(add-to-list 'load-path "~/s/elisp/mw/major-mode-stack")
(require 'major-mode-stack)
;; major-mode-stack:1 ends here

;; execute key-sequence in a thread
;; :PROPERTIES:
;; :ID:       7ca80875-980c-4bb8-b2ba-1bc96b592f43
;; :END:

;; from T.V.Raman.


;; [[file:init.org::*execute key-sequence in a thread][execute key-sequence in a thread:1]]
(defun emacspeak-wizards-execute-asynchronously (key)
  "Read key-sequence, then execute its command on a new thread."
  (interactive (list (read-key-sequence "Key Sequence: ")))
  (let ((l (local-key-binding key))
        (g (global-key-binding key)))
    (cond
     ((commandp l)
      (make-thread l)
      (message "Running %s on a new thread." l))
     ((commandp g)
      (make-thread g)
      (message "Running %s on a new thread." g))
     (t (error "%s is not bound to a command." key)))))
;; execute key-sequence in a thread:1 ends here

;; general src block
;; :PROPERTIES:
;; :ID:       ede48801-6fa7-4c33-ade7-5e23d16a2393
;; :END:


;; [[file:init.org::*general src block][general src block:1]]
(define-skeleton org-skeleton-src-block ""
  \n
  "#+begin_src "  _ \n
  "#+end_src")
;; general src block:1 ends here

;; elisp src block
;; :PROPERTIES:
;; :ID:       0dc658e3-3b4e-4a6e-ad1c-c240223af21d
;; :END:


;; [[file:init.org::*elisp src block][elisp src block:1]]
(define-skeleton org-skeleton-src-block-emacs-lisp ""
  \n
  "#+begin_src emacs-lisp" \n  _ \n
  "#+end_src")
;; elisp src block:1 ends here

;; inline src block
;; :PROPERTIES:
;; :ID:       b1c0fbb1-7438-4154-8d58-d9d28e084bcf
;; :END:


;; [[file:init.org::*inline src block][inline src block:1]]
(define-skeleton org-skeleton-src-block-inline ""
  \n
  "src_" _ "[]{}")
;; inline src block:1 ends here

;; drawers
;; :PROPERTIES:
;; :ID:       e53bbc89-a2b8-4b0a-9101-ba1b8f9a425e
;; :END:


;; [[file:init.org::*drawers][drawers:1]]
(define-skeleton org-skeleton-drawer-doublelinks
  "For insertion of drawer 'doublelinks' via abbrevs."
  \n
  ":doublelinks:"  \n _ \n
  ":end:")
;; drawers:1 ends here

;; [[file:init.org::*drawers][drawers:2]]
(define-skeleton org-skeleton-drawer-catchwords
  "For insertion of drawer 'catchwords' via abbrevs."
  \n
  ":catchwords:"  \n _ \n
  ":end:")
;; drawers:2 ends here

;; date at point
;; :PROPERTIES:
;; :ID:       2b046ae4-94f7-448d-a6c6-3e49765ca175
;; :END:


;; [[file:init.org::*date at point][date at point:1]]
(push "~/s/elisp/mw/date-at-point" load-path)
(require 'date-at-point)
;; date at point:1 ends here

;; functions to ease typical u/mount
;; :PROPERTIES:
;; :ID:       a3871009-0cbf-484b-9f51-294dd229d59c
;; :END:


;; [[file:init.org::*functions to ease typical u/mount][functions to ease typical u/mount:1]]
(defun mw-mount-typical (&optional arg)
  "Prepare mount the typical drive via eshell.  With universal prefix ARG umount."
  (interactive "P")
  (let ((shell (eshell)))
    (set-buffer shell)
    (end-of-buffer)
    (let ((text (if (equal '(4) arg)
                    "sudo umount /mnt"
                  "sudo mount -o gid=users,fmask=113,dmask=002 /dev/sdb1 /mnt")))
      (insert text))
    (switch-to-buffer shell)
    (eshell-send-input)))

(defun mw-mount-typical-open-directories-of-interest ()
  "Open sudo::/mnt and sudo::~/0."
  (interactive)
  (delete-other-windows)
  (find-file "/sudo::/mnt")
  (find-file "/home/b/0")
  (split-window)
  (find-file "/mnt"))
;; functions to ease typical u/mount:1 ends here

;; just one space line
;; :PROPERTIES:
;; :ID:       98a60490-8fdf-4037-8f95-43c49761150f
;; :END:


;; [[file:init.org::*just one space line][just one space line:1]]
(defun mw-just-one-blank-line ()
  "Leave a blank line at cursor."
  (interactive "*")
  (delete-region
   (progn
     (if (not (re-search-backward "[^ \t\n]" nil t))
         (point-min)
       (forward-char)
       (point)))
   (progn
     (if (not (re-search-forward "[^ \t\n]" nil t))
         (point-max)
       (backward-char)
       (point))))
  (open-line 2)
  (forward-char))
;; just one space line:1 ends here

;; duplicate eww buffer
;; :PROPERTIES:
;; :ID:       48c2f799-f29f-477b-903d-114dc89ca484
;; :END:


;; [[file:init.org::*duplicate eww buffer][duplicate eww buffer:1]]
(defun mw-eww-fork ()
  "Create a further eww buffer for current url."
  (interactive)
  (when (eq major-mode 'eww-mode)
    (eww-browse-url (plist-get eww-data :url) t)))
;; duplicate eww buffer:1 ends here

;; additions to eww keymap
;; :PROPERTIES:
;; :ID:       0524e938-4baf-4552-a999-c3cafce51c14
;; :END:

;; This is for somehow saving the page to not loosing it at the next eww
;; call.


;; [[file:init.org::*additions to eww keymap][additions to eww keymap:1]]
(defun mw-eww-browse-with-firefox ()
  "Browse the current eww url with firefox."
  (interactive)
  (cl-assert (eq major-mode 'eww-mode))
  (browse-url-firefox (plist-get eww-data :url)))

(require 'eww)
(with-eval-after-load "eww"
    (progn
      (define-key eww-mode-map "x" #'mw-eww-duplicate-buffer) ;'rename-uniquely
      (message "Added 'x' in eww-mode-map -- mw-eww-duplicate-buffer")
      (define-key eww-mode-map "o" #'mw-eww-browse-with-firefox)
      (message "Added 'o' in eww-mode-map -- mw-eww-browse-with-firefox")
      (define-key eww-mode-map "f" #'eww-lnum-follow)
      (message "Added 'f' in eww-mode-map -- eww-lnum-follow")))
;; additions to eww keymap:1 ends here

;; eww on region
;; :PROPERTIES:
;; :ID:       96c018f3-33b7-451f-974e-aba9dfdf12e0
;; :END:

;; eww


;; [[file:init.org::*eww on region][eww on region:1]]
(defun mw-eww-trigger-with-region-text ()
  "Trigger eww with current region as input."
  (interactive)
  (eww (buffer-substring
        (region-beginning)
        (region-end))))
(defalias 'mw-eww-on-region 'mw-eww-trigger-with-region-text
  "Shorter name.")
;; eww on region:1 ends here

;; open page with firefox
;; :PROPERTIES:
;; :ID:       3d3021ca-b4af-4232-ae28-81b75e0fb341
;; :END:

;; [[help:eww-browse-with-external-browser]] has

;; #+begin_quote
;; (eww-browse-with-external-browser &optional URL)

;; Browse the current URL with an external browser.  The browser to used
;; is specified by the ‘shr-external-browser’ variable.
;; #+end_quote


;; [[file:init.org::*open page with firefox][open page with firefox:1]]
(with-eval-after-load
 "eww"
 (progn
    (define-key eww-mode-map "&"
      (lambda (arg)
        "Open url with external browser.  With prefix ARG use firefox."
        (interactive "P")
        (let ((shr-external-browser
               (if arg 'browse-url-firefox
                 shr-external-browser)))
          (eww-browse-with-external-browser))))
      (message "& in eww with prefix arg means open with firefox.")))
;; open page with firefox:1 ends here

;; switch from w3m to eww and vice versa
;; :PROPERTIES:
;; :ID:       613121b1-9af0-4815-8120-ac236527cc30
;; :END:


;; [[file:init.org::*switch from w3m to eww and vice versa][switch from w3m to eww and vice versa:1]]
(defun mw-w3m-switch-to-eww ()
  "Switch to eww from w3m."
  (interactive)
  (eww w3m-current-url))
;; switch from w3m to eww and vice versa:1 ends here

;; [[file:init.org::*switch from w3m to eww and vice versa][switch from w3m to eww and vice versa:2]]
(defun mw-eww-switch-to-w3m ()
  "Switch to w3m from eww."
  (interactive)
  (w3m (eww-current-url)))
;; switch from w3m to eww and vice versa:2 ends here

;; extend rectangle to longest line
;; :PROPERTIES:
;; :ID:       dbf3f0c1-4471-4101-9877-12bf1aa5e2be
;; :END:


;; [[file:init.org::*extend rectangle to longest line][extend rectangle to longest line:1]]
(defun mw-longest-line-length-in-rectangle (start end)
  "Return max col of lines in rectangle."
  (let ((pt (point))
        (maxi 0))
    (when (< end start)
        (let ((aux end))
          (setq end start start aux)))
    (goto-char start)
    (while (<= (point) end)
     (end-of-line)
     (setq maxi (max maxi (current-column)))
     (forward-line 1))
    (goto-char pt)
    maxi))

(defun mw-rectangle-mark-mode-forward-to-max-col (start end)
  "Forward to longest line's max column of region.
Make sure rectangle-mark-mode is on."
  (interactive "r")
  (unless rectangle-mark-mode
    (rectangle-mark-mode))
  (rectangle-forward-char
   (- (mw-longest-line-length-in-rectangle start end)
      (current-column))))
;; extend rectangle to longest line:1 ends here

;; convert table to tsv :org:
;; :PROPERTIES:
;; :ID:       7cbf12ae-a25e-4c4d-8b09-bb096b62d92f
;; :END:


;; [[file:init.org::*convert table to tsv][convert table to tsv:1]]
(defun mw-org-table-convert-to-tsv ()
  "Convert table to tab separated values."
  (interactive)
  (let ((table (org-table-to-lisp
	        (buffer-substring-no-properties
	         (org-table-begin) (org-table-end)))))
    (delete-region (org-table-begin) (org-table-end))
    (insert (orgtbl-to-tsv table nil) "\n")))
;; convert table to tsv:1 ends here

;; insert calendar date at point in calendar
;; :PROPERTIES:
;; :ID:       15516e6a-99a3-49ea-830b-d2379a7390ab
;; :END:


;; [[file:init.org::*insert calendar date at point in calendar][insert calendar date at point in calendar:1]]
(defun mw-insert-date-from-calendar ()
  "Insert the date from the calendar.
Started with `org-date-from-calendar'."
  (interactive)
  (let ((cal-date (with-current-buffer "*Calendar*"
                    (save-match-data
                      (calendar-cursor-to-date))))
        (separator "/"))
    (insert (format-time-string
             (mapconcat #'identity '("%Y" "%m" "%d") separator)
             (encode-time 0 0 0
                          (nth 1 cal-date) (car cal-date) (nth 2 cal-date))))))
;; insert calendar date at point in calendar:1 ends here

;; clone indirect buffer and narrow to region
;; :PROPERTIES:
;; :ID:       b5d6acc3-564c-4196-a948-77819a86384c
;; :END:


;; [[file:init.org::*clone indirect buffer and narrow to region][clone indirect buffer and narrow to region:1]]
(defun mw-region-clone-indirect-buffer-narrowed-to-region (start end)
  "Show just region in a narrowed clone in other window.
Only do something when region is active."
  (interactive "r")
  (when (region-active-p)
      (clone-indirect-buffer-other-window nil t)
      (narrow-to-region start end)
      (deactivate-mark)))
;; clone indirect buffer and narrow to region:1 ends here

;; delete empty lines in region
;; :PROPERTIES:
;; :ID:       595b0910-e941-478f-9819-1352db6fc2fe
;; :END:


;; [[file:init.org::*delete empty lines in region][delete empty lines in region:1]]
(defun mw-region-delete-empty-lines (start end)
"Delete all empty lines in region."
  (interactive "r")
  (flush-lines "^$" start end))
;; delete empty lines in region:1 ends here

;; [[file:init.org::*delete empty lines in region][delete empty lines in region:2]]
(global-set-key (kbd "M-_") #'mw-region-delete-empty-lines)
;; delete empty lines in region:2 ends here

;; eww on file in dired
;; :PROPERTIES:
;; :ID:       e1525a65-3612-431e-b903-4da4f7f0dd3f
;; :END:

;; Setting key e for opening the file at point in eww.

;; Note there is already key W for opening the file at point in some
;; browser.


;; [[file:init.org::*eww on file in dired][eww on file in dired:1]]
(add-hook 'dired-mode-hook
          (lambda ()
            ;; Set dired-x buffer-local variables here.  For example:
            ;; (dired-omit-mode 1)
            ;;
            ;; lab: "e" for open the file in eww.  Note: I never used
            ;; "e" to start edit a file of a dired (which is the
            ;; default behavior. [2016-07-21 Thu 17:11])
            (define-key dired-mode-map "e"
              (lambda () (interactive)
                "Open file at point in eww.
See also key W in dired."
                (eww-open-file (dired-get-file-for-visit))))
            (define-key dired-mode-map "J"
              (lambda () (interactive)
                "Import ics file at point to diary."
                (icalendar-import-file (dired-get-file-for-visit) (concat mw-personal-org-files-dir "/diary"))))))

;; (setf dired-mode-hook nil)
;; eww on file in dired:1 ends here

;; more fun with pages
;; :PROPERTIES:
;; :ID:       13230a57-6a06-46ab-a427-18aa81b6dd82
;; :END:


;; [[file:init.org::*more fun with pages][more fun with pages:1]]
;; activate some page-related extensions
(require 'page-ext)

;; (define-key pages-directory-mode-map "q" 'ignore)
;; more fun with pages:1 ends here

;; [[file:init.org::*more fun with pages][more fun with pages:2]]
(let ((map pages-directory-mode-map))
  (define-key map (kbd "TAB") #'pages-directory-goto)
  (define-key map "n" #'next-line)
  (define-key map "p" #'previous-line)
  (define-key map "q" 'quit-window) ; fixes an issue with hyperbole?
)
;; more fun with pages:2 ends here

;; Diary entries in agenda on top
;; :PROPERTIES:
;; :ID:       1b87f1d5-5840-4edc-b47b-b8b90cff24a2
;; :END:

;; this can be done with a user defined compare function for the
;; ~org-agenda-sorting-strategy~.  this means to enable ~user-defined-up~
;; in the strategy for 'agenda'.

;; fill ~user-defined-up~ with life by setting
;; ~org-agenda-cmp-user-defined~ to the following function:


;; [[file:init.org::*Diary entries in agenda on top][Diary entries in agenda on top:1]]
(defun org-cmp-diary (a b)
  "Compare the diary entry of strings A and B."
  (let ((a-is-diary-type (string-equal (get-text-property (1- (length a)) 'type a) "diary"))
        (b-is-diary-type (string-equal (get-text-property (1- (length b)) 'type b) "diary")))
    (cond (a-is-diary-type -1)
          (b-is-diary-type +1)
          (t -1))))
;; Diary entries in agenda on top:1 ends here

;; M-RET insert item with date if above is like so
;; :PROPERTIES:
;; :ID:       5c8ad4be-8cef-4260-a0e4-bcd2debf73bd
;; :END:


;; [[file:init.org::*M-RET insert item with date if above is like so][M-RET insert item with date if above is like so:1]]
(defun mw-org-insert-item-with-ina-ts-when-on-such-item ()
  "When on org timestamp item insert org timestamp item with current time.
This holds only for inactive timestamps."
  (when (save-excursion
          (let ((item-pos (org-in-item-p)))
            (when item-pos
              (goto-char item-pos)
              (org-list-at-regexp-after-bullet-p org-ts-regexp-inactive))))
    (let ((item-pos (org-in-item-p))
          (pos (point)))
      (assert item-pos)
      (goto-char item-pos)
      (let* ((struct (org-list-struct))
	     (prevs (org-list-prevs-alist struct))
	     (s (concat (with-temp-buffer
                          (org-insert-time-stamp nil t t)
                          (buffer-string)) " ")))
        (setq struct (org-list-insert-item pos struct prevs nil s))
        (org-list-write-struct struct (org-list-parents-alist struct))
        (looking-at org-list-full-item-re)
	(goto-char (match-end 0))
        (end-of-line)))
    t))
;; M-RET insert item with date if above is like so:1 ends here

;; [[file:init.org::*M-RET insert item with date if above is like so][M-RET insert item with date if above is like so:2]]
(add-hook
 'org-metareturn-hook 'mw-org-insert-item-with-ina-ts-when-on-such-item)
;; M-RET insert item with date if above is like so:2 ends here

;; find init source
;; :PROPERTIES:
;; :ID:       1aff140d-cdce-46f8-b2a1-24c701f2cc4b
;; :END:


;; [[file:init.org::*find init source][find init source:1]]
(defun mw-find-init ()
  (interactive)
  (push-mark)
  (find-file "~/s/elisp/mw/.emacs.d/init.org"))
;; find init source:1 ends here

;; espeak region
;; :PROPERTIES:
;; :ID:       eb78dbf0-f13c-495c-ba43-eca96f2dc2c8
;; :END:


;; [[file:init.org::*espeak region][espeak region:1]]
(defun mw-espeak-en-region (start end)
  (interactive "r")
  (let ((temp-file (make-temp-file "espeak-buffer")))
    (write-region start end temp-file)
    (start-process "espeak" "*espeak-process-buffer" "espeak" "-v" "en" "-s" "110" "-f" temp-file)))

(defun mw-espeak-de-region (start end)
  (interactive "r")
  (let ((temp-file (make-temp-file "espeak-buffer")))
    (write-region start end temp-file)
    (start-process "espeak" "*espeak-process-buffer" "espeak" "-v" "de" "-s" "110" "-f" temp-file)))
;; espeak region:1 ends here

;; espeak string
;; :PROPERTIES:
;; :ID:       32a2d06c-7fb7-4a50-bfc8-9a9f1a99f1e6
;; :END:


;; [[file:init.org::*espeak string][espeak string:1]]
(defun mw-espeak-en-string (string)
  (with-temp-buffer
    (insert string)
    (mw-espeak-en-region (point-min) (point-max))))

(defun mw-espeak-de-string (string)
  (with-temp-buffer
    (insert string)
    (mw-espeak-de-region (point-min) (point-max))))
;; espeak string:1 ends here

;; some older attempt


;; [[file:init.org::*some older attempt][some older attempt:1]]
(defun mw-calc-embedded-narrow-toggle (start end)
  "Narrow to region and enter calc embedded or widen and leave calc
embedded."
  (interactive "r")
  (if calc-embedded-info (widen)
    (narrow-to-region start end))
  (calc-embedded nil))
;; some older attempt:1 ends here

;; [[file:init.org::*some older attempt][some older attempt:2]]
(key-chord-define-global "c3" #'mw-calc-embedded-narrow-toggle)
;; some older attempt:2 ends here

;; calc to normalize region


;; [[file:init.org::*calc to normalize region][calc to normalize region:1]]
(defun mw-region-apply-calc ()
  "Apply calc to region.
This can be helpful to simplify the formula in the region."
  (interactive)
  (when (region-active-p)
    (let (use-empty-active-region)
      (let ((region-substring (mw-region-string)))
        (let ((replacement
               (with-temp-buffer
                 (insert region-substring)
                 (call-interactively #'mark-whole-buffer)
                 (call-interactively #'calc-embedded)
                 (call-interactively #'calc-embedded)
                 (buffer-substring (point-min) (point-max)))))
          (delete-region (region-beginning) (region-end))
          (insert replacement))))))
;; calc to normalize region:1 ends here

;; diff dwim with two dired buffers
;; :PROPERTIES:
;; :ID:       dd28105b-9a16-4dd3-9396-888b3f8d5955
;; :END:


;; [[file:init.org::*diff dwim with two dired buffers][diff dwim with two dired buffers:1]]
(defun mw-dired-diff-to-file-in-other-dired ()
  "Compare file at point with file at point in other dired.
Select the window containing the diff."
  (interactive)
  (let* ((current-win (selected-window))
         (other-win
          (get-window-with-predicate
	   (lambda (window)
             (and (not (eq current-win window))
	          (with-current-buffer (window-buffer window)
	            (eq major-mode 'dired-mode))))))
         (filename
          (and other-win
               (let ((point-in-other
                      (window-point other-win)))
                 (with-current-buffer (window-buffer other-win)
                   (save-excursion
                     (goto-char point-in-other)
                     (dired-get-filename nil t)))))))
    (when filename
      (select-window (dired-diff filename)))))
;; diff dwim with two dired buffers:1 ends here



;; I haven't found one statement that makes both of the above situations
;; work.  I used both simultaneously.


;; [[file:init.org::*fira code setup][fira code setup:3]]
(defconst fira-code-font-lock-keywords-alist
  (mapcar (lambda (regex-char-pair)
            `(,(car regex-char-pair)
              (0 (prog1 ()
                   (compose-region
                    (match-beginning 1)
                    (match-end 1)
                    ,(concat (char-to-string ?\t)
                             (list
                              (decode-char 'ucs (cadr regex-char-pair)))))))))
          '(("\\(www\\)"                   #Xe100) ;;;    ;; 
            ("[^/]\\(\\*\\*\\)[^/]"        #Xe101) ;    
            ("\\(\\*\\*\\*\\)"             #Xe102) ;    
            ("\\(\\*\\*/\\)"               #Xe103) ;    
            ("\\(\\*>\\)"                  #Xe104) ;    
            ("[^*]\\(\\*/\\)"              #Xe105) ;    
            ("\\(\\\\\\\\\\)"              #Xe106) ;    
            ("\\(\\\\\\\\\\\\\\)"          #Xe107) ;    
            ("\\({-\\)"                    #Xe108) ;    
            ("\\(\\[\\]\\)"                #Xe109) ;    
            ("\\(::\\)"                    #Xe10a) ;    
            ("\\(:::\\)"                   #Xe10b) ;    
            ("[^=]\\(:=\\)"                #Xe10c) ;    
            ("\\(!!\\)"                    #Xe10d) ;    
            ("\\(!=\\)"                    #Xe10e) ;    
            ("\\(!==\\)"                   #Xe10f) ;    
            ("\\(-}\\)"                    #Xe110) ;    
            ("\\(--\\)"                    #Xe111) ;    
            ("\\(---\\)"                   #Xe112) ;    
            ("\\(-->\\)"                   #Xe113) ;    
            ("[^-]\\(->\\)"                #Xe114) ;    
            ("\\(->>\\)"                   #Xe115) ;    
            ("\\(-<\\)"                    #Xe116) ;    
            ("\\(-<<\\)"                   #Xe117) ;    
            ("\\(-~\\)"                    #Xe118) ;    
            ("\\(#{\\)"                    #Xe119) ;    
            ("\\(#\\[\\)"                  #Xe11a) ;    
            ("\\(##\\)"                    #Xe11b) ;    
            ("\\(###\\)"                   #Xe11c) ;    
            ("\\(####\\)"                  #Xe11d) ;    
            ("\\(#(\\)"                    #Xe11e) ;    
            ("\\(#\\?\\)"                  #Xe11f) ;    
            ("\\(#_\\)"                    #Xe120) ;    
            ("\\(#_(\\)"                   #Xe121) ;    
            ("\\(\\.-\\)"                  #Xe122) ;    
            ("\\(\\.=\\)"                  #Xe123) ;    
            ("\\(\\.\\.\\)"                #Xe124) ;    
            ("\\(\\.\\.<\\)"               #Xe125) ;    
            ("\\(\\.\\.\\.\\)"             #Xe126) ;    
            ("\\(\\?=\\)"                  #Xe127) ;    
            ("\\(\\?\\?\\)"                #Xe128) ;    
            ("\\(;;\\)"                    #Xe129) ;    
            ("\\(/\\*\\)"                  #Xe12a) ;    
            ("\\(/\\*\\*\\)"               #Xe12b) ;    
            ("\\(/=\\)"                    #Xe12c) ;    
            ("\\(/==\\)"                   #Xe12d) ;    
            ("\\(/>\\)"                    #Xe12e) ;    
            ("\\(//\\)"                    #Xe12f) ;    
            ("\\(///\\)"                   #Xe130) ;    
            ("\\(&&\\)"                    #Xe131) ;    
            ("\\(||\\)"                    #Xe132) ;    
            ("\\(||=\\)"                   #Xe133) ;    
            ("[^|]\\(|=\\)"                #Xe134) ;    
            ("\\(|>\\)"                    #Xe135) ;    
            ("\\(\\^=\\)"                  #Xe136) ;    
            ("\\(\\$>\\)"                  #Xe137) ;    
            ("\\(\\+\\+\\)"                #Xe138) ;    
            ("\\(\\+\\+\\+\\)"             #Xe139) ;    
            ("\\(\\+>\\)"                  #Xe13a) ;    
            ("\\(=:=\\)"                   #Xe13b) ;    
            ("[^!/]\\(==\\)[^>]"           #Xe13c) ;    
            ("\\(===\\)"                   #Xe13d) ;    
            ("\\(==>\\)"                   #Xe13e) ;    
            ("[^=]\\(=>\\)"                #Xe13f) ;    
            ("\\(=>>\\)"                   #Xe140) ;    
            ("\\(<=\\)"                    #Xe141) ;    
            ("\\(=<<\\)"                   #Xe142) ;    
            ("\\(=/=\\)"                   #Xe143) ;    
            ("\\(>-\\)"                    #Xe144) ;    
            ("\\(>=\\)"                    #Xe145) ;    
            ("\\(>=>\\)"                   #Xe146) ;    
            ("[^-=]\\(>>\\)"               #Xe147) ;    
            ("\\(>>-\\)"                   #Xe148) ;    
            ("\\(>>=\\)"                   #Xe149) ;    
            ("\\(>>>\\)"                   #Xe14a) ;    
            ("\\(<\\*\\)"                  #Xe14b) ;    
            ("\\(<\\*>\\)"                 #Xe14c) ;    
            ("\\(<|\\)"                    #Xe14d) ;    
            ("\\(<|>\\)"                   #Xe14e) ;    
            ("\\(<\\$\\)"                  #Xe14f) ;    
            ("\\(<\\$>\\)"                 #Xe150) ;    
            ("\\(<!--\\)"                  #Xe151) ;    
            ("\\(<-\\)"                    #Xe152) ;    
            ("\\(<--\\)"                   #Xe153) ;    
            ("\\(<->\\)"                   #Xe154) ;    
            ("\\(<\\+\\)"                  #Xe155) ;    
            ("\\(<\\+>\\)"                 #Xe156) ;    
            ("\\(<=\\)"                    #Xe157) ;    
            ("\\(<==\\)"                   #Xe158) ;    
            ("\\(<=>\\)"                   #Xe159) ;    
            ("\\(<=<\\)"                   #Xe15a) ;    
            ("\\(<>\\)"                    #Xe15b) ;    
            ("[^-=]\\(<<\\)"               #Xe15c) ;    
            ("\\(<<-\\)"                   #Xe15d) ;    
            ("\\(<<=\\)"                   #Xe15e) ;    
            ("\\(<<<\\)"                   #Xe15f) ;    
            ("\\(<~\\)"                    #Xe160) ;    
            ("\\(<~~\\)"                   #Xe161) ;    
            ("\\(</\\)"                    #Xe162) ;    
            ("\\(</>\\)"                   #Xe163) ;    
            ("\\(~@\\)"                    #Xe164) ;    
            ("\\(~-\\)"                    #Xe165) ;    
            ("\\(~=\\)"                    #Xe166) ;    
            ("\\(~>\\)"                    #Xe167) ;    
            ("[^<]\\(~~\\)"                #Xe168) ;    
            ("\\(~~>\\)"                   #Xe169) ;    
            ("\\(%%\\)"                    #Xe16a) ;    
            ("\\(x\\)"                     #Xe16b) ;   This ended up
                                                   ;  being hard to do
                                                   ;  properly so i'm
                                                   ;  leaving it out.
            ("[^:=]\\(:\\)[^:=]"           #Xe16c) ;  
            ("[^\\+<>]\\(\\+\\)[^\\+<>]"   #Xe16d) ;  
            ("[^\\*/<>]\\(\\*\\)[^\\*/<>]" #Xe16f) ;  
          )))
;; fira code setup:3 ends here



;; bring the fira code font to buffers with the function.


;; [[file:init.org::*fira code setup][fira code setup:4]]
(defun add-fira-code-symbol-keywords ()
  (font-lock-add-keywords nil fira-code-font-lock-keywords-alist))
;; fira code setup:4 ends here

;; failed attempt to activate fira code
;; :PROPERTIES:
;; :ID:       f6dcda65-7fd2-4e07-8844-4db0b0e5846b
;; :END:

;; - [2017-11-30 Thu 15:14] kept this item to warn and possibly revisit.
;; - [2016-07-18] The following was disappointing.

;; source: https://github.com/tonsky/FiraCode/wiki/Setting-up-Emacs


;; [[file:init.org::*failed attempt to activate fira code][failed attempt to activate fira code:1]]
;;(when (window-system)
;; (set-frame-font "Fira Code"))
;;(let ((alist '((33 . ".\\(?:\\(?:==\\|!!\\)\\|[!=]\\)")
;;              (35 . ".\\(?:###\\|##\\|_(\\|[#(?[_{]\\)")
;;              (36 . ".\\(?:>\\)")
;;              (37 . ".\\(?:\\(?:%%\\)\\|%\\)")
;;              (38 . ".\\(?:\\(?:&&\\)\\|&\\)")
;;              (42 . ".\\(?:\\(?:\\*\\*/\\)\\|\\(?:\\*[*/]\\)\\|[*/>]\\)")
;;              (43 . ".\\(?:\\(?:\\+\\+\\)\\|[+>]\\)")
;;              (45 . ".\\(?:\\(?:-[>-]\\|<<\\|>>\\)\\|[<>}~-]\\)")
;;              (46 . ".\\(?:\\(?:\\.[.<]\\)\\|[.=-]\\)")
;;              (47 . ".\\(?:\\(?:\\*\\*\\|//\\|==\\)\\|[*/=>]\\)")
;;              (48 . ".\\(?:x[a-zA-Z]\\)")
;;              (58 . ".\\(?:::\\|[:=]\\)")
;;              (59 . ".\\(?:;;\\|;\\)")
;;              (60 . ".\\(?:\\(?:!--\\)\\|\\(?:~~\\|->\\|\\$>\\|\\*>\\|\\+>\\|--\\|<[<=-]\\|=[<=>]\\||>\\)\\|[*$+~/<=>|-]\\)")
;;              (61 . ".\\(?:\\(?:/=\\|:=\\|<<\\|=[=>]\\|>>\\)\\|[<=>~]\\)")
;;              (62 . ".\\(?:\\(?:=>\\|>[=>-]\\)\\|[=>-]\\)")
;;              (63 . ".\\(?:\\(\\?\\?\\)\\|[:=?]\\)")
;;              (91 . ".\\(?:]\\)")
;;              (92 . ".\\(?:\\(?:\\\\\\\\\\)\\|\\\\\\)")
;;              (94 . ".\\(?:=\\)")
;;              (119 . ".\\(?:ww\\)")
;;              (123 . ".\\(?:-\\)")
;;              (124 . ".\\(?:\\(?:|[=|]\\)\\|[=>|]\\)")
;;              (126 . ".\\(?:~>\\|~~\\|[>=@~-]\\)"))))
;; (dolist (char-regexp alist)
;;   (set-char-table-range composition-function-table (car char-regexp)
;;                         `([,(cdr char-regexp) 0 font-shape-gstring]))))
;; failed attempt to activate fira code:1 ends here

;; enable more mouse-wheel
;; :PROPERTIES:
;; :ID:       ee869ba2-e6fb-45a5-8c6c-5af74d50c28b
;; :END:

;; firefox inspired usage of the mouse wheel.

;; shift+wheel for scrolling horizontally.


;; [[file:init.org::*enable more mouse-wheel][enable more mouse-wheel:1]]
(global-set-key [mouse-4] #'scroll-down)
(global-set-key [mouse-5] #'scroll-up)
(global-set-key [S-mouse-4] #'scroll-right)
(global-set-key [S-mouse-5] #'scroll-left)
;; enable more mouse-wheel:1 ends here



;; control+wheel for zooming the text.


;; [[file:init.org::*enable more mouse-wheel][enable more mouse-wheel:2]]
(global-set-key [(control mouse-4)] (lambda () (interactive) (text-scale-increase 1)))
(global-set-key [(control mouse-5)] (lambda () (interactive) (text-scale-decrease 1)))
;; enable more mouse-wheel:2 ends here

;; mouse wheel for pdf-view-mode
;; :PROPERTIES:
;; :ID:       10bbaf26-f79f-49d5-aae1-aedac964d76c
;; :END:


;; [[file:init.org::*mouse wheel for pdf-view-mode][mouse wheel for pdf-view-mode:1]]
(add-hook
 'pdf-view-mode-hook
 (lambda ()
   (local-set-key [mouse-4] #'pdf-view-previous-line-or-previous-page)
   (local-set-key [mouse-5] #'pdf-view-next-line-or-next-page)
   (local-set-key [(control mouse-4)] (lambda () (interactive) (pdf-view-enlarge 1.25)))
   (local-set-key [(control mouse-5)] (lambda () (interactive) (pdf-view-enlarge 0.8)))))
;; mouse wheel for pdf-view-mode:1 ends here

;; mouse wheel for image-mode
;; :PROPERTIES:
;; :ID:       ebf0b86c-154b-4ed4-8d5d-1510e4299ebf
;; :END:


;; [[file:init.org::*mouse wheel for image-mode][mouse wheel for image-mode:1]]
(add-hook
 'image-mode-hook
 (lambda ()
   (local-set-key [mouse-4] (lambda () (interactive) (image-previous-line 1)))
   (local-set-key [mouse-5] (lambda () (interactive) (image-next-line 1)))
   (local-set-key [(control mouse-4)] (lambda () (interactive) (image-increase-size 1)))
   (local-set-key [(control mouse-5)] (lambda () (interactive) (image-decrease-size 1)))))
;; mouse wheel for image-mode:1 ends here

;; control sound
;; :PROPERTIES:
;; :ID:       07d4ced5-486f-448f-9d47-74f6023a0c2f
;; :END:

;; [2017-03-22 Wed 13:00] When calling the sound goes to 0%!


;; [[file:init.org::*control sound][control sound:1]]
(defun mw-sound-master-pecentage (percent)
  "Set Master via amixer to PERCENT."
  (start-process
   "" "*mw-amixer*" "amixer" "set" "Master"
   (format "%d%%" (max 0 (min 100 percent)))))

(defun mw-sound-master-on ()
  "Set Master on via amixer."
  (start-process
   "" "*mw-amixer*" "amixer" "set" "Master" "on"))

(defun mw-sound-100% ()
  "100% sound."
  (interactive)
  (mw-sound-master-pecentage 100)
  (mw-sound-master-on)
  (message "sound set to 100%%"))

(defun mw-sound-set-enjoyable-volume ()
  "Enjoyable volume for listening with headphones.

  The effect of this function is somewhat subjective."
  (interactive)
  (mw-sound-master-pecentage 30)
  (message "personal sound level set"))

(defun mw-sound-0% ()
  "0% sound."
  (interactive)
  (mw-sound-master-pecentage 0)
  (message "sound set to 0%%"))
;; control sound:1 ends here

;; keys-chords for org-mode
;; :PROPERTIES:
;; :ID:       db8dcc15-92b1-4d2b-8347-9264ec59df88
;; :END:


;; [[file:init.org::*keys-chords for org-mode][keys-chords for org-mode:1]]
(add-hook
 'org-mode-hook
 (lambda ()
   (key-chord-define-local "t1" #'mw-org-attach-screenshot-as-standard-attachment)
   (key-chord-define-local "o7" (lambda (&optional arg)
           "Display outline path and put 2 kills to kill-ring.
   With ARG don't touch the kill-ring."
           (interactive "P")
           (org-display-outline-path nil t)
           (unless arg
             (kill-new (org-display-outline-path nil t nil t))
             (save-excursion
   	   (org-back-to-heading t)
   	   (when (looking-at org-complex-heading-regexp)
                (when-let ((heading-title  (match-string 4)))
                  (kill-new heading-title)))))))
   (key-chord-define-local
    "||"
    (lambda ()
      "Goto end of meta data of orgee."
      (interactive)
      (mw-org-end-of-meta-data)))
))
;; keys-chords for org-mode:1 ends here

;; emms
;; :PROPERTIES:
;; :ID:       005ce28c-c1ba-4f5c-84cf-74145b1b7aff
;; :END:

;; Emms is for playing sound.  I use emms mostly for playing internet
;; radio.

;; BTW =emms-streams= has configured some nice stations AFAICT.


;; [[file:init.org::*emms][emms:1]]
(push "~/s/elisp/external/emms/lisp" load-path)
(require 'emms-setup)
(emms-devel)				; adds +/- in emms-buffer.
(emms-default-players)
(with-eval-after-load 'info
  (progn (info-initialize)
          (add-to-list 'Info-directory-list "~/s/elisp/external/emms/doc")))
;; emms:1 ends here

;; diary
;; :PROPERTIES:
;; :ID:       862fc5d0-8951-491a-a6bb-9ca7ec8ef86a
;; :END:

;; Diary entries are useful sometimes.  E.g. it's possible to import ics
;; files into a diary.

;; Recall that in the org agenda the d key switches diary inclusion on or
;; off.


;; [[file:init.org::*diary][diary:1]]
;; for diary to include other diaries
(add-hook 'diary-list-entries-hook #'diary-include-other-diary-files)
(add-hook 'diary-list-entries-hook #'diary-sort-entries t)
(add-hook 'diary-mark-entries-hook #'diary-mark-included-diary-files)
;; diary:1 ends here

;; interpret region as date
;; :PROPERTIES:
;; :ID:       ded9726c-6662-40e8-8094-4b7c82be22d9
;; :END:


;; [[file:init.org::*interpret region as date][interpret region as date:1]]
(defun mw-region-interpret-as-time (start end)
  "Parse region and return a date."
  (interactive "r")
  (let* ((string (if (string-match-p "[[:digit:]]\\{8\\}" (buffer-substring start end))
                    (concat (buffer-substring start (+ 4 start)) "-"
                            (buffer-substring  (+ 4 start)  (+ 6 start)) "-"
                            (buffer-substring  (+ 6 start)  (+ 8 start)) )
                  (buffer-substring start end))))
        (apply #'encode-time
               (reverse
                (let
                    ((timelist
                      (math-date-to-dt
                       (math-parse-date
                        string))))
                  (append timelist
                          (make-list (- 6 (length timelist)) 0)))))))
;; interpret region as date:1 ends here

;; [[file:init.org::*interpret region as date][interpret region as date:2]]
(defun mw-region-open-calendar-for-date-in-region (start end)
  "Open calendar for the date in the region."
  (interactive "r")
  (let* ((full-date (decode-time (mw-region-interpret-as-time start end)))
         (date (list (nth 4 full-date)
                     (nth 3 full-date)
                     (nth 5 full-date))))
    (calendar)
    (let ((cwin (get-buffer-window "*Calendar*" t)))
           (when cwin
             (let ((calendar-move-hook nil))
	       (with-selected-window cwin
	         (calendar-goto-date
	          ;; (if (listp d) d (calendar-gregorian-from-absolute d))
                  date)))))))
;; interpret region as date:2 ends here

;; convert region to timestamp
;; :PROPERTIES:
;; :ID:       d7af2eca-0e7a-4d24-8a10-7d3382afb69c
;; :END:


;; [[file:init.org::*convert region to timestamp][convert region to timestamp:1]]
(defun mw-region-convert-to-org-time-stamp (start end)
  "Parse region and replace by respective timestamp in Org format.

Example(-!- is point, -m- is mark):

given:

    -!- 11:33 2017/10/07-m-

result:

    -!-[2017-10-07 Sat 11:33]-m-
"
  (interactive "r")
  (let* ((string (if (string-match-p "[[:digit:]]\\{8\\}" (buffer-substring start end))
                    (concat (buffer-substring start (+ 4 start)) "-"
                            (buffer-substring  (+ 4 start)  (+ 6 start)) "-"
                            (buffer-substring  (+ 6 start)  (+ 8 start)) )
                  (buffer-substring start end)))
        (time
         (apply #'encode-time
                (reverse
                 (let
                     ((timelist
                       (math-date-to-dt
                        (math-parse-date
                         string))))
                   (append timelist
                           (make-list (- 6 (length timelist)) 0))))))
        (with-hm t)
        (inactive t))
    (delete-region start end)
    (org-insert-time-stamp
     time
     with-hm
     inactive)))

(make-obsolete 'org-convert-region-to-org-time-stamp 'mw-region-convert-to-org-time-stamp "now")
(make-obsolete 'mw-convert-region-to-org-time-stamp 'mw-region-convert-to-org-time-stamp "now")

(defun mw-region-convert-to-ledger-time-stamp (start end)
  "Parse region and replace by respective timestamp in Org format.

Example(-!- is point, -m- is mark):

given:

    -!- 11:33 2017-10-07-m-

result:

    -!-2017/10/07-m-
"
  (interactive "r")
  (let* ((string (if (string-match-p "[[:digit:]]\\{8\\}" (buffer-substring start end))
                    (concat (buffer-substring start (+ 4 start)) "-"
                            (buffer-substring  (+ 4 start)  (+ 6 start)) "-"
                            (buffer-substring  (+ 6 start)  (+ 8 start)) )
                  (buffer-substring start end)))
        (time
         (apply #'encode-time
                (reverse
                 (let
                     ((timelist
                       (math-date-to-dt
                        (math-parse-date
                         string))))
                   (append timelist
                           (make-list (- 6 (length timelist)) 0)))))))
    (delete-region start end)
    (insert (format-time-string "%Y/%m/%d" time))))
;; convert region to timestamp:1 ends here

;; convenient snapshot of emacs from within
;; :PROPERTIES:
;; :ID:       236c5b50-a3b6-4c02-a40f-96101519d184
;; :END:


;; [[file:init.org::*convenient snapshot of emacs from within][convenient snapshot of emacs from within:1]]
(push "~/s/elisp/mw/emacsshot" load-path)
(require 'emacsshot)
(global-set-key
 [print] ; (kbd "<print>")
 (lambda (&optional prefix)
   (interactive "p")
   (case prefix
     (1 (emacsshot-snap-window))
     (4 (emacsshot-snap-frame))
     (16 (emacsshot-snap-window-include-modeline)))))
;; convenient snapshot of emacs from within:1 ends here

;; [[file:init.org::*convenient snapshot of emacs from within][convenient snapshot of emacs from within:2]]
(global-set-key (kbd "C-M-S-<mouse-1>") 'emacsshot-snap-mouse-drag)
;; convenient snapshot of emacs from within:2 ends here

;; compare windows
;; :PROPERTIES:
;; :ID:       af31b140-aa97-4bac-8392-30bf88e376e1
;; :END:

;; Set ~compare-windows~ to ignore whitespace by default.  Further
;; treat - and + at the beginning of a line as whitespace.


;; [[file:init.org::*compare windows][compare windows:1]]
(setq compare-ignore-whitespace t)
(setq compare-windows-whitespace "\\(\\s-\\|
\\|\240\\|\\(^[+-]\\)\\)+")
;; compare windows:1 ends here



;; Treating + and - as whitespace eases to compare diffs.  Just open the
;; diff buffer in two windows and place the cursors at the beginning of
;; the regions to compare followed by M-x compare-windows .


;; [[file:init.org::*compare windows][compare windows:2]]
(key-chord-define-global "w2" #'compare-windows)
;; compare windows:2 ends here

;; emr
;; :PROPERTIES:
;; :ID:       910630f4-e846-4fd6-8772-4e9f46ff99d7
;; :END:

;; emr is a refactoring tool.


;; [[file:init.org::*emr][emr:1]]
(straight-use-package 'emr)
(with-eval-after-load "emr" (emr-initialize))
(with-eval-after-load "emr"
  (define-key emacs-lisp-mode-map (kbd "M-RET")
    #'emr-show-refactor-menu))
;; emr:1 ends here

;; the package
;; :PROPERTIES:
;; :ID:       7a7ea549-0f9c-4009-9a4b-be650df1f881
;; :END:


;; [[file:init.org::*the package][the package:1]]
(straight-use-package 'seclusion-mode)
(require 'seclusion-mode)
;; the package:1 ends here

;; little-helpers


;; [[file:init.org::*little-helpers][little-helpers:1]]
(push "~/s/elisp/mw/little-helpers" load-path)
(require 'little-helpers)
;; little-helpers:1 ends here

;; auxies
;; :PROPERTIES:
;; :ID:       f1df9204-3090-4333-a5ab-4884d0556c82
;; :END:

;; Another collection of Emacs stuff.


;; [[file:init.org::*auxies][auxies:1]]
(push "~/s/elisp/mw/auxies" load-path)
(require 'auxies-rest)
;; auxies:1 ends here

;; mark a line
;; :PROPERTIES:
;; :ID:       6a7e74d4-fff6-4771-84ba-0612d29f01ae
;; :END:


;; [[file:init.org::*mark a line][mark a line:1]]
(require 'mw-mark)
;; mark a line:1 ends here

;; auxies-eww
;; :PROPERTIES:
;; :ID:       c5c38c29-acf8-4283-8977-ffef26e1176f
;; :END:


;; [[file:init.org::*auxies-eww][auxies-eww:1]]
(push "~/s/elisp/mw/auxies" load-path)
(require 'auxies-eww)
;; auxies-eww:1 ends here

;; pick entry 'user' from password-store
;; :PROPERTIES:
;; :ID:       786e49c6-da48-4dbe-a6bd-045d97177952
;; :END:

;; for some accounts in some situations it's handy to have the 'user'
;; accessible.  user means the value of the user field in the respective
;; password record.  note that the user field is optional with the pass
;; system so the functionality of ~mw-password-store-pick-user~ depends
;; on the maintainer of the pass file to place the user attribute
;; e.g. user: marcowahl.


;; [[file:init.org::*pick entry 'user' from password-store][pick entry 'user' from password-store:1]]
(defun mw-password-store-pick-user (entry)
  "Add value of 'user:' in ENTRY to kill ring."
  (interactive (progn
                 (require 'password-store)
                 (list (password-store--completing-read))))
  (let ((user-text
         (let ((case-fold-search t))
           (seq-find
            (lambda (x) (string-match-p "^user:" x))
            (s-lines (password-store--run-show entry))
            nil))))
    (if (not user-text)
        (error "no entry 'user' set for this account.")
      (message "%s copied to kill-ring (from entry %s.)" user-text entry)
      (kill-new (s-trim-left (nth 1 (s-split ":" user-text)))))))
;; pick entry 'user' from password-store:1 ends here

;; eval region as shell command
;; :PROPERTIES:
;; :ID:       b0240311-9819-42ad-8904-60c83296c2f8
;; :END:

;; use case:
;; - user sees a region you want evaluate as shell code.
;; - mark that region.
;; - evaluate with `mw-region-eval-as-shell-command'.


;; [[file:init.org::*eval region as shell command][eval region as shell command:1]]
(defun mw-region-eval-as-async-shell-command (start end)
"Evaluate region as shell command."
  (interactive "r")
  (async-shell-command
   (buffer-substring-no-properties
    start end)))
;; eval region as shell command:1 ends here

;; pdf-tools
;; :PROPERTIES:
;; :ID:       3bd198a4-7473-42ae-b7c5-3fe8b837741a
;; :END:


;; [[file:init.org::*pdf-tools][pdf-tools:1]]
(straight-use-package 'pdf-tools)
;; pdf-tools:1 ends here

;; delete trailing ws on save
;; :PROPERTIES:
;; :ID:       308b40be-09d3-4b60-88c2-21896b0428bc
;; :END:

;; the idea is that delete trailing ws on save keeps the files somewhat
;; clean.


;; [[file:init.org::*delete trailing ws on save][delete trailing ws on save:1]]
(push
 (lambda ()
   (delete-trailing-whitespace))
 before-save-hook)
;; delete trailing ws on save:1 ends here

;; kill emacs
;; :PROPERTIES:
;; :ID:       fc182380-b1ba-47fb-b4d6-4a859a58c097
;; :END:


;; [[file:init.org::*kill emacs][kill emacs:1]]
(global-set-key (kbd "C-x C-c") #'save-buffers-kill-emacs) ; also kill a possible daemon.
;; kill emacs:1 ends here

;; yes/no -> y/n
;; :PROPERTIES:
;; :ID:       e1e65f87-da4c-473c-86d7-34c120afbf7f
;; :END:

;; type y instead of yes and n for no at several occations.


;; [[file:init.org::*yes/no -> y/n][yes/no -> y/n:1]]
(defalias 'yes-or-no-p 'y-or-n-p)
;; yes/no -> y/n:1 ends here

;; find file
;; :PROPERTIES:
;; :ID:       6b0602ae-2143-40b8-b7fd-9b1607705847
;; :END:

;; find file at point.


;; [[file:init.org::*find file][find file:1]]
(ffap-bindings)
(global-set-key
 ""
 (lambda(&optional prefix)
   (interactive "P")
   (call-interactively
    (if prefix
        #'ido-find-file
      #'find-file-at-point))))
;; find file:1 ends here

;; hide some of agenda
;; :PROPERTIES:
;; :ID:       967de61a-9475-4f8b-8202-f5c166858888
;; :END:


;; [[file:init.org::*hide some of agenda][hide some of agenda:1]]
(defun mw-org-agenda-hide-line-or-region ()
  "Hide the line containing point or lines in the region from the agenda.
This action is just cosmetics in the agenda buffer and does not
affect the sources.  I.e. the lines appear again at the next
refresh for an agenda.

Note: This function has been derived from
`org-agenda-drag-line-forward'.

Note: Of course you can make the agenda buffer writable and use
some standard deletion functionality.  But you need to take the
action of making the agenda buffer writable.  And also take care
about some commands which might have a special meaning in the
agenda buffer e.g. C-k."
  (interactive)
  (let ((inhibit-read-only t))
    (if (region-active-p)
        (delete-region
         (save-excursion
           (goto-char (region-beginning))
           (beginning-of-line)
           (point))
         (progn
           (goto-char (region-end))
           (when (or (not (= (region-end)
                             (save-excursion
                               (goto-char (region-end))
                               (beginning-of-line)
                               (point))))
                     (= (point) (mark)))
             (forward-line))
           (point)))
      (move-beginning-of-line 1)
      (delete-region
       (point)
       (save-excursion (move-beginning-of-line 2) (point))))
    ;; (org-agenda-reapply-filters)
    ;; (org-agenda-mark-clocking-task)
    ))
;; hide some of agenda:1 ends here

;; keybinding org-agenda :org:
;; :PROPERTIES:
;; :ID:       37660ab1-382f-4799-b0da-f92f0669b7dc
;; :END:

;; Using the key 'h' which reminds of hide.  'h' is the standard binding
;; to popup holidays, but they are still accessable on key 'H'.


;; [[file:init.org::*keybinding org-agenda][keybinding org-agenda:1]]
(with-eval-after-load "org-agenda"
  (progn
    ;; (org-defkey org-agenda-mode-map (kbd "h") #'describe-mode)
     (org-defkey org-agenda-mode-map (kbd "h") #'mw-org-agenda-hide-line-or-region)
     (org-defkey org-agenda-mode-map (kbd "W") #'mw-org-agenda-write)
     (org-defkey org-agenda-mode-map (kbd "N")
                 (lambda (&optional arg)
                   (interactive "p")
                   (scroll-up (or arg 1))))
     (org-defkey org-agenda-mode-map (kbd "P")
                 (lambda (&optional arg)
                   (interactive "p")
                   (scroll-down (or arg 1))))
     (org-defkey org-agenda-mode-map (kbd "M-n")
                 (lambda () (interactive)
                   "Narrow to line below.  Widen when on bottom."
                   (widen)
                   (when (= 0 (forward-line))
                     (mw-narrow-to-line))))
     (org-defkey org-agenda-mode-map (kbd "M-p")
                 (lambda ()
                   "Narrow to line above.  Widen when on top."
                   (interactive)
                   (widen)
                   (when (= 0 (forward-line -1))
                     (mw-narrow-to-line))))
     (org-defkey org-agenda-mode-map (kbd "-") (lambda () (interactive) (org-agenda-todo "x")))
     (org-defkey org-agenda-mode-map (kbd "S-SPC")
                 (lambda ()
                   (interactive)
                   (let*
                       ((marker (or (org-get-at-bol 'org-marker) (org-agenda-error)))
	                (buffer (marker-buffer marker))
                        (pos (marker-position marker)))
                     (with-current-buffer buffer
                       ;; (widen)
                       (goto-char pos)
                       (org-display-outline-path 'with-filename)))))))
;; keybinding org-agenda:1 ends here

;; timeclock
;; :PROPERTIES:
;; :ID:       7f55df02-668a-42a8-96ef-5db5bc4962e3
;; :END:

;; this timeclock keymap is the suggested from the timeclock source.


;; [[file:init.org::*timeclock][timeclock:1]]
(define-key ctl-x-map "ti" #'timeclock-in)
(define-key ctl-x-map "to" #'timeclock-out)
(define-key ctl-x-map "tc" #'timeclock-change)
(define-key ctl-x-map "tr" #'timeclock-reread-log)
(define-key ctl-x-map "tu" #'timeclock-update-mode-line)
(define-key ctl-x-map "tv" #'timeclock-visit-timelog)
(define-key ctl-x-map "tw" #'timeclock-when-to-leave-string)
(define-key ctl-x-map "tt" #'timeclock-mode-line-display)
;; timeclock:1 ends here

;; [[file:init.org::*timeclock][timeclock:2]]
(setq timeclock-file "/home/b/busi/archiv/timelogs/timelog")
;; timeclock:2 ends here

;; prettify symbols in org files :org:
;; :PROPERTIES:
;; :ID:       44aa08a5-0573-4d98-900a-5ae1cc07c004
;; :END:

;; print some text using a glyph or something else.

;; activate with M-x prettify-symbols-mode.


;; [[file:init.org::*prettify symbols in org files][prettify symbols in org files:1]]
(add-hook
 'org-mode-hook
 (lambda ()
   (setq prettify-symbols-alist
         (append prettify-symbols-alist
                 '(("see" . ?ᗉ)
                   ("See" . ?ᗉ)
                   ("siehe" . ?ᗉ)
                   ("Siehe" . ?ᗉ)
                   ("note" . ?✎)
                   ("Note" . ?✎)
                   ("notiz" . ?✎)
                   ("Notiz" . ?✎)
                   ("exists" . ?∃))))))
;; prettify symbols in org files:1 ends here

;; enable global-prettify-symbols-mode
;; :PROPERTIES:
;; :ID:       e8d130a0-e080-470e-989b-a52851bf96d1
;; :END:


;; [[file:init.org::*enable global-prettify-symbols-mode][enable global-prettify-symbols-mode:1]]
(global-prettify-symbols-mode)
;; enable global-prettify-symbols-mode:1 ends here

;; foldout
;; :PROPERTIES:
;; :ID:       17975ad7-affa-4d1d-adad-88e95ec8796d
;; :END:

;; Foldout is a companion for outline mode and org mode.  Foldout
;; realizes fold (in the sense of narrow) to a subtree.

;; One can fold into further subtrees and later exit each fold.


;; [[file:init.org::*foldout][foldout:1]]
(require 'foldout)
;; foldout:1 ends here

;; tweaking for org
;; :PROPERTIES:
;; :ID:       4c3ede39-2ad2-4460-bbfb-9f6742582eef
;; :END:

;; The standard ~foldout-zoom-subtree~ reveals drawers which annoys me.


;; [[file:init.org::*tweaking for org][tweaking for org:1]]
(defun foldout-zoom-org-subtree (&optional exposure)
  "Same as `foldout-zoom-subtree' with often nicer zoom in Org mode."
  (interactive "P")
  (cl-letf
      (((symbol-function #'outline-show-entry) (lambda () (org-show-entry))))
    (foldout-zoom-subtree exposure)
    ;; (org-cycle)
    ))
;; tweaking for org:1 ends here

;; async tangle
;; :PROPERTIES:
;; :ID:       bc671ae6-2d91-4183-8806-ec32c198a96e
;; :END:


;; [[file:init.org::*async tangle][async tangle:1]]
(defun mw-org-babel-tangle-buffer-asynchronously ()
  "Tangle current buffer asynchronously."
  (interactive)
  (async-start
   `(lambda ()
      (mapc
       (lambda (x) (push x load-path))
       '("~/s/org/org-mode/lisp"
         "~/s/org/org-mode/contrib/lisp"
         "~/s/elisp/mw/org-pretty-tags"
         "~/s/elisp/external/org-reveal"
         "~/s/elisp/external/project-emacs--folding-mode"
         "~/s/elisp/external/org-bullets"
         "~/s/elisp/external/lentic"
         "~/s/elisp/external/m-buffer-el"
         "~/s/elisp/external/bbdb/lisp"
         "~/s/elisp/external/gopher.el/"
         "~/s/elisp/external/el-csv"
         "~/s/elisp/external/tiro"
         "~/s/elisp/external/org-velocity"
         "~/s/elisp/external/org-mind-map"
         "~/s/elisp/mw/keystrokes/"
         "~/s/elisp/mw/lisp-butt-mode"
         "~/s/elisp/mw/org-section-numbers"
         "~/s/elisp/external/emacsed"
         "~/s/elisp/mw/carry-region"
         "~/s/elisp/mw/reverse-words/dist"
         "~/s/elisp/mw/major-mode-stack"
         "~/s/elisp/mw/date-at-point"
         "~/s/elisp/external/emms/lisp"
         "~/s/elisp/mw/emacsshot"
         "~/s/elisp/mw/auxies"
         "~/s/elisp/mw/auxies"
         "~/s/elisp/mw/fit-text-scale"
         "~/s/elisp/mw/ariadne-marks"
         "~/s/elisp/mw/postbank-to-ledger/"
         "~/s/elisp/mw/org-insert-context-dependent/"
         "~/s/elisp/external/refine"
         "~/s/elisp/external/syntactic-close"
         "~/s/elisp/mw/joy"
         "~/s/elisp/mw/org-reverse-subtrees"
         "~/s/elisp/mw/--rest/little-code-things/"
         "~/s/elisp/mw/quit-emacs"
         "~/s/elisp/mw/tz-conversion/"
         "~/s/elisp/mw/hide-common-indent/"
         "~/s/elisp/mw/lab/org-copy-headlines-only"
         "~/s/elisp/external/zen-reward-mode/"
         "~/s/elisp/mw/hide-parts"
         "~/s/elisp/mw/logbook-sorter"
         "~/s/elisp/external/txr-mode"
         "~/s/elisp/mw/cursor-color-mode"
         "~/s/elisp/mw/lab/org-structure-as-dirs-and-files"
         "~/s/elisp/external/maxima"
         "~/s/elisp/external/jl-encrypt"
         "~/s/elisp/mw/mpages"))
        (require 'org)
        (org-babel-tangle-file ,buffer-file-name)
        (shell-command "espeak -v en -s 155 'Woohoo!  something has been tangled.'"))))
;; async tangle:1 ends here

;; [[file:init.org::*a key for Info-search-next][a key for Info-search-next:3]]
(with-eval-after-load 'info
  (progn (define-key Info-mode-map (kbd "a") #'Info-search-next)))
;; a key for Info-search-next:3 ends here

;; mark inner of certain blocks
;; :PROPERTIES:
;; :ID:       f6293401-458f-4c8b-8a78-aa8e3e13d918
;; :END:


;; [[file:init.org::*mark inner of certain blocks][mark inner of certain blocks:1]]
(defun mw-org-mark-block-content ()
  "Mark block without the delimiter lines."
  (interactive)
  (let ((element (org-element-at-point)))
    (catch 'found
      (while  element
        (when (member (car element) '(src-block verse-block))
          (push-mark
           (save-excursion
             (goto-char (org-element-property :end element))
             (skip-chars-backward "\n\t ")
             (forward-line 0)
             (point))
           t t)
	  (goto-char (org-element-property :begin element))
          (forward-line 1))
        (setq element (plist-get (cadr element) :parent))))))
;; mark inner of certain blocks:1 ends here

;; mark inner of source block :org:
;; :PROPERTIES:
;; :ID:       a0f6de94-24e1-4149-a79a-edd3124d4518
;; :END:

;; use case: mark code for evaluation.

;; function to determine if point is inside a src-block?


;; [[file:init.org::*mark inner of source block][mark inner of source block:1]]
(defun mw-org-at-src-block-at-point ()
  "Return src block at point.  nil if there is none."
  (save-excursion
    (let ((element (org-element-at-point)))
      (while (and element (not (member (car element) '(src-block))))
        (setq element (plist-get (cadr element) :parent)))
      element)))
;; mark inner of source block:1 ends here

;; [[file:init.org::*mark inner of source block][mark inner of source block:2]]
(defun mw-org-mark-src-block-content ()
  "Mark source block without the delimiter lines."
  (interactive)
  (deactivate-mark)
  (let ((element (mw-org-at-src-block-at-point)))
    (when element
      (push-mark
       (save-excursion
         (goto-char (org-element-property :end element))
         (skip-chars-backward "\n\t ")
         (forward-line 0)
         (point))
       t t)
      (goto-char (org-element-property :begin element))
      (forward-line 1))))
;; mark inner of source block:2 ends here

;; directly evaluate elisp of a src block :org:
;; :PROPERTIES:
;; :ID:       6dab9d8f-fae3-4f79-b34a-8829e85fad66
;; :END:


;; [[file:init.org::*directly evaluate elisp of a src block][directly evaluate elisp of a src block:1]]
(defun mw-org-babel-direct-eval-src-block-as-elisp ()
  "Mark and eval src block as direct elisp."
  (interactive)
  (mw-org-mark-src-block-content)
  (exchange-point-and-mark) ; point at the end.
  (eval-region (region-beginning) (region-end)))
;; directly evaluate elisp of a src block:1 ends here

;; [[file:init.org::*directly evaluate elisp of a src block][directly evaluate elisp of a src block:2]]
(defun mw-org-babel-direct-eval-next-src-block-as-elisp ()
  "Mark and eval src block as direct elisp."
  (interactive)
  (org-babel-next-src-block)
  (mw-org-babel-direct-eval-src-block-as-elisp))
;; directly evaluate elisp of a src block:2 ends here

;; the style functionality
;; :PROPERTIES:
;; :ID:       03b22f2d-0bcc-4384-9f8e-486de2aeb91c
;; :END:


;; [[file:init.org::*the style functionality][the style functionality:1]]
(defun mw-blanklines-assure (num)
  "Assure exactly NUM blanklines and set point to the end of those.
The buffer remains unchanged when the blanklines are already
there and empty."
  (skip-chars-backward "\t \n")
  (let ((numplusone-newlines (make-string (1+ num) ?\n)))
    (if (looking-at (concat numplusone-newlines  "[^\n]"))
        (goto-char (1- (match-end 0)))
      (when (looking-at "\\(\\(\t\\| \\)*\n\\)+")
        (delete-region (match-beginning 0) (match-end 0)))
      (insert numplusone-newlines))))

(defun mw-ledger-style-one-empty-line-above-xacts ()
  "Make sure there is exactly one empty line before each xact."
  (interactive)
  (goto-char (point-min))
  (let ((point-before (point)))
    (ledger-navigate-next-xact)
    (while (not (= (point) point-before))
      (mw-blanklines-assure 1)
      (setq point-before (point))
      (ledger-navigate-next-xact))))
;; the style functionality:1 ends here

;; ledger


;; [[file:init.org::*ledger][ledger:1]]
(add-hook 'before-save-hook
          (lambda () (when (equal 'ledger-mode major-mode)
                  (save-excursion
                    (mw-ledger-style-one-empty-line-above-xacts)))))
;; ledger:1 ends here



;; also align the transactions.


;; [[file:init.org::*ledger][ledger:2]]
(add-hook 'before-save-hook
          (lambda () (when (equal 'ledger-mode major-mode)
                  (save-excursion
                    (ledger-post-align-postings (point-min) (point-max))))))
;; ledger:2 ends here

;; org


;; [[file:init.org::*org][org:1]]
(add-hook 'before-save-hook
          (lambda () (when (equal 'org-mode major-mode)
                  (save-excursion
                    (org-style-entry)))))
;; org:1 ends here

;; abbrevs everywhere
;; :PROPERTIES:
;; :ID:       5e088d3a-898c-4a05-b93d-31b2186f7c8c
;; :END:

;; Started with the suggestion about abbreviations on
;; http://www.star.bris.ac.uk/bjm/emacs-tips.html#sec-1-19.


;; [[file:init.org::*abbrevs everywhere][abbrevs everywhere:1]]
(setq-default abbrev-mode t)     ;; enable abbreviations
;(setq-default abbrev-mode nil)     ;; disable abbreviations
;(setq save-abbrevs t)            ;; save abbreviations upon exiting
;; abbrev-file-name ; using the default setting.
(quietly-read-abbrev-file)       ;; reads the abbreviations file on startup
;; abbrevs everywhere:1 ends here

;; define abbrev starting with region
;; :PROPERTIES:
;; :ID:       aa4bf95f-ff1f-4a08-8a3d-c16d2d17329d
;; :END:


;; [[file:init.org::*define abbrev starting with region][define abbrev starting with region:1]]
(defun mw-region-define-global-abbrev (beg end)
  "Define abbrev starting with the region."
  (interactive "r")
  (cl-assert (< beg end))
  (let ((expansion (buffer-substring beg end)))
    (define-global-abbrev
      (read-string (format "Abbrev for \"%s\": " expansion))
      expansion)))
;; define abbrev starting with region:1 ends here

;; fit text scale
;; :PROPERTIES:
;; :ID:       9a698e8f-7982-44f9-b865-5eb4b5652fcb
;; :END:


;; [[file:init.org::*fit text scale][fit text scale:1]]
(add-to-list 'load-path "~/s/elisp/mw/fit-text-scale")
(require 'fit-text-scale)
;; fit text scale:1 ends here

;; [[file:init.org::*fit text scale][fit text scale:2]]
(global-set-key
 (kbd "C-x C-&")
 (lambda (&optional arg)
   (interactive "P")
   (cond
    ((equal arg '(4)) (fit-text-scale-max-font-size-fit-line))
    ((equal arg '(16)) (fit-text-scale-max-font-size-fit-line-up-to-cursor))
    ((and (region-active-p) (< (region-beginning) (region-end)))
     (save-restriction
       (narrow-to-region (region-beginning) (region-end))
       (fit-text-scale-max-font-size-fit-lines)))
    (t (fit-text-scale-max-font-size-fit-lines)))))
;; fit text scale:2 ends here

;; [[file:init.org::*fit text scale][fit text scale:3]]
(global-set-key
 (kbd "C-x C-*")
 (lambda (&optional arg)
   (interactive "P")
   (if (and (region-active-p) (< (region-beginning) (region-end)))
       (save-restriction
         (narrow-to-region (region-beginning) (region-end))
         (fit-text-scale-max-font-size-fit-buffer))
     (fit-text-scale-max-font-size-fit-buffer))))
;; fit text scale:3 ends here

;; rotate windows
;; :PROPERTIES:
;; :ID:       30f174db-4c99-445a-bc67-c2fef0b30b94
;; :END:


;; [[file:init.org::*rotate windows][rotate windows:1]]
(defun mw-rotate-split ()
  "Somehow rotate buffers in the emacs-window.

Originates from gnu.emacs.help group 2006."
  (interactive)
  (let ((root (car (window-tree))))
    (if (listp root)
	(let* ((w1 (nth 2 root))
	       (w2 (nth 3 root))
	       (b1 (window-buffer w1))
	       (b2 (window-buffer w2)))
	  (cond ((car root)
		 (delete-window w2)
		 (set-window-buffer (split-window-horizontally) b2))
		(t
		 (delete-window w1)
		 (set-window-buffer (split-window-vertically) b1))))
      (message "Root window not split"))))
;; rotate windows:1 ends here

;; deadgrep :find:
;; :PROPERTIES:
;; :ID:       a904955e-0825-4b9f-b87d-da8cea7088d6
;; :END:

;; deadgrep is an interface to ripgrep.


;; [[file:init.org::*deadgrep][deadgrep:1]]
(straight-use-package 'deadgrep)
;; deadgrep:1 ends here

;; [[file:init.org::*deadgrep][deadgrep:2]]
(defalias 'dg #'deadgrep)
;; deadgrep:2 ends here

;; rg via grep interface :find:
;; :PROPERTIES:
;; :ID:       68ec7b3a-15aa-4af3-82bb-4dc84c3107fb
;; :END:

;; - [2019-12-12 Thu 09:40] from https://stegosaurusdormant.com/emacs-ripgrep/.


;; [[file:init.org::*rg via grep interface][rg via grep interface:1]]
(require 'grep)

(grep-apply-setting
   'grep-find-command
   '("rg -n -H --no-heading -e '' $(git rev-parse --show-toplevel || pwd)" . 27))
;; rg via grep interface:1 ends here

;; ariadne
;; :PROPERTIES:
;; :ID:       c40b4f41-a75f-44d0-84a2-c650824359c6
;; :END:


;; [[file:init.org::*ariadne][ariadne:1]]
(add-to-list 'load-path "~/s/elisp/mw/ariadne-marks")
(require 'ariadne-marks)
;; ariadne:1 ends here

;; use animate-string
;; :PROPERTIES:
;; :ID:       82a74bc1-a20d-4a99-b460-c8b26597470c
;; :END:

;; this is a start.  somewhat similar to the beacon mode.


;; [[file:init.org::*use animate-string][use animate-string:1]]
(defun mw-dance-around-the-cursor-a ()
  (interactive)
  (save-excursion
    (let
        ((poi (point))
         (start (progn
                  (move-to-window-line 0)
                  (point)))
         (end (progn
                (move-to-window-line -1)
                (point))))
      (mw-create-sha1-hash-named-copy-of-region start end)
      (goto-char (1+ (- poi start)))
      (setq buffer-read-only nil)
      (let ((animate-n-steps 10))
        (animate-string
         "POINT" (line-number-at-pos)
         (current-column)))
      (sit-for 0.1))
    (kill-buffer)))
;; use animate-string:1 ends here

;; move point some 1
;; :PROPERTIES:
;; :ID:       77df5953-3bcf-44a1-b7c9-240d1d8ec22c
;; :END:


;; [[file:init.org::*move point some 1][move point some 1:1]]
(defun mw-dance-around-the-cursor-b ()
  (interactive)
  (save-excursion
    (let ((poi (point)))
      (cl-loop
       for kk from 10 downto 1 do
       (goto-char poi)
       (cl-loop for ii from kk downto 1 do
                (unless (bobp) (backward-char))
                (sit-for 0.01)
                (unless (eobp) (forward-char))
                (sit-for 0.01)
                (unless (eobp) (forward-char))
                (sit-for 0.01)
                (unless (bobp) (backward-char))))))
                (sit-for 0.01))
;; move point some 1:1 ends here

;; move point some 2
;; :PROPERTIES:
;; :ID:       161e3299-f4d3-42df-b504-658f535144e0
;; :END:


;; [[file:init.org::*move point some 2][move point some 2:1]]
(defun mw-dance-around-the-cursor-c ()
  "Move the cursor around the current cursor position.
Rationale: find the cursor easily."
  (interactive)
  (save-excursion
    (let ((poi (point)))
      (cl-loop
       for kk from 10 downto 1 do
       (progn
         (unless (bobp) (backward-char))
         (sit-for 0.01)))
      (goto-char poi)
      (cl-loop
       for kk from 10 downto 1 do
       (progn
         (unless (eobp) (forward-char))
         (sit-for 0.01)))
      (goto-char poi))))
;; move point some 2:1 ends here

;; mouse dance via thread
;; :PROPERTIES:
;; :ID:       ef24bb46-6bc8-4013-ab8c-d81d91374878
;; :END:


;; [[file:init.org::*mouse dance via thread][mouse dance via thread:1]]
(defun mw-mouse-spiral-at-cursor ()
  (let ((pos (cdr (mouse-avoidance-point-position)))
        (stretch-start 15.0)
        (stretch-growth 0.99)
        (delta 0.1)
        (rotations 5.0))
    (cl-do ((alpha 0.0 (+ delta alpha))
            (stretch stretch-start (* stretch stretch-growth)))
        ((< (* rotations 2 3.141) alpha))
      (set-mouse-position
       (selected-frame)
       (+ (car pos) (floor (* stretch (cos alpha))))
       (+ 1 (cdr pos) (floor (* 0.5 stretch (sin alpha)))))
      (sit-for 0.005))))

(defun mw-mouse-dance-at-cursor ()
  (interactive)
  (make-thread #'mw-mouse-spiral-at-cursor))
;; mouse dance via thread:1 ends here

;; goto end-of-subtree :org:
;; :PROPERTIES:
;; :ID:       2455c1f6-571e-4625-94be-a5027cade754
;; :END:

;; provide functionality goto end-of-subtree as command.


;; [[file:init.org::*goto end-of-subtree][goto end-of-subtree:1]]
(defun mw-org-end-of-subtree ()
  (interactive)
  (if (org-before-first-heading-p)
      (goto-char (point-max))
    (outline-end-of-subtree)))
;; goto end-of-subtree:1 ends here

;; key
;; :PROPERTIES:
;; :ID:       2e0d3a2b-fffe-4548-8677-1f61c09d6aa5
;; :END:


;; [[file:init.org::*key][key:1]]
(with-eval-after-load
    "org"
  (progn
     (org-defkey org-mode-map (kbd "C-)") #'mw-org-end-of-subtree)))
;; key:1 ends here

;; hidden mode line
;; :PROPERTIES:
;; :ID:       54610295-83bb-4d71-b7f7-2a04e19ebd04
;; :END:

;; Found the following mode line hiding function at
;; http://bzg.fr/emacs-hide-mode-line.html.  (Bastien)


;; [[file:init.org::*hidden mode line][hidden mode line:1]]
(defvar-local hidden-mode-line-mode nil)

(define-minor-mode hidden-mode-line-mode
  "Minor mode to hide the mode-line in the current buffer."
  :init-value nil
  :global t
  :variable hidden-mode-line-mode
  :group 'editing-basics
  (if hidden-mode-line-mode
      (setq hide-mode-line mode-line-format
            mode-line-format nil)
    (setq mode-line-format hide-mode-line
          hide-mode-line nil))
  (force-mode-line-update)
  ;; Apparently force-mode-line-update is not always enough to
  ;; redisplay the mode-line
  (redraw-display)
  (when (and (called-interactively-p 'interactive)
             hidden-mode-line-mode)
    (run-with-idle-timer
     0 nil 'message
     (concat "Hidden Mode Line Mode enabled.  "
             "Use M-x hidden-mode-line-mode to make the mode-line appear."))))

;; If you want to hide the mode-line in every buffer by default
;; (add-hook 'after-change-major-mode-hook 'hidden-mode-line-mode)

(defun mw-hidden-mode-line-for-all ()
  (interactive)
  (dolist (buffer (buffer-list))
    (set-buffer buffer)
    (unless hidden-mode-line-mode
      (hidden-mode-line-mode))))

(defun mw-hidden-mode-line-deactivate-for-all ()
  (interactive)
  (dolist (buffer (buffer-list))
    (set-buffer buffer)
    (when hidden-mode-line-mode
      (hidden-mode-line-mode))))
;; hidden mode line:1 ends here

;; org attach embedded images :org:
;; :PROPERTIES:
;; :ID:       11db4b25-fb0c-49f1-8576-3233639d1ba8
;; :END:

;; save images in buffer to attachments.

;; this can be useful after paste from eww to org to persist images.

;; loading this extension via the org module interface.  see variable
;; [[elisp:(describe-variable 'org-modules)]].


;; [[file:init.org::*org attach embedded images][org attach embedded images:1]]
(push "~/s/elisp/maintain/org-attach-embedded-images" load-path)
;; org attach embedded images:1 ends here

;; key to switch from fancy diary to calendar :diary:
;; :PROPERTIES:
;; :ID:       5875e3f9-c4cc-4294-8cdc-98a7276efd3d
;; :END:

;; from fancy diary display use =c= to open the calendar.


;; [[file:init.org::*key to switch from fancy diary to calendar][key to switch from fancy diary to calendar:1]]
(with-eval-after-load 'diary-lib
(define-key diary-fancy-overriding-map "c" #'calendar))
;; key to switch from fancy diary to calendar:1 ends here

;; help transform bank data to ledger transactions
;; :PROPERTIES:
;; :ID:       13fbb299-dace-4cd9-945a-e3cf81b37718
;; :END:


;; [[file:init.org::*help transform bank data to ledger transactions][help transform bank data to ledger transactions:1]]
(push "~/s/elisp/mw/postbank-to-ledger/" load-path)
(require 'postbank-to-ledger)
;; help transform bank data to ledger transactions:1 ends here

;; reverse a line char wise
;; :PROPERTIES:
;; :ID:       dfadcd44-48f5-44b6-9ada-171b30a7b1f9
;; :END:


;; [[file:init.org::*reverse a line char wise][reverse a line char wise:1]]
(defun mw-reverse-line ()
  "Reverse the letters of the line containing point.
Cursor ends somewhere in that line."
  (interactive)
  (let* ((pt (point))
         (bol (progn (beginning-of-line) (point)))
         (offset (- pt bol))
         (eol (progn (end-of-line) (point)))
         (reversel (nreverse (buffer-substring bol eol))))
    (delete-region bol eol)
    (insert reversel)
    (goto-char (- eol offset))))

(ert-deftest test-1-mw-reverse-line ()
  (should (string= "zab rab oof"
                   (with-temp-buffer
             (insert "foo bar baz
")
             (goto-char 1)
             (mw-reverse-line)
             (buffer-substring 1 12)))))

(ert-deftest test-2-mw-reverse-line ()
  (should (= 9
             (with-temp-buffer
               (insert "foo bar baz
")
               (goto-char 4)
               (mw-reverse-line)
               (point)))))
;; reverse a line char wise:1 ends here

;; M-RET insert heading with date if above is like so
;; :PROPERTIES:
;; :ID:       8ea790fc-1d00-4942-ac47-a80a67e56557
;; :END:


;; [[file:init.org::*M-RET insert heading with date if above is like so][M-RET insert heading with date if above is like so:1]]
(push "~/s/elisp/mw/org-insert-context-dependent/" load-path)
(require 'org-insert-context-dependent)

(add-hook 'org-metareturn-hook #'org-icd-insert-heading-with-i-ts)
;; M-RET insert heading with date if above is like so:1 ends here

;; subtrees
;; :PROPERTIES:
;; :ID:       7085af7a-04a2-4698-b11a-0aba700a2fdc
;; :END:


;; [[file:init.org::*subtrees][subtrees:1]]
(defun mw-org-subtree-make-last-sibling ()
  "Move sibling so it gets last sibling."
  (org-back-to-heading t)
  (org-cut-special)
  (go-up)
  (mw-org-end-of-subtree)
  (org-yank)
  (org-back-to-heading t))
;; subtrees:1 ends here

;; [[file:init.org::*subtrees][subtrees:2]]
(defun mw-org-subtree-make-first-sibling ()
 "Move sibling so it gets first sibling.
Note: This defun has potential to be implemented more efficiently."
 (while (ignore-errors (org-move-subtree-up))))
;; subtrees:2 ends here

;; plain lists
;; :PROPERTIES:
;; :ID:       8e57012e-54fc-4bf1-82c8-e5f388bfd4bd
;; :END:


;; [[file:init.org::*plain lists][plain lists:1]]
(defun mw-org-move-item-to-bottom ()
  "Move plain list item so it gets last sibling."
  (cl-flet ((user-error (format &rest args) (throw 'end-reached nil)))
    (catch 'end-reached
      (while t
        (org-move-item-down)))))
;; plain lists:1 ends here

;; [[file:init.org::*plain lists][plain lists:2]]
(defun mw-org-move-item-to-top ()
  "Move plain list item so it gets first sibling."
  (cl-flet ((user-error (format &rest args) (throw 'at-border nil)))
    (catch 'at-border
      (while t
        (org-move-item-up)))))
;; plain lists:2 ends here

;; alternative impl


;; [[file:init.org::*alternative impl][alternative impl:1]]
(defun mw-org-item-make-last ()
  "Make item last sibling."
  (interactive)
  (unless (org-at-item-p) (error "Not at an item"))
  (let* ((col (current-column))
	 (item (point-at-bol))
	 (struct (org-list-struct)))
    (setq struct (org-list-send-item item 'end struct))
    (org-list-write-struct struct (org-list-parents-alist struct))
    (org-move-to-column col)))
;; alternative impl:1 ends here

;; [[file:init.org::*alternative impl][alternative impl:2]]
(defun mw-org-item-make-first ()
  "Make item first sibling."
  (interactive)
  (unless (org-at-item-p) (error "Not at an item"))
  (let* ((col (current-column))
	 (item (point-at-bol))
	 (struct (org-list-struct)))
    (setq struct (org-list-send-item item 'begin struct))
    (org-list-write-struct struct (org-list-parents-alist struct))
    (org-move-to-column col)))
;; alternative impl:2 ends here

;; move to the moved item
;; :PROPERTIES:
;; :ID:       2198d32f-dd65-4a3d-85b3-72070fa61034
;; :END:


;; [[file:init.org::*move to the moved item][move to the moved item:1]]
(defun mw-org-move-to-bottom ()
  "Position subtree or item at point at the end of the list."
  (interactive)
  (cond
   ((org-at-item-p) (mw-org-move-item-to-bottom))
   (t (mw-org-subtree-make-last-sibling))))
;; move to the moved item:1 ends here

;; [[file:init.org::*move to the moved item][move to the moved item:2]]
(defun mw-org-move-to-top ()
  "Position subtree or item at point at the top of the list."
  (interactive)
  (cond
   ((org-at-item-p) (mw-org-move-item-to-top))
   (t (mw-org-subtree-make-first-sibling))))
;; move to the moved item:2 ends here

;; first child to end :org:
;; :PROPERTIES:
;; :ID:       3bdf755f-4fc7-4aa1-8f53-37c3f5a64334
;; :END:


;; [[file:init.org::*first child to end][first child to end:1]]
(defun mw-org-move-first-child-subtree-to-end ()
  "Move first child to end."
  (interactive)
  (org-next-visible-heading 1)
  (org-cut-special)
  (backward-char) ; back to parent.  todo: corner case.
  (mw-org-end-of-subtree)
  (skip-chars-forward " \n")
  (save-excursion
    (yank)))
;; first child to end:1 ends here

;; move or copy region
;; :PROPERTIES:
;; :ID:       e0d66cd9-ba5e-4026-83cf-4e6f0919ceea
;; :END:


;; [[file:init.org::*move or copy region][move or copy region:1]]
(defun mw-region-teleport-to-other-window (start end)
  "Move region to position in other window."
  (interactive "r")
  (let ((string (buffer-substring start end)))
    (delete-region start end)
    (other-window 1)
    (let ((beg (point)))
      (insert string)
      (set-mark beg))))
;; move or copy region:1 ends here

;; [[file:init.org::*move or copy region][move or copy region:2]]
(defun mw-region-append-to-other-window (start end)
  "Append region to position in other window."
  (interactive "r")
  (let ((string (buffer-substring start end)))
    (other-window 1)
    (let ((beg (point)))
      (insert string)
      (set-mark beg))
    (other-window 1)))
;; move or copy region:2 ends here

;; [[file:init.org::*move or copy region][move or copy region:3]]
(defun mw-region-append-to-other-frame (start end)
  "Append region to position in other frame."
  (interactive "r")
  (let ((string (buffer-substring start end)))
    (other-frame 1)
    (let ((beg (point)))
      (insert string)
      (set-mark beg))
    (other-frame 1)))
;; move or copy region:3 ends here

;; define some euro units for calc :personal:
;; :PROPERTIES:
;; :catchwords: currency
;; :ID:       b16ce40e-f0d4-4626-9649-348887462751
;; :END:

;; Use this macro (i.e. M-x mw-calc-init-euro-units) in a calc buffer to
;; define units eur, eurd and eurc.  this sets 1 eur = 10 eurd = 100 eurc.


;; [[file:init.org::*define some euro units for calc][define some euro units for calc:1]]
(fset 'mw-calc-init-euro-units
      (kmacro-lambda-form [?1 ?  ?u ?d ?e ?u ?r ?c return ?o ?n ?e ?  ?e ?u ?r ?o ?  ?c ?o ?n ?t backspace backspace backspace ?e ?n ?t return ?\' ?1 ?0 ?  ?e ?u ?r ?c return ?u ?d ?e ?u ?r ?d return ?t ?e ?n ?  ?e ?u ?r ?o ?  ?c ?e ?n ?t return ?\' ?1 ?0 ?  ?e ?u ?r ?d return ?u ?d ?e ?u ?r return ?o ?n ?e ?  ?e ?u ?r ?o return] 0 "%d"))
;; define some euro units for calc:1 ends here

;; hide-parts
;; :PROPERTIES:
;; :ID:       ffa9fd30-c5b7-41a6-919f-780bbd7d3ec3
;; :END:


;; [[file:init.org::*hide-parts][hide-parts:1]]
(add-to-list 'load-path "~/s/elisp/mw/hide-parts")
(require 'hide-parts)
;; hide-parts:1 ends here

;; mark clones
;; :PROPERTIES:
;; :ID:       5255ec75-6d92-422e-ac55-7ab310f28756
;; :END:

;; A hook allows marking the clones easily.


;; [[file:init.org::*mark clones][mark clones:1]]
(add-hook 'clone-indirect-buffer-hook
      (lambda ()
        (make-local-variable 'is-indirect-clone-p)
        (setq is-indirect-clone-p t)))
;; mark clones:1 ends here

;; dired
;; :PROPERTIES:
;; :ID:       d3462b6d-3908-4c83-bb5d-bb495c393dea
;; :END:


;; [[file:init.org::*dired][dired:1]]
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
;; dired:1 ends here

;; just goto clock in agenda
;; :PROPERTIES:
;; :ID:       27be1af1-0fb1-4b4e-98d8-f1a29c6c78ad
;; :END:

;; org-agenda-clock-goto does more.


;; [[file:init.org::*just goto clock in agenda][just goto clock in agenda:1]]
(defun mw-org-agenda-clock-goto ()
  "Jump to the currently clocked in task within the agenda."
  (interactive)
  (let (pos)
    (mapc (lambda (o)
	    (when (eq (overlay-get o 'type) 'org-agenda-clocking)
	      (setq pos (overlay-start o))))
	  (overlays-in (point-min) (point-max)))
    (when pos (goto-char pos))))
;; just goto clock in agenda:1 ends here

;; enable more emacs features
;; :PROPERTIES:
;; :ID:       6d9446c2-447f-4a92-9873-a9ec8a186eb7
;; :END:

;; Enable features that are disabled by default.

;; Emacs writes these lines if the user decides to enable these features.


;; [[file:init.org::*enable more emacs features][enable more emacs features:1]]
(put 'dired-find-alternate-file 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'erase-buffer 'disabled nil)
(put 'list-threads 'disabled nil)
(put 'list-timers 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'upcase-region 'disabled nil)
;; enable more emacs features:1 ends here

;; transpose
;; :PROPERTIES:
;; :ID:       571381e3-0326-437c-8e54-1f011e2e880f
;; :END:


;; [[file:init.org::*transpose][transpose:1]]
(global-set-key (kbd "C-S-t") #'transpose-sentences)
(global-set-key (kbd "C-S-r") #'transpose-regions)
;; transpose:1 ends here

;; show/hide "entry"
;; :PROPERTIES:
;; :ID:       aa1954f6-f5fe-454f-b659-5a1d14a54399
;; :END:


;; [[file:init.org::*show/hide "entry"][show/hide "entry":1]]
(add-hook
 'org-mode-hook
 (lambda ()
   (key-chord-define-local "h5" #'org-hide-entry)
   (key-chord-define-local "s5" #'org-show-entry)))
;; show/hide "entry":1 ends here

;; org highlight up to double colon in heading
;; :PROPERTIES:
;; :ID:       41e16b11-85a0-4ba5-abff-f5eecab4f062
;; :END:

;; Highlight heading up to double colon like the plain list items.


;; [[file:init.org::*org highlight up to double colon in heading][org highlight up to double colon in heading:1]]
(defface mw-bold-face '((t (:inherit bold))) "bold face")
;; org highlight up to double colon in heading:1 ends here

;; [[file:init.org::*org highlight up to double colon in heading][org highlight up to double colon in heading:2]]
(add-hook
 'org-mode-hook
 (lambda () (highlight-regexp "^\*+ .* :: " 'mw-bold-face)))
(add-hook
 'org-journal-mode-hook
 (lambda () (highlight-regexp "^\*+ .* :: " 'mw-bold-face)))
;; org highlight up to double colon in heading:2 ends here

;; org-journal
;; :PROPERTIES:
;; :ID:       204da6b2-4f78-4a74-b2a1-8973ee54ee62
;; :END:

;; Overwrite the key C-c C-j to create a new journal entry.  Per default org-goto is on that key.


;; [[file:init.org::*org-journal][org-journal:1]]
(org-defkey org-mode-map (kbd "C-c C-j") #'org-journal-new-entry)
;; org-journal:1 ends here

;; org-goto
;; :PROPERTIES:
;; :ID:       5aaeadf4-b95b-4c7f-9400-366eccfc5620
;; :END:


;; [[file:init.org::*org-goto][org-goto:1]]
(org-defkey org-mode-map (kbd "C-c j") #'org-goto)
;; org-goto:1 ends here

;; zap-to-char zap-up-to-char
;; :PROPERTIES:
;; :ID:       620c2dab-06f5-40b9-b304-e6c5d0c0e259
;; :END:

;; the standard M-z calls the up-to version.  M-Z for zap-to-char.


;; [[file:init.org::*zap-to-char zap-up-to-char][zap-to-char zap-up-to-char:1]]
(global-set-key (kbd "M-z") #'zap-up-to-char)
(global-set-key (kbd "M-Z") #'zap-to-char)
;; zap-to-char zap-up-to-char:1 ends here

;; up-list
;; :PROPERTIES:
;; :ID:       15f47f95-52b9-4d1e-a753-88bb32a77093
;; :END:

;; doc: "Move forward out of one level of parentheses."


;; [[file:init.org::*up-list][up-list:1]]
(global-set-key (kbd "C-M-S-u") #'up-list)
;; up-list:1 ends here

;; C-xd to open dired
;; :PROPERTIES:
;; :ID:       ba73f90b-c32b-40d6-8232-a5ff249e85c4
;; :END:


;; [[file:init.org::*C-xd to open dired][C-xd to open dired:1]]
(global-set-key (kbd "C-x C-d") #'dired)
;; C-xd to open dired:1 ends here

;; scroll lock
;; :PROPERTIES:
;; :ID:       bab6bf51-1d0a-4d6d-9628-d00220ddeab0
;; :END:

;; scroll lock is on key scroll lock by default IIRC.


;; [[file:init.org::*scroll lock][scroll lock:1]]
(key-chord-define-global "s1" #'scroll-lock-mode)
;; scroll lock:1 ends here

;; recenter to top
;; :PROPERTIES:
;; :ID:       22308076-f4e9-4a57-84cd-c58149ffb3b5
;; :END:


;; [[file:init.org::*recenter to top][recenter to top:1]]
(global-set-key (kbd "C-<") #'mw-recenter-jump-to-top)
;; recenter to top:1 ends here

;; org agenda
;; :PROPERTIES:
;; :ID:       f30a8a52-02b7-4093-84ec-52e8b2e6007a
;; :END:


;; [[file:init.org::*org agenda][org agenda:1]]
(with-eval-after-load "org-agenda"
  (progn
     (org-defkey org-agenda-mode-map (kbd "C-o") #'org-agenda-show)))
;; org agenda:1 ends here

;; help about local char
;; :PROPERTIES:
;; :ID:       5a40ab12-8964-4efe-821b-96dd0fc1b76e
;; :END:

;; Put =describe-char= into the C-h section.  The command is already on
;; C-u C-x =.  Somehow I think the binding in the help section is natural.


;; [[file:init.org::*help about local char][help about local char:1]]
(define-key help-map "=" (lambda () (interactive) (describe-char (point))))
;; help about local char:1 ends here

;; edit-last-kbd-macro
;; :PROPERTIES:
;; :ID:       7b19bb31-9c9a-427c-9dcd-b4c8e9b90423
;; :END:


;; [[file:init.org::*edit-last-kbd-macro][edit-last-kbd-macro:1]]
(global-set-key (kbd "C-x C-k E") #'edit-last-kbd-macro)
;; edit-last-kbd-macro:1 ends here

;; winner
;; :PROPERTIES:
;; :ID:       a80e9632-edf1-45f0-a4b2-a2d1bacb79d4
;; :END:

;; change window layout with prominent keys.


;; [[file:init.org::*winner][winner:1]]
(global-set-key (kbd "C-<left>") #'winner-undo)
(global-set-key (kbd "C-<right>") #'winner-redo)
;; winner:1 ends here

;; multi-occur
;; :PROPERTIES:
;; :ID:       bc4bddc4-8508-47e4-bff4-609d5a367377
;; :END:

;; Find occurrences in several buffers.


;; [[file:init.org::*multi-occur][multi-occur:1]]
(global-set-key (kbd "M-s O") #'multi-occur-in-matching-buffers)
;; multi-occur:1 ends here

;; imenu
;; :PROPERTIES:
;; :ID:       7c7d3dfd-a0cc-4003-8ddd-6dfdaeeccefb
;; :END:

;; imenu provides a mechanism to jump to locations of interest for
;; several file types, e.g. Org.


;; [[file:init.org::*imenu][imenu:1]]
(global-set-key (kbd "C-*") 'imenu)
;; imenu:1 ends here

;; iedit
;; :PROPERTIES:
;; :ID:       12098f10-f528-4a9f-9e51-190ad76fcba8
;; :END:

;; can help to edit all occurances of a variable.


;; [[file:init.org::*iedit][iedit:1]]
(global-set-key (kbd "C-;") 'iedit-mode)
;; iedit:1 ends here

;; rectangle
;; :PROPERTIES:
;; :ID:       0433147d-df2b-4fa6-938b-cdee6202fa60
;; :END:


;; [[file:init.org::*rectangle][rectangle:1]]
(global-set-key (kbd "C-x r u") #'clear-register)
;; rectangle:1 ends here

;; global keys
;; :PROPERTIES:
;; :ID:       a6d2659d-416c-41c8-a652-86942ad8d906
;; :END:


;; [[file:init.org::*global keys][global keys:1]]
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-switchb)
;; global keys:1 ends here

;; [[file:init.org::*global keys][global keys:2]]
(global-set-key (kbd "C-c v") 'org-velocity)
;; global keys:2 ends here

;; following org-mode links given in other modes
;; :PROPERTIES:
;; :ID:       056989f0-a039-4043-a776-6f102786518a
;; :END:

;; To be able to follow an org-mode link in an arbitrary file can be
;; nice, e.g. to get to the original from within a tangled file.


;; [[file:init.org::*following org-mode links given in other modes][following org-mode links given in other modes:1]]
(global-set-key (kbd "C-c o") #'org-open-at-point-global)
;; following org-mode links given in other modes:1 ends here

;; exit org src edit with c-cc
;; :PROPERTIES:
;; :ID:       36981807-e987-4918-bdf4-ad5fb09654cd
;; :END:


;; [[file:init.org::*exit org src edit with c-cc][exit org src edit with c-cc:1]]
(define-key org-src-mode-map "\C-c\C-c" 'org-edit-src-exit)
;; exit org src edit with c-cc:1 ends here

;; for window


;; [[file:init.org::*for window][for window:1]]
(defvar mw-personal-winmap
      (let ((map (make-sparse-keymap)))
	(define-key map "S" #'emacsshot-snap-window-include-modeline)
	(define-key map "s" #'emacsshot-snap-window)
        (define-key map "-" #'mw-split-window-vertically-at-point)
        (define-key map "|" #'mw-split-window-horizontally-at-point)
        (define-key map "<" #'shrink-window-horizontally) ; hyperbole offers this too.
        (define-key map ">" #'enlarge-window-horizontally) ; hyperbole offers this too.
	  (define-key map "r" #'hydra-winsize/body)
          map))
;; for window:1 ends here

;; keymap for frame


;; [[file:init.org::*keymap for frame][keymap for frame:1]]
(defvar mw-personal-frame-bg-map
      (let ((map (make-sparse-keymap)))
        (define-key map "b" (lambda () (interactive) (set-background-color-all-frames "black")))
        (define-key map "d" (lambda () (interactive) (set-background-color-all-frames "#021")))
        (define-key map "w" (lambda () (interactive) (set-background-color-all-frames "white")))
        (define-key map "c" (lambda () (interactive) (set-background-color-all-frames "lightcyan")))
        map))

(defvar mw-personal-framemap
  (let ((map (make-sparse-keymap)))
    (define-key map "B" mw-personal-frame-bg-map)
    (define-key map "s" #'emacsshot-snap-frame)
    map))
;; keymap for frame:1 ends here

;; keymap for calendar


;; [[file:init.org::*keymap for calendar][keymap for calendar:1]]
(defvar mw-personal-calmap
  (let ((map (make-sparse-keymap)))
    (define-key map "c" (lambda () (interactive)
                          (if (get-buffer "*Calendar*")
                              (switch-to-buffer "*Calendar*")
                            (calendar))))
    (define-key map "<" (lambda () (interactive)
                          "Add date of calendar to kill ring."
                          (kill-new
                           (with-temp-buffer
                             (org-date-from-calendar)
                             (buffer-substring-no-properties (point-min) (point-max))))
                          (yank)))
    map))
;; keymap for calendar:1 ends here

;; keymap for sound

;; mw-personal-soundmap


;; [[file:init.org::*keymap for sound][keymap for sound:1]]
(defvar mw-personal-soundmap
  (let ((map (make-sparse-keymap)))
    (define-key map "b"
      (lambda ()
        (interactive)
        (if (not emms-player-playing-p)
            (progn
              (mw-sound-set-enjoyable-volume)
              (emms-play-url "http://www.bassdrive.com/BassDrive.m3u"))
          (emms-player-stop))))
    (define-key map "0" #'mw-sound-0%)
    (define-key map "1" (lambda () (interactive) (dotimes (_ 50) (emms-volume-lower))))
    (define-key map "2" #'emms-volume-lower)
    (define-key map "3" (lambda () (interactive) (progn (emms-volume-amixer-change -100)
				     (emms-volume-amixer-change 50))))
    (define-key map "4" #'emms-volume-raise)
    (define-key map "5" (lambda () (interactive) (dotimes (_ 50) (emms-volume-raise))))
    map))
;; keymap for sound:1 ends here

;; keymap for Org


;; [[file:init.org::*keymap for Org][keymap for Org:1]]
(defvar mw-personal-orgmap
  (let ((map (make-sparse-keymap)))
    (define-key map "a" #'org-agenda-list)
    (define-key map "E" #'org-ol-delete-stored-links)
    (define-key map "l" #'org-insert-link-global) ; insert link everywhere i.e. also outside org.
    (define-key map "L" #'org-insert-all-links)
    (define-key map "s" ; org.el
      (lambda () (interactive) (find-file (concat mw-org-source-directory "/lisp/org.el"))))
    (define-key map "r" ; refile
      (let ((map (make-sparse-keymap)))
        (define-key map "l" #'org-refile-goto-last-stored)
        map)) ;; recall: from org-files there is already C-u C-u C-c C-w.
    (define-key map "t" #'mw-org-babel-tangle-buffer-asynchronously)
    (define-key map "v" #'org-brain-visualize)
    map))
;; keymap for Org:1 ends here

;; keymap for region


;; [[file:init.org::*keymap for region][keymap for region:1]]
(defvar mw-region-keymap
    (let ((map (make-sparse-keymap)))
	(define-key map "m" #'mw-region-as-last-kbd-macro)
	(define-key map "t" #'mw-region-convert-to-org-time-stamp)
	(define-key map "c" #'mw-region-open-calendar-for-date-in-region)
	map))
;; keymap for region:1 ends here

;; personal keymap


;; [[file:init.org::*personal keymap][personal keymap:1]]
(defvar mw-personal-keymap
  (let ((map (make-sparse-keymap)))
    (define-key map "0" (lambda () (interactive) (find-file "~/0")))
    (define-key map "1" (lambda () (interactive) (find-file "~/1")))
    (define-key map "/" #'dg)
    (define-key map "j" #'perspec)
    (define-key map "r" mw-region-keymap)
    (define-key map "f" mw-personal-framemap)
    (define-key map "G" #'gnus)
    (define-key map "g"
      (lambda () (interactive)
        (if (get-buffer "*Group*") ; when gnus is already there...
            (execute-kbd-macro (kbd "C-z & C-z G")) ; ...refresh gnus using a new thread.
          (gnus))))
    (define-key map "l" #'clone-indirect-buffer)
    (define-key map "A" #'mw-append-to-scratch)
    (define-key map "d" #'herald-the-mode-line)
    (define-key map "F" #'fringe-mode)
    (define-key map "&" #'emacspeak-wizards-execute-asynchronously) ;; remember Michaels talk on threads.
    (define-key map "p" #'password-store-copy)
    (define-key map "P" #'mw-password-store-pick-user)
    (define-key map (kbd "C-j") #'org-clock-goto)
    (define-key map "z" #'mw-auxies-delete-to-direction)
    (define-key map "k" #'key-chord-mode) ; sometimes i need turning it off and on again.
    (define-key map "c" mw-personal-calmap)
    (define-key map "q" (lambda (arg)
                          (interactive "P")
                          (if (equal arg '(4))
                              (unbury-buffer)
                            (bury-buffer))))
    (define-key map "u" #'unexpand-abbrev)
    (define-key map "i" #'ido-hacks-mode)
    (define-key map "w" mw-personal-winmap)
    (define-key map "<" #'mw-screen-exchange-slurp-insert)
    (define-key map ">" #'mw-screen-exchange-write-region)
    (define-key map "e" #'calc-embedded)
    (define-key map "(" #'paredit-mode)
    (define-key map ")" #'lispy-mode)
    (define-key map "W" #'calc-embedded-word) ; recall 'q' to leave the mode.
    (define-key map "S" (lambda () (interactive) (switch-to-buffer "*scratch*")))
    (define-key map "s" mw-personal-soundmap)
    (define-key map "o" mw-personal-orgmap)
    (define-key map "O" #'mw-org-kill-new-outline-path) ; Has been useful for refile.
    (define-key map "m" #'mw-region-teleport-to-other-window) ; move
    (define-key map "a" (let ((loca-map (make-sparse-keymap)))
			  (define-key loca-map "w" #'mw-region-append-to-other-window)
			  (define-key loca-map "f" #'mw-region-append-to-other-frame)
			  loca-map))
    (define-key map "=" #'mw-duplicate-term)
;    (define-key map "-" #'hydra-pomodoro/body)
    (define-key map "-" #'hydra-minute-timer/body)
    (define-key map "t" #'org-todo-current-clock-entry)
    (define-key map "%" #'mw-cycle-delimiters)
    map)
  "Personal convenience keymap.")
(global-set-key (kbd "\C-z") mw-personal-keymap)
;; personal keymap:1 ends here

;; winner
;; :PROPERTIES:
;; :ID:       f9451bae-3489-4819-853e-c6d3880fcb8c
;; :END:

;; the following hydra is thought to supplement C-c right and C-c left.


;; [[file:init.org::*winner][winner:1]]
(defhydra hydra-winner (:body-pre (winner-undo))
  "Go back window layouts with 'winner'."
  ("<left>" winner-undo)
  ("<right>" winner-redo "Go back to layout from where winner started.":exit t)
  ("q" ignore "Quit hydra and stay with this layout for now."
  :exit t))

(global-set-key (kbd "C-x <left>") #'hydra-winner/body)
;; winner:1 ends here

;; window size
;; :PROPERTIES:
;; :ID:       0f4ae608-4161-4130-8079-60593672153f
;; :END:


;; [[file:init.org::*window size][window size:1]]
(defhydra hydra-winsize ()
  "Shrink window."
  ("<up>" shrink-window)
  ("<down>" enlarge-window)
  ("<left>" shrink-window-horizontally)
  ("<right>" enlarge-window-horizontally))
;; window size:1 ends here

;; faces
;; :PROPERTIES:
;; :ID:       d6a72b21-42bd-4874-8f7f-3817e0efe286
;; :END:


;; [[file:init.org::*faces][faces:1]]
(defun mw-face-set-default-relatively-big (&optional val)
  "Set the default face height to VAL.
Default is 255 which stands typically for a relatively big font."
(interactive)
  (setq val (or val 255))
  (set-face-attribute
          'default (selected-frame)
          :height val))
;; faces:1 ends here

;; [[file:init.org::*faces][faces:2]]
(defun mw-increase-face-height ()
  (interactive)
  (let ((before-height (face-attribute 'default :height)))
    (cl-loop while (and (< change 42)
                        (= before-height (face-attribute 'default :height)))
             with change = 1
             do (progn
                  (set-face-attribute
                   'default (selected-frame)
                   :height (max 1 (+ before-height change)))
                  (sit-for 0))
             (cl-incf change))
    (message "face height: %d" (face-attribute 'default :height))))

(defun mw-decrease-face-height ()
  (interactive)
  (let ((before-height (face-attribute 'default :height)))
    (cl-loop while (and (< change 42) (= before-height (face-attribute 'default :height)))
             with change = 1
             do (progn
                  (set-face-attribute
                   'default (selected-frame)
                   :height (max 1 (- before-height change)))
                  (sit-for 0))
             (cl-incf change)))
  (message "face height: %d" (face-attribute 'default :height)))

(defhydra hydra-font-control (global-map "C-c C-S-f")
  "Control the font."
  ("-" (mw-decrease-face-height))
  ("+" (mw-increase-face-height))
  ("=" (mw-increase-face-height))
  ("b" (set-face-attribute
        'default nil
        :weight 'bold))
  ("n" (set-face-attribute
        'default nil
        :weight 'normal))
  ("f" (set-face-attribute
        'default nil
        :family "firacode") "firacode")
  ("a" (add-fira-code-symbol-keywords)
   "add some cool fira code symbols. e.g. ~~>")
  ("p" (set-face-attribute
        'default nil
        :family "profont") "profont")
  ("s" (set-face-attribute
        'default nil
        :family "sourcecodepro") "sourcecodepro")
  ("c" (set-face-attribute
        'default nil
        :family "courier") "courier")
  ("d" (set-face-attribute
        'default nil
        :family "deja vu sans mono") "deja vu sans mono")
  ("i" (message "family: %s, weight: %s, height: %s"
                (face-attribute 'default :family)
                (face-attribute 'default :weight)
                (face-attribute 'default :height))
   "info")
  ("q" (ignore) "ok, quit" :exit t))
;; faces:2 ends here

;; alternate up
;; :PROPERTIES:
;; :ID:       af8419ed-e32a-43af-a442-b3cc22b7d7aa
;; :END:


;; [[file:init.org::*alternate up][alternate up:1]]
(global-set-key (kbd "C-^") #'go-up)
(global-set-key (kbd "C-c ^") #'go-up)   ; for terminal emacs
;; alternate up:1 ends here

;; key for alternate up
;; :PROPERTIES:
;; :ID:       0481cbe3-19d9-43ce-91a1-70fd845ff864
;; :END:

;; Expand ~dired-mode-map~ with a key for opening parent directory as
;; alternate dired.


;; [[file:init.org::*key for alternate up][key for alternate up:1]]
(add-hook
 'dired-mode-hook
 (lambda ()
   (define-key dired-mode-map "`" #'go-up)))
;; key for alternate up:1 ends here

;; key for narrow
;; :PROPERTIES:
;; :ID:       9ef15de4-8f07-4466-b729-854a693d3ad3
;; :END:

;; Expand ~dired-mode-map~ with a key for filtering the list of files.


;; [[file:init.org::*key for narrow][key for narrow:1]]
(add-hook
 'dired-mode-hook
 (lambda () (define-key dired-mode-map "/" #'dired-narrow-regexp)))
;; key for narrow:1 ends here

;; key to hide dot files
;; :PROPERTIES:
;; :ID:       a2008caf-4bb8-4f50-89c0-9e31a8f00a6f
;; :END:


;; [[file:init.org::*key to hide dot files][key to hide dot files:1]]
(add-hook
 'dired-mode-hook
 (lambda () (define-key dired-mode-map "." #'mw-dired-hide-dot-files)))
;; key to hide dot files:1 ends here

;; compare files in different dired windows
;; :PROPERTIES:
;; :ID:       1a54cc88-0429-4714-9f6e-b186a392b0ea
;; :END:


;; [[file:init.org::*compare files in different dired windows][compare files in different dired windows:1]]
(add-hook
 'dired-mode-hook
 (lambda () (define-key dired-mode-map "@" #'mw-dired-diff-to-file-in-other-dired)))
;; compare files in different dired windows:1 ends here

;; enhance keys for buffer list
;; :PROPERTIES:
;; :ID:       37de8dcf-392d-419d-aa9a-f69f409e2010
;; :END:

;; - i :: switch to ibuffer.
;; - N :: scroll-up 1.
;; - > :: go to the last line.
;;   - With M-> point goes to the last char (and not one before that.)


;; [[file:init.org::*enhance keys for buffer list][enhance keys for buffer list:1]]
(add-hook
 'Buffer-menu-mode-hook
 (lambda ()
   (local-set-key
    ">" (lambda () (interactive)
          (end-of-buffer)
          (backward-char)))
   (local-set-key
    "N" (lambda () (interactive)
          (scroll-up 1)))
   (local-set-key
    "P" (lambda () (interactive)
          (scroll-down 1)))
   (local-set-key
    "i" (lambda () (interactive)
          (ibuffer)))))
;; enhance keys for buffer list:1 ends here

;; enhance keys for ibuffer
;; :PROPERTIES:
;; :ID:       5cabee80-fbe9-4e8a-a60a-c2b01beb8f5c
;; :END:

;; - i :: switch to buffer-list.


;; [[file:init.org::*enhance keys for ibuffer][enhance keys for ibuffer:1]]
(add-hook 'ibuffer-mode-hook
          (lambda ()
            (local-set-key
             "i" (lambda () (interactive)
                   (if (bufferp (get-buffer "*Buffer List*"))
                       (display-buffer "*Buffer List*" '(display-buffer-same-window . ()))
                     (list-buffers))))))
;; enhance keys for ibuffer:1 ends here

;; repeat
;; :PROPERTIES:
;; :ID:       994f20f5-2572-4a1b-acd2-0a5183f8e938
;; :END:


;; [[file:init.org::*repeat][repeat:1]]
(global-set-key (kbd "C-5") #'repeat)
(global-set-key (kbd "M-=") #'repeat)
;; repeat:1 ends here

;; patch repeat

;; patch in the = as repeat key for subsequent repeats.


;; [[file:init.org::*patch repeat][patch repeat:1]]
(defun repeat (repeat-arg)
  "Repeat most recently executed command.
If REPEAT-ARG is non-nil (interactively, with a prefix argument),
supply a prefix argument to that command.  Otherwise, give the
command the same prefix argument it was given before, if any.

If this command is invoked by a multi-character key sequence, it
can then be repeated by repeating the final character of that
sequence.  This behavior can be modified by the global variable
`repeat-on-final-keystroke'.

`repeat' ignores commands bound to input events.  Hence the term
\"most recently executed command\" shall be read as \"most
recently executed command not bound to an input event\"."
  ;; The most recently executed command could be anything, so surprises could
  ;; result if it were re-executed in a context where new dynamically
  ;; localized variables were shadowing global variables in a `let' clause in
  ;; here.  (Remember that GNU Emacs 19 is dynamically localized.)
  ;; To avoid that, I tried the `lexical-let' of the Common Lisp extensions,
  ;; but that entails a very noticeable performance hit, so instead I use the
  ;; "repeat-" prefix, reserved by this package, for *local* variables that
  ;; might be visible to re-executed commands, including this function's arg.
  (interactive "P")
  (when (eq last-repeatable-command 'repeat)
    (setq last-repeatable-command repeat-previous-repeated-command))
  (cond
   ((null last-repeatable-command)
    (error "There is nothing to repeat"))
   ((eq last-repeatable-command 'mode-exit)
    (error "last-repeatable-command is mode-exit & can't be repeated"))
   ((memq last-repeatable-command repeat-too-dangerous)
    (error "Command %S too dangerous to repeat automatically"
	   last-repeatable-command)))
  (setq this-command last-repeatable-command
	repeat-previous-repeated-command last-repeatable-command
        repeat-num-input-keys-at-repeat num-input-keys)
  (when (null repeat-arg)
    (setq repeat-arg last-prefix-arg))
  ;; Now determine whether to loop on repeated taps of the final character
  ;; of the key sequence that invoked repeat.  The Emacs global
  ;; last-command-event contains the final character now, but may not still
  ;; contain it after the previous command is repeated, so the character
  ;; needs to be saved.
  (let ((repeat-repeat-char ?=
         ;; (if (eq repeat-on-final-keystroke t)
	 ;;     last-command-event
         ;;   ;; Allow only specified final keystrokes.
         ;;   (car (memq last-command-event
         ;;              (listify-key-sequence
         ;;               repeat-on-final-keystroke))))
         ))
    (if (eq last-repeatable-command (caar command-history))
        (let ((repeat-command (car command-history)))
          (repeat-message "Repeating %S" repeat-command)
          (eval repeat-command))
      (if (null repeat-arg)
          (repeat-message "Repeating command %S" last-repeatable-command)
        (setq current-prefix-arg repeat-arg)
        (repeat-message
	 "Repeating command %S %S" repeat-arg last-repeatable-command))
      (when (eq last-repeatable-command 'self-insert-command)
        ;; We used to use a much more complex code to try and figure out
        ;; what key was used to run that self-insert-command:
        ;; (if (<= (- num-input-keys
        ;;            repeat-num-input-keys-at-self-insert)
        ;;         1)
        ;;     repeat-last-self-insert
        ;;   (let ((range (nth 1 buffer-undo-list)))
        ;;     (condition-case nil
        ;;         (setq repeat-last-self-insert
        ;;               (buffer-substring (car range)
        ;;                                 (cdr range)))
        ;;       (error (error "%s %s %s"  ;Danger, Will Robinson!
        ;;                     "repeat can't intuit what you"
        ;;                     "inserted before auto-fill"
        ;;                     "clobbered it, sorry")))))
        (setq last-command-event (char-before)))
      (let ((indirect (indirect-function last-repeatable-command)))
        (if (or (stringp indirect)
                (vectorp indirect))
            ;; Bind last-repeatable-command so that executing the macro does
            ;; not alter it.
            (let ((last-repeatable-command last-repeatable-command))
              (execute-kbd-macro last-repeatable-command))
          (call-interactively last-repeatable-command))))
    (when repeat-repeat-char
      (set-transient-map
       (let ((map (make-sparse-keymap)))
         (define-key map (vector repeat-repeat-char)
           (if (null repeat-message-function) 'repeat
             ;; If repeat-message-function is let-bound, preserve it for the
             ;; next "iterations of the loop".
             (let ((fun repeat-message-function))
               (lambda ()
                 (interactive)
                 (let ((repeat-message-function fun))
                   (setq this-command 'repeat)
		   ;; Beware: messing with `real-this-command' is *bad*, but we
		   ;; need it so `last-repeatable-command' can be recognized
		   ;; later (bug#12232).
                   (setq real-this-command 'repeat)
                   (call-interactively 'repeat))))))
         map)))))
;; patch repeat:1 ends here

;; command repeat


;; [[file:init.org::*command repeat][command repeat:1]]
(defun mw-direct-command-repeat (&optional arg)
  "Direct command Repeat.  With prefix arg show the candidate."
  (interactive "P")
  (funcall
   (if arg
       (function mw-message-last-command)
     (function mw-repeat-last-command))))

(global-set-key
 (kbd "C-6")
 #'mw-direct-command-repeat)
;; command repeat:1 ends here

;; insert time
;; :PROPERTIES:
;; :ID:       ea7647bb-1d25-4236-afa9-3d7987560406
;; :END:


;; [[file:init.org::*insert time][insert time:1]]
(define-key global-map (kbd "<f9>")
  (lambda (&optional prefix)
    "Try insert time.  Act special in Org agenda.
Without prefix argument org-inactive-timestamp (without time.)
With prefix argument: org-inactive-timestamp with time.
With two prefix arguments: yyyymmddHHMM.
With three prefix arguments: yyyy-mm-dd-HHMM.
Special in org-agenda: toggle inactive-timestamps-display."
    (interactive "P")
    (cond
     ((eq major-mode 'org-agenda-mode)
      (setq org-agenda-include-inactive-timestamps (eq nil org-agenda-include-inactive-timestamps))
      (org-agenda-redo))
     ((equal '(4) prefix)
      (org-insert-time-stamp nil t t))
     ((equal '(16) prefix)
      (insert (format-time-string "%Y%m%d%H%M")))
     ((equal '(64) prefix)
      (insert (format-time-string "%Y-%m-%d-%H%M")))
     (t (org-insert-time-stamp nil nil t)))))
;; insert time:1 ends here

;; volume control
;; :PROPERTIES:
;; :ID:       ab5819d9-95e9-4881-a146-7b2af5c39322
;; :END:


;; [[file:init.org::*volume control][volume control:1]]
(global-set-key (kbd "<XF86AudioLowerVolume>") #'emms-volume-lower)
(global-set-key (kbd "<XF86AudioRaiseVolume>") #'emms-volume-raise)

(defhydra hydra-volume-control (global-map "C-c 1")
  "Volume control."
  ("0" (mw-sound-0%) "silence via amixer") ; this different from emmms volume control AFAICS.
  ("1" (dotimes (_ 50) (emms-volume-lower)) "silence")
  ("2" (emms-volume-lower) "lower")
  ("3" (progn (emms-volume-amixer-change -100)
              (emms-volume-amixer-change 50)
              ) "half")
  ("4" (emms-volume-raise) "raise")
  ("5" (dotimes (_ 50) (emms-volume-raise)) "full")
  ("9" (mw-sound-100%) "full via amixer")
  ("q" (ignore) "quit" :exit t))

(global-set-key (kbd "<XF86AudioMute>") #'mw-sound-0%)
(global-set-key (kbd "<XF86AudioNext>") #'mw-sound-100%)
(global-set-key (kbd "<XF86AudioPlay>") #'mw-sound-set-enjoyable-volume)
;; volume control:1 ends here

;; rest
;; :PROPERTIES:
;; :ID:       84a49995-3a33-4f39-b788-e55cc10ab4dc
;; :END:


;; [[file:init.org::*rest][rest:1]]
(global-set-key (kbd "<f1>") #'ignore) ;; e.g. for leaving the zone.
(global-set-key (kbd "<f6>") #'flyspell-mode)
(global-set-key (kbd "M-<f6>") #'mw-cycle-ispell-language-and-input-method)
(global-set-key (kbd "M-<f7>") #'mw-cycle-ispell-completion-dict)
(global-set-key (kbd "C-$") #'ispell-complete-word)
(global-set-key (kbd "S-<f11>") #'mw-rotate-split)
(global-set-key (kbd "C-<f11>") (lambda () "'Rotate' windows two times." (interactive) (mw-rotate-split) (mw-rotate-split)))
(global-set-key (kbd "C-x g") #'magit-status)

;; the following collides with parmode:
;; (global-set-key (kbd "C-M-<right>") #'next-buffer)
;; (global-set-key (kbd "C-M-<left>") #'previous-buffer)
;; [2015-10-12 Mon 15:03] trying key chords.

;(global-set-key (kbd "C-x o") 'ace-window)

;; cycle through amounts of spacing
(global-set-key (kbd "M-SPC") #'cycle-spacing)
(global-set-key (kbd "M-S-SPC") #'just-one-space)
;; rest:1 ends here

;; keys for fit image to width or height
;; :PROPERTIES:
;; :ID:       505c0cbb-4d83-49ce-a619-6a9a00cbe8e8
;; :END:

;; - W on an image to fit its width to the window width.
;; - H for the height respectively.


;; [[file:init.org::*keys for fit image to width or height][keys for fit image to width or height:1]]
(with-eval-after-load
    'image-mode
  (progn
     (define-key image-mode-map "W" #'image-transform-fit-to-width)
     (define-key image-mode-map "H" #'image-transform-fit-to-height)))
;; keys for fit image to width or height:1 ends here

;; open line above
;; :PROPERTIES:
;; :ID:       ec2f4e6d-2859-43ed-9587-b4f395176f8a
;; :END:


;; [[file:init.org::*open line above][open line above:1]]
(global-set-key (kbd "C-S-o") (lambda () (interactive) (open-line 1) (forward-line 1) (transpose-lines 1) (forward-line -2)))
;; open line above:1 ends here

;; various key chords
;; :PROPERTIES:
;; :ID:       b0881716-2a55-49a4-8279-1bff19c7f0d4
;; :END:


;; [[file:init.org::*various key chords][various key chords:1]]
; recall (key-chord-unset-global "__") to undef a key-chord.
(key-chord-define-global ";;" #'symex-mode-interface)
(key-chord-define-global "b4" #'mw-backward-forward-whitespace)
(key-chord-define-global "y5" #'repeat)
(key-chord-define-global ",," #'mw-comma-to-the-left)
(key-chord-define-global ".." #'mw-dot-to-the-left)
(key-chord-define-global "iy"
 (lambda () (interactive)
   (message "this is my personal key!  no operation!  this might change with context.")))
(key-chord-define-global "r5" #'raise-frame)
(key-chord-define-global "g1" #'grep)
(key-chord-define-global "g2" #'grep-find)
(key-chord-define-global "__" #'underline-with-char)
(key-chord-define-global "q0" #'ignore) ; e.g.: abort a hydra gracefully; leave the zone.
(key-chord-define-global "=z" #'overwrite-mode)
(key-chord-define-global "\/\/" #'isearch-forward)
(key-chord-define-global "\?\?" #'isearch-backward)
(key-chord-define-global "xx" #'counsel-M-x) ; execute-extended-command
(key-chord-define-global "w5" #'winner-undo)
(key-chord-define-global "u7" #'global-undo-tree-mode)
(key-chord-define-global "x5" #'mw-command-execute-at-point)
(key-chord-define-global "g7" (lambda (arg) (interactive "P")
                                (if arg (mw-split-window-horizontally-at-point)
                                  (split-window-horizontally))))
(key-chord-define-global "l\\" #'mw-split-window-vertically-at-point)
(key-chord-define-global "x6" #'eval-last-sexp)
(key-chord-define-global "x7" #'mw-eval-outermost-sexp)
(key-chord-define-global "x9" #'kmacro-end-and-call-macro)
(key-chord-define-global "r7" #'mw-org-refile-set-direct-target-bm)
(key-chord-define-global "r8" #'mw-org-refile-refile-to-direct-target)
(key-chord-define-global "r1" #'mw-carry-region-toggle)
(key-chord-define-global "r2" #'count-words-region)
(key-chord-define-global "l1" #'display-line-numbers-mode)
(key-chord-define-global "l5" #'mw-mark-line)
(key-chord-define-global "o6" #'org-open-at-point-global)
(key-chord-define-global "c4" #'recompile)
(key-chord-define-global "c5" #'compile)
(key-chord-define-global "o8" #'ace-link)
(key-chord-define-global "ao" #'other-window)
(key-chord-define-global ";q" #'other-frame)
(key-chord-define-global "qj" #'mw-exchange-to-buddy)
(key-chord-define-global ".," #'next-buffer)
(key-chord-define-global "><"       ; this is "S-.," on dovorak layout
			 #'previous-buffer)
(key-chord-define-global "r9" #'rope-read-mode)
(key-chord-define-global "yy" (lambda (arg)
                                (interactive "P")
                                (cond
                                 ((equal '(4) arg) (mw-duplicate-line-as-term))
                                 ((region-active-p)
                                  (insert-buffer-substring (current-buffer) (region-beginning) (region-end)))
                                 (t (mw-duplicate-line)))))
(key-chord-define-global "YY" #'mw-duplicate-symex)
(key-chord-define-global "''" #'mw-umlautify-before-point)
(key-chord-define-global "uu" (lambda (&optional in-place)
				(interactive "P")
				(if in-place (mw-translate-in-place-eng+deu)
				  (mw-translate-as-message-eng+deu))))
(key-chord-define-global "HH" (lambda () (interactive) (recenter 0))) ; #'recenter-top-bottom; afair H is a respecive vim binding?
(key-chord-define-global "^^" #'dired-jump)
(key-chord-define-global "3." #'delete-window)
(key-chord-define-global "c8" #'delete-other-windows) ; for dvorak on kinesis-advantage
(key-chord-define-global "kx" (lambda () (interactive)
				(if (eq major-mode 'org-mode)
				    (org-edit-special)
				  (org-edit-src-exit))))
(key-chord-define-global "1'" #'org-previous-visible-heading)
(key-chord-define-global "jk" #'tab-next)
(key-chord-define-global "JK" #'tab-previous)
(key-chord-define-global "n1" #'narrow-to-region)
(key-chord-define-global "n2" #'mw-narrow-to-up-sexp)
(key-chord-define-global "n4" #'mw-narrow-to-symex)
;;(key-chord-define-global "n5" #'sp-narrow-to-sexp)
(key-chord-define-global "a'" (lambda (&optional arg) (interactive "P")
                                (mw-kbd-macro-call-agenda-big)
                                (when arg (mw-org-agenda-write-for-log))))
(key-chord-define-global "a," #'mw-kbd-macro-call-agenda-w)
(key-chord-define-global "a." #'org-agenda-list)
(key-chord-define-global "a6" #'ariadne-marks-goto-end)
(key-chord-define-global "a7" #'ariadne-marks-backward)
(key-chord-define-global "a8" #'ariadne-marks-set-mark)
(key-chord-define-global "a9" (lambda (&optional arg)
				(interactive "P")
				(ariadne-marks-unset)
				(when arg (ariadne-marks-unset-all))))
(key-chord-define-global "c1" #'calendar)
(key-chord-define-global "c2" #'chronos-add-timer)
(key-chord-define-global "d1" #'mw-org-link-strip-decoration)
(key-chord-define-global "ii" (lambda (arg)
				(interactive "P")
				(cond
                                 ((equal '(16) arg) (upcase-word -1))
                                 ((equal '(4) arg) (downcase-word -1))
                                 (t (capitalize-word -1)))))
(key-chord-define-global "II" (lambda (arg)
				(interactive "P")
                                (cond
                                 ((equal '(4) arg) (downcase-word -1))
                                 (t (upcase-word -1)))))
(key-chord-define-global "h1" #'hide-parts-mode)
(key-chord-define-global "h2" #'hide-parts-toggle-show-hint)
(key-chord-define-global "h4" #'hide-parts-dwim)
(key-chord-define-global "f1" #'foldout-exit-fold)
(key-chord-define-global "f4" #'foldout-zoom-org-subtree)
(key-chord-define-global "v1" #'evil-mode)
(key-chord-define-global "v2" #'view-mode)
(key-chord-define-global "v7" #'visual-line-mode)
(key-chord-define-global "e6" #'eshell)
(key-chord-define-global "e7" #'recursive-edit)
(key-chord-define-global "e8" #'exit-recursive-edit)
(key-chord-define-global "q6" #'auto-fill-mode)
(key-chord-define-global "e9" #'mw-text-eta)
(key-chord-define-global "w1" #'widen)
(key-chord-define-global "w3" #'shrink-window)
(key-chord-define-global "c6" #'mw-create-sha1-hash-named-copy-of-buffer)
(key-chord-define-global "e5" #'mw-ephemeral-enumerate-visual-lines-down-from-point)
;; various key chords:1 ends here

;; keys for rectangle-mark-mode
;; :PROPERTIES:
;; :ID:       d3186145-fa26-4096-87ce-012f88b7e110
;; :END:


;; [[file:init.org::*keys for rectangle-mark-mode][keys for rectangle-mark-mode:1]]
(define-key rectangle-mark-mode-map "e" #'mw-rectangle-mark-mode-forward-to-max-col)
;; keys for rectangle-mark-mode:1 ends here

;; shortcut to kill beginning of line
;; :PROPERTIES:
;; :ID:       a5d7aebf-b165-41a9-86e0-26e09437c146
;; :END:


;; [[file:init.org::*shortcut to kill beginning of line][shortcut to kill beginning of line:1]]
(global-set-key (kbd "C-M-<backspace>") (lambda () (interactive) (kill-line 0)))
;; shortcut to kill beginning of line:1 ends here

;; private
;; :PROPERTIES:
;; :ID:       409c40ea-ae7a-480e-8512-dc4d8d7dcdf1
;; :END:

;; keep some data which I think only I should know in an extra location.


;; [[file:init.org::*private][private:1]]
(let ((private-code-file "~/m/priv/mw-private.el"))
  (unless (and
           (file-exists-p private-code-file)
           (load private-code-file))
    (warn "load of file %s with private data failed.  recommendation: check the file" private-code-file)))
;; private:1 ends here

;; code
;; :PROPERTIES:
;; :ID:       e3e76944-8db8-40a9-b73c-ac195b2100ba
;; :END:


;; [[file:init.org::*code][code:1]]
(setq custom-file (expand-file-name "init/.emacs-custom.el" user-emacs-directory))
(load custom-file)
;; code:1 ends here

;; provide a string with the sunset
;; :PROPERTIES:
;; :ID:       f4b4c313-3ef5-420c-918e-22afaa4ea146
;; :END:

;; :origin:
;; Function: solar-sunrise-sunset-string
;; :END:


;; [[file:init.org::*provide a string with the sunset][provide a string with the sunset:1]]
(defun solar-sunrise-string (date &optional nolocation)
  "String of *local* times of sunrise and daylight on Gregorian DATE.
Optional NOLOCATION non-nil means do not print the location."
  (let ((l (solar-sunrise-sunset date)))
    (format
     "%s%s (%s hrs daylight)"
     (if (car l)
         (concat "Sunrise " (apply 'solar-time-string (car l)))
       "No sunrise")
     (if nolocation ""
       (format " at %s" (eval calendar-location-name)))
     (nth 2 l))))

(defun solar-sunset-string (date &optional nolocation)
  "String of *local* times of sunset, and daylight on Gregorian DATE.
Optional NOLOCATION non-nil means do not print the location."
  (let ((l (solar-sunrise-sunset date)))
    (format
     "%s%s (%s hrs daylight)"
     (if (cadr l)
         (concat "Sunset " (apply 'solar-time-string (cadr l)))
       "no sunset")
     (if nolocation ""
       (format " at %s" (eval calendar-location-name)))
     (nth 2 l))))
;; provide a string with the sunset:1 ends here

;; create a calendar function
;; :PROPERTIES:
;; :ID:       e8eccb17-a229-4111-8f55-df98b2dc940d
;; :END:

;; this section originated from function diary-sunrise-sunset.


;; [[file:init.org::*create a calendar function][create a calendar function:1]]
(require 'solar)

(defun diary-sunrise ()
  "Local time of sunset as a diary entry.
Accurate to a few seconds.

Depends on variable `date'."
  (or (and calendar-latitude calendar-longitude calendar-time-zone)
      (solar-setup))
  (solar-sunrise-string date))

(defun diary-sunset ()
  "Local time of sunset as a diary entry.
Accurate to a few seconds.

Depends on variable `date'."
  (or (and calendar-latitude calendar-longitude calendar-time-zone)
      (solar-setup))
  (solar-sunset-string date))
;; create a calendar function:1 ends here

;; activate memento mori mode
;; :PROPERTIES:
;; :ID:       4f01bb4c-7c28-4d98-a29c-7581b75a276c
;; :END:


;; [[file:init.org::*activate memento mori mode][activate memento mori mode:1]]
;; (memento-mori-mode)
;; activate memento mori mode:1 ends here

;; help recall abbrevs
;; :PROPERTIES:
;; :ID:       d7f8322a-758c-4408-8344-96e67e4a357b
;; :END:

;; something
;; - this is a proposition to be added to Emacs: [[gnus:nntp+news.gwene.org:gmane.emacs.devel#CABrcCQ6GYzqzxBoeCfpXFEVoNDK6zy8FW4ZXe0Xdgp_6dHocVA@mail.gmail.com][Email from Mathias Dahl: [PATCH] Add abbrev suggestions]]
;; - found accidentially.
;; - remove this section when the suggestion entered emacs.


;; [[file:init.org::*help recall abbrevs][help recall abbrevs:1]]
(defcustom abbrev-suggest t
    "Non-nil means we should suggest abbrevs to the user.
By enabling this option, if abbrev mode is enabled and if the
user has typed some text that exists as an abbrev, suggest to the
user to use the abbrev instead."
    :type 'boolean
    :group 'abbrev-mode)

(defcustom abbrev-suggest-hint-threshold 3
    "Threshold for when to inform the user that there is an abbrev.
The threshold is the number of characters that differs between
the length of the abbrev and the length of the expansion.  The
thinking is that if the expansion is only one or a few characters
longer than the abbrev, the benefit of informing the user is not
that big.  If you always want to be informed, set this value to
`0' or less."
    :type 'number
    :group 'abbrev-mode)

(defun abbrev--suggest-get-active-tables-including-parents ()
  "Return a list of all active abbrev tables, including parent tables."
  (let* ((tables (abbrev--active-tables))
	 (all tables))
    (dolist (table tables)
      (setq all (append (abbrev-table-get table :parents) all)))
    all))

(defun abbrev--suggest-get-active-abbrev-expansions ()
    "Return a list of all the active abbrev expansions.
Includes expansions from parent abbrev tables."
    (let (expansions)
      (dolist (table (abbrev--suggest-get-active-tables-including-parents))
	(mapatoms (lambda (e)
		    (let ((value (symbol-value (abbrev--symbol e table))))
		      (when value
                        (push (cons value (symbol-name e)) expansions))))
		  table))
      expansions))

(defun abbrev--suggest-count-words (expansion)
    "Return the number of words in EXPANSION.
Expansion is a string of one or more words."
    (length (split-string expansion " " t)))

(defun abbrev--suggest-get-previous-words (n)
    "Return the previous N words, spaces included.
Changes newlines into spaces."
    (let ((end (point)))
      (save-excursion
	(backward-word n)
	(replace-regexp-in-string
	 "\\s " " "
	 (buffer-substring-no-properties (point) end)))))

(defun abbrev--suggest-above-threshold (expansion)
    "Return t if we are above the threshold.
EXPANSION is a cons cell where the car is the expansion and the
cdr is the abbrev."
    (>= (- (length (car expansion))
	   (length (cdr expansion)))
	abbrev-suggest-hint-threshold))

(defvar abbrev--suggest-saved-recommendations nil
    "Keeps a list of expansions that have abbrevs defined.
The user can show this list by calling
`abbrev-suggest-show-report'.")

(defun abbrev--suggest-inform-user (expansion)
    "Display a message to the user about the existing abbrev.
EXPANSION is a cons cell where the `car' is the expansion and the
`cdr' is the abbrev."
    (run-with-idle-timer
     1 nil
     (lambda ()
       (message "You can write `%s' using the abbrev `%s'."
                                   (car expansion) (cdr expansion))))
    (push expansion abbrev--suggest-saved-recommendations))

(defun abbrev--suggest-shortest-abbrev (new current)
    "Return the shortest abbrev.
NEW and CURRENT are cons cells where the `car' is the expansion
and the `cdr' is the abbrev."
    (if (not current)
	new
      (if (< (length (cdr new))
	     (length (cdr current)))
	  new
	current)))

(defun abbrev--suggest-maybe-suggest ()
    "Suggest an abbrev to the user based on the word(s) before point.
Uses `abbrev-suggest-hint-threshold' to find out if the user should be
informed about the existing abbrev."
    (let (words abbrev-found word-count)
      (dolist (expansion (abbrev--suggest-get-active-abbrev-expansions))
	(setq word-count (abbrev--suggest-count-words (car expansion))
	      words (abbrev--suggest-get-previous-words word-count))
	(let ((case-fold-search t))
	  (when (and (> word-count 0)
		     (string-match (car expansion) words)
		     (abbrev--suggest-above-threshold expansion))
	    (setq abbrev-found (abbrev--suggest-shortest-abbrev
				expansion abbrev-found)))))
      (when abbrev-found
	(abbrev--suggest-inform-user abbrev-found))))

(defun abbrev--suggest-get-totals ()
    "Return a list of all expansions and their usage.
Each expansion is a cons cell where the `car' is the expansion
and the `cdr' is the number of times the expansion has been
typed."
    (let (total cell)
      (dolist (expansion abbrev--suggest-saved-recommendations)
	(if (not (assoc (car expansion) total))
	    (push (cons (car expansion) 1) total)
	  (setq cell (assoc (car expansion) total))
	  (setcdr cell (1+ (cdr cell)))))
      total))

(defun abbrev-suggest-show-report ()
  "Show the user a report of abbrevs he could have used."
  (interactive)
  (let ((totals (abbrev--suggest-get-totals))
	(buf (get-buffer-create "*abbrev-suggest*")))
    (set-buffer buf)
    (erase-buffer)
        (insert "** Abbrev expansion usage **

Below is a list of expansions for which abbrevs are defined, and
the number of times the expansion was typed manually. To display
and edit all abbrevs, type `M-x edit-abbrevs RET'\n\n")
	(dolist (expansion totals)
	  (insert (format " %s: %d\n" (car expansion) (cdr expansion))))
	(display-buffer buf)))

(defun expand-abbrev ()
  "Expand the abbrev before point, if there is an abbrev there.
Effective when explicitly called even when `abbrev-mode' is nil.
Before doing anything else, runs `pre-abbrev-expand-hook'.
Calls the value of `abbrev-expand-function' with no argument to do
the work, and returns whatever it does.  (That return value should
be the abbrev symbol if expansion occurred, else nil.)"
  (interactive)
  (run-hooks 'pre-abbrev-expand-hook)
  (or (funcall abbrev-expand-function)
      (if abbrev-suggest
          (abbrev--suggest-maybe-suggest))))
;; help recall abbrevs:1 ends here

;; undoable company completion
;; :PROPERTIES:
;; :ID:       bebaa1e3-7e96-407b-926d-84a05426ef4e
;; :END:


;; [[file:init.org::*undoable company completion][undoable company completion:1]]
(defun company-finish (result)
  (let ((pos (point)))
    (company--insert-candidate result)
    (company-cancel result)
    (let ((pos-after-insert (point)))
      (push nil buffer-undo-list)
      (push (cons pos pos-after-insert) buffer-undo-list))))
;; undoable company completion:1 ends here

;; show more info with outline path
;; :PROPERTIES:
;; :ID:       d423838f-6c6e-4e83-afc7-925d5c0c1116
;; :END:

;; only the case /org-agenda-show-outline-path/ is changed.


;; [[file:init.org::*show more info with outline path][show more info with outline path:1]]
(defun org-agenda-do-context-action ()
  "Show outline path and, maybe, follow mode window."
  (let ((m (org-get-at-bol 'org-marker)))
    (when (and (markerp m) (marker-buffer m))
      (and org-agenda-follow-mode
	   (if org-agenda-follow-indirect
	       (org-agenda-tree-to-indirect-buffer nil)
	     (org-agenda-show)))
      (and org-agenda-show-outline-path
           (org-with-point-at m (eq 'org-mode major-mode)) ; e.g. don't act on diary file.
	   (message
	    (concat
	     (org-with-point-at m
	       (apply #'concat
                      (delq nil
                            `(,(org-display-outline-path t nil nil t)
                             "/ "
                             ,(nth 2 (org-heading-components)) " "
                             ,(nth 4 (org-heading-components)) " "
                             ,(nth 5 (org-heading-components))))))))))))
;; show more info with outline path:1 ends here

;; abbrev-prefix-mark reset at second call
;; :PROPERTIES:
;; :ID:       a584a6cd-7e50-4a88-9e58-fe8e8d0eb1cf
;; :END:

;; This is a corner case.  It is a personal convenience thing.  It
;; realizes expansion of an abbrev without an extra space appended.

;; Remove the hyphen when called a second time.  With this patch { M-' M-' }
;; is an alternative to { C-x ' } or { SPC BACKKSPACE }.


;; [[file:init.org::*abbrev-prefix-mark reset at second call][abbrev-prefix-mark reset at second call:1]]
(defun abbrev-prefix-mark (&optional arg)
  "Mark current point as the beginning of an abbrev.
The abbrev to be expanded starts here rather than at beginning of
word.  This way, you can expand an abbrev with a prefix: insert
the prefix, use this command, then insert the abbrev.

This command inserts a hyphen after the prefix, and if the abbrev
is subsequently expanded, this hyphen will be removed.

If the prefix is itself an abbrev, this command expands it,
unless ARG is non-nil.  Interactively, ARG is the prefix
argument.

Remove the hyphen when called a second time."
  (interactive "P")
  (if (and (eq last-command #'abbrev-prefix-mark)
           (= ?\- (char-before)))
      (delete-char -1)
    (or arg (expand-abbrev))
    (setq abbrev-start-location (point-marker)
	abbrev-start-location-buffer (current-buffer))
    (insert "-")))
;; abbrev-prefix-mark reset at second call:1 ends here

;; calendar goto date alternative
;; :PROPERTIES:
;; :ID:       6ccbe664-db66-499f-9f60-2e738a4078f6
;; :END:

;; type month numerically.

;; - precondition :: make sure to be in the calendar buffer.

;; - idea :: what about hex input day and month?


;; [[file:init.org::*calendar goto date alternative][calendar goto date alternative:1]]
(defun mw-calendar-goto-date (date &optional noecho)
  "Move cursor to Julian DATE; echo Julian date unless NOECHO is non-nil."
  (interactive
   (let* ((today (calendar-current-date))
          (year (calendar-read
                 "Julian calendar year (>0): "
                 (lambda (x) (> x 0))
                 (number-to-string
                  (calendar-extract-year
                   (calendar-julian-from-absolute
                    (calendar-absolute-from-gregorian
                     today))))))
          (month (calendar-read
                 "Julian calendar month: "
                 (lambda (x) (and (<= 1 x) (<= x 12)))
                 (number-to-string
                  (calendar-extract-month
                   (calendar-julian-from-absolute
                    (calendar-absolute-from-gregorian
                     today))))))
          (last
           (if (and (zerop (% year 4)) (= month 2))
               29
             (aref [31 28 31 30 31 30 31 31 30 31 30 31] (1- month))))
          (day (calendar-read
                (format "Julian calendar day (%d-%d): "
                        (if (and (= year 1) (= month 1)) 3 1) last)
                (lambda (x)
                  (and (< (if (and (= year 1) (= month 1)) 2 0) x)
                       (<= x last))))))
     (list (list month day year))))
  (calendar-goto-date date)
  (or noecho (calendar-julian-print-date)))
;; calendar goto date alternative:1 ends here

;; switch to included diaries
;; :PROPERTIES:
;; :ID:       b9a5af1a-872c-4bf7-8d4e-34b57a93a09e
;; :END:

;; The switch from org-agenda to an entry of an included diary file fails
;; if the respective diary buffer is not available.  BTW fancy diary does
;; not have this problem since it uses a button for the switch.  The
;; agenda just keeps a marker.

;; Fix by loading all included diary files.


;; [[file:init.org::*switch to included diaries][switch to included diaries:1]]
(defun mw-find-included-diary-files ()
  (mapc (lambda (x) (find-file-noselect x t))
        diary-included-files))
;; switch to included diaries:1 ends here

;; [[file:init.org::*switch to included diaries][switch to included diaries:2]]
(add-hook 'org-agenda-cleanup-fancy-diary-hook #'mw-find-included-diary-files)
;; switch to included diaries:2 ends here

;; use older org-auto-repeat-maybe
;; :PROPERTIES:
;; :ID:       0cff081e-438e-4c63-9b30-da0912ef02b0
;; :END:

;; use the older due to low performance of the new version for several
;; tasks.  I guess long logbooks are an issue.


;; [[file:init.org::*use older org-auto-repeat-maybe][use older org-auto-repeat-maybe:1]]
(defun org-auto-repeat-maybe (done-word)
  "Check if the current headline contains a repeated time-stamp.

If yes, set TODO state back to what it was and change the base date
of repeating deadline/scheduled time stamps to new date.

This function is run automatically after each state change to a DONE state."
  (let* ((repeat (org-get-repeat))
	 (aa (assoc org-last-state org-todo-kwd-alist))
	 (interpret (nth 1 aa))
	 (head (nth 2 aa))
	 (whata '(("h" . hour) ("d" . day) ("m" . month) ("y" . year)))
	 (msg "Entry repeats: ")
	 (org-log-done nil)
	 (org-todo-log-states nil)
	 (end (copy-marker (org-entry-end-position))))
    (unwind-protect
	(when (and repeat (not (zerop (string-to-number (substring repeat 1)))))
	  (when (eq org-log-repeat t) (setq org-log-repeat 'state))
	  (let ((to-state (or (org-entry-get nil "REPEAT_TO_STATE" 'selective)
			      (and (stringp org-todo-repeat-to-state)
				   org-todo-repeat-to-state)
			      (and org-todo-repeat-to-state org-last-state))))
	    (org-todo (cond
		       ((and to-state (member to-state org-todo-keywords-1))
			to-state)
		       ((eq interpret 'type) org-last-state)
		       (head)
		       (t 'none))))
	  (org-back-to-heading t)
	  (org-add-planning-info nil nil 'closed)
	  ;; When `org-log-repeat' is non-nil or entry contains
	  ;; a clock, set LAST_REPEAT property.
	  (when (or org-log-repeat
		    (catch :clock
		      (save-excursion
			(while (re-search-forward org-clock-line-re end t)
			  (when (org-at-clock-log-p) (throw :clock t))))))
	    (org-entry-put nil "LAST_REPEAT" (format-time-string
					      (org-time-stamp-format t t)
					      (current-time))))
	  (when org-log-repeat
	    (if (or (memq 'org-add-log-note (default-value 'post-command-hook))
		    (memq 'org-add-log-note post-command-hook))
		;; We are already setup for some record.
		(when (eq org-log-repeat 'note)
		  ;; Make sure we take a note, not only a time stamp.
		  (setq org-log-note-how 'note))
	      ;; Set up for taking a record.
	      (org-add-log-setup 'state
				 (or done-word (car org-done-keywords))
				 org-last-state
				 org-log-repeat)))
	  (let ((planning-re (regexp-opt
			      (list org-scheduled-string org-deadline-string))))
	    (while (re-search-forward org-ts-regexp end t)
	      (let* ((ts (match-string 0))
		     (planning? (org-at-planning-p))
		     (type (if (not planning?) "Plain:"
			     (save-excursion
			       (re-search-backward
				planning-re (line-beginning-position) t)
			       (match-string 0)))))
		(cond
		 ;; Ignore fake time-stamps (e.g., within comments).
		 ((not (org-at-timestamp-p 'agenda)))
		 ;; Time-stamps without a repeater are usually
		 ;; skipped.  However, a SCHEDULED time-stamp without
		 ;; one is removed, as they are no longer relevant.
		 ((not (string-match "\\([.+]\\)?\\(\\+[0-9]+\\)\\([hdwmy]\\)"
				     ts))
		  (when (equal type org-scheduled-string)
		    (org-remove-timestamp-with-keyword type)))
		 (t
		  (let ((n (string-to-number (match-string 2 ts)))
			(what (match-string 3 ts)))
		    (when (equal what "w") (setq n (* n 7) what "d"))
		    (when (and (equal what "h")
			       (not (string-match-p "[0-9]\\{1,2\\}:[0-9]\\{2\\}"
						    ts)))
		      (user-error
		       "Cannot repeat in Repeat in %d hour(s) because no hour \
has been set"
		       n))
		    ;; Preparation, see if we need to modify the start
		    ;; date for the change.
		    (when (match-end 1)
		      (let ((time (save-match-data
				    (org-time-string-to-time ts))))
			(cond
			 ((equal (match-string 1 ts) ".")
			  ;; Shift starting date to today
			  (org-timestamp-change
			   (- (org-today) (time-to-days time))
			   'day))
			 ((equal (match-string 1 ts) "+")
			  (let ((nshiftmax 10)
				(nshift 0))
			    (while (or (= nshift 0)
				       (not (time-less-p (current-time) time)))
			      (when (= (cl-incf nshift) nshiftmax)
				(or (y-or-n-p
				     (format "%d repeater intervals were not \
enough to shift date past today.  Continue? "
					     nshift))
				    (user-error "Abort")))
			      (org-timestamp-change n (cdr (assoc what whata)))
			      (org-in-regexp org-ts-regexp3)
			      (setq ts (match-string 1))
			      (setq time
				    (save-match-data
				      (org-time-string-to-time ts)))))
			  (org-timestamp-change (- n) (cdr (assoc what whata)))
			  ;; Rematch, so that we have everything in place
			  ;; for the real shift.
			  (org-in-regexp org-ts-regexp3)
			  (setq ts (match-string 1))
			  (string-match "\\([.+]\\)?\\(\\+[0-9]+\\)\\([hdwmy]\\)"
					ts)))))
		    (save-excursion
		      (org-timestamp-change n (cdr (assoc what whata)) nil t))
		    (setq msg
			  (concat
			   msg type " " org-last-changed-timestamp " "))))))))
	  (setq org-log-post-message msg)
	  (message "%s" msg))
      (set-marker end nil))))
;; use older org-auto-repeat-maybe:1 ends here

;; zone for all windows
;; :PROPERTIES:
;; :ID:       e4864ffb-0e97-4b9f-ba64-6b5da5f63527
;; :END:

;; Slight change to =zone= to let all windows display buffer *zone*.


;; [[file:init.org::*zone for all windows][zone for all windows:1]]
(with-eval-after-load "zone"
  (defun zone ()
    "Zone out, completely.
All windows show *zone*."
    (interactive)
    (save-window-excursion
      (let ((f (selected-frame))
            (outbuf (get-buffer-create "*zone*"))
            (text (buffer-substring (window-start) (window-end)))
            (wp (1+ (- (window-point)
                       (window-start)))))
        (let ((win-0 (selected-window)))
          (while (not (equal win-0 (progn
                                     (other-window 1 t)
                                     (selected-window))))
            (switch-to-buffer "*zone*" )))
        (put 'zone 'orig-buffer (current-buffer))
        (switch-to-buffer outbuf)
        (setq mode-name "Zone")
        (erase-buffer)
        (setq buffer-undo-list t
              truncate-lines t
              tab-width (zone-orig tab-width)
              line-spacing (zone-orig line-spacing))
        (insert text)
        (untabify (point-min) (point-max))
        (set-window-start (selected-window) (point-min))
        (set-window-point (selected-window) wp)
        (sit-for 0 500)
        (let ((pgm (elt zone-programs (random (length zone-programs))))
              (ct (and f (frame-parameter f 'cursor-type)))
              (show-trailing-whitespace nil)
              (restore (list '(kill-buffer outbuf))))
          (when ct
            (modify-frame-parameters f '((cursor-type . (bar . 0))))
            (setq restore (cons '(modify-frame-parameters
                                  f (list (cons 'cursor-type ct)))
                                restore)))
          ;; Make `restore' a self-disabling one-shot thunk.
          (setq restore `(lambda () ,@restore (setq restore nil)))
          (condition-case nil
              (progn
                (message "Zoning... (%s)" pgm)
                (garbage-collect)
                ;; If some input is pending, zone says "sorry", which
                ;; isn't nice; this might happen e.g. when they invoke the
                ;; game by clicking the menu bar.  So discard any pending
                ;; input before zoning out.
                (if (input-pending-p)
                    (discard-input))
                (zone-call pgm)
                (message "Zoning...sorry"))
            (error
             (funcall restore)
             (while (not (input-pending-p))
               (message "We were zoning when we wrote %s..." pgm)
               (sit-for 3)
               (message "...here's hoping we didn't hose your buffer!")
               (sit-for 3)))
            (quit
             (funcall restore)
             (ding)
             (message "Zoning...sorry")))
          (when restore (funcall restore)))))))
;; zone for all windows:1 ends here

;; allow triplets in read-color
;; :PROPERTIES:
;; :ID:       de0fe0a6-2466-4753-9f60-8f7fbe892f12
;; :END:

;; - this has been reported.  see
;;   https://debbugs.gnu.org/cgi/bugreport.cgi?bug=38412.
;; - but the effect does not show up with emacs -Q.
;; - so the report has been closed.
;; - it's my personal problem.


;; [[file:init.org::*allow triplets in read-color][allow triplets in read-color:1]]
(defun read-color (&optional prompt convert-to-RGB allow-empty-name msg)
  "Read a color name or RGB triplet.
Completion is available for color names, but not for RGB triplets.

RGB triplets have the form \"#RRGGBB\".  Each of the R, G, and B
components can have one to four digits, but all three components
must have the same number of digits.  Each digit is a hex value
between 0 and F; either upper case or lower case for A through F
are acceptable.

In addition to standard color names and RGB hex values, the
following are available as color candidates.  In each case, the
corresponding color is used.

 * `foreground at point'   - foreground under the cursor
 * `background at point'   - background under the cursor

Optional arg PROMPT is the prompt; if nil, use a default prompt.

Interactively, or with optional arg CONVERT-TO-RGB-P non-nil,
convert an input color name to an RGB hex string.  Return the RGB
hex string.

If optional arg ALLOW-EMPTY-NAME is non-nil, the user is allowed
to enter an empty color name (the empty string).

Interactively, or with optional arg MSG non-nil, print the
resulting color name in the echo area."
  (interactive "i\np\ni\np")    ; Always convert to RGB interactively.
  (let* ((completion-ignore-case t)
	 (colors (or facemenu-color-alist
		     (append '("foreground at point" "background at point")
			     (if allow-empty-name '(""))
                             (if (display-color-p)
                                 (defined-colors-with-face-attributes)
                               (defined-colors)))))
	 (color (completing-read
		 (or prompt "Color (name or #RGB triplet): ")
		 ;; Completing function for reading colors, accepting
		 ;; both color names and RGB triplets.
		 (lambda (string pred flag)
		   (cond
		    ((null flag)        ; Try completion.
		     (or (try-completion string colors pred)
			 (if (color-defined-p string)
			     string)))
		    ((eq flag t)        ; List all completions.
		     (or (all-completions string colors pred)
			 (if (color-defined-p string)
			     (list string))))
		    ((eq flag 'lambda)  ; Test completion.
		     (or (member string colors)
			 (color-defined-p string))))))))

    ;; Process named colors.
    (when (member color colors)
      (cond ((string-equal color "foreground at point")
	     (setq color (foreground-color-at-point)))
	    ((string-equal color "background at point")
	     (setq color (background-color-at-point))))
      (when (and convert-to-RGB
		 (not (string-equal color "")))
	(let ((components (x-color-values color)))
	  (unless (string-match-p "^#\\(?:[[:xdigit:]][[:xdigit:]][[:xdigit:]]\\)+$" color)
	    (setq color (format "#%04X%04X%04X"
				(logand 65535 (nth 0 components))
				(logand 65535 (nth 1 components))
				(logand 65535 (nth 2 components))))))))
    (when msg (message "Color: `%s'" color))
    color))
;; allow triplets in read-color:1 ends here

;; repeat-mode

;; repeat-mode it offers repetition keys at various occasions.  the
;; available keys are listed at that occasions.  e.g. other-buffer.


;; [[file:init.org::*repeat-mode][repeat-mode:1]]
(repeat-mode)
;; repeat-mode:1 ends here

;; flush/keep from isearch


;; [[file:init.org::*flush/keep from isearch][flush/keep from isearch:1]]
(defun mw-flush-lines-isearch-regexp (&optional arg)
  (interactive)
  (flush-lines (or isearch-string isearch-regexp) (point-min) (point-max)))

(defun mw-keep-lines-isearch-regexp (&optional arg)
  (interactive)
  (keep-lines (or isearch-string isearch-regexp) (point-min) (point-max)))
;; flush/keep from isearch:1 ends here

;; [[file:init.org::*flush/keep from isearch][flush/keep from isearch:2]]
(define-key isearch-mode-map  [?\M-f] #'mw-flush-lines-isearch-regexp)
(define-key isearch-mode-map  [?\M-k] #'mw-keep-lines-isearch-regexp)
;; flush/keep from isearch:2 ends here

;; skip whitespace / non-whitespace


;; [[file:init.org::*skip whitespace / non-whitespace][skip whitespace / non-whitespace:1]]
(defun mw-skip-forward-ws-non-ws ()
  "Skip forward (non-)whitespace when on (non-)whitespace."
  (interactive)
  (if (= 0 (car (syntax-after (point))))
      (skip-syntax-forward " ")
    (skip-syntax-forward "^ ")))

(defun mw-skip-backward-ws-non-ws ()
  "Skip backward (non-)whitespace when on (non-)whitespace."
  (interactive)
  (if (= 0 (car (syntax-after (1- (point)))))
      (skip-syntax-backward " ")
    (skip-syntax-backward "^ ")))

(defun mw-skip-forward-current-syntax ()
  "Skip forward current syntax."
  (interactive)
  (skip-syntax-forward (string (syntax-class-to-char (car (syntax-after (point)))))))

(defun mw-skip-backward-current-syntax ()
  "Skip backward current syntax."
  (interactive)
  (skip-syntax-forward (string (syntax-class-to-char (car (syntax-after (point)))))))
;; skip whitespace / non-whitespace:1 ends here

;; [[file:init.org::*skip whitespace / non-whitespace][skip whitespace / non-whitespace:2]]
(key-chord-define-global "f5" #'mw-skip-forward-ws-non-ws)
(key-chord-define-global "b5" #'mw-skip-backward-ws-non-ws)
;; skip whitespace / non-whitespace:2 ends here

;; execute keys in region


;; [[file:init.org::*execute keys in region][execute keys in region:1]]
(defun mw-region-execute-as-keys (start end)
  "Execute keys in region START END."
  (interactive "r")
  (execute-kbd-macro (kbd (buffer-substring start end))))
;; execute keys in region:1 ends here

;; number glasses


;; [[file:init.org::*number glasses][number glasses:1]]
(add-to-list 'load-path "~/s/elisp/mw/number-glasses")
(require 'number-glasses)
;; number glasses:1 ends here

;; create a copy of buffer for anything
;; :PROPERTIES:
;; :ID:       4a8a02de-73a9-4b96-b28e-33fe13e0abdd
;; :END:

;; create a copy of buffer for anything, e.g. readning.


;; [[file:init.org::*create a copy of buffer for anything][create a copy of buffer for anything:1]]
(defun mw-create-copy (start end)
  "Switch to a fresh buffer containing the marked region START END.
Or the whole buffer if there is no region."
  (interactive "r")
  (unless (use-region-p)
    (setq start (point-min))
    (setq end (point-max)))
  (let ((buf (get-buffer-create (concat "forked from " (buffer-name)))))
    (with-current-buffer buf (erase-buffer))
    (append-to-buffer buf start end)
    (switch-to-buffer buf nil t)
    (delete-other-windows)))
;; create a copy of buffer for anything:1 ends here

;; set all buffers view-mode

;; this can help to keep sane when two Emacsens are up and you want to
;; focus on one of them.  e.g. one Emacs for gnus mainly and one for the
;; rest.


;; [[file:init.org::*set all buffers view-mode][set all buffers view-mode:1]]
(defvar mw/switched-to-view-mode nil)

(defun mw/set-buffers-view-mode ()
  "Set all user accessable writable buffers to read only."
  (interactive)
  (cl-assert (null mw/switched-to-view-mode))
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when (and (not (string= (substring (buffer-name) 0 1) " "))
		 (not view-mode))
	(view-mode 1)
	(push buf mw/switched-to-view-mode)))))

(defun mw/unset-buffers-view-mode ()
  "Undo the read only setting from `mw/set-buffers-view-mode'"
  (interactive)
  (while mw/switched-to-view-mode
    (with-current-buffer (pop mw/switched-to-view-mode)
	(view-mode  -1)))
  (cl-assert (null mw/switched-to-view-mode)))
;; set all buffers view-mode:1 ends here

;; duplicate symex


;; [[file:init.org::*duplicate symex][duplicate symex:1]]
(defun mw-duplicate-symex ()
  "Duplicate symex on a new line and go there."
  (interactive)
  (let ((beg (progn (skip-chars-forward "
") (point)))
	(end (progn (forward-sexp) (point))))
    (insert "\n")
    (insert-buffer-substring (current-buffer) beg end)
    (goto-char (1+ end))))
;; duplicate symex:1 ends here

;; duplicate symex


;; [[file:init.org::*duplicate symex][duplicate symex:1]]
(defun mw-narrow-to-symex ()
  "Duplicate symex on a new line and go there."
  (interactive)
  (save-excursion
    (let ((beg (progn (skip-chars-forward "
 ") (point)))
	  (end (progn (forward-sexp) (point))))
      (narrow-to-region beg end))))
;; duplicate symex:1 ends here

;; deactivate mark


;; [[file:init.org::*deactivate mark][deactivate mark:1]]
(defun mw-deactivate-mark ()
  "Provide `deactivate-mark' as command."
  (interactive)
  (deactivate-mark)
  ;; (pop-mark)
  )
;; deactivate mark:1 ends here

;; org-style


;; [[file:init.org::*org-style][org-style:1]]
(push "~/s/elisp/mw/org-style/" load-path)
;; org-style:1 ends here

;; [[file:init.org::*org-style][org-style:2]]
(require 'org-style)
;; org-style:2 ends here

;; squeeze blank lines


;; [[file:init.org::*squeeze blank lines][squeeze blank lines:1]]
(defun mw-squeeze-blank-lines (start end)
  "Squeeze three or more blank lines to just two blank lines.
Restrict to region START END."
  (interactive "r")
  (save-excursion
    (save-restriction
    (narrow-to-region start end)
    (goto-char start)
    (when (looking-at (rx (seq "\n\n" (group (one-or-more "\n")))))
      (delete-region (match-beginning 1) (match-end 1)))
    (condition-case error
	(while t
	  (re-search-forward (rx "\n\n\n\n") end)
	  (backward-char 3)
	  (delete-char -1 t))
      ('error (message "squeeze blank lines done."))))))
;; squeeze blank lines:1 ends here

;; show properties in clone :org:


;; [[file:init.org::*show properties in clone][show properties in clone:1]]
(defun mw-org-properties-show-in-clone ()
  "Display Org properties in a narrowed clone."
  (interactive)
  (cl-assert (eq 'org-mode major-mode))
  (let ((range-block (org-get-property-block)))
    (when range-block
      (clone-indirect-buffer-other-window nil t)
      (narrow-to-region
       (car range-block) (cdr range-block))
      (org-reveal))))
;; show properties in clone:1 ends here

;; [[file:init.org::*show properties in clone][show properties in clone:2]]
(defalias 'mw-org-show-properties-in-clone 'mw-org-properties-show-in-clone)
;; show properties in clone:2 ends here

;; backward-kill-line

;; this can be useful in the minibuffer.

;; notes:
;; - this command is often like C-0k.
;; - derived from backward-kill-sentence.


;; [[file:init.org::*backward-kill-line][backward-kill-line:1]]
(defun backward-kill-line (&optional arg)
  "Kill back from point to start of line.
With ARG, repeat, or kill forward to Nth end of sentence if
negative ARG -N."
  (interactive "p")
  (kill-region (point) (progn (beginning-of-line arg) (point))))
;; backward-kill-line:1 ends here

;; [[file:init.org::*backward-kill-line][backward-kill-line:2]]
(global-set-key (kbd "M-<backspace>") #'backward-kill-line)
;; backward-kill-line:2 ends here

;; org-ol delete all links


;; [[file:init.org::*org-ol delete all links][org-ol delete all links:1]]
(defun org-ol-delete-stored-links ()
  (interactive)
  "Delete all stored links."
  (setf org-stored-links nil))
;; org-ol delete all links:1 ends here

;; insert messages into the list of commit messages


;; [[file:init.org::*insert messages into the list of commit messages][insert messages into the list of commit messages:1]]
(defun mw/add-commit-messages (messages)
  "Add the MESSAGES to the pool of commit messages."
  (dolist (el messages)
    (ring-insert log-edit-comment-ring el)))
;; insert messages into the list of commit messages:1 ends here

;; ask git for a list of latest commit messages


;; [[file:init.org::*ask git for a list of latest commit messages][ask git for a list of latest commit messages:1]]
(defun mw/vc-last-commit-messages ()
  "Return list of last few commit messages."
  (let ((ctr-max 9)
        (ctr 0)
        messages)
    (catch 'break
        (while (not (= ctr ctr-max))
          (with-temp-buffer
            (let* ((revision-spec
                    (concat "HEAD~" (number-to-string ctr)))
                   (retval (vc-git--call (current-buffer) "log" revision-spec "-1" "--pretty=%B")))
              (if (not (= 0 retval))
                  (throw 'break t))
              (setf messages (cons (buffer-string) messages))))
          (cl-incf ctr)))
    messages))
;; ask git for a list of latest commit messages:1 ends here

;; advice magit-status to use the list


;; [[file:init.org::*advice magit-status to use the list][advice magit-status to use the list:1]]
(defun mw/advice/add-last-commit-messages (&optional DIRECTORY CACHE)
  (mw/add-commit-messages (mw/vc-last-commit-messages)))

(advice-add 'magit-status :after  #'mw/advice/add-last-commit-messages)
;; advice magit-status to use the list:1 ends here

;; region as keyboard macro
;; :PROPERTIES:
;; :ID:       a622a235-0f55-4f69-84ca-b9cda5665f6d
;; :END:


;; [[file:init.org::*region as keyboard macro][region as keyboard macro:1]]
(declare-function edmacro-parse-keys "edmacro" (string &optional need-vector))
(defun mw-region-as-last-kbd-macro (start end)
  "Set region as last kbd macro.
START END define the region."
  (interactive "r")
  (setq last-kbd-macro (edmacro-parse-keys (buffer-substring start end)))
  (message "Keyboard macro installed from region"))
;; region as keyboard macro:1 ends here

;; resurrect org-timeline

;; this started from

;;     git checkout 64efc0cd4548f351c5d4b9212ddee63e123d920b^1


;; [[file:init.org::*resurrect org-timeline][resurrect org-timeline:1]]
(defvar org-agenda-show-log-scoped t)
(defvar org-timeline-show-empty-dates nil) ; playing around.
(defvar org-agenda-only-exact-dates nil)

(defcustom org-timeline-show-empty-dates 3
  "Non-nil means `org-timeline' also shows dates without an entry.
When nil, only the days which actually have entries are shown.
When t, all days between the first and the last date are shown.
When an integer, show also empty dates, but if there is a gap of more than
N days, just insert a special line indicating the size of the gap."
  :group 'org-agenda-skip
  :type '(choice
	  (const :tag "None" nil)
	  (const :tag "All" t)
	  (integer :tag "at most")))

(defun org-timeline (&optional dotodo)
  "Show a time-sorted view of the entries in the current Org file.

Only entries with a time stamp of today or later will be listed.

With `\\[universal-argument]' prefix, all unfinished TODO items will also be \
shown, under the current date.

If the buffer contains an active region, only check the region
for dates."
  (interactive "P")
  (let* ((dopast t)
	 (org-agenda-show-log-scoped org-agenda-show-log)
	 (org-agenda-show-log org-agenda-show-log-scoped)
	 (entry (buffer-file-name (or (buffer-base-buffer (current-buffer))
				      (current-buffer))))
	 (date (calendar-current-date))
	 (beg (if (org-region-active-p) (region-beginning) (point-min)))
	 (end (if (org-region-active-p) (region-end) (point-max)))
	 (day-numbers (org-get-all-dates
		       beg end
                       nil nil t
                       ;; 'no-ranges
		       ;; t
                       ;; org-agenda-show-log-scoped ; always include today
		       org-timeline-show-empty-dates))
	 (org-deadline-warning-days 0)
	 (org-agenda-only-exact-dates t)
	 (today (org-today))
	 (past t)
	 args
	 s e rtn d emptyp)
    (setq org-agenda-redo-command
    	  (list 'let
		(list (list 'org-agenda-show-log 'org-agenda-show-log))
    		(list 'org-switch-to-buffer-other-window (current-buffer))
    		(list 'org-timeline (list 'quote dotodo))))
    (put 'org-agenda-redo-command 'org-lprops nil)
    (if (not dopast)
	;; Remove past dates from the list of dates.
	(setq day-numbers (delq nil (mapcar (lambda(x)
					      (if (>= x today) x nil))
					    day-numbers))))
    (org-agenda-prepare (concat "Timeline " (file-name-nondirectory entry)))
    (org-compile-prefix-format 'timeline)
    (org-set-sorting-strategy 'timeline)
    (if org-agenda-show-log-scoped (push :closed args))
    (push :timestamp args)
    (push :deadline args)
    (push :scheduled args)
    (push :sexp args)
    (if dotodo (push :todo args))
    (insert "Timeline of file " entry "\n")
    (add-text-properties (point-min) (point)
			 (list 'face 'org-agenda-structure))
    (org-agenda-mark-header-line (point-min))
    (while (setq d (pop day-numbers))
      (if (and (listp d) (eq (car d) :omitted))
	  (progn
	    (setq s (point))
	    (insert (format "\n[... %d empty days omitted]\n\n" (cdr d)))
	    (put-text-property s (1- (point)) 'face 'org-agenda-structure))
	(if (listp d) (setq d (car d) emptyp t) (setq emptyp nil))
	(if (and (>= d today)
		 dopast
		 past)
	    (progn
	      (setq past nil)
	      (insert (make-string 79 ?-) "\n")))
	(setq date (calendar-gregorian-from-absolute d))
	(setq s (point))
	(setq rtn (and (not emptyp)
		       (apply 'org-agenda-get-day-entries entry
			      date args)))
	(if (or rtn (equal d today) org-timeline-show-empty-dates)
	    (progn
	      (insert
	       (if (stringp org-agenda-format-date)
		   (format-time-string org-agenda-format-date
				       (org-time-from-absolute date))
		 (funcall org-agenda-format-date date))
	       "\n")
	      (put-text-property s (1- (point)) 'face
				 (org-agenda-get-day-face date))
	      (put-text-property s (1- (point)) 'org-date-line t)
	      (put-text-property s (1- (point)) 'org-agenda-date-header t)
	      (if (equal d today)
		  (put-text-property s (1- (point)) 'org-today t))
	      (and rtn (insert (org-agenda-finalize-entries rtn 'timeline) "\n"))
	      (put-text-property s (1- (point)) 'day d)))))
    (goto-char (or (text-property-any (point-min) (point-max) 'org-today t)
		   (point-min)))
    (add-text-properties
     (point-min) (point-max)
     `(org-agenda-type timeline org-redo-cmd ,org-agenda-redo-command))
    (org-agenda-finalize)
    (setq buffer-read-only t)))
;; resurrect org-timeline:1 ends here

;; [[file:init.org::*resurrect org-timeline][resurrect org-timeline:2]]
(defun org-get-all-dates (beg end &optional no-ranges force-today inactive empty pre-re)
  "Return a list of all relevant day numbers from BEG to END buffer positions.
If NO-RANGES is non-nil, include only the start and end dates of a range,
not every single day in the range.  If FORCE-TODAY is non-nil, make
sure that TODAY is included in the list.  If INACTIVE is non-nil, also
inactive time stamps (those in square brackets) are included.
When EMPTY is non-nil, also include days without any entries."
  (let ((re (concat
	     (if pre-re pre-re "")
	     (if inactive org-ts-regexp-both org-ts-regexp)))
	dates dates1 date day day1 day2 ts1 ts2 pos)
    (if force-today
	(setq dates (list (org-today))))
    (save-excursion
      (goto-char beg)
      (while (re-search-forward re end t)
	(setq day (time-to-days (org-time-string-to-time
				 (substring (match-string 1) 0 10)
				)))
	(or (memq day dates) (push day dates)))
      (unless no-ranges
	(goto-char beg)
	(while (re-search-forward org-tr-regexp end t)
	  (setq pos (match-beginning 0))
	  (setq ts1 (substring (match-string 1) 0 10)
		ts2 (substring (match-string 2) 0 10)
		day1 (time-to-days (org-time-string-to-time
				    ts1 ))
		day2 (time-to-days (org-time-string-to-time
				    ts2 )))
	  (while (< (setq day1 (1+ day1)) day2)
	    (or (memq day1 dates) (push day1 dates)))))
      (setq dates (sort dates '<))
      (when empty
	(while (setq day (pop dates))
	  (setq day2 (car dates))
	  (push day dates1)
	  (when (and day2 empty)
	    (if (or (eq empty t)
		    (and (numberp empty) (<= (- day2 day) empty)))
		(while (< (setq day (1+ day)) day2)
		  (push (list day) dates1))
	      (push (cons :omitted (- day2 day)) dates1))))
	(setq dates (nreverse dates1)))
      dates)))
;; resurrect org-timeline:2 ends here

;; display ticked


;; [[file:init.org::*display ticked][display ticked:1]]
(defun mw/display-ticked-4 (beg end)
  "Display region BEG END in new window with ticks inserted every 4 chars.
The fresh buffer is in view-mode.  Recall: C kills it."
  (interactive "r")
  (copy-to-buffer "*tmp" beg end)
  (set-buffer "*tmp")
  (goto-char 1)
  (condition-case nil
      (while t
        (forward-char 4)
        (insert "`"))
    (error nil))
  (switch-to-buffer (get-buffer "*tmp"))
  (view-mode))
;; display ticked:1 ends here

;; kill as invidious

;; kill youtube url as invidious url.


;; [[file:init.org::*kill as invidious][kill as invidious:1]]
(defun mw-region-extract-as-invidious (beg end)
  (let ((input (buffer-substring beg end)))
    (with-temp-buffer
      (insert input)
      (goto-char 1)
      (search-forward "watch?v=")
      (delete-region (point-min) (point))
      (insert "https://yewtu.be/watch?v=")
      (buffer-string))))

(defun mw-region-kill-as-invidious (beg end)
  (interactive "r")
  (kill-new (mw-region-extract-as-invidious beg end) t))
;; kill as invidious:1 ends here

;; backward-forward-whitespace


;; [[file:init.org::*backward-forward-whitespace][backward-forward-whitespace:1]]
(defun mw-backward-forward-whitespace ()
  "E.g. sometimes get to the beginning of the text in the minibuffer."
  (interactive)
  (forward-whitespace -1)
  (forward-whitespace 1))
;; backward-forward-whitespace:1 ends here

;; magit set number of context lines in diff


;; [[file:init.org::*magit set number of context lines in diff][magit set number of context lines in diff:1]]
(defun mw-magit-diff-set-number-context-lines (&optional arg)
  "Set number of context line to ARG.
Precondition: Called in magit status buffer."
  (interactive "p")
  (cl-assert (eq major-mode 'magit-status-mode) nil "Call from a buffer not in magit-status-mode")
  (magit-diff-set-context (lambda (x) (or arg 1))))
;; magit set number of context lines in diff:1 ends here

;; calfw


;; [[file:init.org::*calfw][calfw:1]]
(push "~/s/elisp/external/emacs-calfw/" load-path)
(require 'calfw)
(require 'calfw-org)
(require 'calfw-cal)
;; calfw:1 ends here

;; kill and skip

;; when there is a buffer with stuff to be killed or left this key map
;; can possibly ease the task.

;; example: a buffer with links where you want to keep only some of them.


;; [[file:init.org::*kill and skip][kill and skip:1]]
(defun mw-transient-map-for-kill-and-skip ()
  "Set transient map for killing and skipping.
Leave with a key not in the map e.g. C-g."
  (interactive)
  (set-transient-map
   (let ((map (make-sparse-keymap)))
     (define-key map "k" (lambda () (interactive) (let ((kill-whole-line t)) (kill-line))))
     (define-key map "n" 'next-line)
     (define-key map "p" 'previous-line)
     (define-key map " " (lambda () (interactive) (scroll-up-command 1)))
     (define-key map [?\S-\ ] 'scroll-down-command)
     (define-key map "\C-l" 'recenter-top-bottom)
     map)
   t))
;; kill and skip:1 ends here

;; org-roam

;; - repo https://github.com/org-roam/org-roam


;; [[file:init.org::*org-roam][org-roam:1]]
(straight-use-package '(org-roam :host github :repo "org-roam/org-roam" :branch "v2"))
;; org-roam:1 ends here

;; [[file:init.org::*org-roam][org-roam:2]]
(defvar mw-org-roam-keymap
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "/") #'org-roam-node-find)
    (define-key map (kbd "c") #'org-roam-capture)
    (define-key map (kbd "i") #'org-roam-node-insert)
    (define-key map (kbd "r") #'org-roam-buffer-toggle)
    map)
  "Keymap for org-roam.")

(defun mw/org-roam-load ()
  (interactive)
  (load-library "org-roam")
  (setq org-roam-directory (file-truename "/home/b/o/roam"))
  (setq org-roam-file-extensions '("org"))
  (org-roam-setup)
  (global-set-key (kbd "C-c n") mw-org-roam-keymap))

(mw/org-roam-load)
;; org-roam:2 ends here

;; org-anki-thing

;; homebrew anki.


;; [[file:init.org::*org-anki-thing][org-anki-thing:1]]
(push "~/s/elisp/mw/lab/org-anki-thing" load-path)
(require 'org-anki-thing)
;; org-anki-thing:1 ends here

;; convenience access to process an ankee

;; - happily used with the following "environment":
;;   - have a macro to open the next item.
;;   - at the end of the macro call the function to install the keymap.
;; [[file:../../../../o/roam/20210225003708_index.org][Gutschein]]


;; [[file:init.org::*convenience access to process an ankee][convenience access to process an ankee:1]]
(defun org-anki-thing-set-convenience-keys ()
  "Set keys for hit/fail and call last kbd macro after key press."
  (interactive)
  (set-transient-map
   '(keymap
     (?h . (lambda () (interactive)
             (org-anki-thing-hit)
             (kmacro-call-macro nil)))
     (?f . (lambda () (interactive)
             (org-anki-thing-fail)
             (kmacro-call-macro nil)))
     )
   t (lambda () (message "on-exit of org-anki-thing transient map")))
  (run-at-time 0.1 nil (lambda () (message "(h)it (f)ail"))))
;; convenience access to process an ankee:1 ends here

;; lab patch :patch:

;; add a "property action" to view the properties in the minibuffer.


;; [[file:init.org::*lab patch][lab patch:1]]
(defun org-property-action ()
  "Do an action on properties."
  (interactive)
  (message "Property Action:  [s]et  [d]elete  [D]elete globally  [c]ompute  [v]iew")
  (let ((c (read-char-exclusive)))
    (cl-case c
      (?v (let ((beg-end (org-get-property-block)))
            (when beg-end
              (message "%s" (buffer-substring (car beg-end) (1- (cdr beg-end)))))))
      (?s (call-interactively #'org-set-property))
      (?d (call-interactively #'org-delete-property))
      (?D (call-interactively #'org-delete-property-globally))
      (?c (call-interactively #'org-compute-property-at-point))
      (otherwise (user-error "No such property action %c" c)))))
;; lab patch:1 ends here



;; Great!  Looks good to me.

;; So let's rely on src_text{date} for the hard work.


;; [[file:init.org::*Convert Time in Region to Local Timezone][Convert Time in Region to Local Timezone:3]]
(defun mw-region-time-in-local-tz (start end)
  "Interpret region START END as time. Show time in local timezone.

Example(-!- is point, -m- is mark):

given:

    -!-18:30 january 27, 2021 PST-m-

action:

{ M-x mw-region-time-in-local-tz RET }

result (note: the local zone is CET):

    see the message '2021-01-28 03:30' in the minibuffer.
"
  (interactive "r")
  (message
   "%s"
   (shell-command-to-string
    (concat
     "date --date='" (buffer-substring-no-properties start end) "'"
     " +'%Y-%m-%d %H:%M'"))))
;; Convert Time in Region to Local Timezone:3 ends here

;; publish :org:
;; :PROPERTIES:
;; :ID:       f94e1aec-0bb9-4433-bcdf-6ea4ce742e9f
;; :END:


;; [[file:init.org::*publish][publish:1]]
(require 'ox-html)
(setq org-publish-project-alist
        '(("blog"
           :components ("blog-content"))
          ("blog-content"
           :auto-sitemap nil
           :sitemap-filename "../sitemap.org"
           :sitemap-title "Emacs Fragments"
           :base-directory "~/proj/emacs-blog-mw/src"
           :base-extension "org"
           :html-head "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"RSS\" href=\"https://marcowahl.gitlab.io/emacs-blog-mw/feed.rss\" />"
           :publishing-directory "~/proj/emacs-blog-mw/public"
           :recursive t
           :publishing-function org-html-publish-to-html
           :export-with-tags t
           :headline-levels 8
           :table-of-contents nil
           :section-numbers nil
           :sub-superscript nil
           :todo-keywords nil
           :with-author t
           :with-date t
           :with-creator-info t
           :html-preamble t
           :html-postamble t
           :style "This is raw html for stylesheet <link>'s"
           :timestamp t
           :exclude-tags ("noexport")
           :auto-preamble t)))
;; publish:1 ends here

;; patch org-goto to search whole buffer

;; - don't exclude tags and body for org-goto in mode 'outline.

;; - want to use this for some time to get a feeling if this is something.


;; [[file:init.org::*patch org-goto to search whole buffer][patch org-goto to search whole buffer:1]]
(defun org-goto--local-search-headings (string bound noerror)
  "Search."
  (catch 'return
    (while (if isearch-forward
               (search-forward string bound noerror)
             (search-backward string bound noerror))
      (throw 'return (point)))))
;; patch org-goto to search whole buffer:1 ends here

;; Preparation on the Emacs Side

;; Activate the following code.


;; [[file:init.org::*Preparation on the Emacs Side][Preparation on the Emacs Side:1]]
(add-to-list 'org-protocol-protocol-alist
             '("eww"
               :protocol "eww"
               :function mw-start-eww-for-url))
;; Preparation on the Emacs Side:1 ends here

;; [[file:init.org::*Preparation on the Emacs Side][Preparation on the Emacs Side:2]]
(defun mw-start-eww-for-url (plist)
  "Raise Emacs and call eww with the url in PLIST."
  (raise-frame)
  (eww (plist-get plist :url))
  nil)
;; Preparation on the Emacs Side:2 ends here

;; function to get current region


;; [[file:init.org::*function to get current region][function to get current region:1]]
(defun mw-current-region-string ()
  "Return the region as string when there is a region."
  (when (region-active-p)
    (buffer-substring (region-beginning) (region-end))))
;; function to get current region:1 ends here

;; valign

;; align tables.

;; https://github.com/casouri/valign


;; [[file:init.org::*valign][valign:1]]
(straight-use-package '(valign :host github :repo "casouri/valign"))
(require 'valign)
;; valign:1 ends here

;; org-todo-currently-clocked :org:
;; :PROPERTIES:
;; :ID:       59ec359d-38b2-436e-82a4-d9fe0691f7f4
;; :END:


;; [[file:init.org::*org-todo-currently-clocked][org-todo-currently-clocked:1]]
(defun org-todo-currently-clocked (&optional arg)
  "Change the TODO state of clocked item."
  (interactive "P")
  (save-mark-and-excursion
    (org-clock-goto)
    (org-todo arg)))
;; org-todo-currently-clocked:1 ends here

;; show "minimal" outline
;; :PROPERTIES:
;; :ID:       3a749fce-efeb-41af-9753-be0abbdb141c
;; :END:


;; [[file:init.org::*show "minimal" outline][show "minimal" outline:1]]
(defun mw-org-show-minimal-outline ()
  (interactive)
  (let ((p (point)))
    (goto-char 1)
    (org-ctrl-c-tab)
    (goto-char p)
    (org-reveal)))
;; show "minimal" outline:1 ends here

;; drop last element of a list
;; :PROPERTIES:
;; :ID:       e7af2060-96e6-4aa5-8f2c-a9ab85caa8a4
;; :END:


;; [[file:init.org::*drop last element of a list][drop last element of a list:1]]
(defun mw-drop-last-element (lis)
  "Return list with the last element of LIS dropped."
  (nreverse (cdr (nreverse lis))))
;; drop last element of a list:1 ends here

;; test
;; :PROPERTIES:
;; :ID:       de50f5c3-5bc7-4ac4-b2c3-9a85ec4e026a
;; :END:


;; [[file:init.org::*test][test:1]]
(ert-deftest test-c3b856fa4affac05a85b66d7f906f6529e6a253b ()
  (should (equal '(2 3) (mw-drop-last-element '(2 3 5))))
  (should (not (mw-drop-last-element '(a))))
  (should (not (mw-drop-last-element nil))))
;; test:1 ends here

;; mark whole org plain list
;; :PROPERTIES:
;; :ID:       373f0ef4-0462-4e78-a1e0-e2cf9fcd0bc8
;; :END:


;; [[file:init.org::*mark whole org plain list][mark whole org plain list:1]]
(defun org-mark-plain-list ()
  "Mark the plain list at the cursor."
  (interactive)
  (when-let ((list-struct (org-list-struct)))
    (goto-char (caar list-struct))
    (push-mark (car (last (car (last list-struct)))) nil t)))
;; mark whole org plain list:1 ends here

;; execute region as shell command
;; :PROPERTIES:
;; :ID:       3d91fb84-65e0-4052-982f-2ee667b89402
;; :END:


;; [[file:init.org::*execute region as shell command][execute region as shell command:1]]
(defun mw-region-execute-as-shell-command (beg end)
  (interactive "r")
  (when (region-active-p)
    (shell-command (buffer-substring beg end))))
;; execute region as shell command:1 ends here

;; [[file:init.org::*execute region as shell command][execute region as shell command:2]]
(defun mw-region-execute-as-async-shell-command (beg end)
  (interactive "r")
  (when (region-active-p)
    (async-shell-command (buffer-substring beg end))))
;; execute region as shell command:2 ends here

;; comma, dot insert to the left
;; :PROPERTIES:
;; :ID:       e30cdfc5-cf83-44dd-b9ee-d1aebd06adb1
;; :END:


;; [[file:init.org::*comma, dot insert to the left][comma, dot insert to the left:1]]
(defun mw-insert-char-to-the-left (char)
  "Skip whitespace to the left and place a char there."
  (barf-if-buffer-read-only)
  (if (looking-back (rx (or " " "\t")))
    (let ((p (point)))
      (forward-whitespace -1)
      (insert char)
      (goto-char (1+ p)))
    (insert char)))

(defun mw-comma-to-the-left ()
  "Skip whitespace to the left and place a comma there."
  (interactive)
  (mw-insert-char-to-the-left ","))

(defun mw-dot-to-the-left ()
  "Skip whitespace to the left and place a comma there."
  (interactive)
  (mw-insert-char-to-the-left "."))
;; comma, dot insert to the left:1 ends here

;; adjust region to in/exclude empty lines around
;; :PROPERTIES:
;; :ID:       e9d99689-3881-4d2c-bdd7-19174f564ed7
;; :END:


;; [[file:init.org::*adjust region to in/exclude empty lines around][adjust region to in/exclude empty lines around:1]]
(defun mw-region-exclude-bottom-empty-lines (start end)
  (interactive "r")
  (when (= (point) start)
    (exchange-point-and-mark))
  (assert (<= (mark) (point)))
  (skip-chars-backward " \t\n")
  (when (< (point) (mark))
    (goto-char (mark)))
  (unless (= (point) (point-max))
    (forward-char)))

(defun mw-region-include-top-empty-lines (start end)
  (interactive "r")
  (unless (= (point) start)
    (exchange-point-and-mark))
  ; assert: point is at the start of the region.
  (while (progn (backward-char)
                (looking-at-p "^[ \t]*$"))
    (beginning-of-line))
  (forward-char))

(defun mw-region-exclude-top-empty-lines (start end)
  (interactive "r")
  (unless (= (point) start)
    (exchange-point-and-mark))
  ; assert: point is at the start of the region.
  (while (looking-at-p "[ \t]*$")
    (beginning-of-line 2)))

(defun mw-region-include-top-exclude-bottom-empty-lines ()
  "Let region include empty lines at top and exclude those at
bottom."
  (interactive)
  (when (region-active-p)
    (mw-region-exclude-bottom-empty-lines (region-beginning) (region-end))
    (mw-region-include-top-empty-lines (region-beginning) (region-end))))

(defun mw-region-exclude-top-exclude-bottom-empty-lines ()
  "Let region exclude empty lines at top and bottom."
  (interactive)
  (when (region-active-p)
    (mw-region-exclude-bottom-empty-lines (region-beginning) (region-end))
    (mw-region-exclude-top-empty-lines (region-beginning) (region-end)))
  (unless (= (point) (region-end))
    (exchange-point-and-mark))
  (beginning-of-line))
;; adjust region to in/exclude empty lines around:1 ends here

;; switch to sudo dired
;; :PROPERTIES:
;; :ID:       c9f819ee-4a9c-4210-a5ce-ac438a1e5d92
;; :END:


;; [[file:init.org::*switch to sudo dired][switch to sudo dired:1]]
(defun mw-dired-go-sudo ()
  (interactive)
  (dired (concat "/sudo::" (dired-current-directory))))
;; switch to sudo dired:1 ends here

;; delete register
;; :PROPERTIES:
;; :ID:       a56992bf-1792-421f-84f1-73506ad4a76b
;; :END:

;; AFAICT this function is not in emacs yet.


;; [[file:init.org::*delete register][delete register:1]]
(defun mw-delete-register (register)
  "Delete REGISTER.
Interactively, reads the register using `register-read-with-preview'."
  (interactive (list (register-read-with-preview "Delete register: ")))
  (assoc-delete-all register register-alist))
;; delete register:1 ends here

;; alternative to org-show-entry
;; :PROPERTIES:
;; :ID:       2b2d0113-8837-45a3-8d5e-bd4dcf0bbc34
;; :END:


;; [[file:init.org::*alternative to org-show-entry][alternative to org-show-entry:1]]
(defun mw-org-show-entry-just-the-content ()
  "Reveal the part between end of metadata and the first child.
Derived starting with `org-show-entry'."
  (interactive)
  (save-excursion
    (ignore-errors
      (if (org-before-first-heading-p)
          (goto-char (point-min))
        (org-back-to-heading t)
        (org-end-of-meta-data t))
      (org-flag-region
       (line-end-position 0)
       (save-excursion
	 (if (re-search-forward
	      (concat "[\r\n]\\(" org-outline-regexp "\\)") nil t)
	     (match-beginning 1)
	   (point-max)))
       nil
       'outline))))
;; alternative to org-show-entry:1 ends here

;; convert yt url to invidious
;; :PROPERTIES:
;; :ID:       bb69d935-19ac-4281-96f6-925515fefbec
;; :END:


;; [[file:init.org::*convert yt url to invidious][convert yt url to invidious:1]]
(defun mw-convert-yt-to-invidious-ap ()
  "Convert youtube to invidious url at point."
  (interactive)
  (when-let ((url (thing-at-point 'url)))
    (let ((bounds (bounds-of-thing-at-point 'url)))
      (delete-region (car bounds) (cdr bounds)))
    (insert
     (with-temp-buffer
       (insert url)
       (beginning-of-buffer)
       (replace-string "youtube.com" "invidio.us")
       (buffer-substring (point-min) (point-max))))))
;; convert yt url to invidious:1 ends here

;; call the tool "calendar" from calendar
;; :PROPERTIES:
;; :ID:       dcd9317e-4454-4d29-8c7b-d975a147b12f
;; :END:


;; [[file:init.org::*call the tool "calendar" from calendar][call the tool "calendar" from calendar:1]]
(defun mw-calendar-call-calendar-tool ()
  "Call the tool \"calendar\" on the date at point."
  (interactive)
  (let ((date (calendar-cursor-to-date)))
    (shell-command
     (concat
      "calendar" " -t "
      (format "%4d%02d%02d" (nth 2 date) (nth 0 date) (nth 1 date))))))
;; call the tool "calendar" from calendar:1 ends here

;; [[file:init.org::*call the tool "calendar" from calendar][call the tool "calendar" from calendar:2]]
(eval-after-load "calendar"
  '(add-hook 'calendar-mode-hook (lambda () (local-set-key "C" #'mw-calendar-call-calendar-tool))))
;; call the tool "calendar" from calendar:2 ends here

;; in a quote-block? :org:
;; :PROPERTIES:
;; :ID:       fa708013-8ae0-44ce-ab3f-bf19a3ed4020
;; :END:


;; [[file:init.org::*in a quote-block?][in a quote-block?:1]]
(defun org--in-quote/verse-block-p ()
  "Return non-nil when in a quote/verse-block, nil otherwise."
  (catch 'quote/verse-block
    (let ((context (org-element-context (org-element-at-point)))
          (block-types-of-interest '(quote-block verse-block)))
      (when (memq (org-element-type context) block-types-of-interest)
        (throw 'quote/verse-block 'quote/verse-block))
      (while context
        (setq context (plist-get (cadr context) :parent))
        (when (memq (org-element-type context) block-types-of-interest)
          (throw 'quote/verse-block 'quote/verse-block))))))
;; in a quote-block?:1 ends here

;; special edit quote block
;; :PROPERTIES:
;; :ID:       7afd8476-e3fd-4244-814d-85067a03b382
;; :END:


;; [[file:init.org::*special edit quote block][special edit quote block:1]]
(defun org-edit-quote/verse-block ()
  "Edit text block at point.
\\<org-src-mode-map>
A new buffer is created and the block is copied into it.

When done, exit with `\\[org-edit-src-exit]'.  The edited text \
will then replace
the area in the Org mode buffer.

Throw an error when not at an appropriate block."
  (interactive)
  (let ((element (org-element-at-point))
        (block-types-of-interest '(quote-block verse-block comment-block)))
    (let* ((type "org")
	   (mode (org-src-get-lang-mode type)))
      (org-src--edit-element
       element
       (org-src--construct-edit-buffer-name (buffer-name) type)
       mode
       (lambda () (org-escape-code-in-region (point-min) (point-max)))))
    t))
;; special edit quote block:1 ends here

;; patch org-edit-special to respect quote block
;; :PROPERTIES:
;; :ID:       ef18abbf-e714-4c8d-a550-2fcc6ea8b7cf
;; :END:


;; [[file:init.org::*patch org-edit-special to respect quote block][patch org-edit-special to respect quote block:1]]
(defun org-edit-special (&optional arg)
  "Call a special editor for the element at point.
When at a table, call the formula editor with `org-table-edit-formulas'.
When in a source code block, call `org-edit-src-code'.
When in a fixed-width region, call `org-edit-fixed-width-region'.
When in an export block, call `org-edit-export-block'.
When in a LaTeX environment, call `org-edit-latex-environment'.
When at an #+INCLUDE keyword, visit the included file.
When at a footnote reference, call `org-edit-footnote-reference'.
When at a planning line call, `org-deadline' and/or `org-schedule'.
When at an active timestamp, call `org-time-stamp'.
When at an inactive timestamp, call `org-time-stamp-inactive'.
On a link, call `ffap' to visit the link at point.
Otherwise, return a user error."
  (interactive "P")
  (let ((element (org-element-at-point)))
    (barf-if-buffer-read-only)
    (pcase (org-element-type element)
      (`src-block
       (if (not arg) (org-edit-src-code)
	 (let* ((info (org-babel-get-src-block-info))
		(lang (nth 0 info))
		(params (nth 2 info))
		(session (cdr (assq :session params))))
	   (if (not session) (org-edit-src-code)
	     ;; At a source block with a session and function called
	     ;; with an ARG: switch to the buffer related to the
	     ;; inferior process.
	     (switch-to-buffer
	      (funcall (intern (concat "org-babel-prep-session:" lang))
		       session params))))))
      (`keyword
       (unless (member (org-element-property :key element)
		       '("INCLUDE" "SETUPFILE"))
	 (user-error "No special environment to edit here"))
       (let ((value (org-element-property :value element)))
	 (unless (org-string-nw-p value) (user-error "No file to edit"))
	 (let ((file (and (string-match "\\`\"\\(.*?\\)\"\\|\\S-+" value)
			  (or (match-string 1 value)
			      (match-string 0 value)))))
	   (when (org-file-url-p file)
	     (user-error "Files located with a URL cannot be edited"))
	   (org-link-open-from-string
	    (format "[[%s]]" (expand-file-name file))))))
      (`table
       (if (eq (org-element-property :type element) 'table.el)
           (org-edit-table.el)
         (call-interactively 'org-table-edit-formulas)))
      ;; Only Org tables contain `table-row' type elements.
      (`table-row (call-interactively 'org-table-edit-formulas))
      (`example-block (org-edit-src-code))
      (`export-block (org-edit-export-block))
      (`quote-block (org-edit-quote/verse-block))
      (`verse-block (org-edit-quote/verse-block))
      (`comment-block (org-edit-quote/verse-block))
      (`fixed-width (org-edit-fixed-width-region))
      (`latex-environment (org-edit-latex-environment))
      (`planning
       (let ((proplist (cadr element)))
         (mapc #'call-interactively
               (remq nil
                     (list
                      (when (plist-get proplist :deadline) #'org-deadline)
                      (when (plist-get proplist :scheduled) #'org-schedule))))))
      (_
       ;; No notable element at point.  Though, we may be at a link or
       ;; a footnote reference, which are objects.  Thus, scan deeper.
       (let ((context (org-element-context element)))
	 (pcase (org-element-type context)
	   (`footnote-reference (org-edit-footnote-reference))
	   (`inline-src-block (org-edit-inline-src-code))
	   (`timestamp (if (eq 'inactive (org-element-property :type context))
			   (call-interactively #'org-time-stamp-inactive)
			 (call-interactively #'org-time-stamp)))
	   (`link (call-interactively #'ffap))
	   (_
	    (if (org--in-quote/verse-block-p)
		(progn
		  (org-up-element)
		  (org-edit-quote/verse-block))
	      (user-error "No special environment to edit here")))))))))
;; patch org-edit-special to respect quote block:1 ends here

;; org-agenda-list at a user chosen time
;; :PROPERTIES:
;; :ID:       19cb9ad6-afbe-48e9-b719-7cb8747fa48c
;; :END:

;; This builds on /org-test-at-time/ from org-mode/testing/org-test.el.


;; [[file:init.org::*org-agenda-list at a user chosen time][org-agenda-list at a user chosen time:1]]
(defmacro org-test-at-time (time &rest body)
  "Run body while pretending that the current time is TIME.
TIME can be a non-nil Lisp time value, or a string specifying a date and time."
  (declare (indent 1))
  (let ((tm (cl-gensym))
	(at (cl-gensym)))
    `(let* ((,tm ,time)
	    (,at (if (stringp ,tm)
		     (apply #'encode-time (org-parse-time-string ,tm))
		   ,tm)))
       (cl-letf
	   ;; Wrap builtins whose behavior can depend on the current time.
	   (((symbol-function 'current-time)
	     (lambda () ,at))
	    ((symbol-function 'current-time-string)
	     (lambda (&optional time &rest args)
	       (apply ,(symbol-function 'current-time-string)
		      (or time ,at) args)))
	    ((symbol-function 'current-time-zone)
	     (lambda (&optional time &rest args)
	       (apply ,(symbol-function 'current-time-zone)
		      (or time ,at) args)))
	    ((symbol-function 'decode-time)
	     (lambda (&optional time) (funcall ,(symbol-function 'decode-time)
					       (or time ,at))))
	    ((symbol-function 'encode-time)
	     (lambda (time &rest args)
	       (apply ,(symbol-function 'encode-time) (or time ,at) args)))
	    ((symbol-function 'float-time)
	     (lambda (&optional time)
	       (funcall ,(symbol-function 'float-time) (or time ,at))))
	    ((symbol-function 'format-time-string)
	     (lambda (format &optional time &rest args)
	       (apply ,(symbol-function 'format-time-string)
		      format (or time ,at) args)))
	    ((symbol-function 'set-file-times)
	     (lambda (file &optional time)
	       (funcall ,(symbol-function 'set-file-times) file (or time ,at))))
	    ((symbol-function 'time-add)
	     (lambda (a b) (funcall ,(symbol-function 'time-add)
				    (or a ,at) (or b ,at))))
	    ((symbol-function 'time-equal-p)
	     (lambda (a b) (funcall ,(symbol-function 'time-equal-p)
				    (or a ,at) (or b ,at))))
	    ((symbol-function 'time-less-p)
	     (lambda (a b) (funcall ,(symbol-function 'time-less-p)
				    (or a ,at) (or b ,at))))
	    ((symbol-function 'time-subtract)
	     (lambda (a b) (funcall ,(symbol-function 'time-subtract)
				    (or a ,at) (or b ,at)))))
	 ,@body))))
;; org-agenda-list at a user chosen time:1 ends here

;; [[file:init.org::*org-agenda-list at a user chosen time][org-agenda-list at a user chosen time:2]]
(defun mw-org-agenda-list ()
  (interactive)
  (let* ((input-date (concat (org-read-date) " 11:55"))
         (time (apply #'encode-time (org-parse-time-string input-date))))
    (org-test-at-time
     time
     (org-agenda-list))))
;; org-agenda-list at a user chosen time:2 ends here

;; recenter-mode
;; :PROPERTIES:
;; :ID:       65342c5c-0c7b-4998-8d1b-6e02033842ec
;; :END:


;; [[file:init.org::*recenter-mode][recenter-mode:1]]
(defun mw-recenter-top (&optional _arg _foo)
  (recenter 0))

(defvar *mw-recenter-top-commands*
  '(goto-char
    forward-char
    backward-char
    forward-word
    backward-word
    next-line
    previous-line
    beginning-of-buffer
    end-of-buffer
    scroll-up-command
    scroll-down-command
    move-beginning-of-line
    move-end-of-line))

(define-minor-mode mw-recenter-mode
  "Recenter on certain actions."
  :lighter " line0"
  :global t ; global because can't make this behavior buffer local.
  (cond
   (mw-recenter-mode
    (mapc (lambda (x) (advice-add x :after #'mw-recenter-top)) *mw-recenter-top-commands*))
   (t
    (mapc (lambda (x) (advice-remove x #'mw-recenter-top)) *mw-recenter-top-commands*))))
;; recenter-mode:1 ends here

;; color-rg
;; :PROPERTIES:
;; :ID:       d633c462-fcc5-4f1f-b8f6-8a10e1f6d311
;; :END:


;; [[file:init.org::*color-rg][color-rg:1]]
(straight-use-package '(color-rg :host github :repo "manateelazycat/color-rg"))
(require 'color-rg)
;; color-rg:1 ends here

;; table-lamp/torch
;; :PROPERTIES:
;; :ID:       447f6dd7-4931-4ee9-b137-1c7ea2e18db0
;; :END:


;; [[file:init.org::*table-lamp/torch][table-lamp/torch:1]]
(defun mw-table-lamp ()
  (interactive)
  (make-frame-command)
  (other-frame 1)
  (switch-to-buffer (get-buffer-create "*table-lamp"))
  (set-background-color "white")
  (hidden-mode-line-mode))
;; table-lamp/torch:1 ends here

;; [[file:init.org::*table-lamp/torch][table-lamp/torch:2]]
(defalias 'mw-torch 'mw-table-lamp "Couldn't decide which name is best.")
;; table-lamp/torch:2 ends here

;; code
;; :PROPERTIES:
;; :ID:       36e7b195-fd92-43af-9acd-fc9cd17f64bd
;; :END:

;; - source: https://www.reddit.com/r/emacs/comments/flxqei/cloningmirroring_a_region_to_some_other_location/


;; [[file:init.org::*code][code:1]]
(defvar text-clone--maintaining nil)

(defun my-text-clone--maintain (ol1 after beg end &optional _len)
  "Propagate the changes made under the overlay OL1 to the other clones.
This is used on the `modification-hooks' property of text clones."
  (when (and after (not undo-in-progress)
             (not text-clone--maintaining)
             (overlay-start ol1))
    (let ((margin (if (overlay-get ol1 'text-clone-spreadp) 1 0)))
      (setq beg (max beg (+ (overlay-start ol1) margin)))
      (setq end (min end (- (overlay-end ol1) margin)))
      (when (<= beg end)
        (save-excursion
          (when (overlay-get ol1 'text-clone-syntax)
            ;; Check content of the clone's text.
            (let ((cbeg (+ (overlay-start ol1) margin))
                  (cend (- (overlay-end ol1) margin)))
              (goto-char cbeg)
              (save-match-data
                (if (not (re-search-forward
                          (overlay-get ol1 'text-clone-syntax) cend t))
                    ;; Mark the overlay for deletion.
                    (setq end cbeg)
                  (when (< (match-end 0) cend)
                    ;; Shrink the clone at its end.
                    (setq end (min end (match-end 0)))
                    (move-overlay ol1 (overlay-start ol1)
                                  (+ (match-end 0) margin)))
                  (when (> (match-beginning 0) cbeg)
                    ;; Shrink the clone at its beginning.
                    (setq beg (max (match-beginning 0) beg))
                    (move-overlay ol1 (- (match-beginning 0) margin)
                                  (overlay-end ol1)))))))
          ;; Now go ahead and update the clones.
          (let ((head (- beg (overlay-start ol1)))
                (tail (- (overlay-end ol1) end))
                (str (buffer-substring beg end))
                (nothing-left t)
                (text-clone--maintaining t))
            (dolist (ol2 (overlay-get ol1 'text-clones))
              (with-current-buffer (overlay-buffer ol2) ;;< Tobias
                (let ((oe (overlay-end ol2)))
                  (unless (or (eq ol1 ol2) (null oe))
                    (setq nothing-left nil)
                    (let ((mod-beg (+ (overlay-start ol2) head)))
                      ;;(overlay-put ol2 'modification-hooks nil)
                      (goto-char (- (overlay-end ol2) tail))
                      (unless (> mod-beg (point))
                        (save-excursion (insert str))
                        (delete-region mod-beg (point)))
                      ;;(overlay-put ol2 'modification-hooks '(text-clone--maintain))
                      )))))
            (if nothing-left (delete-overlay ol1))))))))

(defun my-text-clone-create (start end &optional spreadp syntax)
  "Create a text clone of START...END at point.
Text clones are chunks of text that are automatically kept identical:
changes done to one of the clones will be immediately propagated to the other.

The buffer's content at point is assumed to be already identical to
the one between START and END.
If SYNTAX is provided it's a regexp that describes the possible text of
the clones; the clone will be shrunk or killed if necessary to ensure that
its text matches the regexp.
If SPREADP is non-nil it indicates that text inserted before/after the
clone should be incorporated in the clone."
  ;; To deal with SPREADP we can either use an overlay with `nil t' along
  ;; with insert-(behind|in-front-of)-hooks or use a slightly larger overlay
  ;; (with a one-char margin at each end) with `t nil'.
  ;; We opted for a larger overlay because it behaves better in the case
  ;; where the clone is reduced to the empty string (we want the overlay to
  ;; stay when the clone's content is the empty string and we want to use
  ;; `evaporate' to make sure those overlays get deleted when needed).
  ;;
  (let* ((clone-buf (or (and (markerp start) (marker-buffer start))
                        (current-buffer)))
         (pt-end (+ (point) (- end start)))
         (start-margin (if (or (not spreadp) (bobp) (with-current-buffer clone-buf (<= start (point-min))))
                           0 1))
         (end-margin (if (or (not spreadp)
                             (>= pt-end (point-max))
                             (with-current-buffer clone-buf (>= start (point-max))))
                         0 1))
         ;; FIXME: Reuse overlays at point to extend dups!
         (ol1 (make-overlay (- start start-margin) (+ end end-margin) clone-buf t)) ;;< Tobias
         (ol2 (make-overlay (- (point) start-margin) (+ pt-end end-margin) nil t))
         (dups (list ol1 ol2)))
    (overlay-put ol1 'modification-hooks '(my-text-clone--maintain)) ;;< Tobias
    (when spreadp (overlay-put ol1 'text-clone-spreadp t))
    (when syntax (overlay-put ol1 'text-clone-syntax syntax))
    ;;(overlay-put ol1 'face 'underline)
    (overlay-put ol1 'evaporate t)
    (overlay-put ol1 'text-clones dups)
    ;;
    (overlay-put ol2 'modification-hooks '(my-text-clone--maintain)) ;;< Tobias
    (when spreadp (overlay-put ol2 'text-clone-spreadp t))
    (when syntax (overlay-put ol2 'text-clone-syntax syntax))
    ;;(overlay-put ol2 'face 'underline)
    (overlay-put ol2 'evaporate t)
    (overlay-put ol2 'text-clones dups)))

(defun my-clone (b e pt)
  "Clone region from B to E at PT.
B, E, and PT can be markers."
  (interactive (list
                (region-beginning)
                (region-end)
                (save-excursion
                  (message "Choose clone position and finish with C-M-c.")
                  (recursive-edit)
                  (set-marker (make-marker) (point))
                  )))
  (let ((b-mark (set-marker (make-marker) b))
        (e-mark (set-marker (make-marker) e))
        (txt (buffer-substring b e)))
    (with-current-buffer (or (and (markerp pt) (marker-buffer pt))
                             (current-buffer))
      (goto-char pt)
      (insert txt)
      (goto-char pt)
      (my-text-clone-create b-mark e-mark))))
;; code:1 ends here

;; gif-screencast
;; :PROPERTIES:
;; :ID:       2c6288d5-430a-492c-908f-2afe3cded398
;; :END:


;; [[file:init.org::*gif-screencast][gif-screencast:1]]
(straight-use-package 'gif-screencast)
;; gif-screencast:1 ends here

;; epaint
;; :PROPERTIES:
;; :ID:       1f3d623c-7359-44b5-ba9e-b32488bbb36b
;; :END:

;; - interesting.
;; - very experimental and Special.  IIUC.


;; [[file:init.org::*epaint][epaint:1]]
(push "~/s/elisp/external/epaint" load-path)
(require 'epaint)
;; epaint:1 ends here

;; display time
;; :PROPERTIES:
;; :ID:       1c46a505-1c85-4734-b7db-8d57152fc629
;; :END:


;; [[file:init.org::*display time][display time:1]]
(add-hook 'emacs-startup-hook #'display-time)
;; display time:1 ends here

;; activate appointments
;; :PROPERTIES:
;; :ID:       59b215f3-bfd9-4a7e-bd2a-96f5d1fef14d
;; :END:

;; with


;; [[file:init.org::*activate appointments][activate appointments:1]]
(add-hook 'emacs-startup-hook (lambda () (appt-activate 1)))
;; activate appointments:1 ends here

;; pinentry
;; :PROPERTIES:
;; :ID:       190c3e80-5b73-4b4a-a3ff-d3f064165792
;; :END:


;; [[file:init.org::*pinentry][pinentry:1]]
(add-hook 'emacs-startup-hook #'pinentry-start)
;; pinentry:1 ends here

;; activate server at startup
;; :PROPERTIES:
;; :ID:       aad40f03-248d-4ac6-b56f-9914300e9ab8
;; :END:


;; [[file:init.org::*activate server at startup][activate server at startup:1]]
(add-hook 'emacs-startup-hook #'server-start)
;; activate server at startup:1 ends here

;; disable ido-mode enable fido-mode


;; [[file:init.org::*disable ido-mode enable fido-mode][disable ido-mode enable fido-mode:1]]
(ido-mode -1)
(fido-mode 1)
;; disable ido-mode enable fido-mode:1 ends here

;; say something
;; :PROPERTIES:
;; :ID:       ab087c3f-90f3-462f-81a5-923ac0a11165
;; :END:

;; Let Emacs say something at start.


;; [[file:init.org::*say something][say something:1]]
(add-hook 'emacs-startup-hook
          (lambda ()
            (start-process
             "play-a-sound" "*play-a-sound-output*"
             "espeak"
             "-v"
             ;; "en-swedish"
             "en"
             "-s"
             "155" ;; word speed
             ;; "Hi guys! This is a fantastic day! Let's do something!"
             ;; "Hi guys!  Let's make something out of this day!"
             "Woohoo!  I'm back!"
             ))
          91 ; indication to do this action late.
          )
;; say something:1 ends here

;; gnus-summary-toggle-header :gnus:
;; :PROPERTIES:
;; :ID:       cdd26788-3d53-4486-94b4-f0feb560e6f8
;; :END:

;; Can never remember the original name of this fun.  The alias is
;; supposed to help me finding the command.

;; the command switches the representation of a message.  there is the
;; usual nice representation and also a raw representation.


;; [[file:init.org::*gnus-summary-toggle-header][gnus-summary-toggle-header:1]]
(defalias 'mw-gnus-article-toggle-view #'gnus-summary-toggle-header)
;; gnus-summary-toggle-header:1 ends here


;;; init.el ends here
