;;; .emacs-custom.el --- custotmizations

;;; Commentary:
;;

;;; Code:

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 143 :width normal))))
 '(bookmark-face ((t nil)))
 '(comint-highlight-prompt ((t (:inherit minibuffer-prompt :background "khaki"))))
 '(cursor ((t (:background "red"))))
 '(downplay-face ((t (:height 0.25))))
 '(font-lock-function-name-face ((t (:foreground "#11f" :weight bold))))
 '(hl-line ((t (:overline t))))
 '(ledger-font-xact-highlight-face ((t (:inherit nil :extend t :weight bold))))
 '(line-number-current-line ((t (:inherit default :inverse-video t))))
 '(line-number-major-tick ((t (:inherit line-number :weight bold))))
 '(magit-diff-context-highlight ((t nil)))
 '(magit-diff-hunk-heading-highlight ((t nil)))
 '(magit-diff-hunk-region ((t (:background "cyan" :box (:line-width 2 :color "grey75" :style released-button)))))
 '(mode-line ((t (:background "grey75" :foreground "dark blue" :box (:line-width -1 :style released-button)))))
 '(mode-line-inactive ((t (:inherit mode-line :background "grey90" :foreground "grey50" :box (:line-width -1 :color "grey75") :weight light))))
 '(myface ((t (:overline t :weight bold :width extra-expanded))))
 '(org-agenda-clocking ((t (:inherit error))))
 '(org-agenda-done ((t (:foreground "dim gray"))))
 '(org-agenda-restriction-lock ((t (:background "orange"))))
 '(org-agenda-structure ((t (:foreground "cyan" :weight bold))))
 '(org-block ((t (:inherit shadow :extend t))))
 '(org-column ((t (:strike-through nil :underline nil :slant normal :weight normal))))
 '(org-drawer ((t (:foreground "orange" :weight bold))))
 '(org-journal-calendar-entry-face ((t (:foreground "#aa00ee" :slant italic))))
 '(org-level-1 ((t (:inherit outline-1 :extend t :foreground "tomato4"))))
 '(org-list-dt ((t (:weight bold))))
 '(org-meta-line ((t (:inherit font-lock-comment-face :height 0.8 :width normal))))
 '(org-quote ((t (:height 0.9))))
 '(org-verbatim ((t (:height 1.0))))
 '(org-verse ((t (:inherit org-block :height 0.9))))
 '(outline-1 ((t (:inherit default))))
 '(outline-2 ((t (:inherit font-lock-builtin-face))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "dark orange"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "yellow"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "lawn green"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "cyan"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "blue"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "magenta"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "dark violet"))))
 '(rainbow-delimiters-depth-9-face ((t (:foreground "pink"))))
 '(region ((t (:background "rosy brown" :distant-foreground "gtk_selection_fg_color")))))


(provide '.emacs-custom)




(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(2C-beyond-fill-column 0)
 '(Info-hide-note-references nil)
 '(abbrev-suggest-hint-threshold 2)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(appt-message-warning-time 3)
 '(appt-warning-time-regexp "warning-time \\([0-9]+\\)")
 '(async-shell-command-display-buffer nil)
 '(auth-source-debug t)
 '(auth-source-do-cache nil)
 '(auth-source-gpg-encrypt-to '("3E7C2CBACBABDA5BD6995A4C49010A040A3AE6F2"))
 '(auth-source-protocols
   '((imap "imap" "imaps" "143" "993")
     (pop3 "pop3" "pop" "pop3s" "110" "995")
     (ssh "ssh" "22")
     (sftp "sftp" "115")
     (smtp "smtp" "25" "587")))
 '(auth-source-save-behavior nil)
 '(auto-hscroll-mode nil)
 '(auto-insert t)
 '(auto-insert-alist
   '((("\\.\\([Hh]\\|hh\\|hpp\\|hxx\\|h\\+\\+\\)\\'" . "C / C++ header")
      (replace-regexp-in-string "[^A-Z0-9]" "_"
				(replace-regexp-in-string "\\+" "P"
							  (upcase
							   (file-name-nondirectory buffer-file-name))))
      "#ifndef " str n "#define " str "

" _ "

#endif")
     (("\\.\\([Cc]\\|cc\\|cpp\\|cxx\\|c\\+\\+\\)\\'" . "C / C++ program")
      nil "#include \""
      (let
	  ((stem
	    (file-name-sans-extension buffer-file-name))
	   ret)
	(dolist
	    (ext
	     '("H" "h" "hh" "hpp" "hxx" "h++")
	     ret)
	  (when
	      (file-exists-p
	       (concat stem "." ext))
	    (setq ret
		  (file-name-nondirectory
		   (concat stem "." ext))))))
      & 34 | -10)
     (("[Mm]akefile\\'" . "Makefile")
      . "makefile.inc")
     (nxml-mode "Short description: " "<!DOCTYPE html>
<html>
  <head>
    <title></title>
      <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
      </head>
  <body>
  </body>
</html>
")
     (html-mode lambda nil
		(sgml-tag "html"))
     (plain-tex-mode . "tex-insert.tex")
     (bibtex-mode . "tex-insert.tex")
     (latex-mode "options, RET: " "\\documentclass[" str & 93 | -1 123
		 (read-string "class: ")
		 "}
"
		 ("package, %s: " "\\usepackage["
		  (read-string "options, RET: ")
		  & 93 | -1 123 str "}
")
		 _ "
\\begin{document}
" _ "
\\end{document}")
     (("/bin/.*[^/]\\'" . "Shell-Script mode magic number")
      lambda nil
      (if
	  (eq major-mode
	      (default-value 'major-mode))
	  (sh-mode)))
     (ada-mode . ada-header)
     (("\\.[1-9]\\'" . "Man page skeleton")
      "Short description: " ".\\\" Copyright (C), "
      (format-time-string "%Y")
      "  "
      (getenv "ORGANIZATION")
      |
      (progn user-full-name)
      "
.\\\" You may distribute this file under the terms of the GNU Free
.\\\" Documentation License.
.TH "
      (file-name-base)
      " "
      (file-name-extension
       (buffer-file-name))
      " "
      (format-time-string "%Y-%m-%d ")
      "
.SH NAME
"
      (file-name-base)
      " \\- " str "
.SH SYNOPSIS
.B "
      (file-name-base)
      "
" _ "
.SH DESCRIPTION
.SH OPTIONS
.SH FILES
.SH \"SEE ALSO\"
.SH BUGS
.SH AUTHOR
"
      (user-full-name)
      '(if
	   (search-backward "&"
			    (line-beginning-position)
			    t)
	   (replace-match
	    (capitalize
	     (user-login-name))
	    t t))
      '(end-of-line 1)
      " <"
      (progn user-mail-address)
      ">
")
     (("\\.el\\'" . "Emacs Lisp header")
      "Short description: " ";;; "
      (file-name-nondirectory
       (buffer-file-name))
      " --- " str
      (make-string
       (max 2
	    (- 80
	       (current-column)
	       27))
       32)
      "-*- lexical-binding: t; -*-"
      '(setq lexical-binding t)
      "

;; Copyright (C) "
      (format-time-string "%Y")
      "  "
      (getenv "ORGANIZATION")
      |
      (progn user-full-name)
      "

;; Author: "
      (user-full-name)
      '(if
	   (search-backward "&"
			    (line-beginning-position)
			    t)
	   (replace-match
	    (capitalize
	     (user-login-name))
	    t t))
      '(end-of-line 1)
      " <"
      (progn user-mail-address)
      ">
;; Keywords: "
      '(require 'finder)
      '(setq v1
	     (mapcar
	      (lambda
		(x)
		(list
		 (symbol-name
		  (car x))))
	      finder-known-keywords)
	     v2
	     (mapconcat
	      (lambda
		(x)
		(format "%12s:  %s"
			(car x)
			(cdr x)))
	      finder-known-keywords "
"))
      ((let
	   ((minibuffer-help-form v2))
	 (completing-read "Keyword, C-h: " v1 nil t))
       str ", ")
      & -2 "

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; " _ "

;;; Code:



(provide '"
      (file-name-base)
      ")
;;; "
      (file-name-nondirectory
       (buffer-file-name))
      " ends here
")
     (("\\.texi\\(nfo\\)?\\'" . "Texinfo file skeleton")
      "Title: " "\\input texinfo   @c -*-texinfo-*-
@c %**start of header
@setfilename "
      (file-name-base)
      ".info
" "@settitle " str "
@c %**end of header
@copying
"
      (setq short-description
	    (read-string "Short description: "))
      ".

" "Copyright @copyright{} "
      (format-time-string "%Y")
      "  "
      (getenv "ORGANIZATION")
      |
      (progn user-full-name)
      "

@quotation
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled ``GNU
Free Documentation License''.

A copy of the license is also available from the Free Software
Foundation Web site at @url{http://www.gnu.org/licenses/fdl.html}.

@end quotation

The document was typeset with
@uref{http://www.texinfo.org/, GNU Texinfo}.

@end copying

@titlepage
@title " str "
@subtitle " short-description "
@author "
      (getenv "ORGANIZATION")
      |
      (progn user-full-name)
      " <"
      (progn user-mail-address)
      ">
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@c Output the table of the contents at the beginning.
@contents

@ifnottex
@node Top
@top " str "

@insertcopying
@end ifnottex

@c Generate the nodes for this menu with `C-c C-u C-m'.
@menu
@end menu

@c Update all node entries with `C-c C-u C-n'.
@c Insert new nodes with `C-c C-c n'.
@node Chapter One
@chapter Chapter One

" _ "

@node Copying This Manual
@appendix Copying This Manual

@menu
* GNU Free Documentation License::  License for copying this manual.
@end menu

@c Get fdl.texi from http://www.gnu.org/licenses/fdl.html
@include fdl.texi

@node Index
@unnumbered Index

@printindex cp

@bye

@c "
      (file-name-nondirectory
       (buffer-file-name))
      " ends here
")))
 '(auto-insert-mode t)
 '(auto-revert-remote-files t)
 '(avy-all-windows nil)
 '(avy-all-windows-alt t)
 '(avy-keys
   '(101 116 104 117 110 111 115 97 100 105 45 46 99 44 114 108 39 112 121 102 103 109 106 98 107 119 113 118 120 122 48 49 50 51 52 53 54 55 56 57))
 '(avy-style 'at-full)
 '(avy-styles-alist
   '((ivy-avy . post)
     (ace-link-addr . pre)
     (ace-link-custom . pre)
     (ace-link-org . pre)
     (ace-link-gnus . post)
     (ace-link-compilation . post)
     (ace-link-eww . post)
     (ace-link-woman . post)
     (ace-link-help . post)
     (ace-link-info . at)))
 '(backup-by-copying t)
 '(backup-directory-alist '(("." . "/home/b/.emacs.d/backup-files")))
 '(bbdb-allow-duplicates t)
 '(bbdb-complete-mail-allow-cycling t)
 '(bbdb-file "~/.bbdb.d/.bbdb")
 '(bbdb-mail-user-agent 'gnus-user-agent)
 '(bbdb-message-all-addresses t)
 '(bbdb-phone-style nil)
 '(bbdb-print-file "/home/b/bbdb.tex")
 '(bbdb-print-omit-fields
   '(tex-name aka mail-alias creation-date timestamp vm-folder anniversary note))
 '(bbdb-print-require '(or mail phone))
 '(bbdb-print-tex-path '("/usr/local/share" "/home/b/s/elisp/external/bbdb/tex"))
 '(bbdb-update-records-p 'search)
 '(beginend-global-mode t nil (beginend))
 '(blink-cursor-delay 0.2)
 '(browse-url-browser-function 'eww-browse-url)
 '(browse-url-firefox-new-window-is-tab t)
 '(cal-tex-diary t)
 '(calendar-date-style 'iso)
 '(calendar-latitude 48.15)
 '(calendar-left-margin 1)
 '(calendar-longitude 8.05)
 '(calendar-week-start-day 1)
 '(camcorder-mode nil)
 '(camcorder-recording-command
   '("recordmydesktop" " --fps 20 --windowid " window-id " -o " file))
 '(canlock-password "80474fc6f90f07738b957b629a825acd3559d219")
 '(caps-lock-mode nil)
 '(case-fold-search t)
 '(company-auto-commit t)
 '(company-idle-delay 0.2)
 '(company-minimum-prefix-length 3)
 '(compilation-scroll-output t)
 '(conkeror-file-path "/home/b/s/conkeror/conkeror.sh")
 '(counsel-mode t)
 '(csv-field-quotes '("`"))
 '(csv-separators '(";" "	"))
 '(cursor-color-code-mode t)
 '(cursor-color-mode nil)
 '(cursor-in-non-selected-windows '(hbar . 6))
 '(cursor-type '(bar . 5))
 '(custom-file "~/.emacs.d/init/.emacs-custom.el")
 '(custom-safe-themes
   '("0717ec4adc3308de8cdc31d1b1aef17dc61003f09cb5f058f77d49da14e809cf" "cd03a600a5f470994ba01dd3d1ff52d5809b59b4a37357fa94ca50a6f7f07473" "ef0fa37f084e12ab30fdf09a5903d48c2bdadb094f413a40e9dbe84553dc3db8"))
 '(default-input-method "german-prefix")
 '(deft-auto-save-interval 23.0)
 '(deft-directory "/home/b/o/")
 '(deft-extensions '("org" "text" "md" "markdown" "txt" "jnl"))
 '(deft-file-limit 42)
 '(deft-filter-only-filenames nil)
 '(deft-recursive t)
 '(deft-recursive-ignore-dir-regexp "\\(?:\\.\\|\\.\\.\\|data\\)$")
 '(deft-use-filename-as-title nil)
 '(delete-old-versions t)
 '(desktop-save-mode nil)
 '(diary-display-function 'diary-fancy-display)
 '(diary-file "~/o/diary")
 '(dired-dwim-target t)
 '(dired-listing-switches "-lAh")
 '(dired-narrow-exit-when-one-left t)
 '(dired-omit-files "^\\.?#\\|^\\.")
 '(display-buffer-alist '(("\\*Buffer List\\*" display-buffer-same-window (nil))))
 '(display-fill-column-indicator nil)
 '(display-line-numbers-major-tick 10)
 '(display-line-numbers-type 'relative)
 '(display-time-mode t)
 '(echo-keystrokes 0.1)
 '(ediff-window-setup-function 'ediff-setup-windows-plain)
 '(elm-format-on-save t)
 '(elp-function-list '(org-tags-view org-search-view))
 '(emacsshot-filename-suffix "png")
 '(emacsshot-snap-frame-filename "~/.emacsshot/emacsshot.frame.png")
 '(emacsshot-snap-mouse-filename "~/.emacsshot/mouseshots/mouseshot.png")
 '(emacsshot-snap-window-filename "~/.emacsshot/emacsshot.win.jpg")
 '(emacsshot-with-timestamp t)
 '(emms-cache-get-function 'emms-cache-get)
 '(emms-cache-modified-function 'emms-cache-dirty)
 '(emms-cache-set-function 'emms-cache-set)
 '(emms-mode-line-format "")
 '(emms-mode-line-icon-image-cache
   '(image :type xpm :ascent center :data "/* XPM */
static char *note[] = {
/* width height num_colors chars_per_pixel */
\"    10   11        2            1\",
/* colors */
\". c #1ba1a1\",
\"# c None s None\",
/* pixels */
\"###...####\",
\"###.#...##\",
\"###.###...\",
\"###.#####.\",
\"###.#####.\",
\"#...#####.\",
\"....#####.\",
\"#..######.\",
\"#######...\",
\"######....\",
\"#######..#\" };") t)
 '(emms-mode-line-mode-line-function nil)
 '(emms-player-next-function 'emms-score-next-noerror)
 '(emms-playlist-default-major-mode 'emms-playlist-mode)
 '(emms-playlist-update-track-function 'emms-playlist-mode-update-track-function)
 '(emms-track-description-function 'emms-info-track-description)
 '(enable-recursive-minibuffers t)
 '(erc-autojoin-channels-alist '(("irc.libera.chat" "#emacs" "#emacs-berlin" "#org-mode")))
 '(erc-autojoin-mode t)
 '(erc-button-mode t)
 '(erc-fill-mode t)
 '(erc-hide-list '("JOIN"))
 '(erc-irccontrols-mode t)
 '(erc-list-mode t)
 '(erc-match-mode t)
 '(erc-menu-mode t)
 '(erc-move-to-prompt-mode t)
 '(erc-netsplit-mode t)
 '(erc-networks-mode t)
 '(erc-nick "gaa")
 '(erc-noncommands-mode t)
 '(erc-pcomplete-mode t)
 '(erc-prompt-for-password nil)
 '(erc-readonly-mode t)
 '(erc-ring-mode t)
 '(erc-server "irc.libera.chat")
 '(erc-stamp-mode t)
 '(erc-track-minor-mode t)
 '(erc-track-mode t)
 '(eval-expression-print-length nil)
 '(eval-expression-print-level nil)
 '(evil-want-C-u-scroll t)
 '(eww-download-directory "~/0")
 '(eww-suggest-uris
   '(mw-current-region-string eww-links-at-point thing-at-point-url-at-point eww-current-url))
 '(fill-prefix nil)
 '(fit-text-scale-graphic-sugar nil)
 '(fit-text-scale-hesitation 0.1)
 '(flyspell-duplicate-distance 400)
 '(frame-background-mode nil)
 '(garbage-collection-messages nil)
 '(global-hl-line-mode t)
 '(global-lisp-butt-mode t)
 '(global-undo-tree-mode t)
 '(global-visual-line-mode t)
 '(gnus-agent-expire-days 700)
 '(gnus-article-prepare-hook
   '(bbdb-mua-auto-update gnus-article-display-x-face gnus-article-display-face gnus-treat-smiley))
 '(gnus-article-save-directory "~/Mail/archive")
 '(gnus-article-x-face-command 'gnus-display-x-face-in-from)
 '(gnus-default-article-saver 'gnus-summary-save-in-rmail)
 '(gnus-face-properties-alist '((pbm) (png)))
 '(gnus-group-jump-to-group-prompt "^nnimap.*inbox")
 '(gnus-ignored-mime-types nil)
 '(gnus-inhibit-images nil)
 '(gnus-logo-colors '("#1ec1c4" "#bababa") t)
 '(gnus-message-archive-method "archive")
 '(gnus-message-replysign t)
 '(gnus-mime-display-multipart-alternative-as-mixed nil)
 '(gnus-mode-line-image-cache
   '(image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #1ba1a1\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };") t)
 '(gnus-thread-ignore-subject nil)
 '(gnus-user-agent '(gnus))
 '(gnus-verbose 10)
 '(gnus-visible-headers
   '("^From:" "^Newsgroups:" "^Subject:" "^Date:" "^Followup-To:" "^Reply-To:" "^Summary:" "^Keywords:" "^To:" "^[BGF]?Cc:" "^Posted-To:" "^Mail-Copies-To:" "^Mail-Followup-To:" "^Apparently-To:" "^Gnus-Warning:" "^Resent-From:" "^X-Sent:" "^User-Agent:" "^X-Mailer:" "^X-Newsreader:" "^Archived-At" "^X-Face" "^Face"))
 '(google-this-browse-url-function 'eww-browse-url)
 '(hack-time-mode nil)
 '(helm-mode nil)
 '(holiday-bahai-holidays nil)
 '(icomplete-mode nil)
 '(ido-enable-regexp t)
 '(ido-everywhere t)
 '(ido-hacks-mode t)
 '(image-animate-loop t)
 '(image-dired-external-viewer "feh")
 '(image-file-name-extensions
   '("png" "jpeg" "jpg" "gif" "tiff" "tif" "xbm" "xpm" "pbm" "pgm" "ppm" "pnm" "svg" "someimage" "aimage"))
 '(image-file-name-regexps nil)
 '(image-use-external-converter t)
 '(indent-tabs-mode t)
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(initial-scratch-message "")
 '(inverse-video t)
 '(ispell-program-name "/usr/bin/hunspell")
 '(ivy-mode nil)
 '(keyfreq-mode t)
 '(keystrokes-mode nil)
 '(large-file-warning-threshold 100000000)
 '(ledger-highlight-xact-under-point t)
 '(ledger-reconcile-default-commodity "€")
 '(ledger-report-auto-width nil)
 '(ledger-report-use-native-highlighting nil)
 '(ledger-reports
   '(("bal" "%(binary) -f %(ledger-file) bal")
     ("bal-d1" "%(binary) -n -f %(ledger-file) bal")
     ("bal-d2" "%(binary) -f %(ledger-file) --depth 2 bal")
     ("monthly bal" "%(binary) -p \"monthly from jan to jul\" -f %(ledger-file) bal")
     ("bal-2017" "%(binary) -p 2017 -f %(ledger-file) bal")
     ("bal01" "%(binary) -p jan -f %(ledger-file) bal")
     ("bal02" "%(binary) -p feb -f %(ledger-file) bal")
     ("bal03" "%(binary) -p mar -f %(ledger-file) bal")
     ("bal04" "%(binary) -p apr -f %(ledger-file) bal")
     ("bal05" "%(binary) -p may -f %(ledger-file) bal")
     ("bal06" "%(binary) -p jun -f %(ledger-file) bal")
     ("bal07" "%(binary) -p jul -f %(ledger-file) bal")
     ("bal08" "%(binary) -p aug -f %(ledger-file) bal")
     ("bal09" "%(binary) -p sep -f %(ledger-file) bal")
     ("bal10" "%(binary) -p oct -f %(ledger-file) bal")
     ("bal11" "%(binary) -p nov -f %(ledger-file) bal")
     ("bal12" "%(binary) -p dec -f %(ledger-file) bal")
     ("Verm\303\266gen Feb" "%(binary) -p feb -f %(ledger-file) bal Verm\303\266gen")
     ("barkasse" "%(binary) -f %(ledger-file) bal Verm.gen:Barkasse")
     ("Giro" "%(binary) -f %(ledger-file) bal Verm\303\266gen:GiroPB")
     ("Giro up to date" "%(binary) -f %(ledger-file) --end 2014-11-18 bal Verm\303\266gen:GiroPB")
     ("reg" "%(binary) -f %(ledger-file) reg")
     ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
     ("account" "%(binary) -f %(ledger-file) reg %(account)")))
 '(ledger-texi-normalization-args "")
 '(linum-format 'dynamic)
 '(linum-relative-to-point t)
 '(lisp-butt-global-mode t)
 '(lisp-butt-mode-lighter " (.)")
 '(lisp-butt-modes
   '(lisp-mode emacs-lisp-mode clojure-mode inferior-emacs-lisp-mode racket-mode))
 '(lisp-butt-replacement ".")
 '(magit-branch-arguments nil)
 '(magit-log-section-arguments '("--decorate" "-n256"))
 '(magit-use-overlays nil)
 '(mail-self-blind t)
 '(mail-user-agent 'gnus-user-agent)
 '(major-mode-stack-modes
   '(artist-mode emacs-lisp-mode fundamental-mode org-mode text-mode c-mode))
 '(mastodon-instance-url "https://mastodon.sdf.org")
 '(max-lisp-eval-depth 10042)
 '(max-specpdl-size 13000)
 '(menu-bar-mode nil)
 '(message-dont-reply-to-names
   "\"\\\\(marco\\.?wahl\\\\(soft\\\\)?\\\\|adam\\\\)@\\\\(gmail.com\\\\|googlemail.com\\\\)\"")
 '(minibuffer-auto-raise nil)
 '(minute-timer-sound "~/d/sound/birds/Pleci-enggano.ogg")
 '(mm-default-directory "~/0")
 '(mm-inline-large-images 'resize)
 '(mml-default-directory "~/0")
 '(mml-secure-key-preferences
   '((OpenPGP
      (sign)
      (encrypt
       ("marcowahlsoft@gmail.com" "D4A94C1003E8C637F6D4BAF47FADAC175B9533A1")))
     (CMS
      (sign)
      (encrypt))))
 '(mml-secure-safe-bcc-list '("marcowahlsoft@gmail.com"))
 '(mml-secure-smime-sign-with-sender t)
 '(mmm-submode-decoration-level 0)
 '(mouse-avoidance-animation-delay 0.2)
 '(mouse-avoidance-mode nil nil (avoid))
 '(mouse-avoidance-nudge-dist 42)
 '(mouse-avoidance-nudge-var 23)
 '(mouse-avoidance-threshold 5)
 '(moz-controller-global-mode t)
 '(mw-text-eta-wpm 120)
 '(nyan-animate-nyancat nil)
 '(nyan-bar-length 10)
 '(nyan-mode t)
 '(nyan-wavy-trail nil)
 '(org-agenda-block-separator "")
 '(org-agenda-bulk-custom-functions
   '((32
      (lambda nil
	(org-agenda-todo "")))
     (117 org-agenda-bulk-unmark)))
 '(org-agenda-category-icon-alist
   '(("Zazen" "/home/b/media/images/little-moine-transp.png" nil nil :ascent center)
     ("Football" "/home/b/lab/org-world-cup2014/football.svg" nil nil :ascent center)))
 '(org-agenda-clockreport-parameter-plist '(:link t :maxlevel 2))
 '(org-agenda-cmp-user-defined 'org-cmp-diary)
 '(org-agenda-current-time-string "now

")
 '(org-agenda-custom-commands
   '(("A" "@, N, ~, W tasks. (N)ext, (~) in flow, (W)aiting. And agenda."
      ((todo "@" nil)
       (todo "~" nil)
       (todo "N" nil)
       (todo "W" nil)
       (agenda "" nil))
      nil nil)
     ("n" "N, W orgees."
      ((todo "N" nil)
       (todo "W" nil))
      nil)
     ("w" "Where am I?  What next?  Waiting for something?"
      ((todo "@|~" nil)
       (todo "N" nil)
       (todo "W" nil))
      nil)
     ("x" "Custom agenda possibly stored by `mw-org-agenda-store-current-filters-as-custom-agenda'" agenda ""
      ((org-agenda-overriding-header "Custom agenda possibly stored by { M-x mw-org-agenda-store-current-filters-as-custom-agenda RET }")
       (org-agenda-tag-filter-preset 'nil)
       (org-agenda-regexp-filter-preset
	'("+dis"))
       (org-agenda-category-filter-preset
	'("-Anniv"))))
     ("d" "Deadlines only" agenda ""
      ((org-agenda-entry-types
	'(:deadline))))
     ("c" "current mine (no timestamp entries)" agenda ""
      ((org-agenda-entry-types
	'(:deadline :scheduled :timestamp :sexp)))
      ("agenda-of-mine.txt"))
     ("@" "The thing to do now" todo "@" nil)
     ("~" "Tasks in progress" todo "@|~" nil)))
 '(org-agenda-deadline-leaders '("[☠|]" "[☠| in %dd]" "[☠|:o( %dx]"))
 '(org-agenda-diary-entries-first t)
 '(org-agenda-dim-blocked-tasks nil)
 '(org-agenda-files
   '("~/o/steuererkl2020.org" "/home/b/s/elisp/mw/elisp-examples/remove-from-list-of-lists.org" "/home/b/o/heap.org" "/home/b/b/o/busi-todo.org" "/home/b/t/tmp.org" "/home/b/o/timeline.org" "/home/b/o/things.org" "/home/b/o/notes.org" "/home/b/o/captures.org" "/home/b/s/elisp/mw/.emacs.d/init.org" "/home/b/o/comments.org" "/home/b/o/someday.org" "/home/b/o/tickler.org" "/home/b/o/recur.org" "/home/b/o/dates.org"))
 '(org-agenda-finalize-hook
   '(org-pretty-tags-refresh-agenda-lines
     (closure
      (bootstrap-version t)
      nil
      (hl-line-mode 1))
     mw-org-agenda-clock-goto))
 '(org-agenda-follow-indirect nil)
 '(org-agenda-format-date
   '(closure
     (t)
     (date)
     (concat "
"
	     (org-agenda-format-date-aligned date))))
 '(org-agenda-inactive-leader "|")
 '(org-agenda-include-diary t)
 '(org-agenda-loop-over-headlines-in-active-region 'start-level)
 '(org-agenda-prefix-format
   '((agenda . "%i%c %?-12t% s")
     (todo . "%i%c ")
     (tags . "%i%c ")
     (search . "%i%c ")))
 '(org-agenda-scheduled-leaders '(":o)" ":o(%2dx"))
 '(org-agenda-skip-additional-timestamps-same-entry nil)
 '(org-agenda-sorting-strategy
   '((agenda time-up priority-down)
     (todo priority-down category-keep)
     (tags priority-down category-keep)
     (search category-keep)))
 '(org-agenda-span 'day)
 '(org-agenda-start-on-weekday nil)
 '(org-agenda-tags-column 0)
 '(org-agenda-text-search-extra-files '(agenda-archives "~/o/anki.org" "~/b/o/busi-notes.org"))
 '(org-agenda-time-grid
   '((daily today remove-match)
     (800 1000 1200 1400 1600 1800 2000)
     "......" "----------------"))
 '(org-agenda-use-tag-inheritance nil)
 '(org-agenda-use-time-grid nil)
 '(org-agenda-window-setup 'only-window)
 '(org-allow-promoting-top-level-subtree t)
 '(org-archive-reversed-order t)
 '(org-ascii-indented-line-width 0)
 '(org-babel-J-command "ijconsole")
 '(org-babel-ledger-command "/home/b/s/ledger/ledger")
 '(org-babel-load-languages
   '((emacs-lisp . t)
     (awk . t)
     (python . t)
     (C . t)
     (calc . t)
     (latex . t)
     (org . t)
     (js . t)
     (css . t)
     (lisp . t)
     (ditaa . t)
     (dot . t)
     (gnuplot . t)
     (scheme . t)
     (clojure . t)
     (shell . t)
     (screen . t)
     (sql . t)
     (sqlite . t)
     (plantuml . t)
     (java . t)
     (maxima . t)
     (haskell . t)
     (R . t)
     (forth . t)
     (makefile . t)
     (octave . t)
     (sed . t)
     (lua . t)))
 '(org-babel-python-command "python" nil nil "changed from python2 to python [2016-10-06 Thu 15:50]")
 '(org-blank-before-new-entry '((heading . auto) (plain-list-item)))
 '(org-brain-show-history nil)
 '(org-bullets-bullet-list '(" "))
 '(org-capture-templates
   '(("u" "q(u)ick just a headline" entry
      (file "~/o/captures.org")
      "* %U %^{what}" :prepend t :immediate-finish t :empty-lines 1 :empty-lines-after 1)
     ("g" "(g)et something captured" entry
      (file "~/o/captures.org")
      "* %^{what} %^G
:PROPERTIES:
:CREA_DATE:  %U
:END:

- %a
%i
%?" :prepend t :empty-lines 1 :empty-lines-after 1 :clock-in t :clock-resume t)
     ("o" "(o)n the fly task (for being done while captured)" entry
      (file "~/o/things.org")
      "* \\ %^{what} %^G
:PROPERTIES:
:CREA_DATE:  %U
:END:

- %a
%i%?
" :prepend t :empty-lines 1 :clock-in t)
     ("s" "(s)pontaneous action" entry
      (file "~/o/captures.org")
      "* \\ %^{what} %^G
:PROPERTIES:
:CREA_DATE:  %U
:END:

- %a
%i%?
" :prepend t :jump-to-captured t :empty-lines 1 :clock-in t :clock-keep t :clock-resume t)
     ("i" "(i)ncident" entry
      (file "~/o/captures.org")
      "* %u %^{Heading} :%^G

%a%?%i
" :prepend t :empty-lines 1 :clock-in t :clock-resume t :time-prompt t)
     ("n" "incident (n)ow" entry
      (file "~/o/captures.org")
      "* %U %^{PROMPT} :%^G

%a
%i

%?" :prepend t :empty-lines 1 :clock-in t :clock-resume t)
     ("h" "capture to refile with screens(h)ot" entry
      (file "~/o/captures.org")
      "* %^{what} %^G
:PROPERTIES:
:CREA_DATE:  %U
:END:

(progn ; EVALUATE FOR ATTACHING A SCREENSHOT.  PRECONDITION: package org-attach-screenshot
  (org-attach-dir t)
  (org-attach-screenshot nil (format-time-string
                                    \"screenshot-%Y%m%d-%H%M%S.png\"))
  (backward-sexp)
  (backward-kill-sexp))%? %a
%i
" :prepend t :empty-lines 1 :empty-lines-after 1 :clock-in t :clock-resume t)
     ("p" "(p)ersonal activity log for a point in time. the system pings after some time for another entry when the user calls the code inserted at the end of the capture." entry
      (file "~/o/captures.org")
      "* %U %^{PROMPT} :log:autorequested:

- %a

(progn
(run-at-time 600 nil (lambda () (org-capture nil \"p\")))
(backward-kill-sexp))%?" :prepend t :empty-lines 1 :clock-in t :clock-resume t)
     ("z" "kusen (z)en" entry
      (file+headline "~/m/zen/kusen/kusen.org" "Kusen")
      "* (progn (overwrite-mode -1) (kill-sexp -1))(progn (overwrite-mode) (kill-sexp -1) (search-forward \"pc-YYYYMMDD-HHMM-fr-de\") (goto-char (+ 3   (match-beginning 0))))%?
:PROPERTIES:
:EXPORT_FILE_NAME: pc-YYYYMMDD-HHMM-fr-de
:EXPORT_SUBTITLE:
:GODO:
:LOCATION:
:EXPORT_DATE:
:NOTETAKING:
:TRANSLATION:
:LECTOR:
:CREA_DATE:  %U
:END:

- %a

%i
" :prepend t :jump-to-captured t :clock-in t :clock-resume t)
     ("a" "anki entry" entry
      (file "~/o/anki.org")
      "* %^{word} %i
SCHEDULED: %t

** %^{explanation}%?
" :prepend t)
     ("k" "file subtree with timestamp to the clock" entry
      (clock)
      "* %U %?")
     ("c" "file to the (c)lock" item
      (clock)
      "%U %?" :jump-to-captured t)
     ("f" "function to capture" plain
      (file
       (lambda nil "/home/b/tmp/tmp.org"))
      #'(lambda nil "* tadaaaaa
bbbrrb.  grrnnz.  eeeeeeep."))
     ("X" "capture from org-protocol" entry
      (file "~/o/captures.org")
      "* %:description%?
:PROPERTIES:
:CREA_DATE:  %U
:END:

- %:annotation %:link

%i           " :prepend t :empty-lines 1)))
 '(org-catch-invisible-edits 'error)
 '(org-clock-history-length 42)
 '(org-clock-out-when-done nil)
 '(org-confirm-babel-evaluate nil)
 '(org-crypt-key "0x49010A040A3AE6F2")
 '(org-crypt-tag-matcher "CRYPT|crypt")
 '(org-cycle-global-at-bob t)
 '(org-cycle-include-plain-lists t)
 '(org-default-notes-file "~/o/notes.org")
 '(org-ellipsis "↘")
 '(org-export-backends
   '(ascii beamer html icalendar latex md odt org texinfo koma-letter))
 '(org-export-dispatch-use-expert-ui nil)
 '(org-fast-tag-selection-single-key nil)
 '(org-feed-alist
   '(("elpa" "http://tromey.com/elpa/elpa.rss" "~/o/feeds.org" "elpa")))
 '(org-file-apps
   '(("\\.gif\\'" lambda
      (file link)
      (let
	  ((my-image
	    (create-image file))
	   (tmpbuf
	    (get-buffer-create "*gif")))
	(switch-to-buffer tmpbuf)
	(erase-buffer)
	(insert-image my-image)
	(image-animate my-image nil t)))
     (auto-mode . emacs)
     ("\\.mm\\'" . default)
     ("\\.x?html?\\'" . default)
     (t . emacs)))
 '(org-fontify-done-headline nil)
 '(org-fontify-emphasized-text t)
 '(org-fontify-quote-and-verse-blocks t)
 '(org-fontify-whole-heading-line t)
 '(org-footnote-define-inline t)
 '(org-format-latex-options
   '(:foreground default :background default :scale 2.0 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
		 ("begin" "$1" "$" "$$" "\\(" "\\[")))
 '(org-goto-interface 'outline)
 '(org-habit-completed-glyph 120)
 '(org-habit-following-days 7)
 '(org-habit-graph-column 55)
 '(org-habit-preceding-days 7)
 '(org-habit-show-all-today t)
 '(org-habit-show-done-always-green nil)
 '(org-habit-show-habits t)
 '(org-habit-show-habits-only-for-today nil)
 '(org-habit-today-glyph 46)
 '(org-hide-emphasis-markers nil)
 '(org-hide-leading-stars t)
 '(org-hierarchical-todo-statistics nil)
 '(org-id-link-to-org-use-id t)
 '(org-indirect-buffer-display 'current-window)
 '(org-journal-date-format "%Y-%m-%d %a")
 '(org-journal-enable-agenda-integration t)
 '(org-journal-file-format "%Y%m%d.jnl")
 '(org-journal-file-type 'weekly)
 '(org-koma-letter-email nil)
 '(org-level-color-stars-only nil)
 '(org-link-elisp-confirm-function nil)
 '(org-link-frame-setup
   '((vm . vm-visit-folder-other-frame)
     (vm-imap . vm-visit-imap-folder-other-frame)
     (gnus . org-gnus-no-new-news)
     (file . find-file)
     (wl . wl-other-frame)))
 '(org-log-into-drawer t)
 '(org-log-note-headings
   '((done . "CLOSING NOTE %t")
     (state . "State %-12s from %-12S %t")
     (note . "%t")
     (reschedule . "Rescheduled from %S on %t")
     (delschedule . "Not scheduled, was %S on %t")
     (redeadline . "New deadline from %S on %t")
     (deldeadline . "Removed deadline, was %S on %t")
     (refile . "Refiled on %t")
     (clock-out . "")))
 '(org-loop-over-headlines-in-active-region 'start-level)
 '(org-modules
   '(ol-bbdb ol-bibtex org-crypt ol-eww ol-gnus org-habit org-id ol-info org-inlinetask org-protocol org-tempo org-attach-embedded-images))
 '(org-odd-levels-only nil)
 '(org-odt-create-custom-styles-for-srcblocks nil)
 '(org-outline-path-complete-in-steps nil)
 '(org-plantuml-jar-path "/opt/plantuml/plantuml.jar")
 '(org-pretty-entities nil)
 '(org-pretty-tags-agenda-unpretty-habits t)
 '(org-pretty-tags-global-mode t)
 '(org-pretty-tags-mode-lighter " pt")
 '(org-pretty-tags-surrogate-images
   '(("emacs" . "/home/b/d/images/icons/emacs-icon_24x24.png")
     ("org" . "/home/b/d/images/icons/org-unicorn.png")
     ("nb" . "/home/b/sha1-hashed/81955c2a129cc09dc2612a530d38cd35906b2ed1")
     ("1tom" . "/home/b/d/images/things/tomato-mw.png")))
 '(org-pretty-tags-surrogate-strings
   '(("conn" . "o-o")
     ("bugcandidate" . "bug?")
     ("aktion" . "⎇")
     ("work" . "⚒")
     ("health" . "⚕")
     ("gesundheit" . "⚕")
     ("tel" . "☏")
     ("collection" . "⁂")
     ("obs" . "⚟")
     ("joice" . "\\o/")
     ("imp" . "*")
     ("idea" . "!")
     ("money" . "$")
     ("easy" . "₰")
     ("musik" . "♫")
     ("music" . "♫")
     ("in" . "o<-")
     ("out" . "o->")
     ("gruen4" . "⌂")
     ("urg" . "⌛")
     ("question" . "?")
     ("capture" . "+")
     ("organize" . "~")
     ("clarify" . "_")
     ("reflect" . "<")
     ("engage" . "^")
     ("notes" . "✍")
     ("zen" . "▴")
     ("event" . "✼")))
 '(org-priority-default 23)
 '(org-priority-highest 1)
 '(org-priority-lowest 42)
 '(org-refile-targets
   '(("~/o/notes.org" :level . 0)
     (org-agenda-files :level . 0)
     (nil :maxlevel . 4)))
 '(org-refile-use-cache t)
 '(org-refile-use-outline-path 'file)
 '(org-remove-highlights-with-change nil)
 '(org-return-follows-link t)
 '(org-reverse-note-order t)
 '(org-show-context-detail
   '((isearch . lineage)
     (bookmark-jump . lineage)
     (occur-tree . minimal)
     (default . ancestors)))
 '(org-show-hierarchy-above nil)
 '(org-show-notification-handler 'message)
 '(org-show-siblings nil)
 '(org-special-ctrl-a/e 'reversed)
 '(org-special-ctrl-k t)
 '(org-speed-commands
   '(("Outline Navigation")
     ("n" org-speed-move-safe 'org-next-visible-heading)
     ("p" org-speed-move-safe 'org-previous-visible-heading)
     ("f" org-speed-move-safe 'org-forward-heading-same-level)
     ("b" org-speed-move-safe 'org-backward-heading-same-level)
     ("F" . org-next-block)
     ("B" . org-previous-block)
     ("u" org-speed-move-safe 'outline-up-heading)
     ("j" . org-goto)
     ("g" org-refile
      '(4))
     ("Outline Visibility")
     ("c" . org-cycle)
     ("C" . org-shifttab)
     (" " . org-display-outline-path)
     ("k" . org-cut-subtree)
     ("=" . org-columns)
     ("Outline Structure Editing")
     ("U" . org-metaup)
     ("D" . org-metadown)
     ("r" . org-metaright)
     ("l" . org-metaleft)
     ("R" . org-shiftmetaright)
     ("L" . org-shiftmetaleft)
     ("i" progn
      (forward-char 1)
      (call-interactively 'org-insert-heading-respect-content))
     ("^" . org-sort)
     ("w" . org-refile)
     ("a" . org-archive-subtree-default-with-confirmation)
     ("@" . org-mark-subtree)
     ("#" . org-toggle-comment)
     ("Clock Commands")
     ("I" . org-clock-in)
     ("O" . org-clock-out)
     ("Meta Data Editing")
     ("t" . org-todo)
     ("," . avy-goto-line-below)
     ("0" org-priority 32)
     ("1" org-priority 65)
     ("2" org-priority 66)
     ("3" org-priority 67)
     (":" . org-set-tags-command)
     ("e" . org-set-effort)
     ("E" . org-inc-effort)
     ("W" lambda
      (m)
      (interactive "sMinutes before warning: ")
      (org-entry-put
       (point)
       "APPT_WARNTIME" m))
     ("Agenda Views etc")
     ("v" . org-agenda)
     ("/" . org-sparse-tree)
     ("Misc")
     ("o" . org-open-at-point)
     ("?" . org-speed-command-help)
     ("<" org-agenda-set-restriction-lock 'subtree)
     (">" org-agenda-remove-restriction-lock)
     ("personal")
     ("&" . mw-org-end-of-meta-data)
     ("*"
      (lambda
	(&optional arg)
	(interactive "P")
	(avy-goto-char-2 42 32 arg)))
     ("+"
      (lambda nil
	(interactive)
	"Show metainfo lines like org properties."
	(mw-org-show-meta-info-lines)))
     ("-" . mw-org-hide-meta-info-lines)
     ("_" lambda
      (&optional arg)
      (interactive "P")
      (org-cycle-hide-drawers
       (if arg 'all 'subtree)))
     ("."
      (lambda nil
	(interactive)
	(let
	    ((avy-all-windows nil))
	  (call-interactively #'avy-goto-char))))
     ("4" . mw-org-properties-show-in-clone)
     ("5" . org-schedule)
     ("6" . mw-org-count-subtrees)
     ("7" . mw-org-log-show-in-clone)
     ("8" . mw-org-show-properties-in-clone)
     ("9" . org-decrypt-entry)
     (";" . org-timer-set-timer)
     ("A" . org-toggle-archive-tag)
     ("G" . ace-link)
     ("H" . org-rise)
     ("J" . org-clock-goto)
     ("K" . org-cut-subtree)
     ("M" . org-sink)
     ("N" org-speed-move-safe 'outline-next-visible-heading)
     ("P" org-speed-move-safe 'outline-previous-visible-heading)
     ("S" . foldout-exit-fold)
     ("T" lambda
      (&optional arg)
      (interactive "P")
      (let
	  ((org-use-fast-todo-selection 'auto))
	(org-todo arg)))
     ("W" . mw-org-subtree-as-top-level-tree-toggle)
     ("Y" . org-agenda)
     ("\\" . org-narrow-to-subtree)
     ("c" lambda nil
      (org-clone-subtree-with-time-shift 1 ""))
     ("d" . org-attach)
     ("h" . org-refile-other-window)
     ("k" . org-capture)
     ("s" . foldout-zoom-org-subtree)
     ("x" lambda
      (arg)
      "Org export of orgee."
      (interactive "P")
      (let
	  ((org-export-initial-scope 'subtree))
	(org-export-dispatch)))
     ("y" . org-property-action)
     ("z" . org-add-note)
     ("Z" . mw-org-log-show-in-clone)
     ("'" . mw-org-outline-path-kill)))
 '(org-src-lang-modes
   '(("ocaml" . tuareg)
     ("elisp" . emacs-lisp)
     ("ditaa" . artist)
     ("asymptote" . asy)
     ("dot" . fundamental)
     ("sqlite" . sql)
     ("calc" . fundamental)
     ("C" . c)
     ("cpp" . c++)
     ("C++" . c++)
     ("screen" . shell-script)
     ("shell" . sh)
     ("bash" . sh)))
 '(org-src-preserve-indentation t)
 '(org-src-tab-acts-natively t)
 '(org-src-window-setup 'current-window)
 '(org-startup-folded t)
 '(org-startup-indented nil)
 '(org-structure-template-alist
   '(("a" . "export ascii")
     ("c" . "center")
     ("C" . "comment")
     ("e" . "example")
     ("E" . "export")
     ("h" . "export html")
     ("l" . "export latex")
     ("q" . "quote")
     ("s" . "src")
     ("v" . "verse")))
 '(org-stuck-projects '("+proj" ("\\." "~" "n" "x" "g") nil ""))
 '(org-tag-alist nil)
 '(org-tag-faces '(("termin" :foreground "red")))
 '(org-tags-column 0)
 '(org-time-stamp-custom-formats '("<%Y-%m-%d %a>" . "<%Y-%m-%d %a %H:%M>"))
 '(org-use-fast-todo-selection 'auto)
 '(org-use-speed-commands t)
 '(org-use-tag-inheritance '("nobill"))
 '(org-velocity-bucket "/home/b/o/notes.org")
 '(org-velocity-exit-on-match t)
 '(org-velocity-force-new t)
 '(org-velocity-heading-level 3)
 '(org-velocity-search-method 'regexp)
 '(package-archive-upload-base "/home/b/s/elisp/mw/packs")
 '(package-hidden-regexps '("\\`2048-game"))
 '(package-selected-packages
   '(ponylang-mode org-section-numbers rust-mode command-log-mode dired-collapse hack-time-mode ein underline-with-char helm-unicode helpful elm-mode vdiff indium js2-refactor xref-js2 typit rope-read-mode company-emoji ac-emoji noccur org-board ert-runner hledger-mode ledger-mode showkey yalinum nlinum linum-relative js2-mode ripgrep org-attach-screenshot elisp-refs 4clojure soap-client j-mode cider lentic web lispy wgrep erlang io-mode io-mode-inf suggest yaml-mode pass helm-bibtex ebib vlf pdf-tools pt ag trr transmission lyrics interleave google-this gscholar-bibtex git-messenger deft goto-last-change move-text counsel-projectile ivy-hydra haskell-mode julia-mode julia-shell caps-lock flyspell-lazy dired-quick-sort exwm camcorder ace-jump-mode company async evil-numbers cask-mode nyan-mode multishell lfe-mode epkg all html5-schema ack evil lib-requires avy-zap chronos csv-mode epoch-view dired-narrow dired-filter stumpwm-mode keyfreq flycheck-elm counsel focus pinentry reverse-theme restclient mmm-mode rainbow-delimiters speed-type impatient-mode ztree tldr awk-it apu git-auto-commit git-auto-commit-mode git-timemachine sibilant-mode sicp el-mock el-search on-screen smartparens zone-nyan elmacro beacon sotlisp yasnippet wrap-region form-feed page-break-lines request emr buttercup package-build shut-up epl git commander f dash s volume w3 ace-flyspell magit company-web browse-kill-ring php-mode multiple-cursors zeitgeist wcheck-mode visual-regexp use-package typing-practice typing twittering-mode sx ssh-tunnels shadow seclusion-mode screenshot rudel rectangle-utils rase queue quack psgml pretty-mode pomodoro password-store paredit ov nginx-mode navi-mode moz-controller mode-line-in-header message-x mainline macrostep key-chord jabber iedit ido-hacks hydra hide-region haskell-emacs google-translate gnuplot gnu-apl-mode ggtags flylisp flycheck-package fliptext fancy-narrow fabric expand-region eww-lnum eimp ecukes downplay-mode discover dictionary debbugs ddg darkroom conkeror-minor-mode concurrent company-anaconda clojure-mode calfw boxquote babel aurel auctex ascii-art-to-unicode arduino-mode ace-window ace-link ace-jump-buffer ace-isearch))
 '(pages-addresses-file-name "~/m/C-xp_d__pages.txt")
 '(pages-directory-buffer-narrowing-p nil)
 '(paradox-execute-asynchronously 'ask)
 '(paradox-github-token t)
 '(parens-require-spaces nil)
 '(pcre-mode nil)
 '(persp-sort 'created)
 '(pomodoro-break-start-sound "~/media/sound/birds/Pleci-enggano.ogg")
 '(pomodoro-desktop-notification nil)
 '(pomodoro-show-number nil)
 '(pomodoro-sound-player "mpv")
 '(pomodoro-work-start-sound "~/media/sound/birds/Canary-animals065.wav")
 '(pomodoro-work-time 25)
 '(ps-zebra-stripe-height 4)
 '(ps-zebra-stripes t)
 '(python-skeleton-autoinsert t)
 '(quack-programs
   '("mzscheme" "bigloo" "csi" "csi -hygienic"
     #("gosh" 0 4
       (face ido-first-match))
     "gracket" "gsi" "gsi ~~/syntax-case.scm -"
     #("guile" 0 5
       (face ido-only-match))
     "kawa" "mit-scheme" "racket" "racket -il typed/racket" "rs" "scheme" "scheme48" "scsh" "sisc" "stklos" "sxi"))
 '(recenter-positions '(top middle bottom))
 '(recenter-redisplay t)
 '(revert-without-query '("spendenbescheinigung-2014.pdf"))
 '(rope-read-calculate-exact-y-coordinates t)
 '(rope-read-flip-line-horizontally t)
 '(rope-read-flip-line-vertically t)
 '(rope-read-just-reverse-characters nil)
 '(safe-local-variable-values
   '((org-list-description-max-indent . 5)
     (lexical-cast . t)
     (eval when
	   (and
	    (buffer-file-name)
	    (file-regular-p
	     (buffer-file-name))
	    (string-match-p "^[^.]"
			    (buffer-file-name)))
	   (unless
	       (featurep 'package-build)
	     (let
		 ((load-path
		   (cons "../package-build" load-path)))
	       (require 'package-build)))
	   (package-build-minor-mode)
	   (set
	    (make-local-variable 'package-build-working-dir)
	    (expand-file-name "../working/"))
	   (set
	    (make-local-variable 'package-build-archive-dir)
	    (expand-file-name "../packages/"))
	   (set
	    (make-local-variable 'package-build-recipes-dir)
	    default-directory))
     (make-backup-files)
     (Syntax . ANSI-Common-Lisp)
     (Lowercase . Yes)
     (Syntax . Common-lisp)
     (Lowercase . T)
     (Base . 10)
     (Syntax . COMMON-LISP)
     (Package . XLIB)
     (Syntax . Common-Lisp)
     (eval progn
	   (require 'projectile)
	   (puthash
	    (projectile-project-root)
	    "cask exec buttercup -L ." projectile-test-cmd-map))
     (eval this-function-shall-not-exist-funfunfun)
     (eval git-auto-commit-mode t)
     (lentic-init . lentic-orgel-org-init)
     (org-refile-targets
      ("~/b/o/busi.org" :level . 1)
      ("~/o/notes.org" :regexp . "Transform to resonable texts"))
     (org-refile-targets
      (nil :level . 1))
     (org-refile-targets
      (nil :regexp . "Kontaktlisten konkret")
      (org-agenda-files :regexp . "Aufga"))))
 '(save-place-mode t)
 '(scheme-program-name "guile")
 '(screenshot-schemes
   '(("local" :dir "~/_in")
     ("current-directory" :dir default-directory)
     ("remote-ssh" :dir "/tmp/" :ssh-dir "www.example.org:public_html/archive/" :url "http://www.example.org/archive/")
     ("EmacsWiki" :dir "~/.yaoddmuse/EmacsWiki/" :yaoddmuse "EmacsWiki")
     ("local-server" :dir "~/public_html/" :url "http://127.0.0.1/")))
 '(scroll-bar-mode nil)
 '(seclusion-fringe-factor 8)
 '(select-enable-primary t)
 '(selected-not-enable-in-these-modes '(dired-mode))
 '(selectric-mode nil)
 '(send-mail-function 'smtpmail-send-it)
 '(sentence-end-double-space t)
 '(set-mark-command-repeat-pop t)
 '(show-paren-mode t)
 '(shr-use-fonts t)
 '(side-notes-file "side-notes.org")
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 25)
 '(sp-base-key-bindings 'sp)
 '(sql-interactive-mode-hook '((lambda nil (setq sql-prompt-regexp ">>>"))))
 '(standard-indent 1)
 '(straight-vc-git-default-clone-depth 1)
 '(tab-always-indent 'complete)
 '(tags-table-list nil)
 '(timeclock-file "~/b/archiv/timelogs/timelog")
 '(timeclock-mode-line-display t)
 '(tiro-cursor 'box)
 '(tool-bar-mode nil)
 '(tooltip-mode t)
 '(twittering-use-master-password nil)
 '(underline-with-char-fill-char 32)
 '(user-full-name "Marco Wahl")
 '(user-mail-address "marcowahlsoft@gmail.com")
 '(version-control t)
 '(visible-bell t)
 '(wdired-allow-to-change-permissions t)
 '(winner-mode t)
 '(wrap-region-except-modes '(calc-mode dired-mode ibuffer-mode))
 '(yas-prompt-functions
   '(yas-ido-prompt yas-dropdown-prompt yas-completing-prompt yas-x-prompt yas-no-prompt))
 '(yas-wrap-around-region t))

;; local variables:
;; end:

;;; .emacs-custom.el ends here
