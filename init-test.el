;; get a typical version as list
;; :PROPERTIES:
;; :ID:       ae6130a9-9846-41de-aa86-a6f7af1d51fb
;; :END:

;; I forgot what I wanted this for.


;; [[file:init.org::*get a typical version as list][get a typical version as list:1]]
(ert-deftest mw-test-parse-version-string ()
  (should (equal '(1 2 3) (mw-parse-version-string "1.2.3"))))
;; get a typical version as list:1 ends here
